/*************************************************** 
  This is an example for the Adafruit CC3000 Wifi Breakout & Shield

  Designed specifically to work with the Adafruit WiFi products:
  ----> https://www.adafruit.com/products/1469

  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/
 
 /*
This example does a test of the TCP client capability:
  * Initialization
  * Optional: SSID scan
  * AP connection
  * DHCP printout
  * DNS lookup
  * Optional: Ping
  * Connect to website and print out webpage contents
  * Disconnect
SmartConfig is still beta and kind of works but is not fully vetted!
It might not work on all networks!
*/
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
#include "utility/debug.h"
#include "DHT.h"

// These are the interrupt and control pins
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
// These can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an UNO, SCK = 13, MISO = 12, and MOSI = 11
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIVIDER); // you can change this clock speed

#define WLAN_SSID       "putssid"           // cannot be longer than 32 characters!
#define WLAN_PASS       "putpassword"
// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2
#define WLAN_SECURITY   WLAN_SEC_WPA2

#define IDLE_TIMEOUT_MS  3000      // Amount of time to wait (in milliseconds) with no data 
                                   // received before closing the connection.  If you know the server
                                   // you're accessing is quick to respond, you can reduce this value.
#define DHTPIN 6
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE); 
/**************************************************************************/
/*!
    @brief  Sets up the HW and the CC3000 module (called automatically
            on startup)
*/
/**************************************************************************/
#define HOST_IP "192.168.0.2"
uint32_t ip = cc3000.IP2U32(192,168,0,2);
int port =80;
/*gps veriables*/
static const int RXPin = 2, TXPin = 3;
static const uint32_t GPSBaud = 4800;
TinyGPSPlus gps;
SoftwareSerial ss(RXPin, TXPin);

void setup(void)
{
  Serial.begin(9600); 
  //ss.begin(GPSBaud);
  dht.begin();
  Serial.print("Free RAM: "); Serial.println(getFreeRam(), DEC);
  
  /* Initialise the module */
  Serial.println(F("\nInitializing..."));
  if (!cc3000.begin())
  {
    Serial.println(F("Couldn't begin()! Check your wiring?"));
    while(1);
  }
  
  // Optional SSID scan
  // listSSIDResults();
  
  Serial.print(F("\nAttempting to connect to ")); Serial.println(WLAN_SSID);
  if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
    Serial.println(F("Failed!"));
    while(1);
  }
   
  Serial.println(F("Connected!"));
  
  /* Wait for DHCP to complete */
  Serial.println(F("Request DHCP"));
  while (!cc3000.checkDHCP())
  {
    delay(100); // ToDo: Insert a DHCP timeout!
  }  

  /* Display the IP address DNS, Gateway, etc. */  
  while (! displayConnectionDetails()) {
    delay(1000);
  }
  
  // Optional: Do a ping test on the website
  /*
  Serial.print(F("\n\rPinging ")); cc3000.printIPdotsRev(ip); Serial.print("...");  
  replies = cc3000.ping(ip, 5);
  Serial.print(replies); Serial.println(F(" replies"));
  */  

  /* Try connecting to the website.
     Note: HTTP/1.1 protocol is used to keep the server from closing the connection before all data is read.
  */
  // Print data
    
}

void loop(void)
{
//  while (ss.available() > 0){
//    if (gps.encode(ss.read()))
//      getGPSInfo();  
//  }   
    String motion = String((int)digitalRead(7));
    String ldr = String((int)analogRead(A0));
    String sensorID=String(1);
    float humidity = dht.readHumidity();
    float temperature=dht.readTemperature();
    if (isnan(temperature)||isnan(humidity)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
    }
    String temp=String(temperature) ;
    String hum=String(humidity) ;
    
    Serial.print(F("LDR: "));
    Serial.println(ldr);
    Serial.print(F("Temperature: "));
    Serial.print(temperature);
    Serial.println(F(" C"));
    Serial.print(F("Humidity: "));
    Serial.println(hum);
    Serial.print(F("Motion: "));
    Serial.println(motion);
    Serial.println();
   
    // Send request
    String request = "GET /projects/datasensors/upload/upload_measurement.php?measurement=" 
                      + ldr + "&sensorID=" + String(2);
    send_request(request); 
    delay(1000);
    request = "GET /projects/datasensors/upload/upload_measurement.php?measurement=" 
                      + temp + "&sensorID=" + String(1);
    delay(1000);
    send_request(request); 
    
    request = "GET /projects/datasensors/upload/upload_measurement.php?measurement=" 
                      + hum + "&sensorID=" + String(10);
    send_request(request);
    delay(1000);
    request = "GET /projects/datasensors/upload/upload_measurement.php?measurement=" 
                      + motion + "&sensorID=" + String(5);
    send_request(request);
    delay(1000);
}
 
void send_request (String request) {                                                         
     
    // Connect
    Serial.println(F("tarting connection to server..."));                                 
    Adafruit_CC3000_Client client = cc3000.connectTCP(ip, port);
   
    // Send request
    if (client.connected()) {
      char toArray[request.length() + 1];
      request.toCharArray(toArray,request.length() + 1);
      client.fastrprint(toArray);
      client.fastrprint(F(" HTTP/1.1\r\n"));
      client.fastrprint(F("Host: "));client.fastrprint(HOST_IP); client.fastrprint(F("\r\n"));
      client.fastrprint(F("\r\n"));
      client.println();
      Serial.println("Connected & Data sent");
    }
    else {
      Serial.println(F("Connection failed"));
    }
    unsigned long lastRead = millis();
    while (client.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS)) {
    while (client.available()) {
      char c = client.read();
      Serial.print(c);
      lastRead = millis();
    }
  }
    Serial.println(F("Closing connection"));
    Serial.println();
    client.close();
   
}
/**************************************************************************/
/*!
    @brief  Tries to read the IP address and other connection details
*/
/**************************************************************************/
bool displayConnectionDetails(void)
{
  uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;
  
  if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
  {
    Serial.println(F("Unable to retrieve the IP Address!\r\n"));
    return false;
  }
  else
  {
    Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
    Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
    Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
    Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
    Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
    Serial.println();
    return true;
  }
}
void getGPSInfo()
{
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
    
    send_request("GET /projects/SensoMan%20SaaS/upload/upload_hum.php?lat=" + String(gps.location.lat()) + "&lng=" + String(gps.location.lng()) + "&controller=1 HTTP/1.1\r\n");
  } else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.println();
}
