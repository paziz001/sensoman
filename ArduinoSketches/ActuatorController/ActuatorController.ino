/* 
  This a simple example of the aREST Library for Arduino (Uno/Mega/Due/Teensy)
  using the Ethernet library (for example to be used with the Ethernet shield). 
  See the README file for more details.
 
  Written in 2014 by Marco Schwartz under a GPL license. 
*/

// Libraries
#include <SPI.h>
#include <Ethernet.h>
#include <RCSwitch.h>
#include <aREST.h>
#include <avr/wdt.h>

// Enter a MAC address for your controller below.
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0F, 0x46, 0x3B };

// IP address in case DHCP fails
IPAddress ip(192,168,0,3);

// Ethernet server
EthernetServer server(8080);

// Create aREST instance
aREST rest = aREST();

// Variables to be exposed to the API
int temperature;
int humidity;

void setup(void)
{  
  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(7,INPUT);
  // Start Serial
  Serial.begin(9600);

  // Function to be exposed
  rest.function("rf_transmitter",sendRfCode);
  
  // Give name and ID to device
  rest.set_id("1");
  rest.set_name("arduino");

 
  
  // Start the Ethernet connection and the server
  //if (Ethernet.begin(mac) == 0) {
    //Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip);
  //}
  server.begin();
  Serial.print(F("server is at "));
  Serial.println(Ethernet.localIP());

  // Start watchdog
  wdt_enable(WDTO_4S);
}

void loop() {  
   EthernetClient client = server.available();
   rest.handle(client);
   wdt_reset();
}

// Custom function accessible by the API
int sendRfCode(String command) {
  RCSwitch mySwitch = RCSwitch();
   // Transmitter is connected to Arduino Pin #9  
  mySwitch.enableTransmit(9);
  mySwitch.setPulseLength(185);
  char code[command.length()+1];
  command.toCharArray(code,command.length()+1);
  // Optional set pulse length.

  mySwitch.send(code);
  digitalWrite(9, HIGH);
  delay(500);
  return 1;
}
