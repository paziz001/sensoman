<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

$method=getRequestMethod();
$date=new DateTime('now');

if ($method != 'POST') {

    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}  else {
    if (isset($_POST["token"])&&isset($_POST["ruleID"])) {
        $token = $_POST["token"];
        //Check the validation of the token
        $db=DBConnect();
        $stmt=$db->prepare("SELECT * FROM token WHERE token= ?");
        $stmt->bind_param('s',$token);
        $stmt->execute();
        $resultToken = $stmt->get_result();
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "message" => "You have to provide a valid token."
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)) {
                //Find the type of User
                //Find the controller Info
                $ruleID = $_POST["ruleID"];
                $db=DBConnect();
                $stmt=$db->prepare("DELETE FROM `rule` WHERE `rule`.`ruleID` = ?");
                $stmt->bind_param('i',$ruleID);
                $stmt->execute();
                $deleted = $stmt->affected_rows;
                if($deleted<1){
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "The rule was not found"
                    );
                }else{

                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "Successfully deleted the rule"
                    );
                }
            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }elseif(!isset($_POST["ruleID"])){
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "message" => "You have to provide a rule id."
        );
    }
    else {
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);