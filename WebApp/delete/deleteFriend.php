<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

//check request method
if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}

else{

	  if(isset($_POST["token"])){
    	$token = $_POST["token"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
        	//token is valid, check if the token is in the time boundaries
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
    }

    if($token_flag && isset($_POST["userID"])){
        $groupIDs = array();
        $userIDFriend = $_POST["userID"];
        //delete friend from all groups you own
        $sql = "SELECT groupID FROM `sn_group` WHERE ownerID ='".$userID."'";
        DBConnect();
        $result = execQuery($sql);
        DBClose();
        $num = rowCount($result);
        if($num > 0){

            while($aRow=fetchNext($result)){
                    $group = $aRow["groupID"]; 
                    array_push($groupIDs,  $group);   
                     }

            foreach($groupIDs as $groupID) {
            $sql =" DELETE FROM `sn_user-group` WHERE groupID = '".$groupID."' AND userID = '".$userIDFriend."' ";
            DBConnect();
            $result = execQuery($sql);
            DBClose();
        }
    }

    	
    	$sql = "DELETE FROM sn_association WHERE (userID1='".$userID."' AND userID2='".$userIDFriend."') OR (userID2='".$userID."' AND userID1='".$userIDFriend."')";
        $sql2 = "DELETE FROM `sn_requested_sensors` WHERE (applicantID='".$userID."' AND receiverID='".$userIDFriend."') OR (receiverID='".$userID."' AND applicantID ='".$userIDFriend."')";
        $sql3 = "DELETE FROM `sn_shared_boards` WHERE (ownerID='".$userID."' AND watcherID='".$userIDFriend."') OR (watcherID='".$userID."' AND ownerID='".$userIDFriend."')";
        $sql4 = "DELETE FROM `sn_shared_sensors` WHERE (ownerID='".$userID."' AND watcherID='".$userIDFriend."') OR (watcherID='".$userID."' AND ownerID='".$userIDFriend."')";
        
    	DBConnect();
		$result = execQuery($sql);
        $result1 = execQuery($sql2);
        $result2 = execQuery($sql3);
        $result3 = execQuery($sql4);
		DBClose();
		//deleted row from sn_association successfully
		if($result == true){

			 $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Deletion of friend was successfull."
            );
		}
		else{
			$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Deletion of friend was unsuccessfull."
            );
		}
    } else{
    	$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Invalid credentials."
            );
    }

}

echo json_encode($response);
?>