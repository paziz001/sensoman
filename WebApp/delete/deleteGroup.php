<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = false;


if ($method != 'POST') {
    $response=array(
        "status" => "token expired",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
	 if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }

  if($token_flag == true && isset($_POST["groupName"])){
    $groupName = $_POST["groupName"];
    $groupID = "";
    $sql = "SELECT groupID FROM `sn_group` WHERE name = '".$groupName."'";
    DBConnect();
    $result=execQuery($sql);
    DBClose();
    $numRows=rowCount($result);
    if ($numRows<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Invalid group name"
            );
        } else {
           while($aRow=fetchNext($result)){
                $groupID=$aRow["groupID"]; //
               // $checkemail=$aRow["email"];
            }
        

            $sql = "DELETE FROM `sn_user-group` WHERE (groupID='".$groupID."')";
            DBConnect();
            $result = execQuery($sql);
            DBClose();
            //deleted row successfully
            if($result == false){
                $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Deletion of group's users is unsuccessful"
            );
            }
            else{
                $sql = "DELETE FROM `sn_group` WHERE groupID='".$groupID."'";
                DBConnect();
                $result = execQuery($sql);
                DBClose();
                
                if($result == false){
                $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Deletion of group is unsuccessful"
                  );
                }
                else{
                    $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Deletion of group is successful"
                  );
                }

            }

        } //group ID

    

  }


}// ELSE POST


echo json_encode($response);


?>