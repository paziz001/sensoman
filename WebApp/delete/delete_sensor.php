<?php
/*
* Evangelos Aristodemou
*/

include_once('../includes/common.php');
require_once('../includes/connectdb.php');;
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
  if (isset($_POST["token"]) && isset($_POST["id"])){
    $token = $_POST["token"];
    //Check the validation of the token
    $sql="SELECT * FROM token WHERE token='".$token."'";
    DBConnect();
    $resultToken=execQuery($sql);
    DBClose();
    $numToken=rowCount($resultToken);
    if ($numToken<1){
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide a valid token. Non valid Connection"
        );
    }
    else{
        while($aRow=fetchNext($resultToken)){
            $valid=$aRow["validUntil"];
            $userID=$aRow["userID"];
        }
        if (checkToken($valid)){
          $id = $_POST["id"];
          DBConnect();
          $sql = "SELECT * FROM `sensor-controller` sc
                  JOIN `user-controller` uc ON sc.controllerID = uc.controllerID
                  WHERE uc.userID =" . $userID . " AND sc.sensorID =" . $id;
          $result=execQuery($sql);
          DBClose();
          $rows=rowCount($result);
          if ($rows>0){
            $db = DBConnect();
            $sql = "DELETE FROM `sensor` WHERE sensorID = ? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $sql = "DELETE FROM `sensor-controller` WHERE sensorID = ? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $sql = "DELETE FROM `sensor-measurement` WHERE sensorID = ? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $response=array(
              "status" => "success",
              "request time" => $date->format("Y-m-d h:m:s"),
              "IP Address" => $ip,
              "message" => "Sensor deleted successfully."
            );
          }
        else{
          $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You are not authorized to delete this sensor."
          );
        }
    }
    else{
      $response=array(
          "status" => "fail",
          "request time" => $date->format("Y-m-d h:m:s"),
          "IP Address" => $ip,
          "message" => "token expired"
      );
    }
  }
}
  else{
    $response=array(
      "status" => "fail",
      "request time" => $date->format("Y-m-d h:m:s"),
      "IP Address" => $ip,
      "message" => "You have to provide your current token and the sensor id."
    );
  }
}

echo json_encode($response);

?>
