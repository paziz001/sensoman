<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = 0;

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}

else{
  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
    }

    


    if($token_flag == true && isset($_POST["userID"])){
    	$friendID = $_POST["userID"];
    	$sql = "DELETE FROM `sn_request` WHERE (senderID='".$friendID."' AND receiverID='".$userID."')";
    	DBConnect();
		$result = execQuery($sql);
		DBClose();
				if($result == true){

			 $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Deletion of friend request was successfull."
            );
		}
		else{
			$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Deletion of friend request was unsuccessfull."
            );
		}
    }


    }


echo json_encode($response);
    	exit();
 ?>