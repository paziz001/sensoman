<?php
/*
* Evangelos Aristodemou
*/

include_once('../includes/common.php');
require_once('../includes/connectdb.php');;
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
  if (isset($_POST["token"])){
    $token = $_POST["token"];
    $sql="DELETE FROM `token` WHERE `token`= '" . $token . "'";
    DBConnect();
    if (execQuery($sql)){
      $sql="DELETE FROM `token` WHERE `validUntil`< '" . $date->format('Y-m-d h:m:s') . "'";
      execQuery($sql);
      DBClose();
      $response=array(
        "status" => "success",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Token deleted successfully."
      );
    }
    else{
      $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Unable to delete the token."
      );
    }
}
  else{
    $response=array(
      "status" => "fail",
      "request time" => $date->format("Y-m-d h:m:s"),
      "IP Address" => $ip,
      "message" => "You have to provide your current token."
    );
  }
}

echo json_encode($response);

?>
