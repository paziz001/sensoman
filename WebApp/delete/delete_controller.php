<?php
/*
* Evangelos Aristodemou
*/

include_once('../includes/common.php');
require_once('../includes/connectdb.php');;
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
  if (isset($_POST["token"]) && isset($_POST["id"])){
    $token = $_POST["token"];
    //Check the validation of the token
    $sql="SELECT * FROM token WHERE token='".$token."'";
    DBConnect();
    $resultToken=execQuery($sql);
    DBClose();
    $numToken=rowCount($resultToken);
    if ($numToken<1){
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide a valid token. Non valid Connection"
        );
    }
    else{
        while($aRow=fetchNext($resultToken)){
            $valid=$aRow["validUntil"];
            $userID=$aRow["userID"];
        }
        if (checkToken($valid)){
          $id = $_POST["id"];
          DBConnect();
          $sql = "SELECT * FROM `user-controller` WHERE controllerID =" . $id . " AND userID=" . $userID;
          $result=execQuery($sql);
          DBClose();
          $rows=rowCount($result);
          if ($rows>0){
            $rows = 0;
            $db = DBConnect();
            $sql = "SELECT * FROM `sensor-controller` WHERE controllerID =" . $id;
            $result=execQuery($sql);
            $rows+=rowCount($result);
            $sql = "SELECT * FROM `actuator-controller` WHERE controllerID =" . $id;
            $result=execQuery($sql);
            $rows+=rowCount($result);
            if ($rows==0){
              $sql = "DELETE FROM `micro-controller` WHERE controllerID = ? ";
              $stmt = $db->prepare($sql);
              $stmt->bind_param('i', $id);
              $stmt->execute();
              $sql = "DELETE FROM `user-controller` WHERE controllerID = ? ";
              $stmt = $db->prepare($sql);
              $stmt->bind_param('i', $id);
              $stmt->execute();
              $sql = "DELETE FROM `location-controller` WHERE controllerID = ? ";
              $stmt = $db->prepare($sql);
              $stmt->bind_param('i', $id);
              $stmt->execute();
              $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Micro_controller deleted successfully."
              );
              DBClose();
          }
          else{
            $response=array(
              "status" => "fail",
              "request time" => $date->format("Y-m-d h:m:s"),
              "IP Address" => $ip,
              "message" => "There are some sensors or actuators attached on this controller. Delete these sensors/actuators first."
            );
          }
        }
        else{
          $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You are not authorized to delete this controller."
          );
        }
    }
    else{
      $response=array(
          "status" => "fail",
          "request time" => $date->format("Y-m-d h:m:s"),
          "IP Address" => $ip,
          "message" => "token expired"
      );
  }
  }
}
  else{
    $response=array(
      "status" => "fail",
      "request time" => $date->format("Y-m-d h:m:s"),
      "IP Address" => $ip,
      "message" => "You have to provide your current token and the controller id."
    );
  }
}

echo json_encode($response);

?>
