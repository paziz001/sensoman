var URL='http://localhost/projects/datasensors/';
$(document).ready(function(){
    
    //nojs
    $("body").removeClass("no-js");
    
    //------------------------------------------------------------------------//
    
    //fakelink
    $('a[href="#"]').on('click',function(e){e.preventDefault();});
    
    //------------------------------------------------------------------------//
    

    
    //------------------------------------------------------------------------//
    
    //tab
    $('.tabs').delegate('li:not(.active)','click',function(){$(this).addClass('active').siblings().removeClass('active').parents('.tab').find('.box').hide().eq($(this).index()).fadeIn(250);});

    
});//document ready

$("#login").click(function(){
    window.location.href=URL+'pages/login.html';
});

$("#register").click(function(){
    window.location.href=URL+'pages/register.html';
})

$("#download").click(function(){
    window.location.href="https://docs.google.com/forms/d/1RUOUCkeHWo79gekO_s6Cwn-iMuVjAjCp19d1lRb72Pk/viewform?c=0&w=1";
});

