<?php

/*
* Evangelos Aristodemou
*/

include_once('../includes/common.php');
require_once('../includes/connectdb.php');;
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();


if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else {
    if (isset($_POST["token"]) && isset($_POST["fileName"]) && isset($_POST["controller"])
        && isset($_POST["sensors"]) && isset($_POST["pins"]) && isset($_POST["timeInterval"]) && isset($_POST["internet"])) {
        $token = $_POST["token"];
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                $file=$_POST["fileName"];
                $controller=$_POST["controller"];
                $sensors=$_POST["sensors"];
                $pins=$_POST["pins"];
                $timeInterval=$_POST["timeInterval"];
                $internet=$_POST["internet"];
                $sql = "SELECT fileName, f.fileID, f.pins, f.timeInterval FROM `user-file` uf
                        JOIN `file` f ON uf.`fileID`=f.`fileID`
                        WHERE `userID`=" . $userID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                $exists = false;
                $existingid = 0;
                while ($aRow=fetchNext($result)){
                    if ($aRow["fileName"] == $file){
                      $exists = true;
                      $existingid = $aRow["fileID"];
                      $existingpins = $aRow["pins"];
                      $existingtime = $aRow["timeInterval"];
                      break;
                    }
                }
                if ($exists==false){
                  $sql = "INSERT INTO `file` (fileName, controller, sensors, pins, timeInterval, internet )
                          VALUES ('" . $file . "' , '" . $controller . "' , '" . $sensors . "' , '" . $pins . "' ,
                          '" . $timeInterval . "' ,'" . $internet . "')";
                  $temp = $sql;
                  DBConnect();
                  $result=execQuery($sql);
                  DBClose();
                  if ($result){
                      $sql="SELECT * FROM `file` WHERE fileName='".$file."' AND controller= '" . $controller . "'";
                      DBConnect();
                      $res=execQuery($sql);
                      DBClose();
                      while ($aRow=fetchNext($res)){
                          $fileID=$aRow["fileID"];
                      }
                      $sql="INSERT INTO `user-file`(userID, fileID) VALUES ('".$userID."','".$fileID."')";
                      DBConnect();
                      $result=execQuery($sql);
                      DBClose();
                      $response = array(
                          "status" => "success",
                          "request time" => $date->format("Y-m-d h:m:s"),
                          "IP Address" => $ip,
                          "message" => "File added successfully.",
                          "fileID" => $fileID,
                          "timeInterval" => $timeInterval,
                          "pins" => $pins
                      );
                  }
                  else{
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "An error occurred. Unable to save the file.",
                    );
                  }
                }
                else{
                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "This file has already been saved.",
                        "fileID" => $existingid,
                        "pins" => $existingpins,
                        "timeInterval" => $existingtime
                    );
                }
              }
              else{
                  $response=array(
                      "status" => "fail",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "token expired"
                  );
              }
          }
        }
        else{
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide your current token and the file info."
            );
        }
}
echo json_encode($response);

?>
