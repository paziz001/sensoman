<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 08/4/2015
 * Time: 12:02 μμ
 */

/*
* Updated by Evangelos Aristodemou
* Works without providing a location for the controller.
*/

include_once('../includes/common.php');
require_once('../includes/connectdb.php');;
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();


if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else {
    if (isset($_POST["token"]) && isset($_POST["controller"])) {
        $token = $_POST["token"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                $controller=$_POST["controller"];
                $latitude = 0;
                $longitude =0;
                if (isset($_POST["latitude"]) && isset($_POST["longitude"])){
                  $latitude=$_POST["latitude"];
                  $longitude=$_POST["longitude"];
                }
                $sql = "SELECT controllerName FROM `user-controller` uc
                        JOIN `micro-controller` c ON uc.`controllerID`=c.`controllerID`
                        WHERE `userID`=" . $userID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                $exists = false;
                while ($aRow=fetchNext($result)){
                    if ($aRow["controllerName"] == $controller){
                      $exists = true;
                      break;
                    }
                }
                if ($exists==false){
                $type = isset($_POST["type"]) ? $_POST["type"] : null;
                $sql="";
                if ($type!=null){
                  $sql = "INSERT INTO `micro-controller` (controllerName, type) VALUES ('" . $controller . "' , '" . $type . "')";
                }
                else{
                  $sql = "INSERT INTO `micro-controller` (controllerName) VALUES ('" . $controller . "')";
                }
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                if ($result){
                    $sql="SELECT * FROM `micro-controller` WHERE controllerName='".$controller."'";
                    DBConnect();
                    $res=execQuery($sql);
                    DBClose();
                    while ($aRow=fetchNext($res)){
                        $controllerID=$aRow["controllerID"];
                    }
                    $sql="INSERT INTO `user-controller`(userID, controllerID) VALUES ('".$userID."','".$controllerID."')";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    if ($latitude!=0 && $longitude!=0){
                      DBConnect();
                      $latArray=explode('.', $latitude);
                      $latDec=substr($latArray[1],0,6);
                      $latitude=$latArray[0].".".$latDec;
                      $lonArray=explode('.', $longitude);
                      $lonDec=substr($lonArray[1],0,6);
                      $longitude=$lonArray[0].".".$lonDec;
                      $result=execProcedure("FIND_LOCATION('$latitude','$longitude')");
                      DBClose();
                      $num=rowCount($result);
                      if ($num==1){
                          while ($aRow=fetchNext($result)){
                              $locationID=$aRow["locationID"];
                            }
                      }
                      else{
                          $sql="INSERT INTO location (longitude, latitude) VALUES ('".$longitude."','".$latitude."')";
                          DBConnect();
                          $result=execQuery($sql);
                          DBClose();
                          DBConnect();
                          $resLoc=execProcedure("FIND_LOCATION('$latitude','$longitude')");
                          DBClose();
                          while ($aRow=fetchNext($resLoc)){
                            $locationID=$aRow["locationID"];
                          }
                      }

                      $sql="INSERT INTO `location-controller`(locationID, controllerID, dateStart, active, deactivated, admin) VALUES ('".$locationID."','".$controllerID."','".$date->format("Y-m-d h:m:s")."', 1,0,1)";
                      DBConnect();
                      $result=execQuery($sql);
                      DBClose();
                    }
                      $message="Add Microcontroller ID:".$controllerID;
                      $sql="SELECT * FROM user WHERE userID=".$userID;
                      DBConnect();
                      $result=execQuery($sql);
                      DBClose();
                      while($aRow=fetchNext($result)){
                          $type=$aRow["type"];
                      }
                      $sql="SELECT * FROM user WHERE type='admin'";
                      DBConnect();
                      $ad=execQuery($sql);
                      DBClose();
                      while ($aRow=fetchNext($ad)){
                          $admin=$aRow["userID"];
                        }
                      if (strpos($type, 'admin')!==false) {
                        if ($admin!==$userID) {
                            $sql = "INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('" . $userID . "','" . $userID . "','" . $message . "',0)";
                            DBConnect();
                            $result = execQuery($sql);
                            DBClose();
                        }
                    }

                        $sql="INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('".$userID."','".$admin."','".$message."',0)";
                        DBConnect();
                        $result=execQuery($sql);
                        DBClose();
                        $response = array(
                            "status" => "success",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP Address" => $ip,
                            "message" => "Controller added successfully.",
                            "controllerID" => $controllerID
                        );
                }
                else{
                  $response = array(
                      "status" => "fail",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "An error occurred. Unable to add the controller."
                  );
                }
              }
                else{
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "This controller name already exists. Give a different name."
                    );
                }
            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
            }
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token and the controller info."
        );
    }
}
echo json_encode($response);

?>
