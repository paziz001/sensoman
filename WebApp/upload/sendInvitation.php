<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = false;
$associated= false;
$input_flag = false;
$useEmail = false;

if ($method != 'POST') {
    $response=array(
        "status" => "token expired",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid) == true){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }

if($token_flag == true){
    if(isset($_POST["emailFlag"]))
      $useEmail = $_POST["emailFlag"];
      else {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Not given specific parameter"
    );
}


//check the input of user when it's an email address
if($useEmail == 'true' && isset($_POST["usernameOrEmail"])){
   $email = test_input($_POST["usernameOrEmail"]);
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
   $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Invalid email address syntax."
    );
} else{
  $sql="SELECT * FROM user WHERE email='".$email."'";
     DBConnect();
     $result=execQuery($sql);
     DBClose();
    $numRows=rowCount($result);
     if ($numRows<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Invalid email address."
            );
        } else {
           while($aRow=fetchNext($result)){
                $receiverID=$aRow["userID"]; //
               // $checkemail=$aRow["email"];
            }

            if($userID != $receiverID)
            $input_flag = true;
            else{
              $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Cannot send request to yourself!"
            );

            }
        }

}
}

//check the input of user when it's a username
if($useEmail == 'false' && isset($_POST["usernameOrEmail"])){
  $name = test_input($_POST["usernameOrEmail"]);
    if (!preg_match("/^[a-zA-Z0-9 ]*$/",$name)) {
      $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Only letters, white space and numbers allowed."
            );
      
    } else{
    //Check the validation of usename
    $sql="SELECT * FROM user WHERE username='".$name."'";
     DBConnect();
     $resultName=execQuery($sql);
     DBClose();
    $numRows=rowCount($resultName);
    if ($numRows<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Invalid username."
            );
        } else{
          while($aRow=fetchNext($resultName)){
                $receiverID=$aRow["userID"]; //
            }
            //check if the user is trying to send request to himself
            if($userID != $receiverID)
            $input_flag = true;
            else{
              $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Invalid username."
            );

            }
          }
    }  
}

if($input_flag == true){
//check if there is already connection between sender and receiver
$sql="SELECT * FROM sn_association WHERE (userID1='".$receiverID."' AND userID2='".$userID."') OR (userID1='".$userID."' AND userID2='".$receiverID."')";
DBConnect();
$checkUserRow = execQuery($sql);
DBClose();
$numRows=rowCount($checkUserRow);
if($numRows>=1){
  $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You are already associated with this user."
    );
  $associated = false;
} else $associated = true;
}

if($associated == true){

//if the user has send request to receiver before or vice - versa, then he can't send again
$sql="SELECT * FROM sn_request WHERE (receiverID='".$receiverID."' AND senderID='".$userID."') OR (receiverID='".$userID."' AND senderID='".$receiverID."')";
DBConnect();
$checkUserRow = execQuery($sql);
DBClose();
$numRows=rowCount($checkUserRow);
if($numRows>=1){
  $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have either send or received request from this user already."
    );
} else{
  if((isset($_POST["groupName"]) && !empty($_POST["groupName"]))){
    $groupName = test_input($_POST["groupName"]);
  }
//all attributes are valid, insert the request in the database
  if(empty($groupName))
    $groupName = 'Not available';
 /* echo json_encode($groupName);
  exit();*/
 $sql = "INSERT INTO sn_request(senderID,receiverID,groupName) VALUES ('".$userID."', '".$receiverID."', '".$groupName."')";
DBConnect();
$result = execQuery($sql);
DBClose();
if($result == true){
 $response=array(
        "status" => "success",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have successfully send invitation."
    );
} else {
  $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Failure in creating request."
    );

}

}
}
} 

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

echo json_encode($response);

?>