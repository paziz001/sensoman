<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = 0;

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}

else{
  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
    }
 	 

 	 if($token_flag == true && isset($_POST["userID"]) && isset($_POST["username"]) && isset($_POST["groupName"])){
 	 	$userID2 = $_POST["userID"];
        $groupName = $_POST["groupName"];
 	 	//$username = $_POST["username"];


		$sql = "INSERT INTO sn_association(userID1,userID2) VALUES ('".$userID."', '".$userID2."')";
		
       // echo json_encode($asdasdas);
      // exit();
 	 	DBConnect();
		$result = execQuery($sql);
		DBClose();

		if($result == true){
            $sql = "SELECT groupID FROM sn_group WHERE name = '".$groupName."'";
            DBConnect();
            $result = execQuery($sql);
            DBClose();
            $num=rowCount($result);
            if ($num<1){ //no group with this name
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Group does not exist."
            );

            $sql = "DELETE FROM sn_request WHERE receiverID ='".$userID."' AND senderID ='".$userID2."'";
            DBConnect();
            $result2 = execQuery($sql);
            DBClose();
            if($result2 == true){
            $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have successfully accept invitation."
                 );
            }
            else {
                $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Failure in deleting request."
        );

            }


        } else{ //there is group with this name

                 while($aRow=fetchNext($result)){
                         $groupID=$aRow["groupID"]; //get the id
                        }

        
            $sql = "INSERT INTO `sn_user-group`(userID,groupID) VALUES ('".$userID."', '".$groupID."')";
             DBConnect();
            $result = execQuery($sql);
            DBClose();
            if($result == false){
                $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Failure in creating association for group."
                );
            }
            else{

			$sql = "DELETE FROM sn_request WHERE receiverID ='".$userID."' AND senderID ='".$userID2."'";
			DBConnect();
			$result2 = execQuery($sql);
			DBClose();
			if($result2 == true){
			$response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have successfully accept invitation."
   				 );
			}
			else {
				$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Failure in deleting request."
                );

			}
        }

            //retrieve group users and associate the new user with them
         $userInGroup = array();
         $sql = "SELECT * FROM `sn_user-group` WHERE groupID = '".$groupID."' AND userID != '".$userID2."' AND userID != '".$userID."' ";
         DBConnect();
         $result = execQuery($sql);
         DBClose();
         $num=rowCount($result);
         
            if ($num>=1){
                while($aRow=fetchNext($result)){
                        $userG=$aRow["userID"];
                        array_push($userInGroup,$userG);
                    }

             
            foreach($userInGroup as $user) {

                $sql="SELECT * FROM sn_association WHERE (userID1='".$userID."' AND userID2='".$user."') OR (userID1='".$user."' AND userID2='".$userID."')";
                    DBConnect();
                    $result = execQuery($sql);
                    DBClose();
                    $num=rowCount($result);
                    if ($num<1){
                        $sql = "INSERT INTO sn_association(userID1,userID2) VALUES ('".$userID."', '".$user."')";
                        DBConnect();
                        $result = execQuery($sql);
                        DBClose();

                        if($result == true){
                            $response=array(
                                "status" => "success",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP Address" => $ip,
                                "message" => "You have successfully accept invitation."
                                 );
                        } else {
                                $response=array(
                                    "status" => "fail",
                                    "request time" => $date->format("Y-m-d h:m:s"),
                                    "IP Address" => $ip,
                                    "message" => "Failure in creating association with other users in the group."
                                    );

                        }
                    }
                
            }
        /*INSERT HERE*/
        }
        else {
            $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "There are no other users in this group."
        );
        }
        


        } //group

           

            } 
          

 	 }
 	 else {
 	 	$response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Invalid credentials."
        );

 	 }
	}
	  	echo json_encode($response);
 	 	
?>
