<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 08/4/2015
 * Time: 12:02 μμ
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);
include_once('../includes/common.php');
require_once('../includes/connectdb.php');;


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else {
    if ((isset($_POST["email"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["usertype"]) && isset($_POST["controllertype"])  && isset($_POST["controller"]) && isset($_POST["latitude"]) && isset($_POST["longitude"])) || (isset($_POST["email"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["usertype"]))) {
        $email=$_POST["email"];
        $username=$_POST["username"];
        $pass = hash('sha256',$_POST["password"]);
        $type=$_POST["usertype"];
        $sql="INSERT INTO user (email,username, password, type) VALUES ('".$email."','".$username."','".$pass."','".$type."')";
        DBConnect();
        $result=execQuery($sql);
        DBClose();
        if ($result){
            if ((count($_POST))>4){
                $controllerType = $_POST["controllertype"];
                $controller=$_POST["controller"];
                $latitude=$_POST["latitude"];
                $longitude=$_POST["longitude"];
                $sql="SELECT * FROM user WHERE username='".$username."'";
                DBConnect();
                $res=execQuery($sql);
                DBClose();
                while ($aRow=fetchNext($res)){
                    $userID=$aRow["userID"];
                }
                if($controllerType=="actuator"){
                    $sql="INSERT INTO `micro-controller`(controllerName,actuator,sensing) VALUES('".$controller."','1','0')";
                }elseif($controllerType=="sensing"){
                    $sql="INSERT INTO `micro-controller`(controllerName,actuator,sensing) VALUES('".$controller."','0','1')";
                }
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                if ($result){
                    $sql="SELECT * FROM `micro-controller` WHERE controllerName='".$controller."'";
                    DBConnect();
                    $res=execQuery($sql);
                    DBClose();
                    while ($aRow=fetchNext($res)){
                        $controllerID=$aRow["controllerID"];
                    }
                    $sql="INSERT INTO `user-controller`(userID, controllerID) VALUES ('".$userID."','".$controllerID."')";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    $latArray=explode('.', $latitude);
                    $latDec=substr($latArray[1],0,6);
                    $latitude=$latArray[0].".".$latDec;
                    $lonArray=explode('.', $longitude);
                    $lonDec=substr($lonArray[1],0,6);
                    $longitude=$lonArray[0].".".$lonDec;
                    $sql="SELECT * FROM location WHERE latitude='" . $latitude . "' AND longitude='" . $longitude . "'";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    $num=rowCount($result);
                    if ($num==1){
                        while ($aRow=fetchNext($result)){
                            $locationID=$aRow["locationID"];
                        }
                    }
                    else{
                        $sql="INSERT INTO location (longitude, latitude) VALUES ('".$longitude."','".$latitude."')";
                        DBConnect();
                        $result=execQuery($sql);
                        DBClose();
                        $sql="SELECT * FROM location WHERE latitude='" . $latitude . "' AND longitude='" . $longitude . "'";
                        DBConnect();
                        $resLoc=execQuery($sql);
                        DBClose();
                        while ($aRow=fetchNext($resLoc)){
                            $locationID=$aRow["locationID"];
                        }
                    }
                    $sql="INSERT INTO `location-controller`(locationID, controllerID, dateStart, active, deactivated, admin) VALUES ('".$locationID."','".$controllerID."','".$date->format("Y-m-d h:m:s")."', 1,0,1)";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    if ($result){
                        $message="Add Microcontroller ID:".$controllerID;
                        $sql="SELECT * FROM user WHERE userID=".$userID;
                        DBConnect();
                        $result=execQuery($sql);
                        DBClose();
                        while($aRow=fetchNext($result)){
                            $type=$aRow["type"];
                        }
                        $sql="SELECT * FROM user WHERE type='admin'";
                        DBConnect();
                        $ad=execQuery($sql);
                        DBClose();
                        while ($aRow=fetchNext($ad)){
                            $admin=$aRow["userID"];
                        }
                        if (strpos($type, 'admin')!==false) {
                            $sql = "INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('" . $userID . "','" . $userID . "','" . $message . "',0)";
                            DBConnect();
                            $result = execQuery($sql);
                            DBClose();
                        }
                        $sql="INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('".$userID."','".$admin."','".$message."',0)";
                        DBConnect();
                        $result=execQuery($sql);
                        DBClose();
                        $response = array(
                            "status" => "success",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP Address" => $ip,
                            "message" => "Successfully registered user."
                        );
                    }
                    else{
                        $sql="DELETE FROM `user` WHERE userID='".$userID."'";
                        DBConnect();
                        $res=execQuery($sql);
                        DBClose();
                        $sql="DELETE FROM `micro-controller` WHERE controllerID='".$controllerID."'";
                        DBConnect();
                        $res=execQuery($sql);
                        DBClose();
                        $sql="DELETE FROM `user-controller` WHERE controllerID='".$controllerID."' AND userID='".$userID."'";
                        DBConnect();
                        $res=execQuery($sql);
                        DBClose();
                        $sql="DELETE FROM `location-controller` WHERE controllerID='".$controllerID."' AND locationID='".$locationID."'";
                        DBConnect();
                        $res=execQuery($sql);
                        DBClose();
                        $response = array(
                            "status" => "fail",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP Address" => $ip,
                            "message" => "An error occurred while trying to register user-controller.",
                            "clouse" =>"first"

                        );
                    }

                }
                else{
                    $sql="DELETE FROM `user` WHERE userID='".$userID."'";
                    DBConnect();
                    $res=execQuery($sql);
                    DBClose();
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "An error occurred while trying to register user-controller.",
                        "clouse" =>"second"
                    );
                }

            }
            $response = array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Successfully registered user."
            );

        }
        else{
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "An error occurred while trying to register user-controller.",
                "clouse" =>"third"
            );
        }
    }
    else {
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide all the registration fields."
        );
    }

}
echo json_encode($response);
