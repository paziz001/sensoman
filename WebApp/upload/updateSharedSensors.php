<?php


include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();


$token_flag = false;

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
} else {

	  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }



  if($token_flag == true && isset($_POST["watcherID"]) && isset($_POST["sensorID"]) && isset($_POST["boardID"]) && isset($_POST["readValue"]) && isset($_POST["writeValue"]) && isset($_POST["updatePermission"])){

	$watcherID = $_POST["watcherID"];
	$sensorID = $_POST["sensorID"];
	$boardID = $_POST["boardID"];
	$readValue = $_POST["readValue"];
	$writeValue = $_POST["writeValue"];
    $update = $_POST["updatePermission"];

if($update == '0'){

  $sql ="INSERT INTO sn_shared_sensors (ownerID,watcherID,sensorID,readValue,writeValue,boardID) VALUES ('".$userID."','".$watcherID."','".$sensorID."','".$readValue."','".$writeValue."','".$boardID."')";

  		DBConnect();
		$result = execQuery($sql);
		DBClose();

		if($result == true){
			$sql ="INSERT INTO sn_shared_boards (ownerID,watcherID,boardID) VALUES ('".$userID."','".$watcherID."','".$boardID."') ON DUPLICATE KEY UPDATE ownerID = '".$userID."' ";
			DBConnect();
			$result = execQuery($sql);
			DBClose();

			if($result == true){

				$sql="DELETE FROM sn_requested_sensors WHERE applicantID = '".$watcherID."' AND receiverID = '".$userID."' AND sensorID = '".$sensorID."' AND boardID ='".$boardID."' ";	
			
				DBConnect();
				$result = execQuery($sql);
				DBClose();

				if($result == true){
					 $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Sensor is now shared between you!"
           		 );
				

				} else{
					$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Error in deleting request!"
            );

				}


			}//second result
			else{
				$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Error while insertion of shared board!"
            );

			}

		} //first result
		else{

			$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Error while insertion of shared sensor!"
            );

		} //INSERT IF 
    } else{ //update shared sensor

        $sql = "UPDATE sn_shared_sensors SET  readValue ='".$readValue."', writeValue = '".$writeValue."' WHERE boardID = '".$boardID."' AND ownerID = '".$userID."' AND watcherID = '".$watcherID."' AND sensorID ='".$sensorID."'";

        DBConnect();
        $result = execQuery($sql);
        DBClose();

        if($result == true){

                $sql="DELETE FROM sn_requested_sensors WHERE applicantID = '".$watcherID."' AND receiverID = '".$userID."' AND sensorID = '".$sensorID."' AND boardID ='".$boardID."' ";    
            
                DBConnect();
                $result = execQuery($sql);
                DBClose();

                if($result == true){
                    $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Sensor's permissions are updated!"
                 );
                

                } else{
                    $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Error in deleting request!"
            );

                }

        }

    }
  }
  else if($token_flag == true){
			$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Invalid parameters!"
            );

  }


}//POST ELSE

echo json_encode($response);
?>