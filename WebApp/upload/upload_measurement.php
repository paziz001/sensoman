<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');

require __DIR__ . '/../rules_engine/rule-utilities.php';

$db = DBConnect();
$date = new DateTime('now');
$date = $date->format("Y-m-d h:m:s");
$sensorID = $_GET['sensorID'];


$stmt = $db->prepare("SELECT `type-sensor`.`type` FROM `type-sensor`,`sensor` WHERE `sensor`.`sensorID` = ? AND `sensor`.`sensorProperty` = `type-sensor`.`typeSensorID` ");
$stmt->bind_param('i', $sensorID);
$stmt->execute();
$result = $stmt->get_result();
if(rowCount($result)<1){
    echo "no type found";
    return;
}else{
    $sensorType = fetchNext($result)['type'];
}
echo $sensorType;
$measurement = ($sensorType!="motion")?$_GET['measurement']:(($_GET['measurement']==1)?"HIGH":"LOW");
echo $measurement;
$stmt = $db->prepare("INSERT INTO `sensor-measurement` (`sensorID`, `measurement`, `datetime`)
VALUES (?, ?, NOW())");
$stmt->bind_param('is', $sensorID, $measurement);
$stmt->execute();

$stmt = $db->prepare("SELECT `sensor`.`sensorName` FROM `sensor` WHERE `sensor`.`sensorID`= ? ");
$stmt->bind_param('i', $sensorID);
$stmt->execute();
$result = $stmt->get_result();
if(rowCount($result)<1){
    echo "no controllers found";
    return;
}else{
    $sensorName = fetchNext($result)['sensorName'];
}

$stmt = $db->prepare("SELECT DISTINCT `rule`.`definition` FROM `rule`
WHERE INSTR(`rule`.`description`, ?) > 0");
$stmt->bind_param('s', $sensorName);
$stmt->execute();
$rules = $stmt->get_result();

if(rowCount($rules)<1){
    echo "no rules found";
    return;
}else{
    while ($aRow = fetchNext($rules)) {
        $logicalStatements = array();
        $conditionalsCount = 0;
        $row = array();
        processRule($aRow["definition"],$logicalStatements);
    }
}


