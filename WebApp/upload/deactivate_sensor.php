<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 7/2/2015
 * Time: 8:24 μμ
 */

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["sensorID"])){
        $token = $_POST["token"];
        $sensorID=$_POST["sensorID"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                DBConnect();
                $result=execProcedure("FIND_SENSOR('$sensorID')");
                DBClose();
                while ($aRow=fetchNext($result)){
                    $active=$aRow["active"];
                    if ($active==1) {
                        DBConnect();
                        if (execProcedure("DEACTIVATE_SENSOR('$sensorID')")){
                            DBClose();
                            $message="Deactivate Sensor ID:".$sensorID;
                            $sql="SELECT * FROM user WHERE userID=".$userID;
                            DBConnect();
                            $result=execQuery($sql);
                            DBClose();
                            while($aRow=fetchNext($result)){
                                $type=$aRow["type"];
                            }
                            $sql="SELECT * FROM user WHERE type='admin'";
                            DBConnect();
                            $ad=execQuery($sql);
                            DBClose();
                            while ($aRow=fetchNext($ad)){
                                $admin=$aRow["userID"];
                            }
                            if (strpos($type, 'admin')!==false) {
                                if ($admin!==$userID) {
                                    $sql = "INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('" . $userID . "','" . $userID . "','" . $message . "',0)";
                                    DBConnect();
                                    $result = execQuery($sql);
                                    DBClose();
                                }
                            }
                            $sql="INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('".$userID."','".$admin."','".$message."',0)";
                            DBConnect();
                            $result=execQuery($sql);
                            DBClose();

                            $response=array(
                                "status" => "success",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP Address" => $ip,
                                "message" => "Successfully deactivate sensor"
                            );
                        }
                        else{
                            $response=array(
                                "status" => "fail",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP Address" => $ip,
                                "message" => "An error occurred when trying to deactivate the sensor"
                            );
                        }
                    }
                    else{
                        $response=array(
                            "status" => "fail",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP Address" => $ip,
                            "message" => "The sensor is already deactivated"
                        );
                    }
                }

            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token and the sensorID (int)."
        );
    }
}
echo json_encode($response);


