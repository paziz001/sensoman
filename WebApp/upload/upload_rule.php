<?php


include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
require_once __DIR__ . '/../rules_engine/rule-utilities.php';

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$db = DBConnect();
$method = getRequestMethod();
$date = new DateTime('now');
$ip = getClientIP();

if ($method != 'POST') {
    $response = array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '" . $method . " " . $_SERVER['REQUEST_URI'] . "'"
    );
} else {
    if (isset($_POST["token"]) && isset($_POST["rule"])&&isset($_POST["ruleID"])) {
        $token = $_POST["token"];
        //Check the validation of the token
        $sql = "SELECT * FROM token WHERE token='" . $token . "'";
        DBConnect();
        $resultToken = execQuery($sql);
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)) {

                $xml = simplexml_load_string($_POST["rule"]);
                $logicalStatements = array();
                $index = 0;

                try {
                    iterateWholeXml($xml, $logicalStatements, $index);
                } catch (Exception $e) {
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP" => $ip,
                        "message" => "Wrong rule syntax please refer to the instructions"
                    );
                    echo json_encode($response);
                    return 1;
                }

                for ($j = 0; $j < sizeof($logicalStatements); $j++) {

                    if (array_key_exists("logicops", $logicalStatements[$j]) && sizeof($logicalStatements[$j]["logicops"]) < 1 && $j != 3) {
                        if (sizeof($logicalStatements[$j]["compareops"]) < 1) {
                            $response = array(
                                "status" => "fail",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP" => $ip,
                                "message" => "Wrong rule syntax please refer to the instructions"
                            );
                            echo json_encode($response);
                            return 1;
                        } else if (sizeof($logicalStatements[$j]["sensors"]) < 1) {
                            $response = array(
                                "status" => "fail",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP" => $ip,
                                "message" => "Wrong rule syntax please refer to the instructions"
                            );
                            echo json_encode($response);
                            return 1;
                        } else if (sizeof($logicalStatements[$j]["actuators"]) < 1) {
                            $response = array(
                                "status" => "fail",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP" => $ip,
                                "message" => "Wrong rule syntax please refer to the instructions"
                            );
                            echo json_encode($response);
                            return 1;
                        }
                    }
                    if (array_key_exists("logicops", $logicalStatements[$j]) &&
                        (sizeof($logicalStatements[$j]["logicops"]) >= sizeof($logicalStatements[$j]["compareops"]))
                    ) {
                        $response = array(
                            "status" => "fail",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP" => $ip,
                            "message" => "Wrong rule syntax please refer to the instructions"
                        );
                        echo json_encode($response);
                        return 1;
                    }
                    if (array_key_exists("compareops", $logicalStatements[$j]) && sizeof($logicalStatements[$j]["compareops"]) < 1 && $j != 3) {
                        $response = array(
                            "status" => "fail",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP" => $ip,
                            "message" => "Wrong rule syntax please refer to the instructions"
                        );
                        echo json_encode($response);
                        return 1;
                    } else if (array_key_exists("sensors", $logicalStatements[$j]) && sizeof($logicalStatements[$j]["sensors"]) < 1) {
                        $response = array(
                            "status" => "fail",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP" => $ip,
                            "message" => "Wrong rule syntax please refer to the instructions"
                        );
                        echo json_encode($response);
                        return 1;

                    } else if (array_key_exists("actuators", $logicalStatements[$j]) && sizeof($logicalStatements[$j]["actuators"]) < 1) {
                        $response = array(
                            "status" => "fail",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP" => $ip,
                            "message" => "Wrong rule syntax please refer to the instructions"
                        );
                        echo json_encode($response);
                        return 1;
                    }
                }
                $description = "";
                getDescription($logicalStatements, $description);
                if($_POST["ruleID"]=="") {
                    $stmt = $db->prepare("INSERT INTO rule(userID,definition,description) VALUES (?,?,?)");
                    $stmt->bind_param('iss', $userID, $xml->asXML(), $description);
                    $stmt->execute();
                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP" => $ip,
                        "message" => "Rule added successfully"
                    );
                }else{
                    $stmt = $db->prepare("UPDATE `rule` SET `definition`= ?,`description` = ?
                                          WHERE `ruleID`=?");
                    $stmt->bind_param('ssi',$xml->asXML(),$description,$_POST["ruleID"]);
                    $stmt->execute();
                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP" => $ip,
                        "message" => "Rule edited successfully"
                    );
                }

                echo json_encode($response);
                return 0;

            }
        }
    }elseif(!isset($_POST["token"])){
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP" => $ip,
            "message" => "No token found in the post body"
        );
        echo json_encode($response);
        return 1;
    }elseif(!isset($_POST["rule"])||!isset($_POST["ruleID"])){
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP" => $ip,
            "message" => "no rule or ruleID given in body"
        );
        echo json_encode($response);
        return 1;
    }
}
