<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 10/4/2015
 * Time: 11:23 μμ
 */

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["message"]) && isset($_POST["notificationID"])){
        $token = $_POST["token"];
        $notificationID=$_POST["notificationID"];
        $message=$_POST["message"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                if (strpos($message,'Deactivate Sensor')!==false){
                    $sensor=explode(':', $message);
                    $sensorID=$sensor[1];
                    $sql="UPDATE `sensor-controller` SET deactivated=1 WHERE sensorID='".$sensorID."'";
                    DBConnect();
                    if(execQuery($sql)){
                        DBClose();
                        $sql="UPDATE notification SET done=1 WHERE notificationID='".$notificationID."'";
                        DBConnect();
                        if (execQuery($sql)){
                            DBClose();
                            $response=array(
                                "status" => "success",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP Address" => $ip,
                                "message" => "Notification has just been fullfilled."
                            );
                        }
                    }
                }
                else if(strpos($message, 'Change Frequency')!==false){
                    $freq=explode(':',$message);
                    $sensorID=$freq[1];
                    $sql="UPDATE `sensor` SET changedFrequency=0 WHERE sensorID='".$sensorID."'";
                    DBConnect();
                    if(execQuery($sql)){
                        DBClose();
                        $sql="UPDATE notification SET done=1 WHERE notificationID='".$notificationID."'";
                        DBConnect();
                        if (execQuery($sql)){
                            DBClose();
                            $response=array(
                                "status" => "success",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP Address" => $ip,
                                "message" => "Notification has just been fullfilled."
                            );
                        }
                    }
                }
                else if(strpos($message, 'Add Sensor')!==false){
                    $sql="UPDATE notification SET done=1 WHERE notificationID='".$notificationID."'";
                    DBConnect();
                    if (execQuery($sql)){
                        DBClose();
                        $response=array(
                            "status" => "success",
                            "request time" => $date->format("Y-m-d h:m:s"),
                            "IP Address" => $ip,
                            "message" => "Notification has just been fullfilled."
                        );
                    }
                }
                else if(strpos($message, 'Add Microcontroller')!==false){
                    $controllers=explode(':',$message);
                    $controllerID=$controllers[1];
                    $sql="UPDATE `location-controller` SET admin=0 WHERE controllerID='".$controllerID."'";
                    DBConnect();
                    if (execQuery($sql)){
                        DBClose();
                        $sql="UPDATE notification SET done=1 WHERE notificationID='".$notificationID."'";
                        DBConnect();
                        if (execQuery($sql)){
                            DBClose();
                            $response=array(
                                "status" => "success",
                                "request time" => $date->format("Y-m-d h:m:s"),
                                "IP Address" => $ip,
                                "message" => "Notification has just been fullfilled."
                            );
                        }
                    }
                }
            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token notification info."
        );
    }
}
echo json_encode($response);


