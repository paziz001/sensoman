<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = 0;

  if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
} else {
	  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }
    if($token_flag == true && isset($_POST["receiverID"]) && isset($_POST["boardID"]) && isset($_POST["sensorIDs"]) && isset($_POST["readValues"]) && isset($_POST["writeValues"]) && isset($_POST["updatePermissions"]) ){
    $receiverID = $_POST["receiverID"];
    $boardID = $_POST["boardID"];
    $sensors = $_POST["sensorIDs"];
    $readValues = $_POST["readValues"];
    $writeValues = $_POST["writeValues"];
    $updatePermissions = $_POST["updatePermissions"];

    foreach($sensors as $index=> $sensor) {
        $sql = "INSERT INTO sn_requested_sensors(applicantID,receiverID,sensorID,boardID,readValue,writeValue,updatePermissions) VALUES ('".$userID."', '".$receiverID."', '".$sensor."', '".$boardID."','".$readValues[$index]."','".$writeValues[$index]."','".$updatePermissions[$index]."')";
        DBConnect();
        $result = execQuery($sql);
        DBClose();
        if($result == false){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Failure in creating request for sensor(s)"
            );
            echo json_encode($response);
            exit();

        }

    } //for

    $response=array(
         "status" => "success",
         "request time" => $date->format("Y-m-d h:m:s"),
         "IP Address" => $ip,
         "message" => "Request created successfully"
            );

    }
} //POST token
echo json_encode($response);
?>