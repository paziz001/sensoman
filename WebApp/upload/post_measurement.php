<?php

/*
* Evangelos Aristodemou
*/

include_once('../includes/common.php');
require_once('../includes/connectdb.php');;

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();


if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else {
  $data = json_decode(file_get_contents("php://input"));
  $sensorID = $data->sensorID;
  $measurement = $data->measurement;
  $sql = "INSERT INTO `sensor-measurement` (`sensorID`, `measurement`, `datetime`) VALUES (" . $sensorID .", ". $measurement .", NOW())";
  DBConnect();
  $result=execQuery($sql);
  DBClose();
  if ($result){
      $response = array(
          "status" => "success",
          "request time" => $date->format("Y-m-d h:m:s"),
          "IP Address" => $ip,
          "message" => "measurement uploaded successfully.",
      );
    }
    else{
      $response = array(
          "status" => "fail",
          "request time" => $date->format("Y-m-d h:m:s"),
          "IP Address" => $ip,
          "message" => "An error occurred. Unable to upload the measurement.",
      );
    }
}
echo json_encode($response);

?>
