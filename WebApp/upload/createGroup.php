<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = false;

if ($method != 'POST') {
    $response=array(
        "status" => "token expired",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
} else{
	  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }
 //checks the validation of groupName and description parameters
if($token_flag == true && isset($_POST["groupName"]) && isset($_POST["description"])){
	$groupName = test_input($_POST["groupName"]);
	$description = test_input($_POST["description"]);
	if($description == ''){
		$description = "Description is not available.";
	}
	 $sql="SELECT * FROM sn_group WHERE name='".$groupName."'";
     DBConnect();
     $resultName=execQuery($sql);
     DBClose();
    $numRows=rowCount($resultName);
    if ($numRows>=1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "This name is being used already."
            );
        }else{
        	//all attributes are valid, insert the request in the database
		$sql = "INSERT INTO sn_group (ownerID,name,description) VALUES ('".$userID."', '".$groupName."', '".$description."')";
		DBConnect();
		$result = execQuery($sql);
		DBClose();
		
		if($result == true){
			 $response=array(
	        	"status" => "success",
	       	 	"request time" => $date->format("Y-m-d h:m:s"),
	        	"IP Address" => $ip,
	        	"message" => "You have successfully create group."
	    		);
       	 } else{
       	 		 $response=array(
		        "status" => "fail",
		        "request time" => $date->format("Y-m-d h:m:s"),
		        "IP Address" => $ip,
		        "message" => "Failure in creating group."
		    );
       	 }
}

}

} //else POST

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

echo json_encode($response);
?>