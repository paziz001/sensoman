<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 7/2/2015
 * Time: 8:03 μμ
 */

 /*
 * Updated by Evangelos Aristodemou
 * Accepts a sensor name that exists in other controller but not in the same.
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

DBConnect();
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["sensorName"]) && isset($_POST["property"])){
        $token = $_POST["token"];
        $sensorName=$_POST["sensorName"];
        $property=$_POST["property"];
        $type = isset($_POST["type"]) ? $_POST["type"] : "not_specified";
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $controllerID = $_POST["controller"];
              $sql = "SELECT sensorName FROM `sensor-controller` sc
                      JOIN `sensor` s ON sc.`sensorID`=s.`sensorID`
                      WHERE `controllerID`=" . $controllerID;
              DBConnect();
              $result=execQuery($sql);
              DBClose();
              $exists = false;
              while ($aRow=fetchNext($result)){
                  if ($aRow["sensorName"] == $sensorName){
                    $exists = true;
                    break;
                  }
              }
              if ($exists==false){
                $sql="INSERT INTO sensor(sensorName, sensorProperty, type) VALUES ('".$sensorName."','".$property."','" . $type . "')";
                DBConnect();
                if (execQuery($sql)){
                    DBClose();
                    $sql="SELECT * FROM sensor WHERE sensorName='".$sensorName."'";
                    DBConnect();
                    $res=execQuery($sql);
                    DBClose();
                    while ($aRow=fetchNext($res)){
                        $sensorID=$aRow["sensorID"];
                    }
                    if ($controllerID!=NULL){
                      $sql="INSERT INTO `sensor-controller`(sensorID, controllerID, dateStart, dateEnd, active, deactivated)
                            VALUES ('".$sensorID."','".$controllerID."','".$date->format("Y-m-d h:m:s")."','".$date->format("Y-m-d h:m:s")."',1,0)";
                      DBConnect();
                      $result=execQuery($sql);
                      DBClose();
                    }
                    $message="Add Sensor ID:".$sensorID;
                    $sql="SELECT * FROM user WHERE userID=".$userID;
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    while($aRow=fetchNext($result)){
                        $type=$aRow["type"];
                    }
                    $sql="SELECT * FROM user WHERE type='admin'";
                    DBConnect();
                    $ad=execQuery($sql);
                    DBClose();
                    while ($aRow=fetchNext($ad)){
                        $admin=$aRow["userID"];
                    }
                    if (strpos($type, 'admin')!==false) {
                        if ($admin!==$userID) {
                            $sql = "INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('" . $userID . "','" . $userID . "','" . $message . "',0)";
                            DBConnect();
                            $result = execQuery($sql);
                            DBClose();
                        }
                    }
                    $sql="INSERT INTO `notification` (fromUserID, toUserID, message,done) VALUES('".$userID."','".$admin."','".$message."',0)";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Sensor added successfully.",
                        "sensorID" => $sensorID
                    );
                }
                else{
                  $response=array(
                      "status" => "fail",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "An error occurred. Unable to add the sensor."
                  );
                }
              }
                else{
                    $response=array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "This sensor name already exists. Give a different name."
                    );
                }
            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
            }
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token and the sensor name (string) and property (int)."
        );
    }
}
echo json_encode($response);

?>
