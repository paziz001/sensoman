/*
* Evangelos Aristodemou
*/

var token = localStorage.getItem("token");
var username = localStorage.getItem("username");
var fileID = 0;
var nPins = 0;
var newTime = 0;
var numPins = 0;

function checkToken(s){
  if (s == "token expired") {
    swal({
            title: "Session has expired",
            text: "Please connect again to gain access.",
            type: "info",
            showCancelButton: false,
            confirmButtonText: "OK",
            closeOnConfirm: false
        },
        function(){
            localStorage.removeItem("token");
            localStorage.removeItem("current");
            localStorage.removeItem("type");
            localStorage.removeItem("username");
            localStorage.removeItem("email");
            localStorage.removeItem("userlat");
            localStorage.removeItem("userlng");
            window.location.href = "../pages/login.html";
            return false;
        });
  }
  return true;
}

function getTime(time){
  var controller = document.getElementById('modal-controller').value;
  if (controller == "Arduino" || controller == "Intel Galileo"){
    return (time*1000);
  }
  return time;
}

function saveFile(filename, controller, sensors, pins, timeInterval, mod) {
  console.log(filename);
  console.log(controller);
  console.log(sensors);
  console.log(pins);
  console.log(timeInterval);
  console.log(mod);
  numPins = (pins.split(";")).length;
  console.log(numPins);
  var path = '../upload/upload_file.php';
  var token = localStorage.getItem("token");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        console.log(this.responseText);
        fileID = result.fileID;
        newTime = result.timeInterval;
        nPins = result.pins;
        dPins = nPins.replace(/GPIO_/g, "");
        document.getElementById("downloadpins").value = dPins;
        document.getElementById('downloadtime').value = getTime(newTime);
        checkInfo();
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token + "&fileName=" + filename + "&controller=" + controller
              + "&sensors=" + sensors + "&pins=" + pins + "&timeInterval=" + timeInterval + "&internet=" + mod);
}

function isNum(p){
  if (p==""){
    return false;
  }
  for (var i=0; i<p.length; i++){
    if (p.charAt(i)<'0' || p.charAt(i)>'9'){
      return false;
    }
  }
  return true;
}

function editFile(len){
  var mes = document.getElementById('modal-mes');
  var time = document.getElementById('modal-time').value;
  var newpins = document.getElementById('modal-pin0').value;
  var same = false;
  for (var i=1; i<len; i++){
    newpins += ";" + document.getElementById('modal-pin' + i).value;
  }
  var tokens = newpins.split(";");
  if (time==""){
    mes.innerHTML = "Enter time interval." + "<br>"
    return;
  }
  if (!isNum(time)){
    mes.innerHTML = "The time interval must be a number.<br>";
    return;
  }
  for (var i=0; i<tokens.length; i++){
    for (var j=i; j<tokens.length; j++){
      if (i==j) continue;
      if (tokens[i]==tokens[j]){
        same = true;
        break;
      }
    }
  }
  if (same){
    mes.innerHTML = "You can not use the same pin for two or more sensors. <br>";
    return;
  }
  var path = '../edit/edit_file.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        if (result.status=="success"){
          nPins = newpins;
          newTime = time;
          for (var i=0; i<len; i++){
            document.getElementById('pin' + i).innerHTML = tokens[i];
          }
          console.log("Download Pins:" + nPins);
          document.getElementById('tabletime').innerHTML = time;
          document.getElementById("downloadpins").value = nPins;
          document.getElementById('downloadtime').value = getTime(newTime);
          swal("Saved!", "File info has been changed.", "success");
        }
        else{
          swal("Error!", "Unable to change the file info.", "danger");
        }
        $('#editFile').modal('hide');
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token + "&id=" + fileID + "&time=" + time + "&pins=" + newpins);
}

function checkInfo(){
  document.getElementById('tabletime').innerHTML = newTime;
  var sem = false;
  var tokens;
  for (var i=0; i<nPins.length; i++){
    if (nPins.charAt(i) == ';'){
      sem = true;
      break;
    }
  }
  if (sem){
    tokens = nPins.split(";");
  }
  else{
    tokens = [nPins];
  }
  for (var i=0; i<numPins; i++){
    document.getElementById('pin' + i).innerHTML = tokens[i];
  }
}
