/*
* Evangelos Aristodemou
*/

var scriptsURL = new Array(
    "../H5CM/trunk/modules/sensors/SensoMan.js"
);
loadDependencies(scriptsURL);

var token = localStorage.getItem("token");
var username = localStorage.getItem("username");
var sensors = 0;
var nc = false;
var s = [false, false, false, false, false];
var sensor_id = [false, false, false, false, false];
var controllerID = null;
var selController = null;

var arduino_sensors = {
  type: ["Motion (PIR)", "Temperature (DS18B20)", "Temperature (TMP36)", "Humidity (FC28)", "Light (LDR)", "Sound (digital)", "Sound (analog)"],
  id: ["5", "4", "4", "1", "2", "3", "3"]
}
var raspberry_sensors = {
  type: ["Motion (PIR)", "Temperature (DS18B20)", "Humidity (FC28)", "Light (LDR)", "Sound (digital)"],
  id: ["5", "4", "1", "2", "3"]
}
var beaglebone_sensors = {
  type: ["Motion (PIR)", "Temperature (TMP36)", "Humidity (FC28)", "Light (LDR)", "Sound (digital)", "Sound (analog)"],
  id: ["5", "4", "1", "2", "3", "3"]
}
var intelgalileo_sensors = {
  type: ["Motion (PIR)", "Temperature (TMP36)", "Humidity (FC28)", "Light (LDR)", "Sound (digital)", "Sound (analog)"],
  id: ["5", "4", "1", "2", "3", "3"]
}
var arduino_pins = {
  digital: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
  analog: ["A0", "A1", "A2", "A3", "A4", "A5"],
  pwm: ["3", "5", "6", "9", "10", "11"]
};
var raspberry_pins = {
  gpio: ["GPIO_2", "GPIO_3", "GPIO_4", "GPIO_5", "GPIO_6", "GPIO_7", "GPIO_8", "GPIO_9",
  "GPIO_10", "GPIO_11", "GPIO_12", "GPIO_13", "GPIO_14", "GPIO_15", "GPIO_16", "GPIO_17", "GPIO_18", "GPIO_19", "GPIO_20",
  "GPIO_21", "GPIO_22", "GPIO_23", "GPIO_24", "GPIO_25", "GPIO_26", "GPIO_27"]
};
var beaglebone_pins = {
  gpio: ["P8_7", "P8_8", "P8_9", "P8_10", "P8_11", "P8_12", "P8_14", "P8_15", "P8_16", "P8_17", "P8_18", "P8_26",
        "P9_11", "P9_12", "P9_13", "P9_15", "P9_17", "P9_18", "P9_23", "P9_24", "P9_25", "P9_26", "P9_27", "P9_30", "P9_41"],
  analogin: ["P9_33", "P9_35", "P9_36", "P9_37", "P9_38", "P9_39", "P9_40"]
};
var intelgalileo_pins = {
  digital: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
  analog: ["A0", "A1", "A2", "A3", "A4", "A5"],
  pwm: ["3", "5", "6", "9", "10", "11"]
};
var internet1 = ["Adafruit CC3000 Wi-Fi shield", "Ethernet shield"];
var internet2 = ["Wi-Fi adapter", "Ethernet cable"];
var internet3 = ["Ethernet cable"];
var internet4 = [];

function checkToken(s){
  if (s == "token expired") {
    swal({
            title: "Session has expired",
            text: "Please connect again to gain access.",
            type: "info",
            showCancelButton: false,
            confirmButtonText: "OK",
            closeOnConfirm: false
        },
        function(){
            localStorage.removeItem("token");
            localStorage.removeItem("current");
            localStorage.removeItem("type");
            localStorage.removeItem("username");
            localStorage.removeItem("email");
            localStorage.removeItem("userlat");
            localStorage.removeItem("userlng");
            window.location.href = "login.html";
            return false;
        });
  }
  return true;
}

function getSensorId(){
  var id = 0;
  for (var i=0; i<5; i++){
    if (sensor_id[i]==false){
      id = i+1;
      break;
    }
  }
  return id;
}

function changeStyle1(p){
  p.style="background-color: rgb(51, 122, 183); color: white; float: left;"
}

function changeStyle2(p){
  p.style="background-color: white; color: rgb(51, 122, 183); float: left;"
}

function deleteSensor(p){
  var id = p.id.split("-");
  if (id[2]==null){
    if (p.parentElement.name=="fieldset"){
      p.parentElement.remove();
    }
    else{
      p.parentElement.parentElement.remove();
    }
    sensors--;
    sensor_id[id[1]-1]=false;
    if (sensors==0){
      document.getElementById('generate').style.display = 'none';
      document.getElementById('sbt').style.display = 'none';
      if (nc==false){
        document.getElementById('adds').style.display = 'none';
      }
    }
    document.getElementById('adds').style.display = 'block';
  }
  else{
    swal({
      title: "Delete this sensor?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      confirmButtonColor: '#ff0000',
      closeOnConfirm: true
    },
    function() {
        var path = '../delete/delete_sensor.php';
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var result = JSON.parse(this.responseText);
            if (checkToken(result.message)){
              if (p.parentElement.name=="fieldset"){
                p.parentElement.remove();
              }
              else{
                p.parentElement.parentElement.remove();
              }
              sensors--;
              document.getElementById('adds').style.display = 'block';
              sensor_id[id[1]-1]=false;
              if (sensors==0){
                document.getElementById('generate').style.display = 'none';
                document.getElementById('sbt').style.display = 'none';
              }
            }
          }
        };
        xhttp.open("POST", path, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("token=" + token + "&id=" + id[2]);
    });
  }
}

function selectController(){
  var droplist = document.getElementById('existingcontrollers');
  var path = '../retrieve/sensorsOfController.php';
  var val = droplist.value.split(";");
  controllerID = val[0];
  document.getElementById('hiddenexcontrollerID').value = controllerID;
  var type = val[2];
  selController = type;
  var params = {'token' : token, controllerID : controllerID};
  var div = document.getElementById('sensors');
  var generate = document.getElementById('generate');
  document.getElementById('excontrollerType').value = type;
  div.innerHTML="";
  generate.style.display = 'none';
  for (var i=0; i<5; i++){
    sensor_id[i]=false;
  }
  sensors=0;
  if (droplist.value=='0'){
    document.getElementById('adds').style.display = 'none';
    document.getElementById('excontrollerType').value = "";
  }
  else{
    document.getElementById('adds').style.display = 'block';
  }
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var result = JSON.parse(xhttp.responseText);
      if (checkToken(result.message)){
        var sen = result.sensors;
        for (var i=0; i<sen.length; i++){
          sensors++;
          var id = getSensorId();
          sensor_id[id-1]=true;
          var pins = "";
          if (selController=="Arduino"){
            if (sen[i].type=="Sound (digital)" || sen[i].type=="Temperature (DS18B20)" || sen[i].type=="Motion (PIR)"){
              for (var k=0; k<arduino_pins.digital.length; k++){
                pins+='<option value="' + arduino_pins.digital[k] + '">' + arduino_pins.digital[k] + '</option>';
              }
            }
            else{
              for (var k=0; k<arduino_pins.analog.length; k++){
                pins+='<option value="' + arduino_pins.analog[k] + '">' + arduino_pins.analog[k] + '</option>';
              }
            }
          }
          else if (selController=="Raspberry Pi"){
            for (var k=0; k<raspberry_pins.gpio.length; k++){
              pins+='<option value="' + raspberry_pins.gpio[k] + '">' + raspberry_pins.gpio[k] + '</option>';
            }
          }
          else if (selController=="Beaglebone Black"){
            if (sen[i].type=="Sound (digital)" || sen[i].type=="Temperature (DS18B20)" || sen[i].type=="Motion (PIR)"){
              for (var k=0; k<beaglebone_pins.gpio.length; k++){
                pins+='<option value="' + beaglebone_pins.gpio[k] + '">' + beaglebone_pins.gpio[k] + '</option>';
              }
            }
            else{
              for (var k=0; k<beaglebone_pins.analogin.length; k++){
                pins+='<option value="' + beaglebone_pins.analogin[k] + '">' + beaglebone_pins.analogin[k] + '</option>';
              }
            }
          }
          else if (selController=="Intel Galileo"){
            if (sen[i].type=="Sound (digital)" ||  sen[i].type=="Motion (PIR)"){
              for (var k=0; k<intelgalileo_pins.digital.length; k++){
                pins+='<option value="' + intelgalileo_pins.digital[k] + '">' + intelgalileo_pins.digital[k] + '</option>';
              }
            }
            else{
              for (var k=0; k<intelgalileo_pins.analog.length; k++){
                pins+='<option value="' + intelgalileo_pins.analog[k] + '">' + intelgalileo_pins.analog[k] + '</option>';
              }
            }
          }
          div.innerHTML += '<fieldset class="scheduler-border" name="fieldset">\
        <legend class="scheduler-border">Existing Sensor</legend>\
            <div class="form-group col-md-3">\
              <label>Sensor Type</label>\
              <input type="text" class="form-control" name="sensorType' + id +'" value="' + sen[i].type + '" readonly>\
            </div>\
            <div class="form-group form-group col-md-4">\
              <label>Sensor Name</label>\
              <input type="text" class="form-control" name="sensorName' + id + '" value="' + sen[i].sensorName + '" readonly cx>\
            </div>\
            <div class="form-group col-md-4">\
              <label>Pin</label>\
              <select class="form-control" name="sensorPin' + id + '" id="sensorPin' + id + '">\
                <option value="-1" selected>-Select Pin-</option>' + pins + '\
              </select>\
            </div>\
            <input type="hidden" name="sensorID' + id + '" value="' + sen[i].sensorID + '">\
            <div style="clear:both;">\
              <button type="button" class="btn btn-primary" id="delete-' + id + '-' + sen[i].sensorID + '" onclick="deleteSensor(this)">Delete</button>\
            </div>\
          </fieldset>';
        }
        if (i>0){
          generate.style.display = 'block';
        }
        if (sensors>=5){
          document.getElementById('adds').style.display = 'none';
        }
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + params.token + "&controllerID="+params.controllerID);

  var droplist = document.getElementById('internet2');
  var options = '<option value="No internet connection" selected>No internet connection</option>';
  if (type==undefined){
    droplist.innerHTML = '<option value="-1" selected>Select controller</option>';
    return;
  }
  if (selController=="Arduino"){
    for (var i=0; i<internet1.length; i++){
      options += '<option value="' + internet1[i] + '">' + internet1[i] + '</option>';
    }
  }
  else if (selController=="Raspberry Pi"){
    for (var i=0; i<internet2.length; i++){
      options += '<option value="' + internet2[i] + '">' + internet2[i] + '</option>';
    }
  }
  else if (selController=="Beaglebone Black"){
    for (var i=0; i<internet3.length; i++){
      options += '<option value="' + internet3[i] + '">' + internet3[i] + '</option>';
    }
  }
  else if (selController=="Intel Galileo"){
    for (var i=0; i<internet4.length; i++){
      options += '<option value="' + internet4[i] + '">' + internet4[i] + '</option>';
    }
  }

  droplist.innerHTML = options;
}

function isNum(p){
  for (var i=0; i<p.length; i++){
    if (p.charAt(i)<'0' || p.charAt(i)>'9'){
      return false;
    }
  }
  return true;
}

function saveSensor(p){
  var id = (p.id.split("-"))[1]
  var mes = document.getElementById('message_sensor' + id);
  var t = document.getElementById('sensorType' + id).value;
  var tokens = t.split("-");
  var property = tokens[0];
  var type = tokens[1];
  var name = document.getElementsByName('sensorName' + id)[0].value;
  var pin = document.getElementsByName('sensorPin' + id)[0].value;
  console.log(pin);
  var controller = controllerID;
  if (t==0){
    mes.innerHTML="Select sensor type.";
    return;
  }
  if (name==""){
    mes.innerHTML="Give sensor name.";
    return;
  }
  for (var i=0; i<name.length; i++){
    var c = name.charAt(i);
    if ((c<'a' || c>'z') && (c<'A' || c>'Z') && (c<'0' || c>'9') && (c!='-') && (c!='_')){
      mes.innerHTML="Not valid sensor name. (Use only letters and numbers)" ;
      return;
    }
  }
  if (pin==-1){
    mes.innerHTML="Select pin to attach the sensor on the micro-controller.";
    return;
  }
  if (controllerID==null){
    mes.innerHTML="Select a micro-controller first.";
    return;
  }
  var path = '../upload/upload_sensor.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        var message = result.message;
        if (result.status=="fail"){
          mes.innerHTML = message;
        }
        else{
          mes.innerHTML="";
          p.innerHTML="Saved";
          p.disabled = "true";
          document.getElementsByName('sensorName' + id)[0].readOnly = "true";
          document.getElementById('sensorType' + id).disabled = "true";
          var deletebtn = document.getElementById('delete-' + id);
          deletebtn.innerHTML="Delete";
          deletebtn.setAttribute("id",deletebtn.id + '-' + result.sensorID);
          document.getElementById('hiddensensorID' + id).value = result.sensorID;
        }
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token + "&sensorName=" + name + "&property=" + property + "&controller=" + controller + "&type=" + type);
}

function saveController(p){
  var mes = document.getElementById('message_controller');
  var type = document.getElementById('controllerType').value;
  var name = document.getElementById('controllerName').value;
  var time = document.getElementById('timeInterval1').value;
  if (type==-1){
    mes.innerHTML="Select micro-controller type.";
    return;
  }
  if (name==""){
    mes.innerHTML="Give micro-controller name.";
    return;
  }
  for (var i=0; i<name.length; i++){
    var c = name.charAt(i);
    if ((c<'a' || c>'z') && (c<'A' || c>'Z') && (c<'0' || c>'9') && (c!='-') && (c!='_')){
      mes.innerHTML="Not valid micro-controller name. (Use only letters and numbers)" ;
      return;
    }
  }
  if (time==""){
    mes.innerHTML="Enter time interval.";
    return;
  }
  if (!isNum(time)){
    mes.innerHTML="The time interval must be a positive number.";
    return;
  }
  var path = '../upload/upload_controller.php';
  var params = {'token' : token, 'controller' : name, 'type' : type};
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        var message = result.message;
        if (result.status=="fail"){
          mes.innerHTML = message;
        }
        else{
          mes.innerHTML="";
          p.innerHTML="Saved";
          p.disabled = "true";
          var excon = document.getElementById('existingcontrollers');
          var option = document.createElement("option");
          option.text = name;
          option.setAttribute("value", result.controllerID + ';' + name + ';' + type);
          excon.add(option);
          controllerID = result.controllerID;
          selController = type;
          document.getElementById('controllerType').disabled = "true";
          document.getElementById('controllerName').readOnly = "true";
          document.getElementById('hiddencontrollerID').value = controllerID;
          document.getElementById('adds').style.display = 'block';
        }
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + params.token + "&controller=" + params.controller +"&type=" + params.type);
}

function readControllers(){
  var path = '../retrieve/controllersOfUser.php';
  var params = {'token' : token};
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        var con = result.controllers;
        var droplist = document.getElementById('existingcontrollers');
        for (var i=0; i<con.length; i++){
          if (con[i].numFiles=='0'){
            var option = document.createElement("option");
            option.text = con[i].controllerName;
            option.setAttribute("value", con[i].controllerID + ';' + con[i].controllerName + ';' + con[i].controllerType);
            droplist.add(option);
          }
        }
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + params.token);
}

function selectModule(p){
  var div = document.getElementById('wifi-info');
  if (p.value == "Adafruit CC3000 Wi-Fi shield"){
    div.innerHTML = '<fieldset class="scheduler-border">\
      <legend class="scheduler-border">Wi-Fi information</legend>\
      <div class="form-group col-md-5" id="ssid1">\
        <label for="ssid">SSID</label>\
        <input type="text" class="form-control" id="ssid" name="ssid" aria-describedby="ssidHelp" placeholder="Enter your SSID">\
        <i><small id="ssidHelp" class="form-text text-muted">SSID is the name of your local Wi-Fi.</small></i>\
      </div>\
      <div class="form-group col-md-5" id="pass1">\
        <label for="wifipass">Password</label>\
        <input type="text" class="form-control" id="pass" name="pass" placeholder="Enter wi-fi password">\
      </div>\
    </fieldset>';
  }
  else{
    div.innerHTML = "";
  }
}

function selectControllerType(p){
  var type = p.value;
  document.getElementById('hiddencontrollerType').value = type;
  var droplist = document.getElementById('internet1');
  if (type=='-1'){
    droplist.innerHTML = '<option value="-1" selected>Select controller</option>';
    return;
  }

  var options = '<option value="No internet connection" selected>No internet connection</option>';
  if (type=="Arduino"){
    for (var i=0; i<internet1.length; i++){
      options += '<option value="' + internet1[i] + '">' + internet1[i] + '</option>';
    }
  }
  else if (type=="Raspberry Pi"){
    for (var i=0; i<internet2.length; i++){
      options += '<option value="' + internet2[i] + '">' + internet2[i] + '</option>';
    }
  }
  else if (type=="Beaglebone Black"){
    for (var i=0; i<internet3.length; i++){
      options += '<option value="' + internet3[i] + '">' + internet3[i] + '</option>';
    }
  }
  else if (type=="Intel Galileo"){
    for (var i=0; i<internet4.length; i++){
      options += '<option value="' + internet4[i] + '">' + internet4[i] + '</option>';
    }
  }
  droplist.innerHTML = options;
}

function selectSensorType(p){
  var type = (p.value.split("-"))[1];
  var id = p.id.charAt(10);
  document.getElementById('hiddensensorType' + id).value = type;
  var droplist = document.getElementById('sensorPin' + id);
  var pins = '<option value="-1" selected>-Select Pin-</option>';
  if (selController=="Arduino"){
    if (type=="Sound (digital)" || type=="Temperature (DS18B20)" || type=="Motion (PIR)"){
      for (var k=0; k<arduino_pins.digital.length; k++){
        pins+='<option value="' + arduino_pins.digital[k] + '">' + arduino_pins.digital[k] + '</option>';
      }
    }
    else{
      for (var k=0; k<arduino_pins.analog.length; k++){
        pins+='<option value="' + arduino_pins.analog[k] + '">' + arduino_pins.analog[k] + '</option>';
      }
    }
  }
  else if (selController=="Raspberry Pi"){
    if (type=="Temperature (DS18B20)"){
      pins = '<option value="GPIO_4">GPIO_4</option>';
    }
    else{
      for (var k=0; k<raspberry_pins.gpio.length; k++){
        pins+='<option value="' + raspberry_pins.gpio[k] + '">' + raspberry_pins.gpio[k] + '</option>';
      }
    }
  }
  else if (selController=="Beaglebone Black"){
    if (type=="Sound (digital)" || type=="Temperature (DS18B20)" || type=="Motion (PIR)"){
      for (var k=0; k<beaglebone_pins.gpio.length; k++){
        pins+='<option value="' + beaglebone_pins.gpio[k] + '">' + beaglebone_pins.gpio[k] + '</option>';
      }
    }
    else{
      for (var k=0; k<beaglebone_pins.analogin.length; k++){
        pins+='<option value="' + beaglebone_pins.analogin[k] + '">' + beaglebone_pins.analogin[k] + '</option>';
      }
    }
  }
  else if (selController=="Intel Galileo"){
    if (type=="Sound (digital)" || type=="Motion (PIR)"){
      for (var k=0; k<intelgalileo_pins.digital.length; k++){
        pins+='<option value="' + intelgalileo_pins.digital[k] + '">' + intelgalileo_pins.digital[k] + '</option>';
      }
    }
    else{
      for (var k=0; k<intelgalileo_pins.analog.length; k++){
        pins+='<option value="' + intelgalileo_pins.analog[k] + '">' + intelgalileo_pins.analog[k] + '</option>';
      }
    }
  }
  droplist.innerHTML = pins;
}

$(document).ready(function(){
    var sbt = $("#sbt");
    var generate = $("#generate");
    var adds = $("#adds");
    var newcontroller = $("#newcontroller");
    var existingcontroller = $("#existingcontroller");
    var addexm = $("#addexm");
    var addnm = $("#addnm");
    var back1 = $("#changecontroller1");
    var back2 = $("#changecontroller2");
    var savecontroller = $("#savecontroller");
    var user = $("#username");

    user.html(username);
    sbt.hide();
    generate.hide();
    newcontroller.hide();
    existingcontroller.hide();
    adds.hide();
    readControllers();

    addnm.click(function(){
      controllerID = null;
      newcontroller.show();
      addexm.hide();
      addnm.hide();
      $('#controllerType').disabled = "false";
      $("#controllerType").val($("#controllerType option:first").val());
      nc = true;
    });

    addexm.click(function(){
      controllerID=null;
      existingcontroller.show();
      addexm.hide();
      addnm.hide();
      nc = false;
      if (sensors!=0){
        generate.show();
      }
    });

    back1.click(function(){
      controllerID=null;
      $('#controllerName').val('');
      $('#timeInterval1').val('');
      $('#timeInterval2').val('');
      $('#hiddencontrollerID').val('');
      $('#hiddenexcontrollerID').val('');
      $('#c0').attr("selected", true);
      $('#n0').attr("selected", true);
      $('#internet1').html('<option value="-1" selected>Select controller<option>');
      $('#internet2').html('<option value="-1" selected>Select controller<option>');
      $('#wifi-info').html('');
      existingcontroller.hide();
      newcontroller.hide();
      addexm.show();
      addnm.show();
      generate.hide();
      nc = false;
      adds.hide();
      $('#sensors').html('');
      sensors=0;
      savecontroller.html('Save to continue');
      savecontroller.prop("disabled", false);
      $('#controllerName').prop("readOnly", false);
      $('#controllerType').prop("disabled", false);
    });

    back2.click(function(){
      controllerID=null;
      $('#controllerName').val('');
      $('#timeInterval1').val('');
      $('#timeInterval2').val('');
      $('#hiddencontrollerID').val('');
      $('#hiddenexcontrollerID').val('');
      $('#c0').attr("selected", true);
      $('#n0').attr("selected", true);
      $('#internet1').html('<option value="-1" selected>Select controller<option>');
      $('#internet2').html('<option value="-1" selected>Select controller<option>');
      $('#wifi-info').html('');
      existingcontroller.hide();
      newcontroller.hide();
      addexm.show();
      addnm.show();
      generate.hide();
      nc = false;
      adds.hide();
      $('#sensors').html('');
      sensors=0;
      savecontroller.html('Save to continue');
      savecontroller.prop("disabled", false);
      $('#controllerName').prop("readOnly", false);
      $('#controllerType').prop("disabled", false);
    });

    adds.click(function(){
      var sen = "";
      if (selController=="Arduino"){
        for (var i=0; i<arduino_sensors.type.length; i++){
          sen += '<option value="' + arduino_sensors.id[i] + '-' + arduino_sensors.type[i] + '">' + arduino_sensors.type[i] + '</option>';
        }
      }
      else if (selController=="Raspberry Pi"){
        for (var i=0; i<raspberry_sensors.type.length; i++){
          sen += '<option value="' + raspberry_sensors.id[i] + '-' + raspberry_sensors.type[i] + '">' + raspberry_sensors.type[i] + '</option>';
        }
      }
      else if (selController=="Beaglebone Black"){
        for (var i=0; i<beaglebone_sensors.type.length; i++){
          sen += '<option value="' + beaglebone_sensors.id[i] + '-' + beaglebone_sensors.type[i] + '">' + beaglebone_sensors.type[i] + '</option>';
        }
      }
      else if (selController=="Intel Galileo"){
        for (var i=0; i<intelgalileo_sensors.type.length; i++){
          sen += '<option value="' + intelgalileo_sensors.id[i] + '-' + intelgalileo_sensors.type[i] + '">' + intelgalileo_sensors.type[i] + '</option>';
        }
      }
      sensors++;
      var id = getSensorId();
      sensor_id[id-1]=true;
      $("#sensors").append('<fieldset class="scheduler-border" name="fieldset" id="newsensor' + id + '">\
        <legend class="scheduler-border">New Sensor</legend>\
        <p id="message_sensor' + id + '"></p>\
          <div class="form-group col-md-3">\
            <label>Sensor Type</label>\
            <select class="form-control" id="sensorType' + id + '" onchange="selectSensorType(this)">\
              <option value="0" selected="selected">-Select Type-</option>\
              '+ sen +'\
            </select>\
          </div>\
          <input type="hidden" id="hiddensensorType' + id + '" name="sensorType' + id + '" value="0">\
          <div class="form-group form-group col-md-4">\
            <label>Sensor Name</label>\
            <input type="text" class="form-control" name="sensorName' + id + '" placeholder="Enter sensor name">\
          </div>\
          <div class="form-group col-md-4">\
            <label>Pin</label>\
            <select class="form-control" id="sensorPin' + id + '" name="sensorPin' + id + '">\
              <option value="-1" selected>-Select Pin-</option>\
            </select>\
          </div>\
          <input type="hidden" id="hiddensensorID' + id + '" name="sensorID' + id + '" value="">\
          <div style="clear: both;">\
            <button type="button" class="btn btn-primary" style="color: rgb(51, 122, 183); float: left;" id="save-' + id + '" \
            onmouseover="changeStyle1(this)" onmouseout="changeStyle2(this)" onclick="saveSensor(this)">Save</button>\
            <button type="button" class="btn btn-primary" name="delete" id="delete-' + id + '" onclick="deleteSensor(this)">Cancel</button>\
          </div>\
          </fieldset>\
      ');
      generate.show();
      if (sensors>=5){
        adds.hide();
      }
    });

});
