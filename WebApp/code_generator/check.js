/*
* Evangelos Aristodemou
*/

var mes = document.getElementById('message');
var token = localStorage.getItem("token");

function checkToken(s){
  if (s == "token expired") {
    swal({
            title: "Session has expired",
            text: "Please connect again to gain access.",
            type: "info",
            showCancelButton: false,
            confirmButtonText: "OK",
            closeOnConfirm: false
        },
        function(){
            localStorage.removeItem("token");
            localStorage.removeItem("current");
            localStorage.removeItem("type");
            localStorage.removeItem("username");
            localStorage.removeItem("email");
            localStorage.removeItem("userlat");
            localStorage.removeItem("userlng");
            window.location.href = "login.html";
            return false;
        });
  }
  return true;
}

function isNum(p){
  if (p==""){
    return false;
  }
  for (var i=0; i<p.length; i++){
    if (p.charAt(i)<'0' || p.charAt(i)>'9'){
      return false;
    }
  }
  return true;
}

function checkTimeInterval(){
  var time1 = document.getElementById('timeInterval1').value;
  var time2 = document.getElementById('timeInterval2').value;
  if (time1=="" && time2==""){
    mes.innerHTML += "Enter time interval." + "<br>"
    return;
  }
  if (!isNum(time1) && !isNum(time2)){
    mes.innerHTML += "The time interval must be a number.<br>";
    return;
  }
}

function checkFilename(){
  var filename = document.getElementById('filename').value;
  if (filename==""){
    mes.innerHTML += "Enter file name.<br>";
    endCheck();
    return;
  }
  for (var i=0; i<filename.length; i++){
    var c = filename.charAt(i);
    if ((c<'a' || c>'z') && (c<'A' || c>'Z') && (c<'0' || c>'9') && (c!='-') && (c!='_')){
      mes.innerHTML += "Not valid file name. (Use only letters and numbers)<br>";
      endCheck();
      return;
    }
  }
  var path = '../retrieve/checkFilename.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        var message = result.message;
        if (result.status=="fail"){
          mes.innerHTML += message + "<br>";
        }
        else{
          if (result.message == "Filename already exists."){
            mes.innerHTML += "This file name already exists. Give another one.<br>";
          }
        }
        endCheck();
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token + "&filename=" + filename);
}

function checkSensors(){
  console.log("checkSensors");
  var types = [];
  for (var i=1; i<=5; i++){
    var type = document.getElementsByName('sensorType' + i)[0];
    if (type == null || type == undefined){
      console.log("undefined");
      continue;
    }
    else{
      types.push(type.value);
      console.log(type.value);
    }
  }
  var len = types.length;
  var countDS18B20 = 0;
  for (var i=0; i<len; i++){
    if (types[i]=="Temperature (DS18B20)"){
      countDS18B20++;
    }
  }
  if (countDS18B20>1){
    mes.innerHTML += "Multiple temperature DS18B20 sensors are not supported at the moment. Please select only one.<br>";
  }
}

function checkPins(){
  var pins = [];
  for (var i=1; i<=5; i++){
    var pin = document.getElementById('sensorPin' + i);
    if (pin == null || pin == undefined){
      continue;
    }
    else{
      pins.push(pin.value);
    }
  }
  var len = pins.length;
  var same = false;
  for (var i=0; i<len; i++){
    if (pins[i]=="-1"){
      mes.innerHTML += "Select pins for all the sensors. <br>";
      return;
    }
  }
  for (var i=0; i<len; i++){
    for (var j=i; j<len; j++){
      if (i==j) continue;
      if (pins[i]==pins[j]){
        same = true;
        break;
      }
    }
  }
  if (same){
    mes.innerHTML += "You can not use the same pin for two or more sensors. <br>";
  }
}

function endCheck(){
  if (mes.innerHTML==""){
    var sbt = document.getElementById('sbt');
    sbt.innerHTML='<button type="submit" id="submit">Generate</button>';
    document.getElementById('submit').click();
  }
  else{
    mes.scrollIntoView();
  }
}

function checkIfSaved(){
  var allSaved = true;
  for (var i=1; i<=5; i++){
    var save = document.getElementById('save-' + i);
    if (save == null || save == undefined){
      continue;
    }
    else{
      if (save.innerHTML=="Save"){
        allSaved = false;
        break;
      }
    }
  }
  if (!allSaved){
    mes.innerHTML += "Save all the sensors.<br>"
  }
}

function checkInternet(){
  var internet;
  if (document.getElementById('internet1').value!="-1" && document.getElementById('internet1').value!="Select controller"){
    internet = document.getElementById('internet1').value;
  }
  else{
    internet = document.getElementById('internet2').value;
  }
  console.log(internet);
  if (internet == "Adafruit CC3000 Wi-Fi shield"){
    if (document.getElementById('ssid').value==""){
      mes.innerHTML += "Give your ssid (wi-fi name). <br>";
    }
    if (document.getElementById('pass').value==""){
      mes.innerHTML += "Give your wi-fi password. <br>";
    }
  }
}

$("#generate").click(function(){
  var mes = $("#message");
  mes.html('');
  checkTimeInterval();
  checkSensors();
  checkPins();
  checkIfSaved();
  checkInternet();
  checkFilename();
});
