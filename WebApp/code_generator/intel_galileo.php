<?php

$intelgalileo_pins_digital = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13");
$intelgalileo_pins_analog = array("A0", "A1", "A2", "A3", "A4", "A5");

function getIntelGalileoCode($sensorTypes){
  $first = "";
  $setup = "\n\tSerial.begin(9600);\n";
  $loop = "\n";
  $m = 0; $t = 0; $s = 0; $l = 0; $h = 0;

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      $first.= "int motion_pin" . $m . " = <pin". $i . ">;\n";
      $first.= "int motion_value" . $m . ";\n";
      $setup.= "\tpinMode(motion_pin" . $m . ", INPUT); \n";
      $loop.= "\tmotion_value" . $m . " = digitalRead(motion_pin" . $m . "); \n";
      $loop.= "\tSerial.print('Motion = ');\n";
      $loop.="\tSerial.println(motion_value" . $m . "); \n";
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      $first.= "int temperature_pin" . $t . " = <pin". $i . ">;\n";
      $first.= "int temp" . $t . ";\n";
      $first.= "float temperature_value" . $t . ";\n";
      $setup.= "\tpinMode(temperature_pin" . $t . ", INPUT); \n";
      $loop.= "\ttemp" . $t . " = analogRead(temperature_pin" . $t . "); \n";
      $loop.= "\ttemperature_value" . $t . " = temp" . $t . " * 5.0;\n";
      $loop.= "\ttemperature_value" . $t . " /= 1024.0;\n";
      $loop.= "\ttemperature_value" . $t . " = (temperature_value" . $t . " - 0.5) * 100;\n";
      $loop.= "\tSerial.print('Temperature = '); \n";
      $loop.= "\tSerial.print(temperature_value" . $t . "); \n";
      $loop.= "\tSerial.println(' degrees celsius');\n";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      $first.= "int humidity_pin" . $h . " = <pin". $i . ">;\n";
      $first.= "int humidity_value" . $h . ";\n";
      $setup.= "\tpinMode(humidity_pin" . $h . ", INPUT); \n";
      $loop.= "\thumidity_value" . $h . " = analogRead(humidity_pin" . $h . "); \n";
      $loop.= "\t/*\n";
      $loop.= "\t*Uncomment the next line to take the moisture value as percentage.\n";
      $loop.= "\t*Replace the <max_value> with the maximum value of the FC-28 sensor and the <min_value> with the minimum.\n";
      $loop.= "\t*/\n";
      $loop.= "\t//humidity_value" . $h . " = map(output_value,<max_value>,<min_value>,0,100); \n";
      $loop.= "\tSerial.print('Moisture = '); \n";
      $loop.= "\tSerial.println(humidity_value" . $h . "); \n";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      $first.= "int light_pin" . $l . " = <pin". $i . ">;\n";
      $first.= "int light_value" . $l . ";\n";
      $setup.= "\tpinMode(light_pin" . $l . ", INPUT); \n";
      $loop.= "\tlight_value" . $l . " = analogRead(light_pin" . $l . "); \n";
      $loop.= "\tSerial.print('Brightness = '); \n";
      $loop.= "\tSerial.println(light_value" . $l . "); \n";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      $s++;
      $first.= "int sound_pin" . $s . " = <pin". $i . ">;\n";
      $first.= "int sound_value" . $s . ";\n";
      $setup.= "\tpinMode(sound_pin" . $s . ", INPUT); \n";
      $loop.= "\tsound_value" . $s . " = digitalRead(sound_pin" . $s . "); \n";
      $loop.= "\tSerial.print('Sound = '); \n";
      $loop.= "\tSerial.println(sound_value" . $s . "); \n";
    }
    elseif ($sensorTypes[$i]=="Sound (analog)"){
      $s++;
      $first.= "int sound_pin" . $s . " = <pin". $i . ">;\n";
      $first.= "int sound_value" . $s . ";\n";
      $setup.= "\tpinMode(sound_pin" . $s . ", INPUT); \n";
      $loop.= "\tsound_value" . $s . " = analogRead(sound_pin" . $s . "); \n";
      $loop.= "\tSerial.print('Sound = '); \n";
      $loop.= "\tSerial.println(sound_value" . $s . "); \n";
    }
  }
  $loop.= "\tdelay(<time>); \n";
  $content = $first . "\nvoid setup(){" . $setup . "}\n\nvoid loop(){" . $loop . "}";
  return $content;
}

function getIntelGalileoHardware($sensorTypes){
  $hardware="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0;
  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      $motion = "<li> <img src='../code_generator/images/pir.png' height='100' width='120'>" . $m . " PIR motion sensor(s)</li>";
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      $temperature = "<li> <img src='../code_generator/images/tmp36.png' height='100' width='120'>" . $t . " Temperature sesor(s) TMP36</li>";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      $humidity = "<li> <img src='../code_generator/images/soil_moisture.png' height='100' width='120'>" . $h . " Soil moisture sensor(s) FC-28</li>";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      $light = "<li> <img src='../code_generator/images/ldr.png' height='100' width='120'>" . $l . " LDR sensor(s)</li>" .
               "<li> <img src='../code_generator/images/resistor.png' height='100' width='120'>" . $l . " Resistor(s) 100K Ohm.</li>";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      $s++;
      $sound = "<li> <img src='../code_generator/images/sound_sensor.png' height='100' width='120'>" . $s . " Sound sensor(s)</li>";
    }
  }
  $hardware .= "<li> <img src='../code_generator/images/intel_galileo.png' height='100' width='120'> 1 Intel Galileo board + 1 Power Supply + 1 micro USB cable</li>";
  $hardware .= "<li> <img src='../code_generator/images/breadboard.png' height='100' width='120'> 1 Breadboard</li>";
  $hardware .= "<li> <img src='../code_generator/images/jumper_wires.png' height='100' width='120'> Jumper wires</li>";
  $hardware .= $motion . $temperature . $humidity . $light . $sound;
  $hardware .= "</ul>";
  return $hardware;
}

function getIntelGalileoInstructions($sensorTypes){
  $instuctions="<ol>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $sound = "";
  $light = "";

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      if ($motion==""){
        $motion.= "<li>Connect the PIR motion sensor(s) to the Intel Galileo";
        $motion.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $motion.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $motion.= "<li>Connect the data pin of each sensor to the corresponding digital pins of the intel galileo.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      if ($temperature==""){
        $temperature.= "<li>Connect the TMP36 temperature sensor(s) to the Intel Galileo";
        $temperature.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $temperature.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $temperature.= "<li>Connect the data pin of each sensor to the corresponding analog pins of the intel galileo.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      if ($humidity==""){
        $humidity.= "<li>Connect the FC-28 soil moisture sensor(s) to the Intel Galileo";
        $humidity.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $humidity.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $humidity.= "<li>Connect the analog data pin of each sensor to the corresponding analog pins of the intel galileo.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      if ($light==""){
        $light.= "<li>Connect the LDR sensor(s) to the Intel Galileo";
        $light.= "<ul><li>Connect the first leg of each sensor to 5V.</li>";
        $light.= "<li>Connect the one leg of the 10K ohm resistor to the second leg of the sensor(s) and the other leg to ground.</li>";
        $light.= "<li>Connect the second leg of each sensor(s) (with the one leg of the resistor) to the corresponding analog pins of the intel galileo.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      if ($sound==""){
        $sound.= "<li>Connect the sound sensor(s) to the Intel Galileo";
        $sound.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $sound.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $sound.= "<li>Connect the data pin (digital/analog) of each sensor to the corresponding pins of the intel galileo.</li></ul></li>";
      }
    }
  }
  $instuctions .= $motion . $temperature . $humidity . $sound . $light;
  $instuctions .= "<li>Download the created Intel Galileo file to your pc.</li>";
  $instuctions .= "<li>Power up the Intel Galileo using the power supply and wait for few seconds.</li>";
  $instuctions .= "<li>Connect the board to your pc using the micro USB cable (closest to the ethernet connector).</li>";
  $instuctions .= "<li>If this is the first time you connect the Intel Galileo to your pc, go to the next step, otherwise ignore the next step.</li>";
  $instuctions .= "<li>Go to Device Manager and you will see your Intel Galileo (Gadget Serial v2.4) under 'Other devices'. Right click on it and update the driver software." .
  " Choose manually and navigate to the folder of your <a href='https://www.arduino.cc/en/Main/Software' target='_blank'>Arduino IDE</a>, to the subfolder tools.".
  " After updating the driver, the board will appear under 'Ports (COM & LPT)'.</li>";
  $instuctions .= "<li>Open the downloaded file with the <a href='https://www.arduino.cc/en/Main/Software' target='_blank'>Arduino IDE</a>.</li>";
  $instuctions .= "<li>Go to Tool->Board->Boards Manager and install the Intel 586 Boards by Intel (Galileo) version 1.6.2+1.0.</li>";
  $instuctions .= "<li>Select the Intel Galileo board and port from the tools tab.</li>";
  $instuctions .= "<li>Click the upload button to upload the code to the Intel Galileo board.</li>";
  $instuctions .= "</ol>";
  return $instuctions;
}

function getIntelGalileoDiagrams($sensorTypes){
  $diagrams ="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0;

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      if ($m==1){
        $motion .= "<hr><div><h4><b>PIR Motion Sensor</b></h4>";
        $motion .= "<p>This diagram shows the 3 pins of the PIR motion sensor.<br>".
                   "VCC: 3.3-5V DC for power<br>GND: Ground pin<br>Data: Digital output pin</p>";
        $motion .= "<img src='../code_generator/images/pir-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      if ($t==1){
        $temperature .= "<hr><div><h4><b>TMP36 Temperature Sensor</b></h4>";
        $temperature .= "<p>This diagram shows the 3 wires of the TMP36 temperature sensor.<br>".
                        "VCC: 5V DC for power<br>GND: Ground pin<br>Data: Analog output pin</p>";
        $temperature .= "<img src='../code_generator/images/tmp36-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      if ($h==1){
        $humidity .= "<hr><div><h4><b>FC-28 Soil Moisture Sensor</b></h4>";
        $humidity .= "<p>This diagram shows the 4 pins of the FC-28 soil moisture sensor.<br>VCC: 3.3-5V DC for power<br>".
                     "GND: Ground pin<br>Digital output pin<br>Analog output pin</p>";
        $humidity .= "<img src='../code_generator/images/soil-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      if ($l==1){
        $humidity .= "<hr><div><h4><b>Light Dependent Resistor (LDR)</b></h4>";
        $humidity .= "<p>This picture shows the circuit of the LDR with the 10k ohm resistor. The analog input goes to an analog in pin.</p>";
        $humidity .= "<img src='../code_generator/images/ldr-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      $s++;
      if ($s==1){
        $sound .= "<hr><div><h4><b>Sound Sensor</b></h4>";
        $sound .= "<p>This diagram shows the 4 pins of the sound sensor.<br>VCC: 3.3-5V DC for power<br>GND: Ground pin<br>".
                  "DO: Digital output pin<br>A0: Analog output pin</p>";
        $sound .= "<img src='../code_generator/images/sound-ex.png'></div>";
      }
    }
  }
  $diagrams .= "<div><h4><b>Intel Galileo</b></h4>";
  $diagrams .= "<p>This diagram shows the different parts of the Intel Galileo board, like the digital I/O pins, analog in pins, ground pins,".
                " volt power pins, power outlet, ethernet connector, micro SD connector and USB ports.</p>";
  $diagrams .= "<img src='../code_generator/images/galileo-ex.png'></div>";
  $diagrams .= $motion . $temperature . $humidity . $light . $sound;
  $diagrams .= "</ul>";
  return $diagrams;
}

?>
