<?php
$content = isset($_POST["content"]) ? $_POST["content"] : "";
$filename = isset($_POST["filename"]) ? $_POST["filename"] : "noname.txt";
$pins = isset($_POST["pins"]) ? $_POST["pins"] : "";
$sem = false;
for ($i=0; $i<strlen($pins); $i++){
  if ($pins{$i}==';'){
    $sem = true;
    break;
  }
}
if ($sem==true){
  $tokens = explode(";", $pins);
}
else{
  $tokens = array($pins);
}
for ($i=0; $i<sizeof($tokens); $i++){
  $str = "<pin" . $i . ">";
  $content = str_replace($str, $tokens[$i], $content);
}
$time = isset($_POST["time"]) ? $_POST["time"] : "";
$content = str_replace("<time>", $time, $content);
$content = str_replace("<d>", '"', $content);
header('Content-Description: File Transfer');
header('Content-Type: text/plain');
header('Content-Disposition: attachment; filename="' . $filename. '"');
header('Content-Length: '.strlen($content));
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
header('Pragma: public');
echo $content;
?>
