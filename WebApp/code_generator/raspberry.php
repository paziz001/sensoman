<?php

$raspberry_pins_gpio = array("GPIO_2", "GPIO_3", "GPIO_4", "GPIO_5", "GPIO_6", "GPIO_7", "GPIO_8", "GPIO_9",
"GPIO_10", "GPIO_11", "GPIO_12", "GPIO_13", "GPIO_14", "GPIO_15", "GPIO_16", "GPIO_17", "GPIO_18", "GPIO_19", "GPIO_20",
"GPIO_21", "GPIO_22", "GPIO_23", "GPIO_24", "GPIO_25", "GPIO_26", "GPIO_27");

function getRaspberryCode($sensorTypes, $sensorIDs, $internet){
  $importGPIO = "";
  $importW1 = "";
  $imports = "import time\n";
  $first = "";
  $setup = "";
  $functions = "";
  $LDRfunction="";
  $loop = "";
  $m = 0; $t = 0; $s = 0; $l = 0; $h = 0;

  if ($internet!="No internet connection"){
    $imports.= "import requests\n";
    $functions.= "\ndef postMeasurement(sensorID, value):\n";
    $functions.= "\turl = 'http://thesis.in.cs.ucy.ac.cy/sensoman2/upload/post_measurement.php'\n";
    $functions.= "\tdata = {'sensorID': str(sensorID), 'measurement': str(value)}\n";
    $functions.= "\treq = requests.post(url, json = data)\n";
    $functions.= "\tif (req.status_code==200):\n";
    $functions.= "\t\tprint('Measurement uploaded successfully!\\n')\n";
    $functions.= "\telse:\n";
    $functions.= "\t\tprint('Unable to upload the measurement.\\n')\n";
  }

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      if ($importGPIO==""){
        $importGPIO = "import RPi.GPIO as GPIO\n\n";
      }
      $first.= "motion_pin" . $m . " = <pin". $i . ">;\n";
      $setup.= "GPIO.setup(motion_pin". $m .", GPIO.IN)\n";
      $loop.= "\tmotion_value = GPIO.input(motion_pin". $m .")\n";
      $loop.= "\tprint ('Motion = ',motion_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", motion_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      $t++;
      if ($importW1==""){
        $importW1 = "from w1thermsensor import W1ThermSensor\n";
      }
      $first .= "#Connect the temperature sensor to pin GPIO_4;\n";
      $setup .= "sensor = W1ThermSensor()\n";
      $loop .= "\ttemperature_value = sensor.get_temperature()\n";
      $loop .= "\tprint ('Temperature = ',temperature_value,' degrees celsius','\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", temperature_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      if ($importGPIO==""){
        $importGPIO = "import RPi.GPIO as GPIO\n\n";
      }
      $first.= "humidity_pin" . $h . " = <pin". $i . ">;\n";
      $setup.= "GPIO.setup(humidity_pin". $h .", GPIO.IN)\n";
      $loop.= "\thumidity_value = GPIO.input(humidity_pin". $h .")\n";
      $loop.= "\tprint ('Humidity = ',humidity_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", humidity_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      if ($importGPIO==""){
        $importGPIO = "import RPi.GPIO as GPIO\n\n";
      }
      $first.= "light_pin" . $l . " = <pin". $i . ">;\n";
      if ($LDRfunction==""){
        $LDRfunction.= "\ndef getLightValue(pin):\n".
                  "\tcount = 0\n".
                  "\tGPIO.setup(pin, GPIO.OUT)\n".
                  "\tGPIO.output(pin, GPIO.LOW)\n".
                  "\ttime.sleep(0.1)\n".
                  "\tGPIO.setup(pin_to_circuit, GPIO.IN)\n".
                  "\twhile (GPIO.input(pin_to_circuit) == GPIO.LOW):\n".
                  "\t\tcount += 1\n".
                  "\treturn count;\n";
      }
      $loop.= "\tlight_value = getLightValue(light_pin". $l .")\n";
      $loop.= "\tprint ('Light = ',light_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", light_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      $s++;
      if ($importGPIO==""){
        $importGPIO = "import RPi.GPIO as GPIO\n\n";
      }
      $first.= "sound_pin" . $s . " = <pin". $i . ">;\n";
      $setup.= "GPIO.setup(sound_pin". $s .", GPIO.IN)\n";
      $loop.= "\tsound_value = GPIO.input(sound_pin". $s .")\n";
      $loop.= "\tprint ('Sound = ',sound_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", sound_value)\n";
      }
      $loop.= "\n";
    }
  }
  $loop.= "\ttime.sleep(<time>)\n";

  $imports.= $importGPIO . $importW1;
  if ($importGPIO!=""){
    $setup = "GPIO.setmode(GPIO.BCM)\nGPIO.setwarnings(False)\n" . $setup;
    $setup .= $functions;
  }

  $content = $imports . $first . "\n" . $setup . $LDRfunction . "\nwhile True:\n" . $loop;
  return $content;
}

function getRaspberryHardware($sensorTypes, $internet){
  $hardware="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0;
  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      $motion = "<li> <img src='../code_generator/images/pir.png' height='100' width='120'>" . $m . " PIR motion sensor(s)</li>";
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      $t++;
      $temperature = "<li> <img src='../code_generator/images/ds18b20.png' height='100' width='120'>" . $t . " Waterproof temperature sesor(s) DS18B20</li>";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      $humidity = "<li> <img src='../code_generator/images/soil_moisture.png' height='100' width='120'>" . $h . " Soil moisture sensor(s) FC-28</li>";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      $light = "<li> <img src='../code_generator/images/ldr.png' height='100' width='120'>" . $l . " LDR sensor(s)</li>" .
               "<li> <img src='../code_generator/images/capacitor.png' height='100' width='120'>" . $l . " Capacitor(s) 1uF.</li>";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      $s++;
      $sound = "<li> <img src='../code_generator/images/sound_sensor.png' height='100' width='120'>" . $s . " Sound sensor(s)</li>";
    }
  }
  $hardware .= "<li> <img src='../code_generator/images/raspberry_pi.png' height='100' width='120'> 1 Raspberry Pi board + Power supply</li>";
  if ($internet=="Wi-Fi adapter"){
    $hardware .= "<li> <img src='../code_generator/images/wifi-adapter.png' height='100' width='120'> 1 Wi-Fi adapter</li>";
  }
  if ($internet=="Ethernet cable"){
      $hardware .= "<li> <img src='../code_generator/images/ethernet_cable.png' height='100' width='120'> 1 Ethernet Cable</li>";
  }
  $hardware .= "<li> <img src='../code_generator/images/sd.png' height='100' width='120'> 1 SD card (8+ GB)</li>";
  $hardware .= "<li> <img src='../code_generator/images/breadboard.png' height='100' width='120'> 1 Breadboard</li>";
  $hardware .= "<li> <img src='../code_generator/images/jumper_wires.png' height='100' width='120'> Jumper wires</li>";
  $hardware .= "<li> <img src='../code_generator/images/extension_cable.png' height='100' width='120'> 1 GPIO Extension calbe (optional)</li>";
  $hardware .= $motion . $temperature . $humidity . $light . $sound;
  $hardware .= "</ul>";
  return $hardware;
}

function getRaspberryInstructions($sensorTypes, $internet){
  $instuctions="<ol>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $sound = "";
  $light = "";

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      if ($motion==""){
        $motion.= "<li>Connect the PIR motion sensor(s) to the Raspberry Pi Board";
        $motion.= "<ul><li>Connect the VCC pin of each sensor to 5V (on board pins: 2,4).</li>";
        $motion.= "<li>Connect the ground pin of each sensor to ground (on board pins: 6,9,14,20,25,30,34,39).</li>";
        $motion.= "<li>Connect the data pin of each sensor to the corresponding GPIO pins of the raspberry pi.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      if ($temperature==""){
        $temperature.= "<li>Download the <a href='https://github.com/timofurrer/w1thermsensor' target='_blank'>w1thermsensor library</a> (sudo pip3 install w1thermsensor).</li>";
        $temperature.= "<li>Enable the 1-Wire interface on your raspberry pi.</li>";
        $temperature.= "<li>Connect the DS18B20 waterproof temperature sensor to the Raspberry Pi Board.";
        $temperature.= "<ul><li>Connect the VCC pin of the sensor to 5V (on board pins: 2,4).</li>";
        $temperature.= "<li>Connect the ground pin of each sensor to ground (on board pins: 6,9,14,20,25,30,34,39).</li>";
        $temperature.= "<li>Connect the data pin of the sensor to the GPIO_4 pin.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      if ($humidity==""){
        $humidity.= "<li>Connect the FC-28 soil moisture sensor(s) to the Raspberry Pi Board";
        $humidity.= "<ul><li>Connect the VCC pin of each sensor to 5V (on board pins: 2,4).</li>";
        $humidity.= "<li>Connect the ground pin of each sensor to ground (on board pins: 6,9,14,20,25,30,34,39).</li>";
        $humidity.= "<li>Connect the digital data pin of each sensor to the corresponding GPIO pins of the raspberry pi.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      if ($light==""){
        $light.= "<li>Connect the LDR sensor(s) to the Raspberry Pi Board";
        $light.= "<ul><li>Connect the first leg of each sensor to 3.3V (on board pins: 1, 17).</li>";
        $light.= "<li>Connect the one leg of the capacitor to the second leg of the sensor(s) and the other leg to ground (on board pins: 6,9,14,20,25,30,34,39).</li>";
        $light.= "<li>Connect the second leg of each sensor(s) (with the one leg of the capacitor) to the corresponding GPIO pins of the raspberry pi.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      if ($sound==""){
        $sound.= "<li>Connect the sound sensor(s) to the Raspberry Pi Board";
        $sound.= "<ul><li>Connect the VCC pin of each sensor to 5V (on board pins: 2,4).</li>";
        $sound.= "<li>Connect the ground pin of each sensor to ground (on board pins: 6,9,14,20,25,30,34,39).</li>";
        $sound.= "<li>Connect the digital data pin of each sensor to the corresponding GPIO pins of the raspberry pi.</li></ul></li>";
      }
    }
  }
  $instuctions .= $motion . $temperature . $humidity . $sound . $light;
  $instuctions .= "<li>Download the created Raspberry Pi file to your pc.</li>";
  $instuctions .= "<li>Install an image of an operating system to your SD card. (More info <a href='https://www.raspberrypi.org/documentation/installation/installing-images/' target='_blank'>here</a>)</li>";
  $instuctions .= "<li>Insert the SD card to Raspberry Pi.</li>";
  $instuctions .= "<li>Power up the board using the power supply.</li>";
  if ($internet=="Wi-Fi adapter"){
    $instuctions .= "<li>Connect the board to the internet using the wifi adapter.</li>";
  }
  if ($internet=="Ethernet cable"){
    $instuctions .= "<li>Connect the board to your router using the ethernet cable.</li>";
  }
  $instuctions .= "<li>Connect to your Raspberry Pi board using <a href='https://www.raspberrypi.org/documentation/remote-access/ssh/' target='_blank'>SSH</a>. (default username: pi , password: raspberry)</li>";
  $instuctions .= "<li>Install the <a href='https://www.raspberrypi.org/documentation/linux/software/python.md' target='_blank'>Python pachages</a> to the board.</li>";
  $instuctions .= "<li>Create a new python file (.py) to your board and copy the contents of the downloaded file.</li>";
  $instuctions .= "<li>Save and run the script.</li>";
  $instuctions .= "</ol>";
  return $instuctions;
}

function getRaspberryDiagrams($sensorTypes){
  $diagrams ="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0;

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      if ($m==1){
        $motion .= "<hr><div><h4><b>PIR Motion Sensor</b></h4>";
        $motion .= "<p>This diagram shows the 3 pins of the PIR motion sensor.<br>".
                   "VCC: 3.3-5V DC for power<br>GND: Ground pin<br>Data: Digital output pin</p>";
        $motion .= "<img src='../code_generator/images/pir-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      $t++;
      if ($t==1){
        $temperature .= "<hr><div><h4><b>DS18B20 Waterproof Temperature Sensor</b></h4>";
        $temperature .= "<p>This diagram shows the 3 wires of the DS18B20 waterproof temperature sensor.<br>".
                        "VCC: 5V DC for power<br>GND: Ground wire<br>Data: Digital output wire</p>";
        $temperature .= "<img src='../code_generator/images/ds18b20-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      if ($h==1){
        $humidity .= "<hr><div><h4><b>FC-28 Soil Moisture Sensor</b></h4>";
        $humidity .= "<p>This diagram shows the 4 pins of the FC-28 soil moisture sensor.<br>VCC: 3.3-5V DC for power<br>".
                     "GND: Ground pin<br>Digital output pin<br>Analog output pin</p>";
        $humidity .= "<img src='../code_generator/images/soil-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      if ($l==1){
        $humidity .= "<hr><div><h4><b>Light Dependent Resistor (LDR)</b></h4>";
        $humidity .= "<p>This picture shows the circuit of the LDR with the 1uF capacitor. The GPIO goes to a GPIO pin of the raspberry pi.</p>";
        $humidity .= "<img src='../code_generator/images/ldr2-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      $s++;
      if ($s==1){
        $sound .= "<hr><div><h4><b>Sound Sensor</b></h4>";
        $sound .= "<p>This diagram shows the 4 pins of the sound sensor.<br>VCC: 3.3-5V DC for power<br>GND: Ground pin<br>".
                  "DO: Digital output pin<br>A0: Analog output pin</p>";
        $sound .= "<img src='../code_generator/images/sound-ex.png'></div>";
      }
    }
  }
  $diagrams .= "<div><h4><b>Raspberry Pi 2 Model B</b></h4>";
  $diagrams .= "<p>The diagrams show the different parts and pins of the Raspberry Pi 2 Model B board. Use the numbers of the GPIO pins to build your circuit.</p>";
  $diagrams .= "<img src='../code_generator/images/raspberry-ex.png'>";
  $diagrams .= "<img src='../code_generator/images/raspberry_pinout.png'></div>";
  $diagrams .= $motion . $temperature . $humidity . $light . $sound;
  $diagrams .= "</ul>";
  return $diagrams;
}

?>
