<?php
require "../code_generator/arduino.php";
require "../code_generator/raspberry.php";
require "../code_generator/beaglebone_black.php";
require "../code_generator/intel_galileo.php";

$controller = isset($_POST["controller"]) ? $_POST["controller"] : die();
$sensors = isset($_POST["sensors"]) ? $_POST["sensors"] : die();
$ids = isset($_POST["ids"]) ? $_POST["ids"] : die();
$internet = isset($_POST["internet"]) ? $_POST["internet"] : die();
$exists = false;
for($i=0; $i<strlen($sensors); $i++){
  if ($sensors{$i}==';'){
    $exists = true;
    break;
  }
}
if ($exists==true){
  $sensors = explode(";", $sensors);
}
else{
  $sensors = array($sensors);
}

if ($controller=="Arduino"){
  $instructions = getArduinoInstructions($sensors, $internet);
  $hardware = getArduinoHardware($sensors, $internet);
  $code = getArduinoCode($sensors, $ids, $internet);
}
else if ($controller == "Raspberry Pi"){
  $instructions = getRaspberryInstructions($sensors, $internet);
  $hardware = getRaspberryHardware($sensors, $internet);
  $code = getRaspberryCode($sensors, $ids, $internet);
}
else if ($controller == "Beaglebone Black"){
  $instructions = getBeagleboneBlackInstructions($sensors, $internet);
  $hardware = getBeagleboneBlackHardware($sensors, $internet);
  $code = getBeagleboneBlackCode($sensors, $ids, $internet);
}
else if ($controller == "Intel Galileo"){
  $instructions = getIntelGalileoInstructions($sensors);
  $hardware = getIntelGalileoHardware($sensors);
  $code = getIntelGalileoCode($sensors);
}

$response=array(
    "code" => $code,
    "instructions" => $instructions,
    "hardware" => $hardware
);

echo json_encode($response);

?>
