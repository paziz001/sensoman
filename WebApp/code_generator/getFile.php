<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SensoMan - File">
    <meta name="author" content="Evangelos Aristodemou">

    <title>SensoMan - File</title>
    <link rel="shortcut icon" href="../images/infa_sensor.png">

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- sweet alert CSS -->
    <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../code_generator/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
	<!-- font-awesome 4.7.0-->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

	<link rel="import" href="sendInvitation.html">

  <!-- sweet alert-->
  <script src="../bower_components/sweetalert/dist/sweetalert.min.js"></script>

	<script src="../H5CM/trunk/assets/js/googlePlacesScript.js"  type="text/javascript"></script>
  <script src="../code_generator/getFile.js" type="text/javascript"></script>

</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.html"><span id="logo-dashboard">SensoMan</span></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span id="username"></span>
                    <i title ="Logout" class="fa fa fa-user-circle fa-fw" style="width: auto;"></i>
                </a>
                <ul class="dropdown-menu dropdown-user" id="userMenu">
                    <li><a style="cursor:pointer" id="settings" href="../pages/settings.html"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
                    <li><a style="cursor:pointer" id="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="../pages/index.html" id="dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="../pages/code_generator.php" id="codeGenerator"><i class="fa fa-file-code-o" style="font-size:17px"></i> Code Generator</a>
                    </li>
                    <li>
                        <a href="../pages/myFiles.php" id="myfiles"><i class="fa fa-file-text-o fa-fw" style="font-size:17px"></i> My files</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">

      <?php

            require "arduino.php";
            require "raspberry.php";
            require "beaglebone_black.php";
            require "intel_galileo.php";

            $error = "<h4>Create a file using the <a href='../pages/code_generator.php'>Code Generator</a>.</h4>";

            $protocol = isset($_POST["protocol"]) ? $_POST["protocol"] : NULL;
            $ssid = isset($_POST["ssid"]) ? $_POST["ssid"] : NULL;
            $pass = isset($_POST["pass"]) ? $_POST["pass"] : NULL;

            $controllerID = $_POST["controllerID"]!="" ? $_POST["controllerID"] : $_POST["excontrollerID"];
            $controllerName = "";
            if ($_POST["controllerName"]!=""){
              $controllerName = $_POST["controllerName"];
            }
            else{
              $tokens = explode(";",$_POST["excontrollerName"]);
              $controllerName = $tokens[1];
            }
            $controllerType = $_POST["controllerType"]!="-1" ? $_POST["controllerType"] : $_POST["excontrollerType"];
            $timeInterval = $_POST["timeInterval1"]!="" ? $_POST["timeInterval1"] : $_POST["timeInterval2"];
            $internet = $_POST["internet1"]!="-1" ? $_POST["internet1"] : $_POST["internet2"];
            $ssid = isset($_POST["ssid"]) ? $_POST["ssid"] : null;
            $pass = isset($_POST["pass"]) ? $_POST["pass"] : null;

            $filename = isset ($_POST["fileName"]) ? $_POST["fileName"] : die($error);
            $sensorName1 = isset($_POST["sensorName1"]) ? $_POST["sensorName1"] : "*";
            $sensorType1 = isset($_POST["sensorType1"]) ? $_POST["sensorType1"] : "notset";
            $sensorPin1 = isset($_POST["sensorPin1"]) ? $_POST["sensorPin1"] : "*";
            $sensorID1 = isset($_POST["sensorID1"]) ? $_POST["sensorID1"] : "*";
            $sensorName2 = isset($_POST["sensorName2"]) ? $_POST["sensorName2"] : "*";
            $sensorType2 = isset($_POST["sensorType2"]) ? $_POST["sensorType2"] : "*";
            $sensorPin2 = isset($_POST["sensorPin2"]) ? $_POST["sensorPin2"] : "*";
            $sensorID2 = isset($_POST["sensorID2"]) ? $_POST["sensorID2"] : "*";
            $sensorName3 = isset($_POST["sensorName3"]) ? $_POST["sensorName3"] : "*";
            $sensorType3 = isset($_POST["sensorType3"]) ? $_POST["sensorType3"] : "*";
            $sensorPin3 = isset($_POST["sensorPin3"]) ? $_POST["sensorPin3"] : "*";
            $sensorID3 = isset($_POST["sensorID3"]) ? $_POST["sensorID3"] : "*";
            $sensorName4 = isset($_POST["sensorName4"]) ? $_POST["sensorName4"] : "*";
            $sensorType4 = isset($_POST["sensorType4"]) ? $_POST["sensorType4"] : "*";
            $sensorPin4 = isset($_POST["sensorPin4"]) ? $_POST["sensorPin4"] : "*";
            $sensorID4 = isset($_POST["sensorID4"]) ? $_POST["sensorID4"] : "*";
            $sensorName5 = isset($_POST["sensorName5"]) ? $_POST["sensorName5"] : "*";
            $sensorType5 = isset($_POST["sensorType5"]) ? $_POST["sensorType5"] : "*";
            $sensorPin5 = isset($_POST["sensorPin5"]) ? $_POST["sensorPin5"] : "*";
            $sensorID5 = isset($_POST["sensorID5"]) ? $_POST["sensorID5"] : "*";

            $sensorNames = array();
            array_push($sensorNames, $sensorName1, $sensorName2, $sensorName3, $sensorName4, $sensorName5);
            $sensorTypes = array();
            array_push($sensorTypes, $sensorType1, $sensorType2, $sensorType3, $sensorType4, $sensorType5);
            $sensorPins = array();
            array_push($sensorPins, $sensorPin1, $sensorPin2, $sensorPin3, $sensorPin4, $sensorPin5);
            $sensorIDs = array();
            array_push($sensorIDs, $sensorID1, $sensorID2, $sensorID3, $sensorID4, $sensorID5);

            $sensors = "";
            $pins = "";
            for ($i=0; $i<5; $i++){
              if ($sensorIDs[$i]!="*"){
                $sensors .= ";" . $sensorIDs[$i];
                $pins .= ";" . $sensorPins[$i];
              }
            }
            $sensors = substr($sensors,1);
            $pins = substr($pins,1);

            for ($i=0; $i<sizeof($sensorTypes); $i++){
              if ($sensorTypes[$i]{0}>='1' && $sensorTypes[$i]{0}<='5'){
                $temp = explode("-",$sensorTypes[$i]);
                $sensorTypes[$i] = $temp[1];
              }
            }

            $fullinternet = $internet;
            if ($controllerType == "Arduino"){
              if ($internet=="Adafruit CC3000 Wi-Fi shield"){
                $fullinternet.= ";" . $ssid . ";" . $pass;
              }
              $img="../code_generator/images/file-arduino.png";
              $alt="Arduino File";
              $filename .= ".ino";
              $content = getArduinoCode($sensorTypes, $sensorIDs, $fullinternet);
              $hardware = getArduinoHardware($sensorTypes, $fullinternet);
              $instructions = getArduinoInstructions($sensorTypes, $fullinternet);
              $diagrams = getArduinoDiagrams($sensorTypes);
            }
            else if ($controllerType == "Raspberry Pi"){
              $img="../code_generator/images/file-raspberry.png";
              $alt="Raspberry Pi File";
              $filename .= ".py";
              $content = getRaspberryCode($sensorTypes, $sensorIDs, $internet);
              $hardware = getRaspberryHardware($sensorTypes, $internet);
              $instructions = getRaspberryInstructions($sensorTypes, $internet);
              $diagrams = getRaspberryDiagrams($sensorTypes);
            }
            else if ($controllerType == "Beaglebone Black"){
              $img="../code_generator/images/file-beaglebone.png";
              $alt="Beaglebone Black File";
              $filename .= ".py";
              $content = getBeagleboneBlackCode($sensorTypes, $sensorIDs, $internet);
              $hardware = getBeagleboneBlackHardware($sensorTypes, $internet);
              $instructions = getBeagleboneBlackInstructions($sensorTypes, $internet);
              $diagrams = getBeagleboneBlackDiagrams($sensorTypes);
            }
            else if ($controllerType == "Intel Galileo"){
              $img="../code_generator/images/file-intel.png";
              $alt="Intel Galileo File";
              $filename .= ".ino";
              $content = getIntelGalileoCode($sensorTypes);
              $hardware = getIntelGalileoHardware($sensorTypes);
              $instructions = getIntelGalileoInstructions($sensorTypes);
              $diagrams = getIntelGalileoDiagrams($sensorTypes);
            }
      ?>

      <ul class="nav nav-pills">
          <li class="active"><a data-toggle="pill" href="#menu1">File</a></li>
          <li><a data-toggle="pill" href="#menu2">Hardware</a></li>
          <li><a data-toggle="pill" href="#menu3">Instuctions</a></li>
          <li><a data-toggle="pill" href="#menu4">Diagrams</a></li>
        </ul>

        <div class="tab-content">
          <div id="menu1" class="tab-pane fade in active">
            <fieldset class="scheduler-border">
              <legend class="scheduler-border">File Created!</legend>
              <label>File Name: </label><span><?php echo " " . $filename ?><span><br>
              <form method="POST" action="createFile.php">
                <input type="hidden" name="content" value="<?php echo $content ?>">
                <input type="hidden" name="filename" value="<?php echo $filename ?>">
                <input type="hidden" name="pins" id="downloadpins">
                <input type="hidden" name="time" id="downloadtime">
                <button type="submit" class="btn btn-primary download">
                  <img src=<?php echo $img ?> alt=<?php echo $alt ?> height=65 width=55 style="right: 90pt;">
                  <div style="clear:both;">
                    <span class="glyphicon glyphicon-download-alt"></span> Download
                  </div>
                </button>
              </form>
            </fieldset>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editFile" id="editbtn"
            data-whatever="<?php echo $filename; ?>"><span class="glyphicon glyphicon-edit"></span> Edit File</button>
            <fieldset class="scheduler-border">
              <legend class="scheduler-border">Micro-controller</legend>
                <table class="table table-sm" style="width: auto;">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Type</th>
                      <th scope="col">Internet Connection</th>
                      <th scope="col">Time Interval</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?php echo $controllerName; ?></td>
                      <td><?php echo $controllerType; ?></td>
                      <td><?php echo $internet; ?></td>
                      <td id="tabletime"><?php echo $timeInterval; ?></td>
                    </tr>
                  </tbody>
                </table>
            </fieldset>

            <fieldset class="scheduler-border">
              <legend class="scheduler-border">Sensors</legend>
                <table class="table table-sm" style="width: auto;">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Name</th>
                      <th scope="col">Type</th>
                      <th scope="col">Pin</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    $i=0;
                    $count = 0;
                    while($i<5){
                      if ($sensorNames[$i]!="*"){
                  ?>
                    <tr>
                      <th scope="row"><?php echo ($count+1); ?></th>
                      <td><?php echo $sensorNames[$i]; ?></td>
                      <td><?php echo $sensorTypes[$i]; ?></td>
                      <td id="pin<?php echo $count; ?>"><?php echo $sensorPins[$i]; ?></td>
                    </tr>
                  <?php $count++;} $i++; } ?>
                  </tbody>
                </table>
            </fieldset>
          </div>
          <div id="menu2" class="tab-pane fade">
            <fieldset class="scheduler-border">
              <legend class="scheduler-border">Hardware</legend>
              <?php echo $hardware; ?>
            </fieldset>
          </div>
          <div id="menu3" class="tab-pane fade">
            <fieldset class="scheduler-border">
              <legend class="scheduler-border">Instuctions</legend>
              <?php echo $instructions; ?>
            </fieldset>
          </div>
          <div id="menu4" class="tab-pane fade">
            <fieldset class="scheduler-border">
              <legend class="scheduler-border">Helpful Diagrams</legend>
              <?php echo $diagrams; ?>
            </fieldset>
          </div>
        </div>

<div class="modal fade" id="editFile" tabindex="-1" role="dialog" aria-labelledby="title">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title">Edit File</h4>
      </div>
      <div class="modal-body">
        <form>
          <div id="modal-mes" style="color: red; font-weight: bold; font-style: italic"></div>
          <br>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Time Interval</label>
            <input type="text" class="form-control time-interval" id="modal-time">
            <input type="hidden" id="modal-controller" value="<?php echo $controllerType; ?>">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Sensors</label>
            <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Type</th>
                <th scope="col">Pin</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $numPins=1;
                $sem = false;
                for ($s=0; $s<strlen($pins); $s++){
                  if ($pins{$s}==';'){
                    $sem=true;
                    break;
                  }
                }
                if ($sem){
                  $numPins = sizeof(explode(";", $pins));
                }
                $i=0;
                $count=0;
                while($i<5){
                  if ($sensorNames[$i]!="*"){
                    $droplist = "";
                    if ($controllerType=="Arduino"){
                      if ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Temperature (DS18B20)" || $sensorTypes[$i]=="Motion (PIR)"){
                        for ($k=0; $k<sizeof($arduino_pins_digital); $k++){
                          $droplist.='<option value="' . $arduino_pins_digital[$k] . '">' . $arduino_pins_digital[$k] . '</option>';
                        }
                      }
                      else{
                        for ($k=0; $k<sizeof($arduino_pins_analog); $k++){
                          $droplist.='<option value="' . $arduino_pins_analog[$k] . '">' . $arduino_pins_analog[$k] . '</option>';
                        }
                      }
                    }
                    else if ($controllerType=="Raspberry Pi"){
                      for ($k=0; $k<sizeof($raspberry_pins_gpio); $k++){
                        $droplist.='<option value="' . $raspberry_pins_gpio[$k] . '">' . $raspberry_pins_gpio[$k] . '</option>';
                      }
                    }
                    else if ($controllerType=="Beaglebone Black"){
                      if ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Temperature (DS18B20)" || $sensorTypes[$i]=="Motion (PIR)"){
                        for ($k=0; $k<sizeof($beaglebone_pins_gpio); $k++){
                          $droplist.='<option value="' . $beaglebone_pins_gpio[$k] . '">' . $beaglebone_pins_gpio[$k] . '</option>';
                        }
                      }
                      else{
                        for ($k=0; $k<sizeof($beaglebone_pins_analogin); $k++){
                          $droplist.='<option value="' . $beaglebone_pins_analogin[$k] . '">' . $beaglebone_pins_analogin[$k] . '</option>';
                        }
                      }
                    }
                    else if ($controllerType=="Intel Galileo"){
                      if ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Motion (PIR)"){
                        for ($k=0; $k<sizeof($intelgalileo_pins_digital); $k++){
                          $droplist.='<option value="' . $intelgalileo_pins_digital[$k] . '">' . $intelgalileo_pins_digital[$k] . '</option>';
                        }
                      }
                      else{
                        for ($k=0; $k<sizeof($intelgalileo_pins_analog); $k++){
                          $droplist.='<option value="' . $intelgalileo_pins_analog[$k] . '">' . $intelgalileo_pins_analog[$k] . '</option>';
                        }
                      }
                    }
               ?>
              <tr>
                <th scope="row"><?php echo ($count+1); ?></th>
                <td><?php echo $sensorNames[$i]; ?></td>
                <td><?php echo $sensorTypes[$i]; ?></td>
                <td>
                  <select class="form-control list<?php echo $count; ?>" id="modal-pin<?php echo $count; ?>">
                  <?php echo $droplist; ?>
                  </select>
                </td>
              </tr>
            <?php $count++;}$i++; } ?>
            </tbody>
          </table>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="editFile(<?php echo $numPins; ?>)">Save</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>


<script>
saveFile("<?php echo $filename ?>", "<?php echo $controllerID ?>", "<?php echo $sensors ?>",
                 "<?php echo $pins ?>" , "<?php echo $timeInterval ?>", "<?php echo $fullinternet ?>");
</script>


<script src="../bower_components/markerclustererplus/dist/markerclusterer.min.js"></script>
<!-- jQuery -->
<script src="../bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>



<!-- Datatables -->
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<!-- Index Javascript (Map, Call MyRestfulService - H5CM) -->
<script src="../H5CM/trunk/assets/js/Q.js" type="text/javascript"></script>
<!--vis graph -->
<script type="text/javascript" src="../graph_resources/vis-4.18.1/dist/vis.js"></script>
<script src="../H5CM/trunk/dashboard.js"></script>
<script src="../H5CM/trunk/context/ContextManagerSingleton.js" type="text/javascript"></script>
<script src="../H5CM/trunk/assets/js/sha-256.js" type="text/javascript"></script>
<script src="../H5CM/trunk/myContextAwareApplication.js"></script>

<script>
var user = $("#username");
var scriptsURL = new Array(
    "../H5CM/trunk/modules/sensors/SensoMan.js"
);
loadDependencies(scriptsURL);
user.html(username);
$('#editFile').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var filename = button.data('whatever');
  var time = newTime;
  var pins = nPins.split(";");
  var modal = $(this);
  $("#modal-mes").html("");
  modal.find('.modal-title').text('Edit file "' + filename + '"');
  modal.find('.time-interval').val(time);
  for (var i=0; i<pins.length; i++){
    modal.find('.list' + i).val(pins[i]);
  }
})
</script>



</body>

</html>
