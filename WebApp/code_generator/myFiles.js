/*
* Evangelos Aristodemou
*/

var scriptsURL = new Array(
    "../H5CM/trunk/modules/sensors/SensoMan.js"
);
loadDependencies(scriptsURL);
var username = localStorage.getItem("username");
var token = localStorage.getItem("token");

var arduino_pins = {
  digital: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
  analog: ["A0", "A1", "A2", "A3", "A4", "A5"],
  pwm: ["3", "5", "6", "9", "10", "11"]
};
var raspberry_pins = {
  gpio: ["GPIO_2", "GPIO_3", "GPIO_4", "GPIO_5", "GPIO_6", "GPIO_7", "GPIO_8", "GPIO_9",
  "GPIO_10", "GPIO_11", "GPIO_12", "GPIO_13", "GPIO_14", "GPIO_15", "GPIO_16", "GPIO_17", "GPIO_18", "GPIO_19", "GPIO_20",
  "GPIO_21", "GPIO_22", "GPIO_23", "GPIO_24", "GPIO_25", "GPIO_26", "GPIO_27"]
};
var beaglebone_pins = {
  gpio: ["P8_7", "P8_8", "P8_9", "P8_10", "P8_11", "P8_12", "P8_14", "P8_15", "P8_16", "P8_17", "P8_18", "P8_26",
        "P9_11", "P9_12", "P9_13", "P9_15", "P9_17", "P9_18", "P9_23", "P9_24", "P9_25", "P9_26", "P9_27", "P9_30", "P9_41"],
  analogin: ["P9_33", "P9_35", "P9_36", "P9_37", "P9_38", "P9_39", "P9_40"]
};
var intelgalileo_pins = {
  digital: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
  analog: ["A0", "A1", "A2", "A3", "A4", "A5"],
  pwm: ["3", "5", "6", "9", "10", "11"]
};

function checkToken(s){
  if (s == "token expired") {
    swal({
            title: "Session has expired",
            text: "Please connect again to gain access.",
            type: "info",
            showCancelButton: false,
            confirmButtonText: "OK",
            closeOnConfirm: false
        },
        function(){
            localStorage.removeItem("token");
            localStorage.removeItem("current");
            localStorage.removeItem("type");
            localStorage.removeItem("username");
            localStorage.removeItem("email");
            localStorage.removeItem("userlat");
            localStorage.removeItem("userlng");
            window.location.href = "login.html";
            return false;
        });
  }
  return true;
}

function clicked(p){
  var arrow = document.getElementById('arrow' + p);
  if (arrow.innerHTML == '<i class="fa fa-plus-square"></i>'){
    arrow.innerHTML = '<i class="fa fa-minus-square"></i>';
  }
  else{
    arrow.innerHTML = '<i class="fa fa-plus-square"></i>';
  }
}

function deleteFile(p){
  var tokens = p.id.split("-");
  var id1 = tokens[1];
  var id2 = tokens[2];
  swal({
    title: "Delete this file?",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Delete",
    confirmButtonColor: '#ff0000',
    closeOnConfirm: true
  },
  function() {
      var path = '../delete/delete_file.php';
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var result = JSON.parse(this.responseText);
          if (checkToken(result.message)){
            var row = document.getElementById("visiblerow" + id1);
            row.parentNode.removeChild(row);
            var row2 = document.getElementById("hiddenrow" + id1);
            row2.parentNode.removeChild(row2);
          }
        }
      };
      xhttp.open("POST", path, true);
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhttp.send("token=" + token + "&id=" + id2);
  });
}

function readFiles(){
  var path = '../retrieve/filesOfUser.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        var table = document.getElementById('filestable');
        var files = result.files;
        for (var i=0; i<files.length; i++){
          var sensors = "";
          var sensorNames = "";
          var sensorTypes = "";
          var sensorIDs = "";
          var tokens = files[i].pins.split(";");
          for (var k=0; k<files[i].sensors.length; k++){
            sensors += '<tr>\
              <td>'+ files[i].sensors[k].sensorName +'</td>\
              <td>'+ files[i].sensors[k].sensorType +'</td>\
              <td id="pin'+ i + '-' + k +'">'+ tokens[k] + '</td>\
            </tr>';
            sensorNames += files[i].sensors[k].sensorName + ";";
            sensorTypes += files[i].sensors[k].sensorType + ";";
            sensorIDs += files[i].sensors[k].sensorID + ";";
          }
          sensorNames = sensorNames.substring(0, sensorNames.length - 1);
          sensorTypes = sensorTypes.substring(0, sensorTypes.length - 1);
          sensorIDs = sensorIDs.substring(0, sensorIDs.length - 1);
          var image = "";
          var alt ="";
          var sem = false;
          for (var w=0; w<files[i].internet.length; w++){
            if (files[i].internet.charAt(w)==';'){
              sem=true;
              break;
            }
          }
          var splits = [];
          if (sem){
            splits = files[i].internet.split(";");
          }
          else{
            splits.push(files[i].internet);
          }
          var newinternet = splits[0];
          if (files[i].controllerType=="Arduino"){
            image = "file-arduino.png";
            alt = "Arduino File";
          }
          else if (files[i].controllerType=="Raspberry Pi"){
            image = "file-raspberry.png";
            alt = "Raspberry Pi File";
          }
          else if (files[i].controllerType=="Beaglebone Black"){
            image = "file-beaglebone.png";
            alt = "BeagleBone Black File";
          }
          else if (files[i].controllerType=="Intel Galileo"){
            image = "file-intel.png";
            alt = "Intel Galileo File";
          }
          table.innerHTML+='\
          <tr class="accordion-toggle" data-target="#demo'+ i +'" data-toggle="collapse" onclick="clicked('+ i +')" id="visiblerow' + i + '">\
              <td><img src="../code_generator/images/'+ image +'" alt="'+ alt +'" height=40 width=33></td>\
              <td>'+ files[i].fileName +'</td>\
              <td>'+ files[i].controllerName +'</td>\
              <td>'+ files[i].controllerType +'</td>\
              <td>'+ files[i].sensors.length +'</td>\
              <td id="arrow'+ i +'"><i class="fa fa-plus-square"></i></td>\
          </tr>\
          <tr id="hiddenrow' + i + '">\
              <td class="hiddenRow" colspan="12">\
                <div class="accordian-body collapse" id="demo'+ i +'">\
                  <table style="width:55%; margin: 10pt auto;">\
                    <tr>\
                      <th>Time interval:</th>\
                      <td id="time'+ i +'">'+ files[i].timeInterval +' seconds</td>\
                    </tr>\
                    <tr>\
                      <th>Internet Connection:</th>\
                      <td>'+ newinternet +'</td>\
                    </tr>\
                    <tr>\
                      <th>Sensors:</th>\
                      <td>\
                        <table style="width:130%; margin: 10pt auto;">\
                          <tr>\
                            <th>Name</th>\
                            <th>Type</th>\
                            <th>Pin</th>\
                          </tr>' + sensors + '\
                        </table>\
                      </td>\
                    </tr>\
                  </div>\
                  </table>\
                  <div style="width: 55%; margin: 10pt auto;">\
                  <button type="button" class="btn btn-primary download" onclick="downloadFile('+ i +')" >\
                  <span class="glyphicon glyphicon-download-alt"></span> Download</button>\
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editFile" id="editbtn"\
                      data-filename="'+ files[i].fileName +'" \
                      data-realid="'+ files[i].fileID +'" \
                      data-fakeid="'+ i +'" \
                      data-contype="'+ files[i].controllerType +'" \
                      data-sensornames="'+ sensorNames + '" \
                      data-sensorpins="'+ files[i].sensors.length + '" \
                      data-sensortypes="'+ sensorTypes +'">\
                      <span class="glyphicon glyphicon-edit"></span> Edit\
                    </button>\
                    <button type="button" class="btn btn-primary info" data-toggle="modal" data-target="#fileInfo" \
                    data-filename="'+ files[i].fileName +'" \
                    data-controller="'+ files[i].controllerType +'" \
                    data-internet="'+ files[i].internet +'" \
                    data-sensorIDs="'+ sensorIDs +'" \
                    data-sensortypes="'+ sensorTypes +'">\
                    <span class="glyphicon glyphicon-info-sign"></span> More Info</button>\
                    <button type="button" class="btn btn-primary delete" onclick="deleteFile(this)" \
                    id="delete-'+ i +'-'+ files[i].fileID +'">\
                    <span class="glyphicon glyphicon-trash"></span> Delete</button>\
                    <form action="../code_generator/createFile.php" method="post">\
                          <input type="hidden" name="content" id="content'+ i +'">\
                          <input type="hidden" name="filename" id="filename'+ i +'" value="'+ files[i].fileName +'">\
                          <input type="hidden" name="pins" id="downloadpins'+ i +'" value="'+ files[i].pins +'">\
                          <input type="hidden" name="time" id="downloadtime'+ i +'" value="'+ files[i].timeInterval +'">\
                          <input type="hidden" id="sensorTypes'+ i +'" value="'+ sensorTypes +'">\
                          <input type="hidden" id="controllerType'+ i +'" value="'+ files[i].controllerType +'">\
                          <input type="hidden" id="sensorIDs'+ i +'" value="'+ sensorIDs +'">\
                          <input type="hidden" id="internet'+ i +'" value="'+ files[i].internet +'">\
                          <button type="submit" id="btn'+ i +'" hidden></button>\
                      </form>\
                  </div>\
                  <br><br>\
              </td>\
          </tr>\
          ';
        }
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token);
}

function isNum(p){
  if (p==""){
    return false;
  }
  for (var i=0; i<p.length; i++){
    if (p.charAt(i)<'0' || p.charAt(i)>'9'){
      return false;
    }
  }
  return true;
}

function editFile(realid, fakeid, len){
  var mes = document.getElementById('modal-mes');
  var time = document.getElementById('modal-time').value;
  var newpins = document.getElementById('modal-pin0').value;
  var same = false;
  for (var i=1; i<len; i++){
    newpins += ";" + document.getElementById('modal-pin' + i).value;
  }
  var tokens = newpins.split(";");
  if (time==""){
    mes.innerHTML = "Enter time interval." + "<br>"
    return;
  }
  if (!isNum(time)){
    mes.innerHTML = "The time interval must be a number.<br>";
    return;
  }
  for (var i=0; i<tokens.length; i++){
    for (var j=i; j<tokens.length; j++){
      if (i==j) continue;
      if (tokens[i]==tokens[j]){
        same = true;
        break;
      }
    }
  }
  if (same){
    mes.innerHTML = "You can not use the same pin for two or more sensors. <br>";
    return;
  }
  var path = '../edit/edit_file.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        if (result.status=="success"){
          for (var i=0; i<len; i++){
            document.getElementById('pin' + fakeid + '-' + i).innerHTML = tokens[i];
          }
          document.getElementById('time' + fakeid).innerHTML = time;
          document.getElementById('downloadtime' + fakeid).value = time;
          document.getElementById('downloadpins' + fakeid).value = newpins;
          swal("Saved!", "File info has been changed.", "success");
        }
        else{
          swal("Error!", "Unable to change the file info.", "danger");
        }
        $('#editFile').modal('hide');
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token + "&id=" + realid + "&time=" + time + "&pins=" + newpins);
}

function getInfo(sensors, controller, ids, internet){
  var path = '../code_generator/getInfo.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      var str = '<h4>Hardware</h4><br>\
      ' + result.hardware + '<br>\
      <hr><h4>Instructions</h4><br>\
      '+ result.instructions;
      var info = document.getElementById('info-body');
      info.innerHTML = str;
      info.scrollTop = 0;
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("sensors=" + sensors + "&controller=" + controller + "&ids=" + ids + "&internet=" + internet);
}

function downloadFile(i){
  console.log(i);
  var sensors = document.getElementById('sensorTypes' + i).value;
  var ids = document.getElementById('sensorIDs' + i).value;
  var controller = document.getElementById('controllerType' + i).value;
  var internet = document.getElementById('internet' + i).value;
  var cont = document.getElementById('content' + i);
  var btn = document.getElementById('btn' + i);
  if (controller == "Arduino" || controller == "Intel Galileo"){
    document.getElementById('downloadtime' + i).value = document.getElementById('downloadtime' + i).value*1000;
  }
  var path = '../code_generator/getInfo.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      var content = result.code;
      cont.value = content;
      btn.click();
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("sensors=" + sensors + "&controller=" + controller + "&ids=" + ids + "&internet=" + internet);
}

$(document).ready(function(){
  var user = $("#username");
  user.html(username);
  readFiles();

$('#editFile').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var filename = button.data('filename');
  var realid = button.data('realid');
  var fakeid = button.data('fakeid');
  var names = button.data('sensornames');
  var types = button.data('sensortypes');
  var num = button.data('sensorpins');
  var contype = button.data('contype');
  var modal = $(this);
  var time = (document.getElementById("time" + fakeid).innerHTML).split(" ")[0];
  modal.find('.modal-title').text('Edit file "' + filename + '"');
  modal.find('.time-interval').val(time);
  var table = $('#modalsensors');
  $("#modal-mes").html("");
  var p = [];
  for (var i=0; i<num; i++){
    p.push(document.getElementById('pin' + fakeid + "-" + i).innerHTML);
  }
  var n,t;
  if (p.length>1){
    var n = names.split(";");
    var t = types.split(";");
  }
  else{
    n = [names];
    t = [types];
  }
  var str = "";
  for (var i=0; i<n.length; i++){
    var droplist = "";
    if (contype=="Arduino"){
      if (t[i]=="Sound (digital)" || t[i]=="Temperature (DS18B20)" || t[i]=="Motion (PIR)"){
        for (var k=0; k<arduino_pins.digital.length; k++){
          droplist+='<option value="' + arduino_pins.digital[k] + '">' + arduino_pins.digital[k] + '</option>';
        }
      }
      else{
        for (var k=0; k<arduino_pins.analog.length; k++){
          droplist+='<option value="' + arduino_pins.analog[k] + '">' + arduino_pins.analog[k] + '</option>';
        }
      }
    }
    else if (contype=="Raspberry Pi"){
      for (var k=0; k<raspberry_pins.gpio.length; k++){
        droplist+='<option value="' + raspberry_pins.gpio[k] + '">' + raspberry_pins.gpio[k] + '</option>';
      }
    }
    else if (contype=="Beaglebone Black"){
      if (t[i]=="Sound (digital)" || t[i]=="Temperature (DS18B20)" || t[i]=="Motion (PIR)"){
        for (var k=0; k<beaglebone_pins.gpio.length; k++){
          droplist+='<option value="' + beaglebone_pins.gpio[k] + '">' + beaglebone_pins.gpio[k] + '</option>';
        }
      }
      else{
        for (var k=0; k<beaglebone_pins.analogin.length; k++){
          droplist+='<option value="' + beaglebone_pins.analogin[k] + '">' + beaglebone_pins.analogin[k] + '</option>';
        }
      }
    }
    else if (contype=="Intel Galileo"){
      if (t[i]=="Sound (digital)" || t[i]=="Motion (PIR)"){
        for (var k=0; k<intelgalileo_pins.digital.length; k++){
          droplist+='<option value="' + intelgalileo_pins.digital[k] + '">' + intelgalileo_pins.digital[k] + '</option>';
        }
      }
      else{
        for (var k=0; k<intelgalileo_pins.analog.length; k++){
          droplist+='<option value="' + intelgalileo_pins.analog[k] + '">' + intelgalileo_pins.analog[k] + '</option>';
        }
      }
    }
    str += '\
    <tr>\
      <th scope="row">'+ (i+1) +'</th>\
      <td>'+ n[i] +'</td>\
      <td>'+ t[i] +'</td>\
      <td>\
        <select class="form-control list'+ i +'" id="modal-pin'+ i +'">\
        '+ droplist +'\
        </select>\
      </td>\
    </tr>';
  }
  table.html(str);
  for (var j=0; j<p.length; j++){
    modal.find('.list' + j).val(p[j]);
  }
  var method = "editFile(" + realid + ", " + fakeid + "," + p.length + ")";
  $("#savebtn").attr("onclick", method);
})

$('#fileInfo').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget);
  var filename = button.data('filename');
  var sensors = button.data('sensortypes');
  var controller = button.data('controller');
  var internet = button.data('internet');
  var ids = button.data('ids');
  var modal = $(this);
  modal.find('.modal-title').text('File info: "' + filename + '"');
  $("#info-body").html('');
  getInfo(sensors, controller, ids, internet);
})

});
