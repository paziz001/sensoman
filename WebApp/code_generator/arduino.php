<?php

$arduino_pins_digital = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13");
$arduino_pins_analog = array("A0", "A1", "A2", "A3", "A4", "A5");

function getArduinoCode($sensorTypes, $sensorIDs, $internet){
  $includes = "";
  $functions = "";
  $includeOneWire="";
  $first = "";
  $setup = "\tSerial.begin(9600);\n";
  $loop = "\n";
  $m = 0; $t = 0; $s = 0; $l = 0; $h = 0;

  $sem = false;
  for ($i=0; $i<strlen($internet); $i++){
    if ($internet{$i}==';'){
      $sem = true;
      break;
    }
  }

  if ($sem==true){
    $tokens = explode(";", $internet);
    $internet = $tokens[0];
    $ssid = $tokens[1];
    $pass = $tokens[2];
  }

  if ($internet=="Ethernet shield"){
    $includes.= "#include <SPI.h>\n";
    $includes.= "#include <Ethernet.h>\n";
    $functions.="void sendMeasurement(String id, String value){\n";
    $functions.="\tString data= <d>sensorID=<d> + id + <d>&measurement=<d> + value;\n";
    $functions.="\tif (client.connect(server, 80)){\n";
    $functions.= "\t\tclient.println(<d>POST /upload/post_measurement.php HTTP/1.1<d>);\n";
    $functions.= "\t\tclient.print(<d>Host: <d>);\n";
    $functions.= "\t\tclient.println(server);\n";
    $functions.= "\t\tclient.println(<d>Content-Type: application/x-www-form-urlencoded<d>);\n";
    $functions.= "\t\tclient.println(<d>Connection: close<d>);\n";
    $functions.= "\t\tclient.println(<d>User-Agent: Arduino/1.0<d>);\n";
    $functions.= "\t\tclient.print(<d>Content-Length: <d>);\n";
    $functions.= "\t\tclient.println(value.length());\n";
    $functions.= "\t\tclient.println();\n";
    $functions.= "\t\tclient.print(data);\n";
    $functions.= "\t\tclient.println();\n";
    $functions.="\t}\n";
    $functions.="\telse{\n";
    $functions.="\t\tSerial.println(<d>Cannot connect to Server<d>);\n";
    $functions.="\t}\n";
    $functions.="}\n";
    $first.= "\n// Enter a MAC address for your controller below.\n";
    $first.= "byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };\n";
    $first.= "char server[] = <d>http://thesis.in.cs.ucy.ac.cy/sensoman2<d>;\n\n";
    $first.= "// Enter the static IP address to use if the DHCP fails to assign\n";
    $first.= "IPAddress ip(192, 168, 0, 177);\n\n";
    $first.= "EthernetClient client;\n";
    $setup.= "\tif (Ethernet.begin(mac) == 0) {\n";
    $setup.= "\t\tSerial.println(<d>Failed to configure Ethernet using DHCP<d>);\n";
    $setup.= "\t\tEthernet.begin(mac, ip);\n";
    $setup.= "\t}\n";
    $setup.= "\tSerial.println(<d>Connecting to internet...<d>);\n";
    $setup.= "\tdelay(1000);\n";
  }

  if ($internet=="Adafruit CC3000 Wi-Fi shield"){
    $includes.= "#include <Adafruit_CC3000.h>\n";
    $includes.= "#include <ccspi.h>\n";
    $includes.= "#include <SPI.h>\n";
    $includes.= "#include <string.h>\n";
    $includes.= "#include <d>utility/debug.h<d>\n";
    $first.= "// These are the interrupt and control pins\n";
    $first.= "#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!\n";
    $first.= "// These can be any two pins\n";
    $first.= "#define ADAFRUIT_CC3000_VBAT  5\n";
    $first.= "#define ADAFRUIT_CC3000_CS    10\n";
    $first.= "// Use hardware SPI for the remaining pins\n";
    $first.= "// On an UNO, SCK = 13, MISO = 12, and MOSI = 11\n";
    $first.= "Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,SPI_CLOCK_DIVIDER);\n\n";
    $first.= "#define WLAN_SSID       <d>". $ssid ."<d>           // cannot be longer than 32 characters!\n";
    $first.= "#define WLAN_PASS       <d>". $pass ."<d>\n";
    $first.= "// Security can be WLAN_SEC_UNSEC, WLAN_SEC_WEP, WLAN_SEC_WPA or WLAN_SEC_WPA2\n";
    $first.= "#define WLAN_SECURITY   WLAN_SEC_WPA2\n\n";
    $first.= "#define WEBSITE      <d>http://thesis.in.cs.ucy.ac.cy/sensoman2<d>\n";
    $first.= "uint32_t ip;\n";
    $first.= "Adafruit_CC3000_Client www;\n";
    $setup.= "\tSerial.println(F(<d>Hello, CC3000!<d>)); \n";
    $setup.= "\tSerial.print(<d>Free RAM: <d>); Serial.println(getFreeRam(), DEC);\n\n";
    $setup.= "\tSerial.println(F(<d>Initializing...<d>));\n";
    $setup.= "\tif (!cc3000.begin()){\n";
    $setup.= "\t\tSerial.println(F(<d>Couldn't begin()! Check your wiring?<d>));\n";
    $setup.= "\t\twhile(1);\n";
    $setup.= "\t}\n\n";
    $setup.= "\tSerial.print(F(<d>Attempting to connect to <d>)); Serial.println(WLAN_SSID);\n";
    $setup.= "\tif (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {\n";
    $setup.= "\t\tSerial.println(F(<d>Failed!<d>));\n";
    $setup.= "\t\twhile(1);\n";
    $setup.= "\t}\n\n";
    $setup.= "\tSerial.println(F(<d>Connected!<d>));\n\n";
    $setup.= "\t/* Wait for DHCP to complete */\n";
    $setup.= "\tSerial.println(F(<d>Request DHCP<d>));\n";
    $setup.= "\twhile (!cc3000.checkDHCP()){\n";
    $setup.= "\t\tdelay(100); // ToDo: Insert a DHCP timeout!\n";
    $setup.= "\t}\n\n";
    $setup.= "\t/* Display the IP address DNS, Gateway, etc. */  \n";
    $setup.= "\twhile (! displayConnectionDetails()) {\n";
    $setup.= "\t\tdelay(1000);\n";
    $setup.= "\t}\n\n";
    $setup.= "\tip = 0;\n";
    $setup.= "\t// Try looking up the website's IP address\n";
    $setup.= "\tSerial.print(WEBSITE); Serial.print(F(<d> -> <d>));\n";
    $setup.= "\twhile (ip == 0) {\n";
    $setup.= "\t\tif (! cc3000.getHostByName(WEBSITE, &ip)) {\n";
    $setup.= "\t\t\tSerial.println(F(<d>Couldn't resolve!<d>));\n";
    $setup.= "\t\t}\n";
    $setup.= "\t\tdelay(500);\n";
    $setup.= "\t}\n\n";
    $setup.= "\tcc3000.printIPdotsRev(ip);\n";
    $functions.= "\nbool displayConnectionDetails(void){\n";
    $functions.= "\tuint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;\n\n";
    $functions.= "\tif(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv)){\n";
    $functions.= "\t\tSerial.println(F(<d>Unable to retrieve the IP Address!<d>));\n";
    $functions.= "\t\treturn false;\n";
    $functions.= "\t}\n";
    $functions.= "\telse{\n";
    $functions.= "\t\tSerial.println(F(<d>IP Addr: <d>)); cc3000.printIPdotsRev(ipAddress);\n";
    $functions.= "\t\tSerial.println(F(<d>Netmask: <d>)); cc3000.printIPdotsRev(netmask);\n";
    $functions.= "\t\tSerial.println(F(<d>Gateway: <d>)); cc3000.printIPdotsRev(gateway);\n";
    $functions.= "\t\tSerial.println(F(<d>DHCPsrv: <d>)); cc3000.printIPdotsRev(dhcpserv);\n";
    $functions.= "\t\tSerial.println(F(<d>DNSserv: <d>)); cc3000.printIPdotsRev(dnsserv);\n";
    $functions.= "\t\tSerial.println();\n";
    $functions.= "\t\treturn true;\n";
    $functions.= "\t}\n";
    $functions.= "}\n";
    $functions.= "\nvoid sendMeasurement(String id, String value){\n";
    $functions.= "\tString data= <d>sensorID=<d> + id + <d>&measurement=<d> + value;\n";
    $functions.= "\twww = cc3000.connectTCP(ip, 80);\n";
    $functions.= "\tif (www.connected()) {\n";
    $functions.= "\t\twww.println(<d>POST /upload/post_measurement.php HTTP/1.1<d>);\n";
    $functions.= "\t\twww.print(<d>Host: <d>);\n";
    $functions.= "\t\twww.println(WEBSITE);\n";
    $functions.= "\t\twww.println(<d>Content-Type: application/x-www-form-urlencoded<d>);\n";
    $functions.= "\t\twww.println(<d>Connection: close<d>);\n";
    $functions.= "\t\twww.println(<d>User-Agent: Arduino/1.0<d>);\n";
    $functions.= "\t\twww.print(<d>Content-Length: <d>);\n";
    $functions.= "\t\twww.println(value.length());\n";
    $functions.= "\t\twww.println();\n";
    $functions.= "\t\twww.print(data);\n";
    $functions.= "\t\twww.println();\n";
    $functions.= "\t}\n";
    $functions.= "\telse {\n";
    $functions.= "\t\tSerial.println(F(<d>Connection failed<d>));    \n";
    $functions.= "\t\treturn;\n";
    $functions.= "\t}\n";
    $functions.= "\twww.close();\n";
    $functions.= "}\n";
  }

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      $first.= "int motion_pin" . $m . " = <pin". $i . ">;\n";
      $first.= "int motion_value" . $m . ";\n";
      $setup.= "\tpinMode(motion_pin" . $m . ", INPUT); \n";
      $loop.= "\tmotion_value" . $m . " = digitalRead(motion_pin" . $m . "); \n";
      $loop.= "\tSerial.print(<d>Motion = <d>);\n";
      $loop.="\tSerial.println(motion_value" . $m . "); \n";
      if ($internet=="Ethernet shield" || $internet=="Adafruit CC3000 Wi-Fi shield"){
        $loop.= "\tsendMeasurement(String(" . $sensorIDs[$i] . "), String(motion_value" . $m . "));\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      $t++;
      if ($includeOneWire == ""){
        $includeOneWire.="#include <OneWire.h>\n#include <DallasTemperature.h>\n";
      }
      $first.="#define ONE_WIRE_BUS <pin". $i . ">\n";
      $first.="float temperature_value" . $t . ";\n";
      $first.="OneWire oneWire(ONE_WIRE_BUS);\n";
      $first.="DallasTemperature sensors(&oneWire);\n";
      $setup.="\tsensors.begin();\n";
      $loop.="\tSerial.println(<d>Measuring temperature...<d>);\n";
      $loop.="\tsensors.requestTemperatures();\n";
      $loop.= "\ttemperature_value" . $t . " = sensors.getTempCByIndex(0);\n";
      $loop.= "\tSerial.print(<d>Temperature = <d>); \n";
      $loop.= "\tSerial.print(temperature_value" . $t . "); \n";
      $loop.= "\tSerial.println(<d> degrees celsius<d>);\n";
      if ($internet=="Ethernet shield" || $internet=="Adafruit CC3000 Wi-Fi shield"){
        $loop.= "\tsendMeasurement(String(" . $sensorIDs[$i] . "), String(temperature_value" . $t . "));\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      $first.= "int humidity_pin" . $h . " = <pin". $i . ">;\n";
      $first.= "int humidity_value" . $h . ";\n";
      $setup.= "\tpinMode(humidity_pin" . $h . ", INPUT); \n";
      $loop.= "\thumidity_value" . $h . " = analogRead(humidity_pin" . $h . "); \n";
      $loop.= "\t/*\n";
      $loop.= "\t*Uncomment the next line to take the moisture value as percentage.\n";
      $loop.= "\t*Replace the <max_value> with the maximum value of the FC-28 sensor and the <min_value> with the minimum.\n";
      $loop.= "\t*/\n";
      $loop.= "\t//humidity_value" . $h . " = map(output_value,<max_value>,<min_value>,0,100); \n";
      $loop.= "\tSerial.print(<d>Moisture = <d>); \n";
      $loop.= "\tSerial.println(humidity_value" . $h . "); \n";
      if ($internet=="Ethernet shield" || $internet=="Adafruit CC3000 Wi-Fi shield"){
        $loop.= "\tsendMeasurement(String(" . $sensorIDs[$i] . "), String(humidity_value" . $h . "));\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      $first.= "int light_pin" . $l . " = <pin". $i . ">;\n";
      $first.= "int light_value" . $l . ";\n";
      $setup.= "\tpinMode(light_pin" . $l . ", INPUT); \n";
      $loop.= "\tlight_value" . $l . " = analogRead(light_pin" . $l . "); \n";
      $loop.= "\tSerial.print(<d>Brightness = <d>); \n";
      $loop.= "\tSerial.println(light_value" . $l . "); \n";
      if ($internet=="Ethernet shield" || $internet=="Adafruit CC3000 Wi-Fi shield"){
        $loop.= "\tsendMeasurement(String(" . $sensorIDs[$i] . "), String(light_value" . $l . "));\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      $s++;
      $first.= "int sound_pin" . $s . " = <pin". $i . ">;\n";
      $first.= "int sound_value" . $s . ";\n";
      $setup.= "\tpinMode(sound_pin" . $s . ", INPUT); \n";
      $loop.= "\tsound_value" . $s . " = digitalRead(sound_pin" . $s . "); \n";
      $loop.= "\tSerial.print(<d>Sound = <d>); \n";
      $loop.= "\tSerial.println(sound_value" . $s . "); \n";
      if ($internet=="Ethernet shield" || $internet=="Adafruit CC3000 Wi-Fi shield"){
        $loop.= "\tsendMeasurement(String(" . $sensorIDs[$i] . "), String(sound_value" . $s . "));\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Sound (analog)"){
      $s++;
      $first.= "int sound_pin" . $s . " = <pin". $i . ">;\n";
      $first.= "int sound_value" . $s . ";\n";
      $setup.= "\tpinMode(sound_pin" . $s . ", INPUT); \n";
      $loop.= "\tsound_value" . $s . " = analogRead(sound_pin" . $s . "); \n";
      $loop.= "\tSerial.print(<d>Sound = <d>); \n";
      $loop.= "\tSerial.println(sound_value" . $s . "); \n";
      if ($internet=="Ethernet shield" || $internet=="Adafruit CC3000 Wi-Fi shield"){
        $loop.= "\tsendMeasurement(String(" . $sensorIDs[$i] . "), String(sound_value" . $s . "));\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      $first.= "int temperature_pin" . $t . " = <pin". $i . ">;\n";
      $first.= "int temperature_value" . $t . ";\n";
      $setup.= "\tpinMode(temperature_pin" . $t . ", INPUT); \n";
      $loop.= "\ttemperature_value" . $t . " = analogRead(temperature_pin" . $t . "); \n";
      $loop.= "\tfloat temp =  (((temperature_value" . $t . " * 5.0) / 1024.0) - 0.5) * 100\n";
      $loop.= "\tSerial.print(<d>Temperature = <d>); \n";
      $loop.= "\tSerial.print(temp); \n";
      $loop.= "\tSerial.println(<d>degrees celsius<d>);\n";
      if ($internet=="Ethernet shield" || $internet=="Adafruit CC3000 Wi-Fi shield"){
        $loop.= "\tsendMeasurement(String(" . $sensorIDs[$i] . "), String(temp));\n";
      }
      $loop.= "\n";
    }
  }
  $loop.= "\tSerial.println();\n";
  $loop.= "\tdelay(<time>); \n";
  $includes .= $includeOneWire;
  $content = $includes . "\n" . $first . "\n" . $functions . "\nvoid setup(){\n" . $setup . "}\n\nvoid loop(){" . $loop . "}";
  return $content;
}

function getArduinoHardware($sensorTypes, $internet){
  $tokens = explode(";", $internet);
  $hardware="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0;
  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      $motion = "<li> <img src='../code_generator/images/pir.png' height='100' width='120'>" . $m . " PIR motion sensor(s)</li>";
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      $t++;
      $temperature = "<li> <img src='../code_generator/images/ds18b20.png' height='100' width='120'>" . $t . " Waterproof temperature sesor(s) DS18B20</li>" .
                    "<li> <img src='../code_generator/images/resistor.png' height='100' width='120'>" . $t . " Resistor(s) 4,7K Ohm</li>";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      $humidity = "<li> <img src='../code_generator/images/soil_moisture.png' height='100' width='120'>" . $h . " Soil moisture sensor(s) FC-28</li>";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      $light = "<li> <img src='../code_generator/images/ldr.png' height='100' width='120'>" . $l . " LDR sensor(s)</li>" .
               "<li> <img src='../code_generator/images/resistor.png' height='100' width='120'>" . $l . " Resistor(s) 10K Ohm.</li>";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      $s++;
      $sound = "<li> <img src='../code_generator/images/sound_sensor.png' height='100' width='120'>" . $s . " Sound sensor(s)</li>";
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      $temperature = "<li> <img src='../code_generator/images/tmp36.png' height='100' width='120'>" . $t . " Temperature sesor(s) TMP36</li>";
    }
  }
  $hardware .= "<li> <img src='../code_generator/images/arduino_uno.png' height='100' width='120'> 1 Arduino board + USB cable</li>";
  if ($tokens[0]=="Adafruit CC3000 Wi-Fi shield"){
    $hardware .= "<li> <img src='../code_generator/images/cc3000.png' height='100' width='120'> 1 Adafruit CC3000 Wi-Fi shield</li>";
  }
  else if ($tokens[0]=="Ethernet shield"){
    $hardware .= "<li> <img src='../code_generator/images/ethernet_shield.png' height='100' width='120'> 1 Ethernet shield</li>";
  }
  $hardware .= "<li> <img src='../code_generator/images/breadboard.png' height='100' width='120'> 1 Breadboard</li>";
  $hardware .= "<li> <img src='../code_generator/images/jumper_wires.png' height='100' width='120'> Jumper wires</li>";
  $hardware .= $motion . $temperature . $humidity . $light . $sound;
  $hardware .= "</ul>";
  return $hardware;
}

function getArduinoInstructions($sensorTypes, $internet){
  $tokens = explode(";", $internet);
  $instuctions="<ol>";
  $motion = "";
  $temperature = "";
  $temperature2 = "";
  $humidity = "";
  $sound = "";
  $light = "";

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      if ($motion==""){
        $motion.= "<li>Connect the PIR motion sensor(s) to the Arduino Board";
        $motion.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $motion.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $motion.= "<li>Connect the data pin of each sensor to the corresponding digital pins of the arduno.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      if ($temperature==""){
        $temperature.= "<li>Download the OneWire library from <a href='https://github.com/milesburton/Arduino-Temperature-Control-Library' target='_blank'>here</a>.</li>";
        $temperature.= "<li>Unzip the file to the Libraries folder of your <a href='https://www.arduino.cc/en/Main/Software' target='_blank'>Arduino IDE</a>.</li>";
        $temperature.= "<li>Connect the DS18B20 waterproof temperature sensor(s) to the Arduino Board";
        $temperature.= "<ul><li>Connect the ground pin of each sensor to ground.</li>";
        $temperature.= "<li>Connect the one leg of the 4.7K ohm resistor to the data pin of the sensor(s) and the other leg to the VCC pin of the sensor(s).</li>";
        $temperature.= "<li>Connect the data pin of each sensor(s) (with the one leg of the resistor) to the corresponding digital pins of the arduno.</li>";
        $temperature.= "<li>Connect the VCC pin of each sensor(s) (with the other leg of the resistor) to 5V.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      if ($humidity==""){
        $humidity.= "<li>Connect the FC-28 soil moisture sensor(s) to the Arduino Board";
        $humidity.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $humidity.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $humidity.= "<li>Connect the analog data pin of each sensor to the corresponding analog pins of the arduno.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      if ($light==""){
        $light.= "<li>Connect the LDR sensor(s) to the Arduino Board";
        $light.= "<ul><li>Connect the first leg of each sensor to 5V.</li>";
        $light.= "<li>Connect the one leg of the 10K ohm resistor to the second leg of the sensor(s) and the other leg to ground.</li>";
        $light.= "<li>Connect the second leg of each sensor(s) (with the one leg of the resistor) to the corresponding analog pins of the arduno.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      if ($temperature2==""){
        $temperature2.= "<li>Connect the TMP36 temperature sensor(s) to the Arduino Board";
        $temperature2.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $temperature2.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $temperature2.= "<li>Connect the data pin of each sensor to the corresponding analog pins of the arduno.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      if ($sound==""){
        $sound.= "<li>Connect the sound sensor(s) to the Arduino Board";
        $sound.= "<ul><li>Connect the VCC pin of each sensor to 5V.</li>";
        $sound.= "<li>Connect the ground pin of each sensor to ground.</li>";
        $sound.= "<li>Connect the data pin (digital/analog) of each sensor to the corresponding pins of the arduno.</li></ul></li>";
      }
    }
  }
  $temperature .= $temperature2;
  if ($tokens[0]=="Adafruit CC3000 Wi-Fi shield"){
    $instuctions .= "<li>Place the CC3000 wi-fi shield on the Arduino board.</li>";
  }
  else if ($tokens[0]=="Ethernet shield"){
    $instuctions .= "<li>Place the ethernet shield on the Arduino board.</li>";
  }
  $instuctions .= $motion . $temperature . $humidity . $sound . $light;
  $instuctions .= "<li>Download the created Arduino file to your pc.</li>";
  $instuctions .= "<li>Connect the Arduino board to your pc using the USB cable.</li>";
  $instuctions .= "<li>Open the downloaded file with the <a href='https://www.arduino.cc/en/Main/Software' target='_blank'>Arduino IDE</a>.</li>";
  $instuctions .= "<li>Select the corresponding Arduino board and port from the tools tab.</li>";
  $instuctions .= "<li>Click the upload button to upload the code to the Arduino board.</li>";
  $instuctions .= "</ol>";
  return $instuctions;
}

function getArduinoDiagrams($sensorTypes){
  $diagrams ="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0; $t2=0;

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      if ($m==1){
        $motion .= "<hr><div><h4><b>PIR Motion Sensor</b></h4>";
        $motion .= "<p>This diagram shows the 3 pins of the PIR motion sensor.<br>".
                   "VCC: 3.3-5V DC for power<br>GND: Ground pin<br>Data: Digital output pin</p>";
        $motion .= "<img src='../code_generator/images/pir-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (DS18B20)"){
      $t++;
      if ($t==1){
        $temperature .= "<hr><div><h4><b>DS18B20 Waterproof Temperature Sensor</b></h4>";
        $temperature .= "<p>This diagram shows the 3 wires of the DS18B20 waterproof temperature sensor.<br>".
                        "VCC: 5V DC for power<br>GND: Ground wire<br>Data: Digital output wire</p>";
        $temperature .= "<img src='../code_generator/images/ds18b20-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      if ($h==1){
        $humidity .= "<hr><div><h4><b>FC-28 Soil Moisture Sensor</b></h4>";
        $humidity .= "<p>This diagram shows the 4 pins of the FC-28 soil moisture sensor.<br>VCC: 3.3-5V DC for power<br>".
                     "GND: Ground pin<br>Digital output pin<br>Analog output pin</p>";
        $humidity .= "<img src='../code_generator/images/soil-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      if ($l==1){
        $humidity .= "<hr><div><h4><b>Light Dependent Resistor (LDR)</b></h4>";
        $humidity .= "<p>This picture shows the circuit of the LDR with the 10k ohm resistor. The analog input goes to an analog in pin.</p>";
        $humidity .= "<img src='../code_generator/images/ldr-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t2++;
      if ($t2==1){
        $temperature .= "<hr><div><h4><b>TMP36 Temperature Sensor</b></h4>";
        $temperature .= "<p>This diagram shows the 3 wires of the TMP36 temperature sensor.<br>".
                        "VCC: 5V DC for power<br>GND: Ground pin<br>Data: Analog output pin</p>";
        $temperature .= "<img src='../code_generator/images/tmp36-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      $s++;
      if ($s==1){
        $sound .= "<hr><div><h4><b>Sound Sensor</b></h4>";
        $sound .= "<p>This diagram shows the 4 pins of the sound sensor.<br>VCC: 3.3-5V DC for power<br>GND: Ground pin<br>".
                  "DO: Digital output pin<br>A0: Analog output pin</p>";
        $sound .= "<img src='../code_generator/images/sound-ex.png'></div>";
      }
    }
  }
  $diagrams .= "<div><h4><b>Arduino Uno</b></h4>";
  $diagrams .= "<p>This diagram shows the different parts of the Arduino Uno board, like the digital I/O pins, analog in pins, ground pins,".
                " volt power pins and the USB plug.</p>";
  $diagrams .= "<img src='../code_generator/images/arduino-ex.png'></div>";
  $diagrams .= $motion . $temperature . $humidity . $light . $sound;
  $diagrams .= "</ul>";
  return $diagrams;
}

?>
