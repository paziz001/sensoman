<?php

$beaglebone_pins_gpio = array("P8_7", "P8_8", "P8_9", "P8_10", "P8_11", "P8_12", "P8_14", "P8_15", "P8_16", "P8_17", "P8_18", "P8_26",
                              "P9_11", "P9_12", "P9_13", "P9_15", "P9_17", "P9_18", "P9_23", "P9_24", "P9_25", "P9_26", "P9_27", "P9_30", "P9_41");
$beaglebone_pins_pwmout = array("P8_13", "P8_19", "P9_14", "P9_16", "P9_21", "P9_22");
$beaglebone_pins_analogin = array("P9_33", "P9_35", "P9_36", "P9_37", "P9_38", "P9_39", "P9_40");

function getBeagleboneBlackCode($sensorTypes, $sensorIDs, $internet){
  $imports = "import time\n";
  $importGPIO = "";
  $importADC = "";
  $setupADC = "";
  $first = "";
  $functions = "";
  $setup = "";
  $loop = "";
  $last = "\tGPIO.cleanup()\n";
  $m = 0; $t = 0; $s = 0; $l = 0; $h = 0;

  if ($internet!="No internet connection"){
    $imports.= "import requests\n";
    $functions.= "\ndef postMeasurement(sensorID, value):\n";
    $functions.= "\turl = 'http://thesis.in.cs.ucy.ac.cy/sensoman2/upload/post_measurement.php'\n";
    $functions.= "\tdata = {'sensorID': str(sensorID), 'measurement': str(value)}\n";
    $functions.= "\treq = requests.post(url, json = data)\n";
    $functions.= "\tif (req.status_code==200):\n";
    $functions.= "\t\tprint('Measurement uploaded successfully!\\n')\n";
    $functions.= "\telse:\n";
    $functions.= "\t\tprint('Unable to upload the measurement.\\n')\n";
  }

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      if ($importGPIO==""){
        $importGPIO.= "import Adafruit_BBIO.GPIO as GPIO\n";
      }
      $first.= "motion_pin" . $m . " = <pin". $i . ">;\n";
      $setup.= "GPIO.setup(motion_pin". $m . ", GPIO.IN)\n";
      $loop.= "\tmotion_value = GPIO.input(motion_pin" . $m . ")\n";
      $loop.= "\tprint ('Motion = ',motion_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", motion_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      if ($importADC==""){
        $importADC.= "import Adafruit_BBIO.ADC as ADC\n";
      }
      $first.= "temperature_pin" . $t . " = <pin" . $i . ">;\n";
      if ($setupADC == ""){
        $setupADC = "ADC.setup()\n";
      }
      $loop.= "\ttemperature_value = ADC.read(temperature_pin" . $t . ")\n";
      $loop.= "\ttemperature_value = ((temperature_value * 1800)-500)/10;\n";
      $loop.= "\tprint ('Temperature = ',temperature_value, ' degrees celsius','\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", temperature_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      if ($importADC==""){
        $importADC.= "import Adafruit_BBIO.ADC as ADC\n";
      }
      $first.= "humidity_pin" . $h . " = <pin" . $i . ">;\n";
      if ($setupADC == ""){
        $setupADC = "ADC.setup()\n";
      }
      $loop.= "\thumidity_value = ADC.read(humidity_pin" . $h . ")\n";
      $loop.= "\tprint ('Moisture = ',humidity_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", humidity_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      if ($importADC==""){
        $importADC.= "import Adafruit_BBIO.ADC as ADC\n";
      }
      $first.= "light_pin" . $l . " = <pin" . $i . ">;\n";
      if ($setupADC == ""){
        $setupADC = "ADC.setup()\n";
      }
      $loop.= "\tlight_value = ADC.read(light_pin" . $l . ")\n";
      $loop.= "\tprint ('Light = ',light_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", light_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      $s++;
      if ($importGPIO==""){
        $importGPIO.= "import Adafruit_BBIO.GPIO as GPIO\n";
      }
      $first.= "sound_pin" . $s . " = <pin". $i . ">;\n";
      $setup.= "GPIO.setup(sound_pin". $s . ", GPIO.IN)\n";
      $loop.= "\tsound_value = GPIO.input(sound_pin" . $s . ")\n";
      $loop.= "\tprint ('Sound = ',sound_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", sound_value)\n";
      }
      $loop.= "\n";
    }
    elseif ($sensorTypes[$i]=="Sound (analog)"){
      $s++;
      if ($importADC==""){
        $importADC.= "import Adafruit_BBIO.ADC as ADC\n";
      }
      $first.= "sound_pin" . $s . " = <pin". $i . ">;\n";
      if ($setupADC == ""){
        $setupADC = "ADC.setup()\n";
      }
      $loop.= "\tsound_value = ADC.read(sound_pin" . $s . ")\n";
      $loop.= "\tprint ('Sound = ',sound_value,'\\n')\n";
      if ($internet!="No internet connection"){
        $loop .= "\tpostMeasurement(". $sensorIDs[$i] .", sound_value)\n";
      }
      $loop.= "\n";
    }
  }

  $loop.= "\ttime.sleep(<time>)\n";
  $imports .= $importGPIO . $importADC;
  $setup .= $setupADC;
  $setup .= $functions;
  $content = $imports . "\n" . $first . "\n" . $setup . "\nwhile True:\n" . $loop . $last;
  return $content;
}

function getBeagleboneBlackHardware($sensorTypes, $internet){
  $hardware="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0;
  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      $motion = "<li> <img src='../code_generator/images/pir.png' height='100' width='120'>" . $m . " PIR motion sensor(s)</li>";
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      $temperature = "<li> <img src='../code_generator/images/tmp36.png' height='100' width='120'>" . $t . " Temperature sesor(s) TMP36</li>";
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      $humidity = "<li> <img src='../code_generator/images/soil_moisture.png' height='100' width='120'>" . $h . " Soil moisture sensor(s) FC-28</li>";
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      $light = "<li> <img src='../code_generator/images/ldr.png' height='100' width='120'>" . $l . " LDR sensor(s)</li>" .
               "<li> <img src='../code_generator/images/resistor.png' height='100' width='120'>" . $l . " Resistor(s) 10k ohm.</li>";
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      $s++;
      $sound = "<li> <img src='../code_generator/images/sound_sensor.png' height='100' width='120'>" . $s . " Sound sensor(s)</li>";
    }
  }
  $hardware .= "<li> <img src='../code_generator/images/beaglebone_black.png' height='100' width='120'> 1 BeagleBone Black board + Power supply</li>";
  if ($internet=="Ethernet cable"){
      $hardware .= "<li> <img src='../code_generator/images/ethernet_cable.png' height='100' width='120'> 1 Ethernet Cable</li>";
  }
  $hardware .= "<li> <img src='../code_generator/images/breadboard.png' height='100' width='120'> 1 Breadboard</li>";
  $hardware .= "<li> <img src='../code_generator/images/jumper_wires.png' height='100' width='120'> Jumper wires</li>";
  $hardware .= $motion . $temperature . $humidity . $light . $sound;
  $hardware .= "</ul>";
  return $hardware;
}

function getBeagleboneBlackInstructions($sensorTypes, $internet){
  $instuctions="<ol>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $sound = "";
  $light = "";

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      if ($motion==""){
        $motion.= "<li>Connect the PIR motion sensor(s) to the BeagleBone Black";
        $motion.= "<ul><li>Connect the VCC pin of each sensor to 5V (on board pins: P9_5, P9_6, P9_7, P9_8).</li>";
        $motion.= "<li>Connect the ground pin of each sensor to ground (on board pins: P8_1, P8_2, P9_1, P9_2, P9_43, P9_44, P9_45, P9_46).</li>";
        $motion.= "<li>Connect the data pin of each sensor to the corresponding digital pins of the beaglebone black.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      if ($temperature==""){
        $temperature.= "<li>Connect the TMP36 temperature sensor(s) to the BeagleBone Black";
        $temperature.= "<ul><li>Connect the VCC pin of each sensor to 3.3V (on board pins: P9_3, P9_4).</li>";
        $temperature.= "<li>Connect the ground pin of each sensor to ground (on board pins: P8_1, P8_2, P9_1, P9_2, P9_43, P9_44, P9_45, P9_46).</li>";
        $temperature.= "<li>Connect the data pin of each sensor to the corresponding analog pins of the beaglebone black.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      if ($humidity==""){
        $humidity.= "<li>Connect the FC-28 soil moisture sensor(s) to the BeagleBone Black";
        $humidity.= "<ul><li>Connect the VCC pin of each sensor to 5V (on board pins: P9_5, P9_6, P9_7, P9_8).</li>";
        $humidity.= "<li>Connect the ground pin of each sensor to ground (on board pins: P8_1, P8_2, P9_1, P9_2, P9_43, P9_44, P9_45, P9_46).</li>";
        $humidity.= "<li>Connect the data pin of each sensor to the corresponding analog pins of the beaglebone black.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      if ($light==""){
        $light.= "<li>Connect the LDR sensor(s) to the BeagleBone Black";
        $light.= "<ul><li>Connect the first leg of each sensor to 3.3V (on board pins: P9_3, P9_4).</li>";
        $light.= "<li>Connect the one leg of the 10k ohm resistor to the second leg of the sensor(s) and the other leg to ground (on board pins: P8_1, P8_2, P9_1, P9_2, P9_43, P9_44, P9_45, P9_46).</li>";
        $light.= "<li>Connect the second leg of each sensor(s) (with the one leg of the resistor) to the corresponding analog pins of the beaglebone black.</li></ul></li>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)" || $sensorTypes[$i]=="Sound (analog)"){
      if ($sound==""){
        $sound.= "<li>Connect the sound sensor(s) to the BeagleBone Black";
        $sound.= "<ul><li>Connect the VCC pin of each sensor to 5V (on board pins: P9_5, P9_6, P9_7, P9_8).</li>";
        $sound.= "<li>Connect the ground pin of each sensor to ground (on board pins: P8_1, P8_2, P9_1, P9_2, P9_43, P9_44, P9_45, P9_46).</li>";
        $sound.= "<li>Connect the data pin (digital/analog) of each sensor to the corresponding pins of the beaglebone black.</li></ul></li>";
      }
    }
  }
  $instuctions .= $motion . $temperature . $humidity . $sound . $light;
  $instuctions .= "<li>Download the created BeagleBone Black file to your pc.</li>";
  $instuctions .= "<li>Install the <a href='https://learn.adafruit.com/ssh-to-beaglebone-black-over-usb/installing-drivers-windows' target='_blank'>BeagleBone Black drivers</a> to your pc.</li>";
  $instuctions .= "<li>Power up the board using the power supply.</li>";
  if ($internet=="Ethernet cable"){
    $instuctions .= "<li>Connect the board to your router using the ethernet cable.</li>";
  }
  $instuctions .= "<li>Connect to your BeagleBone Black board using <a href='https://learn.adafruit.com/ssh-to-beaglebone-black-over-usb/ssh-with-windows-and-putty' target='_blank'>SSH</a>. (default username: root , no password)</li>";
  $instuctions .= "<li>Install the <a href='https://learn.adafruit.com/setting-up-io-python-library-on-beaglebone-black/installation' target='_blank'>Python pachages</a> to the board.</li>";
  $instuctions .= "<li>Create a new python file (.py) to your board and copy the contents of the downloaded file.</li>";
  $instuctions .= "<li>Save and run the script.</li>";
  $instuctions .= "</ol>";
  return $instuctions;
}

function getBeagleboneBlackDiagrams($sensorTypes){
  $diagrams ="<ul>";
  $motion = "";
  $temperature = "";
  $humidity = "";
  $light = "";
  $sound = "";
  $m=0; $t=0; $h=0; $l=0; $s=0;

  for ($i=0; $i<sizeof($sensorTypes); $i++){
    if ($sensorTypes[$i]=="Motion (PIR)"){
      $m++;
      if ($m==1){
        $motion .= "<hr><div><h4><b>PIR Motion Sensor</b></h4>";
        $motion .= "<p>This diagram shows the 3 pins of the PIR motion sensor.<br>".
                   "VCC: 3.3-5V DC for power<br>GND: Ground pin<br>Data: Digital output pin</p>";
        $motion .= "<img src='../code_generator/images/pir-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Temperature (TMP36)"){
      $t++;
      if ($t==1){
        $temperature .= "<hr><div><h4><b>TMP36 Temperature Sensor</b></h4>";
        $temperature .= "<p>This diagram shows the 3 wires of the TMP36 temperature sensor.<br>".
                        "VCC: 5V DC for power<br>GND: Ground pin<br>Data: Analog output pin</p>";
        $temperature .= "<img src='../code_generator/images/tmp36-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Humidity (FC28)"){
      $h++;
      if ($h==1){
        $humidity .= "<hr><div><h4><b>FC-28 Soil Moisture Sensor</b></h4>";
        $humidity .= "<p>This diagram shows the 4 pins of the FC-28 soil moisture sensor.<br>VCC: 3.3-5V DC for power<br>".
                     "GND: Ground pin<br>Digital output pin<br>Analog output pin</p>";
        $humidity .= "<img src='../code_generator/images/soil-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Light (LDR)"){
      $l++;
      if ($l==1){
        $humidity .= "<hr><div><h4><b>Light Dependent Resistor (LDR)</b></h4>";
        $humidity .= "<p>This picture shows the circuit of the LDR with the 10k ohm resistor. The analog input goes to an analog in pin.</p>";
        $humidity .= "<img src='../code_generator/images/ldr-ex.png'></div>";
      }
    }
    elseif ($sensorTypes[$i]=="Sound (digital)"){
      $s++;
      if ($s==1){
        $sound .= "<hr><div><h4><b>Sound Sensor</b></h4>";
        $sound .= "<p>This diagram shows the 4 pins of the sound sensor.<br>VCC: 3.3-5V DC for power<br>GND: Ground pin<br>".
                  "DO: Digital output pin<br>A0: Analog output pin</p>";
        $sound .= "<img src='../code_generator/images/sound-ex.png'></div>";
      }
    }
  }
  $diagrams .= "<div><h4><b>BeagleBone Black</b></h4>";
  $diagrams .= "<p>The diagrams show the different parts and pins of the BeagleBone Black board. Use the name of the column (P8 or P9) underscore the physical number of pins to build your circuit (e.g. P8_15).</p>";
  $diagrams .= "<img src='../code_generator/images/beaglebone-ex.png'>";
  $diagrams .= "<img src='../code_generator/images/beaglebone_pinout.png'></div>";
  $diagrams .= $motion . $temperature . $humidity . $light . $sound;
  $diagrams .= "</ul>";
  return $diagrams;
}

?>
