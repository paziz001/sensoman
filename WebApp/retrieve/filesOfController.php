<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["controllerID"])){
        $token = $_POST["token"];
        $userID= -1;
        $controllerID = $_POST["controllerID"];
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)){
              $sql = "SELECT * FROM `file`
          						WHERE `controller`=" . $controllerID;
              DBConnect();
              $result=execQuery($sql);
              DBClose();
              $files = array();
              while ($aRow=fetchNext($result)){
                $aFile=array("fileID" => $aRow["fileID"], "fileName" => $aRow["fileName"],
                                   "controller" => $aRow["controller"]);
                array_push($files, $aFile);
              }
              $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Successfully retrieved",
                "files" => $files
              );
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
              }
    }
  }
  else{
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have to provide your token and the controllerID."
    );
  }
}

echo json_encode($response);

?>
