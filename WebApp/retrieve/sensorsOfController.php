<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["controllerID"])){
        $token = $_POST["token"];
        $controllerID=$_POST["controllerID"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
            }
            if (checkToken($valid)){
              $sensors=array();
                //Find the sensor Info
                $sql = "SELECT * FROM `sensor-controller` sc
            						JOIN `sensor` s ON sc.`sensorID`=s.`sensorID`
            						WHERE `controllerID`=" . $controllerID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                while ($aRow=fetchNext($result)){
                    $aSensor=array("sensorID" => $aRow["sensorID"], "sensorName" => $aRow["sensorName"], "sensorProperty" => $aRow["sensorProperty"],
                                   "type" => $aRow["type"]);
                    array_push($sensors, $aSensor);
                }
                $response=array(
                  "status" => "success",
                  "request time" => $date->format("Y-m-d h:m:s"),
                  "IP Address" => $ip,
                  "message" => "Successfully retrieved",
                  "sensors" => $sensors
                );
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
              }
    }
  }
  else{
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have to provide your token and a controllerID."
    );
  }
}

echo json_encode($response);

?>
