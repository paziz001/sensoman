<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();


if ($method != 'POST') {

    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}  else {

	if (isset($_POST["token"]) && isset($_POST["ownerID"])) {
		
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $ownerID = $_POST["ownerID"];
              $modulesArr = array();
              $sharedBoards = array();
              $sql ="SELECT DISTINCT controllerID,controllerName, ip, port FROM sn_shared_boards INNER JOIN `micro-controller` on sn_shared_boards.boardID = `micro-controller`.`controllerID` WHERE ownerID = '".$ownerID."' AND watcherID ='".$userID."'";
               	DBConnect();
        		$result=execQuery($sql);
        		DBClose();
        		$num=rowCount($result);
        		if($num <1){
        			$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "There are no shared boards."
            );

        		} else {

        				$sql = "SELECT boardID, COUNT(boardID) as NUM_OF_MODULES FROM sn_shared_sensors GROUP BY boardID";
        				DBConnect();
        				$result2=execQuery($sql);
        				DBClose();
        				$num2=rowCount($result2);
        				if($num2 <1){
		        			$response=array(
				                "status" => "fail",
				                "request time" => $date->format("Y-m-d h:m:s"),
				                "IP Address" => $ip,
				                "message" => "There are no shared sensors."
				           			 );

		        			}

		        			else{
		        				
		        				while($aRow = fetchNext($result2)){
		        					$row1 = array();
		        					$row1["boardID"]=$aRow["boardID"];
		        					$row1["NUM_OF_MODULES"] = $aRow["NUM_OF_MODULES"];
		        					array_push($modulesArr, $row1);
		        				}




        			 			while($aRow=fetchNext($result)){
		        			 		$row = array();
		        			 		$row["controllerID"] = $aRow["controllerID"];
		        			 		$row["controllerName"] = $aRow["controllerName"];
		        			 		$row["IP"] = $aRow["ip"];
		        			 		$row["Port"] = $aRow["port"];
		        			 		foreach ($modulesArr as $mod) {
										   	if(strcmp ($mod["boardID"],$row["controllerID"]) == 0)
		                						$row["no_of_modules"] = $mod["NUM_OF_MODULES"];
										}
		                			
		                			array_push($sharedBoards, $row);
		            			}


				            			$response = array(
				                        "status" => "success",
				                        "request time" => $date->format("Y-m-d h:m:s"),
				                        "IP Address" => $ip,				          
				                        "message" => "Successfully retrieved shared boards.",				           
				                      	"sharedBoards" => $sharedBoards

				                    );


            		
            			

            		}
        		}

            } else {

            	 $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token."
            		);
            	
            	}



        }
    } else {
    		 $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Invalid Credentials."
            		);


    }
	






} //ELSE POST
echo json_encode($response);
?>