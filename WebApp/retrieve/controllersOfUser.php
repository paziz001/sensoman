<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])){
        $token = $_POST["token"];
        $userID= -1;
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)){
              $controllers=array();
                $sql = "SELECT * FROM `user-controller` uc
            						JOIN `micro-controller` c ON uc.`controllerID`=c.`controllerID`
            						WHERE `userID`=" . $userID;
                DBConnect();
                $result=execQuery($sql);
                while ($aRow=fetchNext($result)){
                  $sql2 = "SELECT * FROM `sensor-controller` sc
              						JOIN `sensor` s ON sc.`sensorID`=s.`sensorID`
              						WHERE `controllerID`=" . $aRow["controllerID"];
                  $result2=execQuery($sql2);
                  $numSensors = rowCount($result2);
                  $sql3 = "SELECT * FROM `file`
              						WHERE `controller`=" . $aRow["controllerID"];
                  $result3=execQuery($sql3);
                  $numFiles = rowCount($result3);
                  $aController=array("controllerID" => $aRow["controllerID"], "controllerName" => $aRow["controllerName"],
                                     "controllerType" => $aRow["type"], "numSensors" => $numSensors, "numFiles" => $numFiles);
                  array_push($controllers, $aController);
                  }
                  DBClose();
                  $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Successfully retrieved",
                    "controllers" => $controllers
                  );
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
              }
    }
  }
  else{
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have to provide your token."
    );
  }
}

echo json_encode($response);

?>
