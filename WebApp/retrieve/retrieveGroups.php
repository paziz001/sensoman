<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$ownerHasGroups = 0;
$group_names = array();
$owner_names = array();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
} else {
	  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }

if($token_flag == true){
	//retrieve all groups that user owns
	$sql="SELECT name FROM sn_group WHERE ownerID = '".$userID."'";
	DBConnect();
	$result=execQuery($sql);
	DBClose();

	$num=rowCount($result);
	if ($num<1){
        $ownerHasGroups = 0;
        }
        else{
        	//store all groups
        		while($aRow=fetchNext($result)){
 						$name = $aRow["name"];
                        array_push($owner_names,$name);
        		}
        		$ownerHasGroups = 1; //there are group that user owns
        	}
        	//retrieve all groups that user has joined them
        	$sql="SELECT sn_group.name, sn_group.ownerID FROM `sn_user-group` INNER JOIN sn_group on `sn_user-group`.groupID = sn_group.groupID WHERE `sn_user-group`.userID ='".$userID."' AND sn_group.ownerID <> '".$userID."'";
			DBConnect();
			$result=execQuery($sql);
			DBClose();
			$num2=rowCount($result);
			//$num2 = 0;
			if ($num2<1){
				if($ownerHasGroups == 1){
					$response = array(
                	"status" => "success",
                	"IP Address" => $ip,
                	"request time" => $date->format("Y-m-d h:m:s"),
                	"ownerGroups" => $owner_names,
                	"message" => "Successfully retrieved owner's groups."
            		);
				} else{
					$response = array(
                	"status" => "fail",
                	"IP Address" => $ip,
                	"request time" => $date->format("Y-m-d h:m:s"),
                	"message" => "There are no groups associated with you."
            		);
				}

       	 } else{
				$group_names = array();
        	//store all groups
        		while($aRow=fetchNext($result)){
 						$name = $aRow["name"];
                        array_push($group_names,$name);
        		}
        		if($ownerHasGroups == 1){
        		$response = array(
                	"status" => "success",
                	"IP Address" => $ip,
                	"request time" => $date->format("Y-m-d h:m:s"),
                	"ownerGroups" => $owner_names,
                	"groups" => $group_names,
                	"message" => "Successfully retrieved both owner's and associated groups"
            		);
        	} else{
        			$response = array(
                	"status" => "success",
                	"IP Address" => $ip,
                	"request time" => $date->format("Y-m-d h:m:s"),
                	"groups" => $group_names,
                	"message" => "Successfully retrieved associated groups");
        		}
       	 	}

	}

} //else POST

echo json_encode($response);
?>