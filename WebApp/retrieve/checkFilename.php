<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["filename"])){
        $token = $_POST["token"];
        $filename = $_POST["filename"];
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)){
              $sql = "SELECT fileName FROM `user-file` uf
                      JOIN `file` f ON uf.`fileID`=f.`fileID`
                      WHERE `userID`=" . $userID;
              DBConnect();
              $result=execQuery($sql);
              DBClose();
              $num=rowCount($result);
              $exists = false;
              while ($aRow=fetchNext($result)){
                $temp = explode(".",$aRow["fileName"]);
                $file = $temp[0];
                if ($file == $filename){
                  $exists = true;
                  break;
                }
              }
                if ($exists==false){
                  $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "No file with this name."
                  );
                }
                else{
                  $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Filename already exists."
                  );
                }
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
              }
    }
  }
  else{
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have to provide your token and filename."
    );
  }
}

echo json_encode($response);

?>
