<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 17/12/2014
 * Time: 12:37 πμ
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$date=new DateTime('now');
$method=getRequestMethod();
$ip=getClientIP();


if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])){
        $token = $_POST["token"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
            }
            if (checkToken($valid)){
                //Find the measurements
                $sql="SELECT * FROM `sensor-measurement`";
                DBConnect();
                $resultMeasurements=execQuery($sql);
                DBClose();
                $numMeasurements=rowCount($resultMeasurements);
                $sql="SELECT * FROM motion";
                DBConnect();
                $resultMotion=execQuery($sql);
                DBClose();
                $numOfMotion=rowCount($resultMotion);
                if (($numOfMotion+$numMeasurements)<1){
                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "None log is found."
                    );
                }
                else{
                    $logs=array();
                    while ($aRow=fetchNext($resultMeasurements)){
                        $row=array();
                        $row["Measurement ID"]= $aRow["measurementID"];
                        $row["Sensor ID"]= $aRow["sensorID"];
                        $sql="SELECT * FROM sensor, `type-sensor`  WHERE sensorID=".$row["Sensor ID"]." AND sensorProperty=typeSensorID";
                        DBConnect();
                        $resultSensor=execQuery($sql);
                        DBClose();
                        while ($a=fetchNext($resultSensor)){
                            $row["Sensor Name"]=$a["sensorName"];
                            $row["Sensor Type"]=$a["type"];
                        }
                        $sql="SELECT * FROM `sensor-controller` WHERE sensorID=".$row["Sensor ID"]. " AND (('".$aRow["datetime"]."'>=dateStart AND active=1) OR ('".$aRow["datetime"]."'>=dateStart AND dateEnd>='".$aRow["datetime"]."'))";
                        DBConnect();
                        $resC=execQuery($sql);
                        DBClose();
                        while ($a=fetchNext($resC)){
                            $row["Controller ID"]=$a["controllerID"];
                        }
                        $sql="SELECT * FROM `micro-controller` WHERE controllerID=".$row["Controller ID"];
                        DBConnect();
                        $resC=execQuery($sql);
                        DBClose();
                        while($a=fetchNext($resC)){
                            $row["Controller Name"]=$a["controllerName"];
                        }
                        $sql="SELECT * FROM `location-controller`, location WHERE controllerID=".$row["Controller ID"]. " AND (('".$aRow["datetime"]."'>=dateStart AND active=1) OR ('".$aRow["datetime"]."'>=dateStart AND dateEnd>='".$aRow["datetime"]."')) AND `location-controller`.locationID=location.locationID";
                        DBConnect();
                        $resLocation=execQuery($sql);
                        DBClose();
                        while($a=fetchNext($resLocation)){
                            $row["Latitude"]=$a["latitube"];
                            $row["Longitude"]=$a["longitube"];
                        }
                        $row["Measurement"]=$aRow["measurement"];
                        $row["Timestamp"]=$aRow["datetime"];
                        array_push($logs, $row);
                    }
                    while($aRow=fetchNext($resultMotion)){
                        $row=array();
                        $row["Measurement ID"]= "M".$aRow["motionID"];
                        $row["Sensor ID"]= $aRow["sensorID"];
                        $sql="SELECT * FROM sensor, `type-sensor`  WHERE sensorID=".$row["Sensor ID"]." AND sensorProperty=typeSensorID";
                        DBConnect();
                        $resultSensor=execQuery($sql);
                        DBClose();
                        while ($a=fetchNext($resultSensor)){
                            $row["Sensor Name"]=$a["sensorName"];
                            $row["Sensor Type"]=$a["type"];
                        }
                        $sql="SELECT * FROM `sensor-controller` WHERE sensorID=".$row["Sensor ID"]. " AND (('".$aRow["datetimeStart"]."'>=dateStart AND active=1) OR ('".$aRow["datetimeStart"]."'>=dateStart AND dateEnd>='".$aRow["datetimeEnd"]."'))";
                        DBConnect();
                        $resC=execQuery($sql);
                        DBClose();
                        while ($a=fetchNext($resC)){
                            $row["Controller ID"]=$a["controllerID"];
                        }
                        $sql="SELECT * FROM `micro-controller` WHERE controllerID=".$row["Controller ID"];
                        DBConnect();
                        $resC=execQuery($sql);
                        DBClose();
                        while($a=fetchNext($resC)){
                            $row["Controller Name"]=$a["controllerName"];
                        }
                        $sql="SELECT * FROM `location-controller`, location WHERE controllerID=".$row["Controller ID"]. " AND (('".$aRow["datetimeStart"]."'>=dateStart AND active=1) OR ('".$aRow["datetimeStart"]."'>=dateStart AND dateEnd>='".$aRow["datetimeEnd"]."')) AND `location-controller`.locationID=location.locationID";
                        DBConnect();
                        $resLocation=execQuery($sql);
                        DBClose();
                        while($a=fetchNext($resLocation)){
                            $row["Latitude"]=$a["latitube"];
                            $row["Longitude"]=$a["longitube"];
                        }
                        $row["Timestamp Start"]=$aRow["datetimeStart"];
                        $row["Timestamp End"]=$aRow["datetimeEnd"];
                        array_push($logs, $row);
                    }

                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Successfully retrieved.",
                        "count" => count($logs),
                        "history Logs" => $logs

                    );
                }
            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{
        if(!isset($_POST["token"])){
            $response = array(
                "status" => "expired token",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "The token has expired"
            );
        }else {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide your current token."
            );
        }
    }
}
echo json_encode($response);
