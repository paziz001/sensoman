<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])){
        $token = $_POST["token"];
        $userID= -1;
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)){
              $files=array();
                $sql = "SELECT * FROM `user-file` uf
            						JOIN `file` f ON uf.`fileID`=f.`fileID`
                        JOIN `micro-controller` c ON f.`controller`=c.`controllerID`
            						WHERE `userID`=" . $userID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                while ($aRow=fetchNext($result)){
                    DBConnect();
                    $tokens = explode(";",$aRow["sensors"]);
                    $sensors= array();
                    for ($i=0; $i<sizeof($tokens); $i++){
                      $sql = "SELECT sensorName, type, sensorID FROM `sensor`
                              WHERE `sensorID`=" . $tokens[$i];
                      $result2=execQuery($sql);
                      $row=fetchNext($result2);
                      array_push($sensors, array("sensorID" => $row["sensorID"], "sensorName" => $row["sensorName"], "sensorType" => $row["type"]));
                    }
                    $aFile=array("fileID" => $aRow["fileID"], "fileName" => $aRow["fileName"],
                                 "controllerType" => $aRow["type"],
                                 "sensors" => $sensors, "pins" => $aRow["pins"],
                                 "controllerName" => $aRow["controllerName"], "internet" => $aRow["internet"],
                                 "timeInterval" => $aRow["timeInterval"]);
                    array_push($files, $aFile);
                  }
                  $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Successfully retrieved",
                    "files" => $files
                  );
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
              }
    }
  }
  else{
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have to provide your token."
    );
  }
}

echo json_encode($response);

?>
