<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 7/2/2015
 * Time: 8:16 μμ
 */
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])){
        $token = $_POST["token"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
            }
            if (checkToken($valid)){
                //Find the types
                DBConnect();
                $resultProperties = execQuery("SELECT * FROM datasensors.`type-sensor`");
                DBClose();
                $numberOfRecords=rowCount($resultProperties);
                $prop = array();
                while ($aRow = fetchNext($resultProperties)) {
                    $row = array();
                    $row["Type ID"]= $aRow["typeSensorID"];
                    $row["Property"] = $aRow["type"];
                    $row["Counter"] = $aRow["counter"];
                    array_push($prop, $row);
                }

                $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Successfully retrieved.",
                    "count" => $numberOfRecords,
                    "result" =>$prop
                );

            }
            else{
                $response=array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{

        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token."
        );
    }
}
echo json_encode($response);

/*include_once('../includes/common.php');
require_once('../includes/connectdb.php');

DBConnect();

$resultProperties = execQuery("SELECT * FROM datasensors.`type-sensor`");

$numberOfRecords=rowCount($resultProperties);

$prop = array();
while ($aRow = fetchNext($resultProperties)) {
    $row = array();
    $row["ID"]= $aRow["typeSensorID"];
    $row["Property"] = $aRow["type"];
    $row["Counter"] = $aRow["counter"];

    array_push($prop, $row);
}

$result=array(
    "status" => "success",
    "message" => "Successfully retrieved.",
    "count" => $numberOfRecords,
    "result" =>$prop
);
DBClose();
echo json_encode($result);*/