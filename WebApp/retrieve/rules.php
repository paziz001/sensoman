<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

$method=getRequestMethod();
$date=new DateTime('now');

if ($method != 'POST') {

    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}  else {
    if (isset($_POST["token"])&&isset($_POST["username"])) {
        $token = $_POST["token"];
        $username=$_POST["username"];
        //Check the validation of the token
        /*$db=DBConnect();
        $stmt=$db->prepare("SELECT * FROM token WHERE token= ?");
        $stmt->bind_param('s',$token);
        $stmt->execute();
        $resultToken = $stmt->get_result();
        DBClose();*/

        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "message" => "You have to provide a valid token."
            );
        } else{
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)) {
                //Find the type of User
                //Find the controller Info
                $rules=array();
                /*$db=DBConnect();
                $stmt=$db->prepare("SELECT * FROM `rule`
                            WHERE `rule`.`userID`=?");
                $stmt->bind_param('i',$userID);
                $stmt->execute();
                $result = $stmt->get_result();*/
                $sql ="SELECT * FROM `rule`
                            WHERE `rule`.`userID`=?";
                DBConnect();
                $result=execQuery($sql);
                DBClose();            
                $numRuleCont = rowCount($result);
                if($numRuleCont<1){
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "No rules found for $username."
                    );
                }else {
                    while ($aRow = fetchNext($result)) {
                        array_push($rules, $aRow);
                    }

                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "Successfully retrieved rules",
                        "count" => count($rules),
                        "rules" => $rules
                    );
                }
            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }else {

        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);