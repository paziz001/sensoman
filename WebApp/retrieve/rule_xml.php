<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

$method=getRequestMethod();
$date=new DateTime('now');

if ($method != 'POST') {

    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}  else {
    if (isset($_POST["token"])&&isset($_POST["username"])&&isset($_POST["ruleID"])) {
        $token = $_POST["token"];
        $username=$_POST["username"];
        //Check the validation of the token
        $db=DBConnect();
        $stmt=$db->prepare("SELECT * FROM token WHERE token= ?");
        $stmt->bind_param('s',$token);
        $stmt->execute();
        $resultToken = $stmt->get_result();
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "message" => "You have to provide a valid token."
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)) {
                //Find the type of User
                //Find the controller Info
                $ruleID = $_POST["ruleID"];
                $db=DBConnect();
                $stmt=$db->prepare("SELECT `definition` FROM `rule` WHERE `rule`.`ruleID` = ?");
                $stmt->bind_param('i',$ruleID);
                $stmt->execute();
                $result = $stmt->get_result();
                if(rowCount($result)<1){
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "The rule was not found"
                    );
                }else {
                    while ($aRow = fetchNext($result)) {
                        $rule = $aRow['definition'];
                    }

                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "Successfully retrieved rule",
                        "rule_definition" => $rule
                    );
                }
            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }elseif(!isset($_POST["ruleID"])){
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "message" => "You have to provide a rule id."
        );
    }
    else {
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);