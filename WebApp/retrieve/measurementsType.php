<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 19/3/2015
 * Time: 9:23 μμ
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

$date = new DateTime('now');
$method = getRequestMethod();
$ip = getClientIP();

if ($method != 'POST') {
    $response = array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '" . $method . " " . $_SERVER['REQUEST_URI'] . "'"
    );
} else {
    if ((isset($_POST["token"])) && isset($_POST["sensorType"])) {
        $token = $_POST["token"];
        $sensorType = $_POST["sensorType"];
        //Check the validation of the token
        $sql = "SELECT * FROM token WHERE token='" . $token . "'";
        DBConnect();
        $resultToken = execQuery($sql);
        DBClose();
        $numToken = rowCount($resultToken);
        if (false && $numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)) {
                //Find the measurements
                $sql = "SELECT * FROM `type-sensor` WHERE type='" . $sensorType . "'";
                DBConnect();
                $resultType = execQuery($sql);
                DBClose();
                while ($aRow = fetchNext($resultType)) {
                    $typeID = $aRow["typeSensorID"];
                }
                $sql = "SELECT * FROM `sensor` WHERE sensorProperty=" . $typeID;
                DBConnect();
                $resultSensors = execQuery($sql);
                DBClose();
                $sensorsID = array();
                while ($aRow = fetchNext($resultSensors)) {
                    $sensorID = $aRow["sensorID"];
                    array_push($sensorsID, $sensorID);
                }
                $counter = 0;
                $sensors = array();
                $sensor = array();
                while ($counter < count($sensorsID)) {
                    $sql = "SELECT * FROM sensor  WHERE sensorID=" . $sensorsID[$counter];
                    DBConnect();
                    $resultSensor = execQuery($sql);
                    DBClose();
                    $sensor["SensorID"] = $sensorsID[$counter];
                    while ($a = fetchNext($resultSensor)) {
                        $sensor["SensorName"] = $a["sensorName"];
                    }
                    $db=DBConnect();
                    if (isset($_POST["numMeasurements"])) {
                        $sql = "SELECT * FROM `sensor-measurement` WHERE `sensor-measurement`.sensorID='".$sensorsID[$counter]."' ORDER BY datetime DESC LIMIT '".$_POST["numMeasurements"]."'";

                       // $sql = "SELECT * FROM `sensor-measurement` WHERE `sensor-measurement`.sensorID=? ORDER BY datetime DESC LIMIT ?";
                       // $stmt=$db->prepare($sql);
                       // $stmt->bind_param('ii',$sensorsID[$counter],$_POST["numMeasurements"]);
                    } else {
                         $sql = "SELECT * FROM `sensor-measurement` WHERE `sensor-measurement`.sensorID= '".$sensorsID[$counter]."' ORDER BY datetime DESC LIMIT 7";
                        
                       // $sql = "SELECT * FROM `sensor-measurement` WHERE `sensor-measurement`.sensorID= ? ORDER BY datetime DESC LIMIT 7";
                        //$stmt=$db->prepare($sql);
                       // $stmt->bind_param('i',$sensorsID[$counter]);
                    }



                    DBConnect();
                    $resultMeasurements = execQuery($sql);
                    DBClose();

                    //$stmt->execute();
                    //$resultMeasurements = $stmt->get_result();
                    $numMeasurements = rowCount($resultMeasurements);
                    $logsSensor = array();
                    if ($numMeasurements > 0) {
                        while ($aRow = fetchNext($resultMeasurements)) {
                            $row = array();
                            $row["MeasurementID"] = $aRow["measurementID"];
                            $row["SensorID"] = $aRow["sensorID"];
                            $sql = "SELECT * FROM `sensor-controller` WHERE sensorID=" . $sensor["SensorID"] . " AND (('" . $aRow["datetime"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetime"] . "'>=dateStart AND dateEnd>='" . $aRow["datetime"] . "'))";
                            DBConnect();
                            $resC = execQuery($sql);
                            while ($a = fetchNext($resC)) {
                                $row["ControllerID"] = $a["controllerID"];
                            }
                            $sql = "SELECT * FROM `micro-controller` WHERE controllerID=" . $row["ControllerID"];
                            DBConnect();
                            $resC = execQuery($sql);
                            while ($a = fetchNext($resC)) {
                                $row["ControllerName"] = $a["controllerName"];
                            }
                            $sql = "SELECT * FROM `location-controller`, location WHERE controllerID=" . $row["ControllerID"] . " AND (('" . $aRow["datetime"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetime"] . "'>=dateStart AND dateEnd>='" . $aRow["datetime"] . "')) AND `location-controller`.locationID=location.locationID";
                            DBConnect();
                            $resLocation = execQuery($sql);
                            while ($a = fetchNext($resLocation)) {
                                $row["Latitude"] = $a["latitude"];
                                $row["Longitude"] = $a["longitude"];
                            }
                            $row["Measurement"] = $aRow["measurement"];
                            $row["Timestamp"] = $aRow["datetime"];
                            array_push($logsSensor, $row);
                        }
                    } else {
                        if (isset($_POST["numMeasurements"])) {
                            $sql = "SELECT * FROM motion WHERE motion.sensorID=" . $sensorsID[$counter] . " ORDER BY datetimeEnd DESC LIMIT " . $_POST["numMeasurements"];
                        } else {
                            $sql = "SELECT * FROM motion WHERE motion.sensorID=" . $sensorsID[$counter];
                        }

                        DBConnect();
                        $resultMotion = execQuery($sql);
                        $numOfMotion = rowCount($resultMotion);
                        while ($aRow = fetchNext($resultMotion)) {
                            $row = array();
                            $row["MeasurementID"] = "M" . $aRow["motionID"];
                            $sql = "SELECT * FROM `sensor-controller` WHERE sensorID=" . $sensor["SensorID"] . " AND (('" . $aRow["datetimeStart"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetimeStart"] . "'>=dateStart AND dateEnd>='" . $aRow["datetimeEnd"] . "'))";
                            DBConnect();
                            $resC = execQuery($sql);
                            while ($a = fetchNext($resC)) {
                                $row["ControllerID"] = $a["controllerID"];
                            }
                            $sql = "SELECT * FROM `micro-controller` WHERE controllerID=" . $row["ControllerID"];
                            DBConnect();
                            $resC = execQuery($sql);
                            while ($a = fetchNext($resC)) {
                                $row["ControllerName"] = $a["controllerName"];
                            }
                            $sql = "SELECT * FROM `location-controller`, location WHERE controllerID=" . $row["ControllerID"] . " AND (('" . $aRow["datetimeStart"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetimeStart"] . "'>=dateStart AND dateEnd>='" . $aRow["datetimeEnd"] . "')) AND `location-controller`.locationID=location.locationID";
                            DBConnect();
                            $resLocation = execQuery($sql);
                            while ($a = fetchNext($resLocation)) {
                                $row["Latitude"] = $a["latitude"];
                                $row["Longitude"] = $a["longitude"];
                            }
                            $row["TimestampStart"] = $aRow["datetimeStart"];
                            $row["TimestampEnd"] = $aRow["datetimeEnd"];
                            array_push($logsSensor, $row);
                        }
                    }
                    $counter++;
                    $sensor["NumberOfMeasurements"] = count($logsSensor);
                    $sensor["Measurements"] = $logsSensor;
                    array_push($sensors, $sensor);
                }

                DBClose();


                $response = array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Successfully retrieved.",
                    "SensorCount" => count($sensors),
                    "Sensors" => $sensors

                );

                //added by angelos
               
                $n_sensors  = array();
                foreach ($sensors as $sensor) {
                    $sql ="SELECT  controllerID from `sensor-controller` WHERE `sensor-controller`.sensorID = '".$sensor["SensorID"]."' AND  `sensor-controller`.active = '1' ";
                     DBConnect();
                     $result = execQuery($sql);
                     DBClose();

                     while($aRow=fetchNext($result)){
                        $controllerID = $aRow["controllerID"]; 
                     }

                     //shared sensors
                     $sql = "SELECT watcherID FROM `sn_shared_sensors` WHERE `sn_shared_sensors`.sensorID = '".$sensor["SensorID"]."' ";
                     DBConnect();
                     $result = execQuery($sql);
                     DBClose();
                     $watcherID = -1;
                      while($aRow=fetchNext($result)){
                        $watcherID = $aRow["watcherID"]; 
                     }
                     
                     //both sensors
                    $sql ="SELECT * FROM `user-controller` WHERE `user-controller`.controllerID = '".$controllerID."' AND `user-controller`.userID = '".$userID."'";

                    DBConnect();
                     $result = execQuery($sql);
                     DBClose();
                      $num = rowCount($result);


                      if($num > 0 || $watcherID == $userID){ //  user owns the sensor or user just shares the sensor
                        array_push($n_sensors,$sensor); //store measurements

                      }


                } //for

               // $sensors = array_merge(array(), $n_sensors);
                /* echo json_encode($n_sensors);
                exit();*/
                $response = array(
                                    "status" => "success",
                                    "request time" => $date->format("Y-m-d h:m:s"),
                                    "IP Address" => $ip,
                                    "message" => "Successfully retrieved.",
                                    "SensorCount" => count($sensors),
                                    "Sensors" => $n_sensors

                                );

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                                

            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    } else {
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token and the sensorID."
        );
    }



}


echo json_encode($response);
//echo json_encode($sensors[0]["SensorID"]);

