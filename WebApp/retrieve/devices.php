<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {

    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}  else {
    if (isset($_POST["token"])&&isset($_POST["controllerID"])) {
        $token = $_POST["token"];
        $controllerID=$_POST["controllerID"];
        //Check the validation of the token
        $db=DBConnect();
        $stmt=$db->prepare("SELECT * FROM token WHERE token= ?");
        $stmt->bind_param('s',$token);
        $stmt->execute();
        $resultToken = $stmt->get_result();
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token."
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)) {
                //Find the type of User
                //Find the controller Info
                $devices=array();
                $db=DBConnect();
                $stmt=$db->prepare("SELECT deviceName FROM `device-actuator`,`device`,`actuator`,`actuator-controller`
                WHERE `actuator-controller`.actuatorID = `actuator`.actuatorID AND
                `device-actuator`.`actuatorID` = `actuator`.`actuatorID` AND
                 `device-actuator`.`deviceID` = `device`.`deviceID` AND
                 `actuator-controller`.`actuatorID` = `actuator`.`actuatorID` AND
                 `actuator-controller`.controllerID = ?");
                $stmt->bind_param('i',$controllerID);
                $stmt->execute();
                $devicesResult = $stmt->get_result();
                $numOfdevices = rowCount($devicesResult);
                if($numOfdevices<1){
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "No devices found."
                    );
                }else {
                    while ($aRow = fetchNext($devicesResult)) {
                        $row = array();
                        $deviceName = $aRow["deviceName"];
                        array_push($devices, $deviceName);
                    }

                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Successfully retrieved devices",
                        "count" => count($devices),
                        "devices" => $devices
                    );
                }
            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    } else {

        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);