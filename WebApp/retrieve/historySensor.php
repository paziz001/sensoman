<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 24/2/2015
 * Time: 9:10 μμ
 */
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$date=new DateTime('now');
$method=getRequestMethod();
$ip=getClientIP();


if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if ((isset($_POST["token"])) && isset($_POST["sensorID"])){
        $token = $_POST["token"];
        $sensorID=$_POST["sensorID"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
            }
            if (checkToken($valid)){
                //Find the measurements a
                if (isset($_POST["numMeasurements"])) {
                    $sql="SELECT * FROM `sensor-measurement` WHERE `sensor-measurement`.sensorID=".$sensorID. " ORDER BY datetime DESC LIMIT ". $_POST["numMeasurements"];
                }else{
                    $sql="SELECT * FROM `sensor-measurement` WHERE `sensor-measurement`.sensorID=".$sensorID;
                }

                DBConnect();
                $resultMeasurements=execQuery($sql);
                DBClose();
                $numMeasurements=rowCount($resultMeasurements);
                $logs = array();
                if ($numMeasurements>0) {
                    while ($aRow = fetchNext($resultMeasurements)) {
                        $row = array();
                        $row["Measurement ID"] = $aRow["measurementID"];
                        $row["Sensor ID"] = $aRow["sensorID"];
                        $sql = "SELECT * FROM sensor, `type-sensor`  WHERE sensorID=" . $row["Sensor ID"] . " AND sensorProperty=typeSensorID";
                        DBConnect();
                        $resultSensor = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resultSensor)) {
                            $row["Sensor Name"] = $a["sensorName"];
                            $row["Sensor Type"] = $a["type"];
                        }
                        $sql = "SELECT * FROM `sensor-controller` WHERE sensorID=" . $row["Sensor ID"] . " AND (('" . $aRow["datetime"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetime"] . "'>=dateStart AND dateEnd>='" . $aRow["datetime"] . "'))";
                        DBConnect();
                        $resC = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resC)) {
                            $row["Controller ID"] = $a["controllerID"];
                        }
                        $sql = "SELECT * FROM `micro-controller` WHERE controllerID=" . $row["Controller ID"];
                        DBConnect();
                        $resC = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resC)) {
                            $row["Controller Name"] = $a["controllerName"];
                        }
                        $sql = "SELECT * FROM `location-controller`, location WHERE controllerID=" . $row["Controller ID"] . " AND (('" . $aRow["datetime"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetime"] . "'>=dateStart AND dateEnd>='" . $aRow["datetime"] . "')) AND `location-controller`.locationID=location.locationID";
                        DBConnect();
                        $resLocation = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resLocation)) {
                            $row["Latitude"] = $a["latitube"];
                            $row["Longitude"] = $a["longitube"];
                        }
                        $row["Measurement"] = $aRow["measurement"];
                        $row["Timestamp"] = $aRow["datetime"];
                        array_push($logs, $row);
                    }
                }
                else {
                    if (isset($_POST["numMeasurements"])){
                        $sql = "SELECT * FROM motion WHERE motion.sensorID=" . $sensorID." ORDER BY datetimeEnd DESC LIMIT " . $_POST["numMeasurements"];
                    }else{
                        $sql = "SELECT * FROM motion WHERE motion.sensorID=" . $sensorID;
                    }
                    DBConnect();
                    $resultMotion = execQuery($sql);
                    DBClose();
                    $numOfMotion = rowCount($resultMotion);
                    while ($aRow = fetchNext($resultMotion)) {
                        $row = array();
                        $row["Measurement ID"] = "M" . $aRow["motionID"];
                        $row["Sensor ID"] = $aRow["sensorID"];
                        $sql = "SELECT * FROM sensor, `type-sensor`  WHERE sensorID=" . $row["Sensor ID"] . " AND sensorProperty=typeSensorID";
                        DBConnect();
                        $resultSensor = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resultSensor)) {
                            $row["Sensor Name"] = $a["sensorName"];
                            $row["Sensor Type"] = $a["type"];
                        }
                        $sql = "SELECT * FROM `sensor-controller` WHERE sensorID=" . $row["Sensor ID"] . " AND (('" . $aRow["datetimeStart"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetimeStart"] . "'>=dateStart AND dateEnd>='" . $aRow["datetimeEnd"] . "'))";
                        DBConnect();
                        $resC = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resC)) {
                            $row["Controller ID"] = $a["controllerID"];
                        }
                        $sql = "SELECT * FROM `micro-controller` WHERE controllerID=" . $row["Controller ID"];
                        DBConnect();
                        $resC = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resC)) {
                            $row["Controller Name"] = $a["controllerName"];
                        }
                        $sql = "SELECT * FROM `location-controller`, location WHERE controllerID=" . $row["Controller ID"] . " AND (('" . $aRow["datetimeStart"] . "'>=dateStart AND active=1) OR ('" . $aRow["datetimeStart"] . "'>=dateStart AND dateEnd>='" . $aRow["datetimeEnd"] . "')) AND `location-controller`.locationID=location.locationID";
                        DBConnect();
                        $resLocation = execQuery($sql);
                        DBClose();
                        while ($a = fetchNext($resLocation)) {
                            $row["Latitude"] = $a["latitube"];
                            $row["Longitude"] = $a["longitube"];
                        }
                        $row["Timestamp Start"] = $aRow["datetimeStart"];
                        $row["Timestamp End"] = $aRow["datetimeEnd"];
                        array_push($logs, $row);
                    }
                }
                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Successfully retrieved.",
                        "count" => count($logs),
                        "history Logs" => $logs

                    );

            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token and the sensorID."
        );
    }
}
echo json_encode($response);