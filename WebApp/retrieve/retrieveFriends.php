<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = $first_fail = $second_fail = 0;

  if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
} else {
	  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }

  	if($token_flag == true){
		//retreive all friends
		//check both columns of table sn_association for userID
		//first check sn_association.userID1
		$sql="SELECT user.username, user.userID FROM sn_association INNER JOIN user on sn_association.userID2 = user.userID WHERE sn_association.userID1='".$userID."'";
		DBConnect();
        $result=execQuery($sql);
        DBClose();
        $numToken=rowCount($result);
        if ($numToken<1){
        	$first_fail = true;
        }
        //check sn_association.userID2
        $sql="SELECT user.username, user.userID FROM sn_association INNER JOIN user on sn_association.userID1 = user.userID WHERE sn_association.userID2='".$userID."'";
		DBConnect();
        $result2=execQuery($sql);
        DBClose();
        $numToken=rowCount($result2);
        if ($numToken<1){
        	$second_fail = true;
        }


	}
	//if there are entries 
	if($first_fail == false || $second_fail == false){
		    $names = array();
        	$userIDs = array();
        	$counter = 0;
        	//store all the usernames and userIDs that user is associated with
        		while($aRow=fetchNext($result)){

        			$names[$counter] = $aRow["username"];
        			$userIDs[$counter] = $aRow["userID"];
        			$counter = $counter +1;
        			
        		}
        		while($aRow=fetchNext($result2)){

        			$names[$counter] = $aRow["username"];
        			$userIDs[$counter] = $aRow["userID"];
        			$counter = $counter +1;
        		}
        		$response=array(
        	"status" => "success",
        	"request time" => $date->format("Y-m-d h:m:s"),
        	"IP Address" => $ip,
        	"Usernames" => $names,
        	"UserIDs" => $userIDs,
        	"message" => "Successfully retrieved associated members."
    	);

	}
	else {
		 $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "There are no associated members with you."
            );
	}

}
echo json_encode($response);
?>