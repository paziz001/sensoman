<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 6/2/2015
 * Time: 8:57 μμ
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])) {
        $token = $_POST["token"];
        //Check the validation of the token
        $sql = "SELECT * FROM token WHERE token='" . $token . "'";
        DBConnect();
        $resultToken = execQuery($sql);
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            $sql = "SELECT * FROM user WHERE userID=" . $userID;
            DBConnect();
            $resU = execQuery($sql);
            DBClose();
            while ($aRow = fetchNext($resU)) {
                $userType = $aRow["type"];
            }
            if (checkToken($valid)) {
                if ((strcmp($userType, "admin") == 0) || (strcmp($userType, "simple") == 0)) {
                    $sql = "SELECT * FROM `user-controller`";
                } else {
                    $sql = "SELECT * FROM `user-controller` WHERE userID=" . $userID;
                }
                DBConnect();
                $resUserController = execQuery($sql);
                DBClose();
                $micro = array();
                while ($aRow = fetchNext($resUserController)) {
                    $microID = $aRow["controllerID"];
                    array_push($micro, $microID);
                }
                $j = 0;
                $location = array();
                while ($j < count($micro)) {
                    $mID = $micro[$j];
                    $sql = "SELECT * FROM `location-controller` WHERE controllerID=" . $mID . " AND active=1 AND admin=0";
                    DBConnect();
                    $resLC = execQuery($sql);
                    DBClose();
                    if (rowCount($resLC) > 0) {
                        while ($aRow = fetchNext($resLC)) {
                            $lID = $aRow["locationID"];
                            array_push($location, $lID);
                        }
                    }
                    $j++;
                }
                $location=array_unique($location);
                //Continue to find the controllers
                //find the active controllers in the distance
                if (count($location) < 1) {
                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "None sensor was found"
                    );
                } else {
                    $i = 0;
                    //find Controllers
                    $controllers = array();
                    $counter = 0;
                    $locationInfo=array();
                    while ($counter < count($location)) {
                        $locID = $location[$counter];
                        $sql="SELECT * FROM location WHERE locationID=".$locID;
                        DBConnect();
                        $r=execQuery($sql);
                        DBClose();
                        while ($aRow=fetchNext($r)){
                            $locationInfo[$counter]["locationID"]=$locID;
                            $locationInfo[$counter]["longitude"]=$aRow["longitube"];
                            $locationInfo[$counter]["latitude"]=$aRow["latitube"];
                        }

                        if ((strcmp($userType, "admin") == 0) || (strcmp($userType, "simple") == 0)) {
                            $sql = "SELECT * FROM `location-controller` WHERE locationID=" . $locID . " AND active=1 AND admin=0";
                        } else {
                            $sql = "SELECT * FROM `location-controller`,`user-controller` WHERE locationID=" . $locID . " AND active=1 AND admin=0 AND `user-controller`.userID=" . $userID . " AND `user-controller`.controllerID=`location-controller`.controllerID";
                        }

                        DBConnect();
                        $resultContr = execQuery($sql);
                        $rec = rowCount($resultContr);
                        DBClose();
                        if ($rec > 0) {
                            while ($aRow = fetchNext($resultContr)) {
                                $contrID = $aRow["controllerID"];
                                array_push($controllers, $contrID);
                            }
                        }
                        $i = 0;
                        $contr = array();
                        $sensorMeasurements = array();
                        while ($i < count($controllers)) {
                            $conID = $controllers[$i];
                            $sensors = array();
                            $sql = "SELECT * FROM `sensor-controller` WHERE controllerID=" . $conID . " AND active=1 AND deactivated=0";
                            DBConnect();
                            $resultSen = execQuery($sql);
                            DBClose();
                            $recS = rowCount($resultSen);
                            //Find the sensors of a controller
                            if ($recS > 0) {
                                while ($aRow = fetchNext($resultSen)) {
                                    $r = array();
                                    $r["senID"] = $aRow["sensorID"];
                                    $r["start"] = $aRow["dateStart"];
                                    array_push($sensors, $r);
                                }
                            }

                            $k = 0;
                            while ($k < count($sensors)) {
                                $sID = $sensors[$k]["senID"];
                                $sDate = $sensors[$k]["start"];
                                $sql = "SELECT * FROM sensor,`type-sensor` WHERE sensorID=" . $sID . " AND sensorProperty=typeSensorID";
                                DBConnect();
                                $resS = execQuery($sql);
                                DBClose();
                                while ($aRow = fetchNext($resS)) {
                                    $sensorID = $aRow["sensorID"];
                                    $sensorType = $aRow["type"];
                                }
                                if ($sensorType == "motion") {
                                    $sql = "SELECT * FROM motion WHERE sensorID=" . $sensorID . " AND datetimeStart>'" . $sDate . "' ORDER BY datetimeEnd";
                                    DBConnect();
                                    $resMotion = execQuery($sql);
                                    DBClose();
                                    $numMotion = rowCount($resMotion);
                                    if ($numMotion > 0) {
                                        while ($aRow = fetchNext($resMotion)) {
                                            $row = array();
                                            $row["Sensor ID"] = $sensorID;
                                            $row["Sensor Type"] = $sensorType;
                                            $row["Measurement ID"] = "M" . $aRow["motionID"];
                                            $row["Timestamp Start"] = $aRow["datetimeStart"];
                                            $row["Timestamp End"] = $aRow["datetimeEnd"];
                                            array_push($sensorMeasurements, $row);
                                        }
                                    }
                                } else {
                                    $sql = "SELECT * FROM `sensor-measurement` WHERE sensorID=" . $sensorID . " AND datetime>'" . $sDate . "' ORDER BY datetime";
                                    DBConnect();
                                    $resMeas = execQuery($sql);
                                    DBClose();
                                    $numM = rowCount($resMeas);
                                    if ($numM > 0) {
                                        while ($aRow = fetchNext($resMeas)) {
                                            $row = array();
                                            $row["Sensor ID"] = $sensorID;
                                            $row["Sensor Type"] = $sensorType;
                                            $row["Measurement ID"] = $aRow["measurementID"];
                                            $row["Measurement"] = $aRow["measurement"];
                                            $row["Timestamp"] = $aRow["datetime"];
                                            array_push($sensorMeasurements, $row);
                                        }
                                    }
                                }
                                $k++;
                            }
                            $i++;
                        }
                        $locationInfo[$counter]["count"]=count($sensorMeasurements);
                        $locationInfo[$counter]["Measurements"]=$sensorMeasurements;
                        $counter++;
                    }
                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Successfully retrieved.",
                        "count" => count($locationInfo),
                        "resultLocations" => $locationInfo
                    );
                }
            }
            else{
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "You have to provide a valid token. Connect again to gain access"
                    );
                }
            }
        }
        else{
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide your current token."
            );
        }
}
echo json_encode($response);