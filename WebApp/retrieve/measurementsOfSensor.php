<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["sensorID"])){
        $token = $_POST["token"];
        $sensorID=$_POST["sensorID"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
            }
            if (checkToken($valid)){
                $measurements=array();
                //Find the sensor Info
                $sql = "SELECT * FROM `sensor-measurement`
            						WHERE `sensorID`=" . $sensorID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                $rows=rowCount($result);
                if ($rows>0) {
                  while ($aRow=fetchNext($result)){
                      $aMeasurement = array("time" => $aRow["datetime"], "value" => $aRow["measurement"]);
                      array_push($measurements, $aMeasurement);
                    }
                    $response=array(
                      "status" => "success",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "Successfully retrieved",
                      "measurements" => $measurements
                    );
                }
                else{
                  $response=array(
                      "status" => "fail",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "This sensor has no measurements."
                    );
                }
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
              }
    }
  }
  else{
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have to provide your token and a sensorID."
    );
  }
}

echo json_encode($response);

?>
