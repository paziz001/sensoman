<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 7/1/2015
 * Time: 1:46 μμ
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["longitude"]) && isset( $_POST["latitude"]) && isset($_POST["radius"])){
        $token = $_POST["token"];
        $longitude=$_POST["longitude"];
        $latitude=$_POST["latitude"];
        $radius=$_POST["radius"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            $sql="SELECT * FROM user WHERE userID=".$userID;
            DBConnect();
            $resU=execQuery($sql);
            DBClose();
            while ($aRow=fetchNext($resU)){
                $userType=$aRow["type"];
            }
            if (checkToken($valid)){
                //Continue to find the controllers
                $location=findLocation($latitude,$longitude,$radius); //the location within a distance given as radius
                $loc=array(
                    "latitude" => $latitude,
                    "longitude" => $longitude,
                    "radius" => $radius."KM"
                );
                //find the active controllers in the distance
                if (count($location)<1){
                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "Location Requested" => $loc,
                        "message" => "None controller was found within the distance given"
                    );
                }
                else{
                    $i=0;
                    $controllers=array();
                    while ($i<count($location)){
                        $locID=$location[$i]["locationID"];
                        if ((strcmp($userType,"admin")==0) || (strcmp($userType,"simple")==0)){
                            $sql="SELECT * FROM `location-controller` WHERE locationID=".$locID." AND active=1 AND admin=0";
                        }
                        else {
                            $sql = "SELECT * FROM `location-controller`,`user-controller` WHERE locationID=" . $locID . " AND active=1 AND admin=0 AND `user-controller`.userID=" . $userID . " AND `user-controller`.controllerID=`location-controller`.controllerID";
                        }
                        DBConnect();
                        $resultContr=execQuery($sql);
                        $rec=rowCount($resultContr);
                        if ($rec>0){
                            while($aRow=fetchNext($resultContr)){
                                $contrID=$aRow["controllerID"];
                                array_push($controllers, $contrID);
                            }
                        }
                        $i++;
                    }
                    $i=0;
                    $contr=array();
                    while($i<count($controllers)){
                        $conID=$controllers[$i];
                        $sql="SELECT *  FROM `micro-controller`  WHERE controllerID=".$conID."";
                        DBConnect();
                        $res=execQuery($sql);
                        DBClose();
                        $sensors=array();
                        $sql="SELECT * FROM `sensor-controller` WHERE controllerID=".$conID." AND active=1";
                        DBConnect();
                        $resultSen=execQuery($sql);
                        $recS=rowCount($resultSen);
                        if ($recS>0){
                            while ($aRow=fetchNext($resultSen)){
                                $r=array();
                                $r["senID"]=$aRow["sensorID"];
                                $r["start"]=$aRow["dateStart"];
                                array_push($sensors,$r);
                            }
                        }

                        $sensorMeasurements=array();
                        $k=0;
                        while ($k<count($sensors)){
                            $sID=$sensors[$k]["senID"];
                            $sDate=$sensors[$k]["start"];
                            $sql="SELECT * FROM sensor,`type-sensor` WHERE sensorID=".$sID." AND sensorProperty=typeSensorID";
                            DBConnect();
                            $resS=execQuery($sql);
                            DBClose();
                            while ($aRow=fetchNext($resS)){
                                $sensorID=$aRow["sensorID"];
                                $sensorName=$aRow["sensorName"];
                                $sensorType=$aRow["type"];
                                $sensorFrequency=$aRow["frequency"];
                            }
                            if ($sensorType=="motion"){
                                $sql="SELECT * FROM motion WHERE sensorID=".$sensorID. " AND datetimeStart>'".$sDate."' ORDER BY datetimeEnd DESC LIMIT 1";
                                DBConnect();
                                $resMotion=execQuery($sql);
                                DBClose();
                                $numMotion=rowCount($resMotion);
                                if ($numMotion>0){
                                    while($aRow=fetchNext($resMotion)){
                                        $row=array();
                                        $row["Sensor ID"] = $sensorID;
                                        $row["Sensor Name"] = $sensorName;
                                        $row["Sensor Type"] = $sensorType;
                                        $row["SensorFrequency"]=$sensorFrequency;
                                        $row["Measurement ID"] = "M".$aRow["motionID"];
                                        $row["Timestamp Start"] = $aRow["datetimeStart"];
                                        $row["Timestamp End"] = $aRow["datetimeEnd"];
                                        array_push($sensorMeasurements, $row);
                                    }
                                }
                            }
                            else {
                                $sql = "SELECT * FROM `sensor-measurement` WHERE sensorID=" . $sensorID . " AND datetime>'" . $sDate . "' ORDER BY datetime DESC LIMIT 1";
                                DBConnect();
                                $resMeas = execQuery($sql);
                                DBClose();
                                $numM = rowCount($resMeas);
                                if ($numM > 0) {
                                    while ($aRow = fetchNext($resMeas)) {
                                        $row = array();
                                        $row["Sensor ID"] = $sensorID;
                                        $row["Sensor Name"] = $sensorName;
                                        $row["Sensor Type"] = $sensorType;
                                        $row["SensorFrequency"]=$sensorFrequency;
                                        $row["Measurement ID"] = $aRow["measurementID"];
                                        $row["Measurement"] = $aRow["measurement"];
                                        $row["Timestamp"] = $aRow["datetime"];
                                        array_push($sensorMeasurements, $row);
                                    }
                                }
                            }
                            $k++;
                        }
                        $resultSensors=array(
                            "status" => "success",
                            "message" => "Successfully retrieved",
                            "count" => count($sensorMeasurements),
                            "resultSensors" => $sensorMeasurements
                        );
                        while($aRow=fetchNext($res)){
                            $row=array();
                            $row["Controller ID"]=$conID;
                            $row["Controller Name"]=$aRow["controllerName"];
                            $row["Sensors"]=$resultSensors;
                            array_push($contr,$row);
                        }
                        $i++;
                    }
                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "Location Requested" => $loc,
                        "message" => "Successfully retrieved.",
                        "count" => count($contr),
                        "controllers" => $contr

                    );
                }
            }
            else{
                $response=array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{

        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token, longitude,latitude and radius."
        );
    }
}
echo json_encode($response);

/*//Get the results
$resultHumidity = execQuery("SELECT * FROM datasensors.humidity ORDER BY datetime DESC LIMIT 1");
$resultSound= execQuery("SELECT * FROM datasensors.sound ORDER BY datetime DESC LIMIT 1");
$resultMotion= execQuery("SELECT * FROM datasensors.motion ORDER BY datetimeEnd DESC LIMIT 1");
$resultTemp= execQuery("SELECT * FROM datasensors.temperature ORDER BY datetime DESC LIMIT 1");
$resultLDR= execQuery("SELECT * FROM datasensors.LDR ORDER BY datetime DESC LIMIT 1");
$resultController=execQuery("SELECT * FROM datasensors.`micro-controller`");

//Get the total number of records
$numHum=rowCount($resultHumidity);
$numS=rowCount($resultSound);
$numM=rowCount($resultMotion);
$numT=rowCount($resultTemp);
$numLDR=rowCount($resultLDR);
$numContr=rowCount($resultController);
$numberOfRecords= $numHum+ $numS + $numM+$numT+ $numLDR ;

if ($numContr<1){
    $result = array(
        "status" => "success",
        "message" => "Successfully retrieved.",
        "count" => "0"
    );
}
else {
    if ($numberOfRecords < 1) {
        $resultSensor = array(
            "status" => "success",
            "message" => "Successfully retrieved.",
            "count" => "0"
        );
    } else {
        $i=1;
        //Humidity Results
        $sensors = array();
        while ($aRow = fetchNext($resultHumidity)) {
            $row = array();
            $row["ID"] = $i;
            $row["Sensor"] = $aRow["sensorName"];
            $row["Type"]= "humidity";
            $row["Measurement"] = $aRow["humidityMeasurement"];
            $row["Timestamp"] = $aRow["datetime"];

            array_push($sensors, $row);
        }
        $i++;
        //Sound Results

        while ($aRow = fetchNext($resultSound)) {
            $row = array();
            $row["ID"] = $i;;
            $row["Sensor"] = $aRow["sensorName"];
            $row["Type"]= "sound";
            $row["Measurement"] = $aRow["soundMeasurement"];
            $row["Timestamp"] = $aRow["datetime"];
            array_push($sensors, $row);
        }

        $i++;
        //Temperature Results

        while ($aRow = fetchNext($resultTemp)) {
            $row = array();
            $row["ID"] = $i;
            $row["Sensor"] = $aRow["sensorName"];
            $row["Type"]= "temperature";
            $row["Measurement"] = $aRow["tempMeasurement"];
            $row["Timestamp"] = $aRow["datetime"];

            array_push($sensors, $row);
        }

        $i++;

        //LDR Results

        while ($aRow = fetchNext($resultLDR)) {
            $row = array();
            $row["ID"] = $i;
            $row["Sensor"] = $aRow["sensorName"];
            $row["Type"]= "LDR";
            $row["Measurement"] = $aRow["LDRMeasurement"];
            $row["Timestamp"] = $aRow["datetime"];

            array_push($sensors, $row);
        }

$i++;
        //Motion Results

        while ($aRow = fetchNext($resultMotion)) {
            $row = array();
            $row["ID"] = $i;
            $row["Sensor"] = $aRow["sensorName"];
            $row["Type"]= "motion";
            $row["Timestamp Start"] = $aRow["datetimeStart"];
            $row["Timestamp End"] = $aRow["datetimeEnd"];

            array_push($sensors, $row);
        }

        //Get all the results together
        $resultSensor = array(
            "status" => "success",
            "message" => "Successfully retrieved.",
            "count" => $numberOfRecords,
            "resultSensors" => $sensors
        );
    }
    $controller=array();
    while($aRow=fetchNext($resultController)){
        $row=array();
        $row["ID"] = $aRow["controllerID"];
        $row["Name"] = $aRow["controllerName"];
        if ($row["ID"]==1){
            $row["Sensors"] = $resultSensor;
        }
        else{
            $res= array(
                "status" => "success",
                "message" => "Successfully retrieved",
                "count"=> "0",
                "resultSensors" => array()
            );
            $row["Sensors"]= $res;
        }

        array_push($controller,$row);
    }

        $result = array(
            "status" => "success",
            "message" => "Successfully retrieved.",
            "count" => $numContr,
            "resultControllers" => $controller
        );

}
echo json_encode($result);*/
