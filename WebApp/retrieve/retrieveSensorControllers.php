<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

$method=getRequestMethod();
$date=new DateTime('now');

if ($method != 'POST') {

    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}  else {
    if (isset($_POST["token"])&&isset($_POST["username"])) {
        $token = $_POST["token"];
        $username=$_POST["username"];
        //Check the validation of the token
        $db=DBConnect();
        $stmt=$db->prepare("SELECT * FROM token WHERE token= ?");
        $stmt->bind_param('s',$token);
        $stmt->execute();
        $resultToken = $stmt->get_result();
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "message" => "You have to provide a valid token."
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            
            if (checkToken($valid)) {
                //Find the type of User
                //Find the controller Info
                $sensor_controllers=array();
                $db=DBConnect();
                $stmt=$db->prepare("SELECT DISTINCT (CONCAT(`controllerName`,',',`user-controller`.`controllerID`))   AS `controllerName` FROM `micro-controller`,`sensor-controller`,`user-controller`,`user` WHERE `micro-controller`.controllerID = `user-controller`.controllerID AND `user-controller`.userID=`user`.userID AND `user`.username = ? AND `micro-controller`.sensing=1 AND `sensor-controller`.active=1");
                $stmt->bind_param('s',$username);
                $stmt->execute();
                $resultSenCont = $stmt->get_result();
                $numSenCont = rowCount($resultSenCont);
                if($numSenCont<1){
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "No sensor-controllers found."
                    );
                }else {
                    while ($aRow = fetchNext($resultSenCont)) {
                        $row = array();
                        $controllerName = $aRow["controllerName"];
                        array_push($sensor_controllers, $controllerName);
                    }
                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "message" => "Successfully retrieved sensor-controllers",
                        "count" => count($sensor_controllers),
                        "sensorControllers" => $sensor_controllers
                    );
                }
            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    } else {
        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);