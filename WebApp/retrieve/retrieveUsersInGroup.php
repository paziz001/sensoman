<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();
$token_flag = false;
if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );

}else {
	  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }

  if($token_flag == true && isset($_POST["username"]) && isset($_POST["groupName"])){
  		$username = test_input($_POST["username"]);
  		$groupName = test_input($_POST["groupName"]);
  		$sql = "SELECT groupID FROM sn_group WHERE `sn_group`.name ='".$groupName."'";
  		DBConnect();
		$result=execQuery($sql);
		DBClose();
		$num=rowCount($result);
		if($num<1){
		 $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "This group has no users"
            );
		} else{
			while($aRow=fetchNext($result)){
 					$groupID = $aRow["groupID"];
		 		}

  			$sql="SELECT user.username, user.email, user.type, user.userID FROM `sn_user-group` INNER JOIN user on `sn_user-group`.userID = user.userID WHERE `sn_user-group`.groupID = '".$groupID."'";

  		DBConnect();
		$result=execQuery($sql);
		DBClose();
		$num=rowCount($result);
		if($num<1){
		 $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "This group has no users"
            );
		}
		 else{
		 	$usernamesArray = array();
		 	$idArray = array();
		 	$emailsArray = array();
		 	$typesArray = array();
		 	while($aRow=fetchNext($result)){
 					$name = $aRow["username"];
 					$id = $aRow["userID"];
 					$email = $aRow["email"];
 					$type = $aRow["type"];
                    array_push($usernamesArray,$name);
                    array_push($idArray,$id);
                    array_push($emailsArray,$email);
                    array_push($typesArray,$type);
		 		}
		 		$response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "Usernames" => $usernamesArray,
                "Ids" => $idArray,
                "Emails" => $emailsArray,
                "Types" => $typesArray,
                "message" => "Successfully retrieved."
            );

		}
	}
  }


  }//else POST


function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

echo json_encode($response);
?>