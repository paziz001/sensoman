<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = 0;

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
} else {

	  //check token's qualifications
  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }
	if($token_flag == true){
		//$name = $_POST["username"];
		//retreive all the requests 
		$sql="SELECT user.username, user.userID, sn_request.groupName FROM sn_request INNER JOIN user on sn_request.senderID = user.userID WHERE sn_request.receiverID='".$userID."'";
		DBConnect();
        $result=execQuery($sql);
        DBClose();
        $numToken=rowCount($result);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "There are no requests."
            );
            $friendRequests = 0;
        } else{
            $friendRequests =1 ;
        	$names = array();
        	$userIDs = array();
            $groups = array();
        	$counter = 0;
        	//store all the usernames and userIDs that have sent requests
        		while($aRow=fetchNext($result)){

        			$names[$counter] = $aRow["username"];
        			$userIDs[$counter] = $aRow["userID"];
                    $groups[$counter] = $aRow["groupName"];
        			$counter = $counter +1;
        			
        		}
            }

                $sql="SELECT user.username, user.userID, `micro-controller`.controllerName, `micro-controller`.controllerID, sensor.sensorID, sensor.sensorName, `sn_requested_sensors`.writeValue, `sn_requested_sensors`.readValue, `sn_requested_sensors`.updatePermissions FROM sn_requested_sensors INNER JOIN user on sn_requested_sensors.applicantID = user.userID INNER JOIN `sensor` on sn_requested_sensors.sensorID = sensor.sensorID INNER JOIN `micro-controller` on sn_requested_sensors.boardID = `micro-controller`.controllerID WHERE sn_requested_sensors.receiverID='".$userID."'";

                DBConnect();
                $result=execQuery($sql);
                DBClose();
                $numToken=rowCount($result);
                if ($numToken<1){
                    $response=array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "There are no sensor requests."
                    );
                    $sensorRequests = 0;
                } else {
                        $sensorRequests = 1;
                        $s_userNames = array();
                        $s_userIDs = array();
                        $s_controllerNames = array();
                        $s_controllerIDs = array();
                        $s_sensorNames = array();
                        $s_sensorIDs = array();
                        $s_sensorWrite = array();
                        $s_sensorRead = array();
                        $s_updatePermissions = array();
                        $counter = 0;
                        while($aRow=fetchNext($result)){

                    $s_userNames[$counter] = $aRow["username"];
                    $s_userIDs[$counter] = $aRow["userID"];
                    $s_controllerNames[$counter] = $aRow["controllerName"];
                    $s_sensorNames[$counter] = $aRow["sensorName"];
                    $s_sensorIDs[$counter] = $aRow["sensorID"];
                    $s_controllerIDs[$counter] = $aRow["controllerID"];
                    $s_sensorWrite[$counter] = $aRow["writeValue"];
                    $s_sensorRead[$counter] = $aRow["readValue"];
                    $s_updatePermissions[$counter] = $aRow["updatePermissions"];
                    $counter = $counter +1;
                    
                    }


                }

                if($sensorRequests == 1 &&  $friendRequests == 1)
		  $response=array(
        	"statusFriend" => "success", //retrieve friend requests
            "statusSensor" => "success", // retrieve sensors requests
        	"request time" => $date->format("Y-m-d h:m:s"),
        	"IP Address" => $ip,
        	"Usernames" => $names,
        	"UserIDs" => $userIDs,
            "groups" => $groups,
            "sUsernames" => $s_userNames,
            "sUserIDs" => $s_userIDs,
            "controllerNames" => $s_controllerNames,
            "sensorNames" => $s_sensorNames,
            "sensorIDs" => $s_sensorIDs,
            "controllerIDs" => $s_controllerIDs,
            "writeValues" => $s_sensorWrite,
            "readValues" =>$s_sensorRead,
            "permissions" => $s_updatePermissions,
        	"message" => "Successfully retrieved all requests."
    	);

         else if($sensorRequests == 0 &&  $friendRequests == 1)
	       $response=array(
            "statusFriend" => "success", //retrieve friend requests
            "statusSensor" => "fail", // retrieve sensors requests
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "Usernames" => $names,
            "UserIDs" => $userIDs,
            "groups" => $groups,
            "message" => "Successfully retrieved Friend requests."
        
        );
        else if($sensorRequests == 1 &&  $friendRequests == 0)
          $response=array(
            "statusFriend" => "fail", //retrieve friend requests
            "statusSensor" => "success", // retrieve sensors requests
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "sUsernames" => $s_userNames,
            "sUserIDs" => $s_userIDs,
            "controllerNames" => $s_controllerNames,
            "sensorNames" => $s_sensorNames,
            "writeValues" => $s_sensorWrite,
            "sensorIDs" => $s_sensorIDs,
            "controllerIDs" => $s_controllerIDs,
            "readValues" =>$s_sensorRead,
            "permissions" => $s_updatePermissions,
            "message" => "Successfully retrieved sensors requests."
        );
      else if($sensorRequests == 0 &&  $friendRequests == 0)
            $response=array(
                "statusFriend" => "fail", //retrieve friend requests
                "statusSensor" => "fail", // retrieve sensors requests
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "There are no requests."
            );

    

}
}

echo json_encode($response);
?>