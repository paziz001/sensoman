<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

$method = getRequestMethod();
$date = new DateTime('now');
$ip = getClientIP();

if ($method != 'POST') {

    $response = array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '" . $method . " " . $_SERVER['REQUEST_URI'] . "'"
    );
} else {
    if (isset($_POST["token"]) && isset($_POST["username"])) {
        $token = $_POST["token"];
        $username = $_POST["username"];
        //Check the validation of the token
       /* $db = DBConnect();
        $stmt = $db->prepare("SELECT * FROM token WHERE token= ?");
        $stmt->bind_param('s', $token);
        $stmt->execute();
        $resultToken = $stmt->get_result();
        DBClose();*/
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token."
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)) {
                //Find the type of User
                //Find the controller Info
                $actuator_controllers = array();
                /*$db = DBConnect();
                $stmt = $db->prepare("SELECT DISTINCT (CONCAT(`controllerName`,',',`micro-controller`.`controllerID`)) AS `controllerName` FROM `micro-controller`,`user-controller`,`user`
                WHERE `micro-controller`.controllerID = `user-controller`.controllerID AND`user-controller`.userID=`user`.userID
                AND `user`.username = ? AND `micro-controller`.actuator=1 ");
                $stmt->bind_param('s', $username);
                $stmt->execute();
                $resultActController = $stmt->get_result();*/
                $sql = "SELECT DISTINCT (CONCAT(`controllerName`,',',`micro-controller`.`controllerID`)) AS `controllerName` FROM `micro-controller`,`user-controller`,`user`
                WHERE `micro-controller`.controllerID = `user-controller`.controllerID AND`user-controller`.userID=`user`.userID
                AND `user`.username = '".$username."' AND `micro-controller`.actuator=1 ";
                DBConnect();
                $resultActController=execQuery($sql);
                DBClose();
                $numActControllers = rowCount($resultActController);
                
                if ($numActControllers < 1) {
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "No actuator-controllers found."
                    );
                } else {
                    while ($aRow = fetchNext($resultActController)) {
                        $row = array();
                        $controllerName = $aRow["controllerName"];
                        array_push($actuator_controllers, $controllerName);
                    }

                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Successfully retrieved actuator-controllers",
                        "count" => count($actuator_controllers),
                        "actuatorControllers" => $actuator_controllers
                    );
                }
            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    } else {

        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);