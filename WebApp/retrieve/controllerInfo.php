<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 15/2/2015
 * Time: 5:27 μμ
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["controllerID"])){
        $token = $_POST["token"];
        $controllerID=$_POST["controllerID"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
            }
            if (checkToken($valid)){
                //Find the controller Info
                $sql="SELECT * FROM   `micro-controller` WHERE  `micro-controller`.controllerID=".$controllerID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                while ($aRow=fetchNext($result)){
                    $controllerName=$aRow["controllerName"];
                    $type=$aRow["type"];
                }
                $sql="SELECT * FROM `location-controller`,location WHERE `location-controller`.controllerID=".$controllerID." AND active=1 AND location.locationID=`location-controller`.locationID";
                DBConnect();
                $resultLoc=execQuery($sql);
                DBClose();
                $loc=array();
                while ($aRow=fetchNext($resultLoc)){
                    $loc["longitude"]=$aRow["longitube"];
                    $loc["latitude"]=$aRow["latitube"];
                }
                $sensors=array();
                $sql="SELECT * FROM `sensor-controller` WHERE controllerID=".$controllerID." AND active=1";
                DBConnect();
                $resultSen=execQuery($sql);
                $recS=rowCount($resultSen);
                if ($recS>0) {
                    while ($aRow = fetchNext($resultSen)) {
                        $r = array();
                        $r["senID"] = $aRow["sensorID"];
                        $r["start"] = $aRow["dateStart"];
                        array_push($sensors, $r);
                    }
                    $sensorMeasurements = array();
                    $k = 0;
                    while ($k < count($sensors)) {
                        $sID = $sensors[$k]["senID"];
                        $sDate = $sensors[$k]["start"];
                        $sql = "SELECT * FROM sensor,`type-sensor` WHERE sensorID=" . $sID . " AND sensorProperty=typeSensorID";
                        DBConnect();
                        $resS = execQuery($sql);
                        DBClose();
                        while ($aRow = fetchNext($resS)) {
                            $sensorID = $aRow["sensorID"];
                            $sensorName = $aRow["sensorName"];
                            $sensorType = $aRow["type"];
                        }
                        if ($sensorType == "motion") {
                            $sql = "SELECT * FROM motion WHERE sensorID=" . $sensorID . " AND datetimeStart>'" . $sDate . "' ORDER BY datetimeEnd DESC LIMIT 1";
                            DBConnect();
                            $resMotion = execQuery($sql);
                            DBClose();
                            $numMotion = rowCount($resMotion);
                            if ($numMotion > 0) {
                                while ($aRow = fetchNext($resMotion)) {
                                    $row = array();
                                    $row["Sensor ID"] = $sensorID;
                                    $row["Sensor Name"] = $sensorName;
                                    $row["Sensor Type"] = $sensorType;
                                    $row["Measurement ID"] = "M" . $aRow["motionID"];
                                    $row["Timestamp Start"] = $aRow["datetimeStart"];
                                    $row["Timestamp End"] = $aRow["datetimeEnd"];
                                    array_push($sensorMeasurements, $row);
                                }
                            }
                        } else {
                            $sql = "SELECT * FROM `sensor-measurement` WHERE sensorID=" . $sensorID . " AND datetime>'" . $sDate . "' ORDER BY datetime DESC LIMIT 1";
                            DBConnect();
                            $resMeas = execQuery($sql);
                            DBClose();
                            $numM = rowCount($resMeas);
                            if ($numM > 0) {
                                while ($aRow = fetchNext($resMeas)) {
                                    $row = array();
                                    $row["Sensor ID"] = $sensorID;
                                    $row["Sensor Name"] = $sensorName;
                                    $row["Sensor Type"] = $sensorType;
                                    $row["Measurement ID"] = $aRow["measurementID"];
                                    $row["Measurement"] = $aRow["measurement"];
                                    $row["Timestamp"] = $aRow["datetime"];
                                    array_push($sensorMeasurements, $row);
                                }
                            }
                        }
                        $k++;
                    }
                    $latest = 0;
                    $k = 0;
                    $time = 0;
                    while ($k < count($sensorMeasurements)) {
                        if ($sensorMeasurements[$k]["Timestamp"] != null) {
                            if (strtotime($sensorMeasurements[$k]["Timestamp"]) > $time) {
                                $time = strtotime($sensorMeasurements[$k]["Timestamp"]);
                                $latest = $k;
                            }
                        } else {
                            if (strtotime($sensorMeasurements[$k]["Timestamp End"]) > $time) {
                                $time = strtotime($sensorMeasurements[$k]["Timestamp End"]);
                                $latest = $k;
                            }
                        }
                        $k++;
                    }

                    if ($sensorMeasurements[$latest]["Timestamp"] != null) {
                        $datetime = $sensorMeasurements[$latest]["Timestamp"];
                    } else {
                        $datetime = $sensorMeasurements[$latest]["Timestamp End"];
                    }
                    $lastM = array();
                    $lastM["Sensor ID"] = $sensorMeasurements[$latest]["Sensor ID"];
                    $lastM["Sensor Name"] = $sensorMeasurements[$latest]["Sensor Name"];
                    $lastM["Sensor Type"] = $sensorMeasurements[$latest]["Sensor Type"];
                    $lastM["Timestamp"] = $datetime;
                    $contr = array();
                    $contr["Controller ID"] = $controllerID;
                    $contr["Controller Name"] = $controllerName;
                    $contr["Controller Type"] = $type;
                    $contr["Location"] = $loc;
                    $contr["Last Measurement"] = $lastM;
                }
                else{
                    $lastM=array();
                    $contr["Controller ID"] = $controllerID;
                    $contr["Controller Name"] = $controllerName;
                    $contr["Controller Type"] = $type;
                    $contr["Location"] = $loc;
                    $contr["Last Measurement"] = $lastM;
                }
                $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Successfully retrieved",
                    "Controller Info" => $contr
                );
            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{
        if(!isset($_POST["token"])){
            $response = array(
                "status" => "expired token",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "The token has expired"
            );
        }else {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide your current token and a controller ID (int)."
            );
        }
    }
}
echo json_encode($response);
