<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 6/2/2015
 * Time: 8:57 μμ
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
header('Content-Disposition: attachement; filename="CSV.csv";');
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])) {
        $token = $_POST["token"];
        //Check the validation of the token
        $sql = "SELECT * FROM token WHERE token='" . $token . "'";
        DBConnect();
        $resultToken = execQuery($sql);
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            $sql = "SELECT * FROM user WHERE userID=" . $userID;
            DBConnect();
            $resU = execQuery($sql);
            DBClose();
            while ($aRow = fetchNext($resU)) {
                $userType = $aRow["type"];
            }
            if (checkToken($valid)) {
                if ((strcmp($userType, "admin") == 0) || (strcmp($userType, "simple") == 0)) {
                    $sql = "SELECT * FROM `user-controller`";
                } else {
                    $sql = "SELECT * FROM `user-controller` WHERE userID=" . $userID;
                }
                DBConnect();
                $resUserController = execQuery($sql);
                DBClose();
                $micro = array();
                while ($aRow = fetchNext($resUserController)) {
                    $microID = $aRow["controllerID"];
                    array_push($micro, $microID);
                }
                $j = 0;
                $location = array();
                while ($j < count($micro)) {
                    $mID = $micro[$j];
                    $sql = "SELECT * FROM `location-controller` WHERE controllerID=" . $mID . " AND active=1 AND admin=0";
                    DBConnect();
                    $resLC = execQuery($sql);
                    DBClose();
                    if (rowCount($resLC) > 0) {
                        while ($aRow = fetchNext($resLC)) {
                            $lID = $aRow["locationID"];
                            array_push($location, $lID);
                        }
                    }
                    $j++;
                }
                $location=array_unique($location);
                //Continue to find the controllers
                //find the active controllers in the distance
                if (count($location) < 1) {
                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "None sensor was found"
                    );
                } else {
                    $i = 0;
                    //find Controllers
                    $controllers = array();
                    $counter = 0;
                    $locationInfo=array();
                    while ($counter < count($location)) {
                        $sensorMeasurements=array();
                        $locID = $location[$counter];
                        $sql="SELECT * FROM location WHERE locationID=".$locID;
                        DBConnect();
                        $r=execQuery($sql);
                        DBClose();
                        while ($aRow=fetchNext($r)){
                            $locationInfo[$counter]["locationID"]=$locID;
                            $locationInfo[$counter]["longitude"]=$aRow["longitube"];
                            $locationInfo[$counter]["latitude"]=$aRow["latitube"];
                        }

                        if ((strcmp($userType, "admin") == 0) || (strcmp($userType, "simple") == 0)) {
                            $sql = "SELECT * FROM `location-controller` WHERE locationID=" . $locID . " AND active=1 AND admin=0";
                        } else {
                            $sql = "SELECT * FROM `location-controller`,`user-controller` WHERE locationID=" . $locID . " AND active=1 AND admin=0 AND `user-controller`.userID=" . $userID . " AND `user-controller`.controllerID=`location-controller`.controllerID";
                        }

                        DBConnect();
                        $resultContr = execQuery($sql);
                        $rec = rowCount($resultContr);
                        $controllers=array();
                        DBClose();
                        if ($rec > 0) {
                            while ($aRow = fetchNext($resultContr)) {
                                $contrID = $aRow["controllerID"];
                                array_push($controllers, $contrID);
                            }
                        }
                        $i = 0;
                        $contr = array();
                        $sensorMeasurements = array();
                        while ($i < count($controllers)) {
                            $conID = $controllers[$i];
                            $sensors = array();
                            $sql = "SELECT * FROM `sensor-controller` WHERE controllerID=" . $conID . " AND active=1 AND deactivated=0";
                            DBConnect();
                            $resultSen = execQuery($sql);
                            DBClose();
                            $recS = rowCount($resultSen);
                            //Find the sensors of a controller
                            if ($recS > 0) {
                                while ($aRow = fetchNext($resultSen)) {
                                    $r = array();
                                    $r["senID"] = $aRow["sensorID"];
                                    $r["start"] = $aRow["dateStart"];
                                    array_push($sensors, $r);
                                }
                            }

                            $k = 0;
                            while ($k < count($sensors)) {
                                $sID = $sensors[$k]["senID"];
                                $sDate = $sensors[$k]["start"];
                                $sql = "SELECT * FROM sensor,`type-sensor` WHERE sensorID=" . $sID . " AND sensorProperty=typeSensorID";
                                DBConnect();
                                $resS = execQuery($sql);
                                DBClose();
                                while ($aRow = fetchNext($resS)) {
                                    $sensorID = $aRow["sensorID"];
                                    $sensorType = $aRow["type"];
                                }
                                if ($sensorType == "motion") {
                                    $sql = "SELECT * FROM motion WHERE sensorID=" . $sensorID . " AND datetimeStart>'" . $sDate . "' ORDER BY datetimeEnd";
                                    DBConnect();
                                    $resMotion = execQuery($sql);
                                    DBClose();
                                    $numMotion = rowCount($resMotion);
                                    if ($numMotion > 0) {
                                        while ($aRow = fetchNext($resMotion)) {
                                            $row = array();
                                            $row["Sensor ID"] = $sensorID;
                                            $row["Sensor Type"] = $sensorType;
                                            $row["Measurement ID"] = "M" . $aRow["motionID"];
                                            $row["Timestamp Start"] = $aRow["datetimeStart"];
                                            $row["Timestamp End"] = $aRow["datetimeEnd"];
                                            $row["NumPerson"]= rand(0,6);
                                            array_push($sensorMeasurements, $row);
                                        }
                                    }
                                } else {
                                    $sql = "SELECT * FROM `sensor-measurement` WHERE sensorID=" . $sensorID . " AND datetime>'" . $sDate . "' ORDER BY datetime ASC";
                                    DBConnect();
                                    $resMeas = execQuery($sql);
                                    DBClose();
                                    $numM = rowCount($resMeas);
                                    if ($numM > 0) {
                                        while ($aRow = fetchNext($resMeas)) {
                                            $row = array();
                                            $row["Sensor ID"] = $sensorID;
                                            $row["Sensor Type"] = $sensorType;
                                            $row["Measurement ID"] = $aRow["measurementID"];
                                            $row["Measurement"] = $aRow["measurement"];
                                            $row["Timestamp"] = $aRow["datetime"];
                                            $row["NumPerson"]= 4;
                                            array_push($sensorMeasurements, $row);
                                        }
                                    }
                                }
                                $k++;
                            }
                            $i++;
                        }
                        $locationInfo[$counter]["count"]=count($sensorMeasurements);
                        $locationInfo[$counter]["Measurements"]=$sensorMeasurements;
                        if ((count($sensorMeasurements)%3 == 0))
                            $locationInfo[$counter]["MachineLearningMessage"] = "YES";
                        else
                            $locationInfo[$counter]["MachineLearningMessage"] = "NO";
                        $counter++;
                    }

                    $z=0;
                    while ($z<count($locationInfo)){
                        $measurements=$locationInfo[$z]["Measurements"];
                        for ($pass=1; $pass < count($measurements); $pass++) {
                            for ($c = 0; $c < (count($measurements) - $pass); $c++) {
                                if (($measurements[$c]["Sensor Type"]=="motion") || ($measurements[$c+1]["Sensor Type"]=="motion")){
                                    if (($measurements[$c]["Sensor Type"]=="motion") && ($measurements[$c+1]["Sensor Type"]=="motion")) {
                                        $startCurrent = new Datetime($measurements[$c]["Timestamp Start"]);
                                        $startNext = new Datetime($measurements[$c + 1]["Timestamp Start"]);
                                    }
                                    else{
                                        if($measurements[$c]["Sensor Type"]=="motion"){
                                            $startCurrent = new Datetime($measurements[$c]["Timestamp Start"]);
                                            $startNext = new Datetime($measurements[$c + 1]["Timestamp"]);
                                        }
                                        else{
                                            $startCurrent = new Datetime($measurements[$c]["Timestamp"]);
                                            $startNext = new Datetime($measurements[$c + 1]["Timestamp Start"]);
                                        }
                                    }
                                    if ($startCurrent >= $startNext) {
                                        $m = $measurements[$c];
                                        $measurements[$c] = $measurements[$c + 1];
                                        $measurements[$c + 1] = $m;
                                    }
                                }
                                else {
                                    $current = new DateTime($measurements[$c]["Timestamp"]);
                                    $next = new DateTime($measurements[$c + 1]["Timestamp"]);
                                    if ($current >= $next) {
                                        $m = $measurements[$c];
                                        $measurements[$c] = $measurements[$c + 1];
                                        $measurements[$c + 1] = $m;
                                    }
                                }
                            }
                        }
                        $i=0;
                        $previous="";
                        $instances=array();
                        $j=0;
                        $datetimeEnd="";
                        while ($j<count($measurements)) {
                            $sensorID = $measurements[$j]["Sensor ID"];
                            $type=$measurements[$j]["Sensor Type"];
                            if ($type=="motion"){
                                $datetime = $measurements[$j]["Timestamp Start"];
                                $datetimeEnd=$measurements[$j]["Timestamp End"];
                                $sensorMeasurement=1;
                            }
                            else{
                                $datetime = $measurements[$j]["Timestamp"];
                                $sensorMeasurement = $measurements[$j]["Measurement"];
                            }
                            $find=strtotime($datetime);
                            $info=getdate($find);
                            $hour=$info["hours"];
                            $min=$info["minutes"];
                            $sec=$info["seconds"];
                            $weekDay=$info["weekday"];
                            if (($weekDay=="Saturday") || ($weekDay=="Sunday")){
                                $working=0;
                            }
                            else{
                                $working=1;
                            }
                            //echo $hour."\n".$min."\n".$sec."\n".$weekDay."\n".$working."\n";

                            if ($previous == "") {
                                $previous = $datetime;
                                $instances[$i]["hour"] = $hour;
                                $instances[$i]["minute"] = $min;
                                $instances[$i]["second"] = $sec;
                                $instances[$i]["weekday"] = $weekDay;
                                $instances[$i]["working"] = $working;
                                $instances[$i][$type] = $sensorMeasurement;
                            } else {
                                if ($previous == $datetime) {
                                    $instances[$i][$type] = $sensorMeasurement;

                                } else {
                                    if (!isset($instances[$i]["ldr"])){
                                        $instances[$i]["ldr"]="";
                                    }
                                    if (!isset($instances[$i]["temperature"])){
                                        $instances[$i]["temperature"]="";
                                    }
                                    if (!isset($instances[$i]["sound"])){
                                        $instances[$i]["sound"]="";
                                    }
                                    if (!isset($instances[$i]["humidity"])){
                                        $instances[$i]["humidity"]="";
                                    }
                                    if (!isset($instances[$i]["motion"])){
                                        if ($datetimeEnd!=="") {
                                            if (strtotime($datetimeEnd) >= strtotime($datetime)) {
                                                $instances[$i]["motion"] = 1;
                                            }
                                            else{
                                                $instances[$i]["motion"] = 0;
                                            }
                                        }
                                        else{
                                            $instances[$i]["motion"]=0;
                                        }
                                    }
                                    $instances[$i]["numPeople"]="?";
                                    $i++;
                                    $previous = $datetime;
                                    $instances[$i]["hour"] = $hour;
                                    $instances[$i]["minute"] = $min;
                                    $instances[$i]["second"] = $sec;
                                    $instances[$i]["weekday"] = $weekDay;
                                    $instances[$i]["working"] = $working;
                                    $instances[$i][$type] = $sensorMeasurement;
                                }
                            }
                            $j++;
                        }
                        $l=0;
                        $fp = fopen($locationInfo[$z]["locationID"].".csv", 'w');
                        $title=array("hour","humidity","ldr","minute","motion", "numPeople","second", "sound", "temperature","weekDay", "working");
                        fputcsv($fp,$title);
                        while ($l<count($instances)){
                            ksort($instances[$l]);
                            fputcsv($fp, $instances[$l]);
                            $l++;
                        }
                        fclose($fp);
                        $z++;
                    }


                    $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "count" => count($locationInfo),
                        "resultlocation" =>$locationInfo,
                        "message" => "Successfully created measurements csv."

                    );
                }
            }
            else{
                $response = array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token."
        );
    }
}
echo json_encode($response);