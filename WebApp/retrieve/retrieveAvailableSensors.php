<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

$token_flag = 0;

  if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
} else {
	  if(isset($_POST["token"])){
    $token = $_POST["token"];
     //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
        } else{ 
              while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              $token_flag = true;
            } else{
              $response=array(
                "status" => "token expired",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection."
            );
            }
        }
  }

  if($token_flag == true && isset($_POST["friendID"]) && isset($_POST["sensorName"]) ){
  	$friendID = $_POST["friendID"];
  	$sensorName = $_POST["sensorName"];
  	$sql ="SELECT sensor.sensorID FROM sensor WHERE sensor.sensorName='".$sensorName."'";
  	DBConnect();
        $result=execQuery($sql);
        DBClose();
        $numToken=rowCount($result);
        if ($numToken<1){
        	$response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Sensor not found."
            );
        }
       else{
       		while($aRow=fetchNext($result)){
       			  $sensorID = $aRow["sensorID"];	
        		}

       		$sql="SELECT sn_shared_sensors.sharedID FROM sn_shared_sensors WHERE sn_shared_sensors.sensorID='".$sensorID."' AND sn_shared_sensors.ownerID ='".$friendID."' AND watcherID ='".$userID."'";
		  DBConnect();
        $result=execQuery($sql);
        DBClose();
        $numToken=rowCount($result);
        if ($numToken<1){
        	$response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "available" => "no",
                "message" => "Sensor is not available."
            );
        }
       else{
       	$response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "available" => "yes",
                "message" => "Sensor is available."
            );
       }
       }

  } 


}
echo json_encode($response);
?>