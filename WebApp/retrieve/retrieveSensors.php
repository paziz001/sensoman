<?php

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');
include_once('../includes/location.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {

    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}  else {
    if (isset($_POST["token"]) && isset($_POST["controllerID"])) {
        $token = $_POST["token"];
        $controllerID=$_POST["controllerID"];
        //Check the validation of the token
        $db=DBConnect();
        $stmt=$db->prepare("SELECT * FROM token WHERE token= ?");
        $stmt->bind_param('s',$token);
        $stmt->execute();
        $resultToken = $stmt->get_result();
        DBClose();
        $numToken = rowCount($resultToken);
        if ($numToken < 1) {
            $response = array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token."
            );
        } else {
            while ($aRow = fetchNext($resultToken)) {
                $valid = $aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid) ) {

                if(isset($_POST["ownerID"])){
                    $ownerID = $_POST["ownerID"];
                //Find the type of User
                //Find the controller Info    
                $sensors=array();
                $sensorIDs=array();
                $sensorTypes = array();
                $sensorDates = array();

                $timestamps = array();
                $measurements = array();
                $sensorFrequency = array();

                //$db=DBConnect();
                //$stmt=$db->prepare("SELECT sensorName,sensorID FROM `sensor-controller`,`sensor`
               // WHERE `sensor-controller`.sensorID = `sensor`.sensorID
               // AND `sensor-controller`.controllerID='".$controllerID."'");
                //$stmt->bind_param('i',$controllerID);
                //$stmt->execute();
                //$sensorsResult = $stmt->get_result();
                //$numOfSensors = rowCount($sensorsResult);

                $sql = "SELECT sensorName,`sensor-controller`.sensorID, `sensor-controller`.dateStart FROM `sensor-controller`,`sensor` WHERE `sensor-controller`.sensorID = `sensor`.sensorID AND `sensor-controller`.controllerID='".$controllerID."' AND active=1";
                DBConnect();
                $sensorsResult=execQuery($sql);
                DBClose();
                $numOfSensors = rowCount($sensorsResult);
                //echo json_encode($controllerID);
                 // exit();
                if($numOfSensors<1){
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "No sensors found."
                    );
                }else {
                    while ($aRow = fetchNext($sensorsResult)) {
                        $row = array();
                        $sensorName = $aRow["sensorName"];
                        $sensorID = $aRow["sensorID"];
                        $sensorDate = $aRow["dateStart"];
                        array_push($sensors, $sensorName);
                        array_push($sensorIDs,$sensorID);
                        array_push($sensorDates, $sensorDate);
                    }
                    //retreive timestamps and last measurements value
                    for ($i = 0; $i < sizeof($sensorIDs); $i++)
                    {

                    $sensorID = $sensorIDs[$i];
                    $date1 = $sensorDates[$i];
                   /* echo json_encode($sensorIDs[1]);
                    exit();*/
                    
                    $sql = "SELECT * FROM `sensor-measurement` WHERE sensorID=" . $sensorID . " AND datetime >'" . $date1 . "' ORDER BY datetime DESC LIMIT 1";
                    DBConnect();

                    $result=execQuery($sql);
                    DBClose();    
                    $numOfSensors = rowCount($result);

                    if($numOfSensors < 1){
                         $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "No measurements found for "+$sensorID+" sensor."
                    );
                    } else{
                    
                    while ($aRow = fetchNext($result)) {

                        $measurement = $aRow["measurement"];
                        $timestamp = $aRow["datetime"];
                        array_push($measurements, $measurement);
                        array_push($timestamps,$timestamp);
                    }

                    }
                    //retreive sensor types
                    $sql = "SELECT * FROM sensor,`type-sensor` WHERE sensorID= '". $sensorID ."' AND sensorProperty=typeSensorID";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    $numOfSensors = rowCount($result);
                    if($numOfSensors<1){
                    $response = array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "No sensor types found."
                    );
                }else {

                    while ($aRow = fetchNext($result)) {
                        //$row = array();

                        $type = $aRow["type"];

                        $frequency = $aRow["frequency"];

                        array_push($sensorTypes, $type);

                        array_push($sensorFrequency,$frequency);
                    }

                    } //retreive types


                    }//for

                    //retreive shared sensors
                    $sql="SELECT * FROM sn_shared_sensors WHERE (ownerID = '".$ownerID."' AND watcherID = '".$userID."') OR (ownerID = '".$userID."' AND watcherID = '".$ownerID."' ) AND boardID='".$controllerID."'";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    $num=rowCount($result);
                    $availableSensors = array();
                        $readValues = array();
                        $writeValues = array();
                        $requestedSensors = array();
                    if($num<1){
                         $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Successfully retrieved sensors and shared-sensors",
                        "count" => count($sensors),
                        "sensors" => $sensors,
                        "sensorIDs" => $sensorIDs,
                        "availableSensors" => $availableSensors,
                        "readValue" => $readValues,
                        "writeValue" => $writeValues,
                        "sensorTypes" => $sensorTypes,
                        "sensorFrequency" => $sensorFrequency,
                        "sensorMeasurements" => $measurements,
                        "sensorTimestamps" => $timestamps,
                        "requestedSensors" => $requestedSensors
                    );
                    
                    } else{
    
                        while ($aRow = fetchNext($result)) {
                        $sensorID = $aRow["sensorID"];
                        $read = $aRow["readValue"];
                        $write =  $aRow["writeValue"];
                        array_push($availableSensors, $sensorID);
                        array_push($readValues, $read);
                        array_push($writeValues, $write);
                    }

                } //else


                    $sql="SELECT * FROM sn_requested_sensors WHERE receiverID = '".$ownerID."' AND applicantID = '".$userID."' AND boardID='".$controllerID."'";
                    DBConnect();
                    $result=execQuery($sql);
                    DBClose();
                    $num=rowCount($result);
                    if($num>=1){
                        while ($aRow = fetchNext($result)) {
                            $sensorID2 = $aRow["sensorID"];
                            array_push($requestedSensors, $sensorID2);
                        }


                    }

                        $response = array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Successfully retrieved sensors",
                        "count" => count($sensors),
                        "sensors" => $sensors,
                        "sensorIDs" => $sensorIDs,
                        "availableSensors" => $availableSensors,
                        "readValue" => $readValues,
                        "writeValue" => $writeValues,
                        "sensorTypes" => $sensorTypes,
                        "sensorFrequency" => $sensorFrequency,
                        "sensorMeasurements" => $measurements,
                        "sensorTimestamps" => $timestamps,
                        "requestedSensors" => $requestedSensors
                    );

                    
                
                 //retreive types
                } //retreive sensors

            }//isset owner
            } else {
                $response = array(
                    "status" => "token expired",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You have to provide a valid token. Connect again to gain access"
                );
            }
        }
    } else {

        $response = array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);