<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');


$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])){
        $token = $_POST["token"];
        $userID= -1;
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID = $aRow["userID"];
            }
            if (checkToken($valid)){
              $sensors=array();
                //Find the sensor Info
                $sql = "SELECT s.`sensorID`, s.`sensorName`, s.`type` sensorType, c.`controllerName`, c.`type` controllerType FROM `sensor` s
            						JOIN `sensor-controller` sc ON sc.`sensorID`=s.`sensorID`
                        JOIN `user-controller` uc ON uc.`controllerID`=sc.`controllerID`
                        JOIN `micro-controller` c ON c.`controllerID`=uc.`controllerID`
            						WHERE `userID`=" . $userID;
                DBConnect();
                $result=execQuery($sql);
                $sql2 = "SELECT * FROM `user-file` uf
                        JOIN `file` f ON uf.`fileID`=f.`fileID`
                        JOIN `micro-controller` c ON f.`controller`=c.`controllerID`
                        WHERE `userID`=" . $userID;
                $result2=execQuery($sql2);
                $allSensors = array();
                while ($row=fetchNext($result2)){
                  array_push($allSensors, $row["sensors"]);
                }
                DBClose();
                while ($aRow=fetchNext($result)){
                  $exists = false;
                  $e = 0;
                  for ($k=0; $k<sizeof($allSensors); $k++){
                    $s = $allSensors[$k];
                    $sem = false;
                    for ($i=0; $i<strlen($s); $i++){
                      if ($s{$i}==';'){
                        $sem=true;
                        break;
                      }
                    }
                    if ($sem==true){
                      $tokens = explode( ";", $s);
                    }
                    else{
                      $tokens = array($s);
                    }
                    for ($i=0; $i<sizeof($tokens); $i++){
                      if ($tokens[$i]==$aRow["sensorID"]){
                        $exists = true;
                        $e = 1;
                        break;
                      }
                    }
                    if ($exists==true){
                      break;
                    }
                  }
                  $aSensor=array("sensorID" => $aRow["sensorID"], "sensorName" => $aRow["sensorName"],
                                     "sensorType" => $aRow["sensorType"], "controllerName" => $aRow["controllerName"],
                                   "controllerType" => $aRow["controllerType"], "inFile" => $e);
                  array_push($sensors, $aSensor);
                }
                  $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Successfully retrieved",
                    "sensors" => $sensors
                  );
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "token expired"
                );
              }
    }
  }
  else{
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "You have to provide your token."
    );
  }
}

echo json_encode($response);

?>
