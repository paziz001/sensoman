

var weka = RequireJS('lib/weka-lib.js');
var data ='test.arff';//ARFF json format (see [node-arff](https://github.com/chesles/node-arff))

//See Weka Documentation
var options = {
    //'classifier': 'weka.classifiers.bayes.NaiveBayes',
    'classifier': 'weka.classifiers.functions.SMO',
    'params'    : ''
};

var testData = {
    length    : '4.9',
    color      : 'Red',
    age: 30,
    neighbours   : 1,
    class      : 'no' // last is class attribute
};

function start(){
    console.log("Stella");
    weka.classify(data, testData, options, function (err, result) {

        console.log(result); //{ predicted: 'yes', prediction: '1' }

    });
}
/**
 * Created by Stella Constantinou on 6/8/2015.
 */