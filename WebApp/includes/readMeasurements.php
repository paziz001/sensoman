<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 10/3/2015
 * Time: 9:43 μμ
 */
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
include_once('common.php');
require_once('connectdb.php');
$date=new DateTime('now');
$method=getRequestMethod();
$ip=getClientIP();
if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    $filename=$_POST["filename"];
    $handle = fopen($filename, "r");
    if ($handle) {
        $lineNum=1;
        $motions=array();
        $high=false;
        while (($line = fgets($handle)) !== false) {
            if ($lineNum==1){
                $split=preg_split('/ /',$line);
                $dateS=$split[0];
                $time=$split[1];
                $splitDate=preg_split('/-/',$dateS);
                $year=$splitDate[0];
                $month=check($splitDate[1]);
                $day=check($splitDate[2]);
                $splitTime=preg_split('/:/',$time);
                $hour=check($splitTime[0]);
                $minute=check($splitTime[1]);
                $sec=check($splitTime[2]);
                $dt=$year."-".$month."-".$day." ".$hour.":".$minute.":".$sec;
                $timestamp= strtotime($dt);
                $lineNum++;
            }
            else {
                $split = preg_split('/ /', $line);
                $controllerName = $split[0];
                $sensorName = $split[1];
                $sensorMeasurement = $split[2];
                $datetime = $split[3];
                $seconds = $datetime / 1000;
                $datetime = $timestamp+$seconds;
                $datetime=date("Y-m-d H:i:s", $datetime);
                $sql = "SELECT * FROM sensor, `type-sensor` WHERE sensorName='" . $sensorName."' AND sensor.sensorProperty=`type-sensor`.typeSensorID";
                DBConnect();
                $result = execQuery($sql);
                DBClose();
                while ($aRow = fetchNext($result)) {
                    $sensorID = $aRow["sensorID"];
                    $type=$aRow["type"];
                }
                if ($type=="motion") {
                    if (($high==true)){
                        if ($sensorMeasurement==1) {
                            array_push($motions, $datetime);
                        }
                        else{
                            $sql="INSERT INTO `motion`(sensorID, datetimeStart, datetimeEnd) VALUES ('".$sensorID."','".$motions[0]."','".$motions[count($motions)-1]."')";
                            DBConnect();
                            execQuery($sql);
                            DBClose();
                            $motions=array();
                            $high=false;
                        }
                    }
                    else{
                        if ($sensorMeasurement==1){
                            array_push($motions,$datetime);
                            $high=true;
                        }
                    }
                }
                else{
                    if ($type=="humidity"){
                        if ($sensorMeasurement==1){
                            $sensorMeasurement="HIGH";
                        }
                        else{
                            $sensorMeasurement="LOW";
                        }
                    }
                    $sql = "INSERT INTO `sensor-measurement` (sensorID, measurement,datetime) VALUES('" . $sensorID . "','" . $sensorMeasurement . "','" . $datetime . "')";
                    DBConnect();
                    execQuery($sql);
                    DBClose();
                }


            }
        }
        fclose($handle);
        $response=array(
            "status" => "success",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "All records have successfully added in database"
        );
    } else {
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "Failed to open file"
        );
    }
}
echo json_encode($response);

function check($num){
    $res=$num/10;
    if ($res<1){
        return "0".$num;
    }
    else{
        return $num;
    }

}