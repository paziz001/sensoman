<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 18/1/2015
 * Time: 11:09 μμ
 */

include_once('common.php');
require_once('connectdb.php');

function checkToken($v) {
    //return true if the current date is smaller than the valid token date
    $valid=new DateTime($v);
    $date=new DateTime('now');
    if (($valid->getTimestamp() > $date->getTimestamp())) {
        return true;
    } else {
        return false;
    }
}

function setToken($user){
    DBConnect();
    srand((double)microtime() * 1000000);
    $token = substr(md5(rand(0, 9999999)), 0, 64);
    $date=new DateTime('now');
    $date->add(new DateInterval('PT30M'));
    $valid = $date->format('Y-m-d H:i:s');
    execProcedure("SET_TOKEN('$user', '$token', '$valid')");
    DBClose();
    return $token;
}