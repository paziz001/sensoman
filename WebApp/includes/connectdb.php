<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 12/12/2014
 * Time: 11:41 μμ
 */

$con;

function DBConnect()
{
    $serverName = "localhost";
    $database = "sensors";
    $username = "root";
    $password = "";

    global $con;
    $con = mysqli_connect($serverName,$username,$password);

    if(!$con)
    {
        $response = array(
            "status" => "error",
            "message" => mysql_error()
        );
        die(json_encode($response));
    }

    mysqli_select_db($con, $database);

    return $con;
}

function execQuery($query)
{
    global $con;
    mysqli_query($con, "SET NAMES utf8");
    $res = mysqli_query($con, $query);

    return $res;
}

function execProcedure($procedure)
{
    return execQuery("CALL ".$procedure);
}

function fetchNext($result)
{
    return mysqli_fetch_array($result);
}

function rowCount($result)
{
    $n = mysqli_num_rows($result);
    if ($n == NULL) return 0;
    return $n;
}

function DBClose()
{
    global $con;
    mysqli_close($con);
}