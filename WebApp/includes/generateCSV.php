<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 10/3/2015
 * Time: 10:12 μμ
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
header('Content-Disposition: attachement; filename="CSV.csv";');
include_once('common.php');
require_once('connectdb.php');

$date=new DateTime('now');
$method=getRequestMethod();
$ip=getClientIP();
if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    $filename=$_POST["filename"];
    $sql="SELECT * FROM `sensor-measurement` ORDER BY datetime";
    DBConnect();
    $result=execQuery($sql);
    DBClose();
    $previous="";
    $instances=array();
    $i=0;
    while($aRow=fetchNext($result)) {
        $sensorID = $aRow["sensorID"];
        $sql = "SELECT * FROM sensor,`type-sensor` WHERE sensor.sensorID=" . $sensorID . " AND sensor.sensorProperty=`type-sensor`.typeSensorID";
        DBConnect();
        $r1 = execQuery($sql);
        DBClose();
        while ($a = fetchNext($r1)) {
            $type = $a["type"];
        }
        $sensorMeasurement = $aRow["measurement"];
        $datetime = $aRow["datetime"];
        if ($previous =="") {
            $previous = $datetime;
            $instances[$i]["hour"] = $datetime;
            $instances[$i][$type] = $sensorMeasurement;
        }
        else{
            if ($datetime==$previous) {
                $instances[$i][$type] = $sensorMeasurement;
            } else {
                $i++;
                $previous = $datetime;
                $instances[$i]["hour"] = $datetime;
                $instances[$i][$type] = $sensorMeasurement;
            }
        }
    }
    $k=0;
    $fp = fopen($filename, 'w');
    $title=array("hour", "ldr", "temperature", "sound", "humidity");
    fputcsv($fp,$title);
    while ($k<count($instances)){
        fputcsv($fp, $instances[$k]);
        $k++;
    }
    fclose($fp);
    $response=array(
        "status" => "success",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "All measurements are stored in CSV file"
    );

}

echo json_encode($response);

