<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 19/1/2015
 * Time: 12:14 πμ
 */

include_once('common.php');
require_once('connectdb.php');

function findLocation($lat, $lon, $rad){

    $R = 6371;  // earth's mean radius, km
    $db=DBConnect();
    //equation to find the locations with the radius given the user near a location given
    $stmt=$db->prepare('SELECT locationID,longitude,latitude, ( ? * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin( radians( latitude ) ) ) ) AS distance FROM location HAVING distance < ? ORDER BY distance');
    $stmt->bind_param('idddi',$R,$lat,$lon,$lat,$rad);
    $stmt->execute();
    $result=$stmt->get_result();
    //$R$lat$lon$lat$rad
    DBClose();
    $location=array();
    //Fetch the results
    while($aRow=fetchNext($result)){
        $row = array();
        $row["locationID"]=$aRow["locationID"];
        $row["latitude"]=$aRow["latitude"];
        $row["longitude"]=$aRow["longitude"];

        array_push($location, $row);
    }
    return $location;

}