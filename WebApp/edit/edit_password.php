<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

DBConnect();
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["newpassword1"]) && isset($_POST["newpassword2"]) && isset($_POST["oldpassword"])){
        $token = $_POST["token"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                $new1=$_POST["newpassword1"];
                $new2=$_POST["newpassword2"];
                $old = hash('sha256',$_POST["oldpassword"]);
                if ($new1==$new2){
                  $sql = "SELECT * FROM `user` WHERE `userID` =" . $userID . " && `password` = '" . $old . "'";
                  DBConnect();
                  $result=execQuery($sql);
                  DBClose();
                  $rows=rowCount($result);
                  if ($rows>0){
                    $password = hash('sha256',$new1);
                    $sql = "UPDATE `user` SET `password` = '". $password . "' WHERE `userID` =" . $userID;
                    DBConnect();
                    if (execQuery($sql)){
                      $response=array(
                          "status" => "success",
                          "request time" => $date->format("Y-m-d h:m:s"),
                          "IP Address" => $ip,
                          "message" => "Password has changed successfully."
                        );
                      }
                      else{
                        $response=array(
                          "status" => "fail",
                          "request time" => $date->format("Y-m-d h:m:s"),
                          "IP Address" => $ip,
                          "message" => "Unable to change the password."
                        );
                      }
                  }
                  else{
                    $response=array(
                      "status" => "fail",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "Wrong old password."
                    );
                  }
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "The two passwords do not match."
                );
              }
         }
         else{
           $response=array(
               "status" => "fail",
               "request time" => $date->format("Y-m-d h:m:s"),
               "IP Address" => $ip,
               "message" => "token expired"
           );
         }
       }
     }
     else{
       $response=array(
         "status" => "fail",
         "request time" => $date->format("Y-m-d h:m:s"),
         "IP Address" => $ip,
         "message" => "You have to provide your current token, the old password and two times the new password."
       );
     }
}
echo json_encode($response);

?>
