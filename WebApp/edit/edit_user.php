<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
DBConnect();
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["password"]) && isset($_POST["oldpassword"]) && isset($_POST["id"])){
        $token = $_POST["token"];
        $password=$_POST["password"];
        $id = $_POST["id"];
        $old = hash('sha256',$_POST["oldpassword"]);
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                $sql = "SELECT * FROM `user` WHERE `userID` =" . $userID . " && `password` = '" . $old . "'";
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                $rows=rowCount($result);
                if ($rows>0){
                  $password = hash('sha256',$password);
                  $sql = "UPDATE `user` SET `password` = '". $password . "' WHERE `userID` =" . $id;
                  DBConnect();
                  if (execQuery($sql)){
                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Password has changed successfully."
                      );
                    }
                    else{
                      $response=array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Unable to change the password."
                      );
                    }
                }
                else{
                  $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Wrong old password."
                  );
                }
         }
         else{
           $response=array(
               "status" => "fail",
               "request time" => $date->format("Y-m-d h:m:s"),
               "IP Address" => $ip,
               "message" => "You have to provide a valid token. Connect again to gain access."
           );
         }
       }
     }
     else{
       $response=array(
         "status" => "fail",
         "request time" => $date->format("Y-m-d h:m:s"),
         "IP Address" => $ip,
         "message" => "You have to provide your current token, the user id and the new password."
       );
     }
}
echo json_encode($response);

?>
