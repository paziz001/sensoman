<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

DBConnect();
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["username"])){
        $token = $_POST["token"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                $username = $_POST["username"];
                $sql = "SELECT `username` name FROM `user` WHERE `userID` =" . $userID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();
                $aRow=fetchNext($result);
                $name = $aRow["name"];
                if ($name!=$username){
                  $sql = "UPDATE `user` SET `username` = '". $username . "' WHERE `userID` =" . $userID;
                  DBConnect();
                  if (execQuery($sql)){
                    $response=array(
                        "status" => "success",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Username has changed successfully."
                      );
                    }
                    else{
                      $response=array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "Unable to change the username."
                      );
                    }
                  }
                  else{
                    $response=array(
                        "status" => "fail",
                        "request time" => $date->format("Y-m-d h:m:s"),
                        "IP Address" => $ip,
                        "message" => "The username is the same."
                    );
                  }
         }
         else{
           $response=array(
               "status" => "fail",
               "request time" => $date->format("Y-m-d h:m:s"),
               "IP Address" => $ip,
               "message" => "token expired"
           );
         }
       }
     }
     else{
       $response=array(
         "status" => "fail",
         "request time" => $date->format("Y-m-d h:m:s"),
         "IP Address" => $ip,
         "message" => "You have to provide your current token and your new username."
       );
     }
}
echo json_encode($response);

?>
