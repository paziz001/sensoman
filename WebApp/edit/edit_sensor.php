<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
DBConnect();
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["name"]) && isset($_POST["id"])){
        $token = $_POST["token"];
        $sensorName=$_POST["name"];
        $id = $_POST["id"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                $sql = "UPDATE `sensor` SET `sensorName` = '". $sensorName . "' WHERE `sensorID` =" . $id;
                DBConnect();
                if (execQuery($sql)){
                  $response=array(
                      "status" => "success",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "Sensor name has changed successfully."
                  );
                }
                else{
                  $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Unable to change the sensor name."
                  );
                }
         }
         else{
           $response=array(
               "status" => "fail",
               "request time" => $date->format("Y-m-d h:m:s"),
               "IP Address" => $ip,
               "message" => "You have to provide a valid token. Connect again to gain access."
           );
         }
       }
     }
     else{
       $response=array(
         "status" => "fail",
         "request time" => $date->format("Y-m-d h:m:s"),
         "IP Address" => $ip,
         "message" => "You have to provide your current token, the sensor id and the new sensor name."
       );
     }
}
echo json_encode($response);

?>
