<?php
/**
 * Evangelos Aristodemou
 */

include_once('../includes/common.php');
require_once('../includes/connectdb.php');
include_once('../includes/token.php');

DBConnect();
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"]) && isset($_POST["id"]) && isset($_POST["time"]) && isset($_POST["pins"])){
        $token = $_POST["token"];
        $time=$_POST["time"];
        $pins = $_POST["pins"];
        $id = $_POST["id"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
              DBConnect();
              $sql = "SELECT * FROM `user-file` WHERE fileID =" . $id . " AND userID=" . $userID;
              $result=execQuery($sql);
              DBClose();
              $rows=rowCount($result);
              if ($rows>0){
                $sql = "UPDATE `file` SET `timeInterval` = '". $time . "', `pins` = '" . $pins ."' WHERE `fileID` =" . $id;
                DBConnect();
                if (execQuery($sql)){
                  $response=array(
                      "status" => "success",
                      "request time" => $date->format("Y-m-d h:m:s"),
                      "IP Address" => $ip,
                      "message" => "File info has changed successfully."
                  );
                }
                else{
                  $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Unable to change the file info."
                  );
                }
              }
              else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "You are not authorized to change this file."
                );
              }
         }
         else{
           $response=array(
               "status" => "fail",
               "request time" => $date->format("Y-m-d h:m:s"),
               "IP Address" => $ip,
               "message" => "token expired"
           );
         }
       }
     }
     else{
       $response=array(
         "status" => "fail",
         "request time" => $date->format("Y-m-d h:m:s"),
         "IP Address" => $ip,
         "message" => "You have to provide your current token, the file id, the new time interval and the new pins."
       );
     }
}
echo json_encode($response);

?>
