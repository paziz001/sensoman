<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 10/1/2015
 * Time: 8:42 μμ
 */

include_once('includes/common.php');
require_once('includes/connectdb.php');
include_once('includes/token.php');

$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["username"]) && isset($_POST["password"])) {
        $user = $_POST["username"];
        $pass = hash('sha256',$_POST["password"]);
        DBConnect();
        $resultUser = execProcedure("USER_VALIDATION('$user','$pass')");
        DBClose();
        $numU=rowCount($resultUser);
        if ($numU==1){
            while($aRow=fetchNext($resultUser)){
                $userID=$aRow["userID"];
                $userType=$aRow["type"];
                $username=$aRow["username"];
                $email=$aRow["email"];
            }
            DBConnect();
            $resultToken= execProcedure("USER_AUTHENTICATION('$userID')");
            DBClose();
            $numT=rowCount($resultToken);
            if ($numT<1){
                //put users token in the database - create token
                $token=setToken($userID);
            }
            else{
                //check the validation of token
                while($aRow=fetchNext($resultToken)){
                    $validUntil=$aRow["validUntil"];
                    $currentToken=$aRow["token"];
                }
                if (checkToken($validUntil)){
                    $token=$currentToken;
                }
                else{
                    $token=setToken($userID);
                }
            }
            $response=array(
                "status" => "success",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Successfully connected.",
                "token" => $token,
                "userType" => $userType,
                "username" =>$username,
                "email" => $email
            );
        }
        else{
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "Non valid credentials"
            );
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide both username and password");
    }
}
echo json_encode($response);

