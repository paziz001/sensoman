<?php
include_once('../includes/common.php');
require_once('../includes/connectdb.php');
require __DIR__ . '/../vendor/autoload.php';
function processRule($rule, &$logicalStatements)
{
    $conditionalsCount = 0;
    $xml = simplexml_load_string("$rule");
    iterateWholeXml($xml, $logicalStatements, $conditionalsCount);
    $i = 0;
    $finalOutput = findFinalOutputOfLogicalOperation($logicalStatements[$i]);

    if ($finalOutput == 1) {

        for ($j = 0; $j < sizeof($logicalStatements[$i]["actuators"]); $j++) {

            $deviceCommandName = ((int)(explode(",", $logicalStatements[$i]["actuators"][$j])[1]) == 1) ? "ON" : "OFF";
            $deviceName = explode(",", $logicalStatements[$i]["actuators"][$j])[0];
            $ip_and_port = getActuatorIPAndPort($deviceName);
            $code = getActuatorCommandCode($deviceName,$deviceCommandName);
            callActuator($ip_and_port["ip"], $ip_and_port["port"], $code);
        }
    }
    if ($finalOutput == 0 && sizeof($logicalStatements) > 1) {
        $i++;

        if (array_key_exists("compareops", $logicalStatements[$i])) {
            $finalOutput = findFinalOutputOfLogicalOperation($logicalStatements[$i]);

            if ($finalOutput == 1) {
                for ($j = 0; $j < sizeof($logicalStatements[$i]["actuators"]); $j++) {
                    $deviceCommandName = ((int)(explode(",", $logicalStatements[$i]["actuators"][$j])[1]) == 1) ? "ON" : "OFF";
                    $deviceName = explode(",", $logicalStatements[$i]["actuators"][$j])[0];
                    $ip_and_port = getActuatorIPAndPort($deviceName);
                    $code = getActuatorCommandCode($deviceName,$deviceCommandName);
                    print callActuator($ip_and_port["ip"], $ip_and_port["port"], $code);
                }
            }
        } else {
            for ($j = 0; $j < sizeof($logicalStatements[$i]["actuators"]); $j++) {
                $deviceCommandName = ((int)(explode(",", $logicalStatements[$i]["actuators"][$j])[1]) == 1) ? "ON" : "OFF";
                $deviceName = explode(",", $logicalStatements[$i]["actuators"][$j])[0];
                $ip_and_port = getActuatorIPAndPort($deviceName);
                $code = getActuatorCommandCode($deviceName,$deviceCommandName);
                print callActuator($ip_and_port["ip"], $ip_and_port["port"], $code);
            }
        }

    }
}

function initializeModuledRuleArray(&$logicalStatements, &$conditionalsCount)
{
    $logicalStatements[$conditionalsCount] = array();
    $logicalStatements[$conditionalsCount]["logicops"] = array();
    $logicalStatements[$conditionalsCount]["compareops"] = array();
    $logicalStatements[$conditionalsCount]["sensors"] = array();
    $logicalStatements[$conditionalsCount]["actuators"] = array();
}

function getDescription($ruleArrays, &$rule)
{
    $count=1;
    $rule = "when";
    foreach ($ruleArrays as $ruleArray) {

        if (array_key_exists("logicops", $ruleArray) && sizeof($ruleArray["logicops"])) {
            $rule.=($count==2)?"else when":"";
            $i=0;
            $clouse = "\n";
            foreach ($ruleArray["logicops"] as $logicop) {
                if (sizeof($ruleArray["compareops"])<=$i + 1 ) {
                    break;
                }
                $op = convertBlocklyCompOps($ruleArray["compareops"][$i]);
                $sensorName = explode(",", $ruleArray["sensors"][$i])[0];
                $sensorValue = explode(",", $ruleArray["sensors"][$i])[1];
                $lastToCheck = explode(",", $ruleArray["sensors"][$i])[2];
                $clouse .= "\t$sensorName $op $sensorValue | sensitivity: check last $lastToCheck values $logicop\n";
                $i++;
            }
            $op = convertBlocklyCompOps($ruleArray["compareops"][$i]);
            $sensorName = explode(",", $ruleArray["sensors"][$i])[0];
            $sensorValue = explode(",", $ruleArray["sensors"][$i])[1];
            $lastToCheck = explode(",", $ruleArray["sensors"][$i])[2];
            $clouse .= "\t$sensorName $op $sensorValue | sensitivity: check last $lastToCheck values\n";
            $rule .= $clouse;
            $then = "then\n";
            foreach ($ruleArray["actuators"] as $action) {
                $actuator = explode(",", $action)[0];
                $state = ((int)(explode(",", $action)[1]) == 1) ? "ON" : "OFF";
                $then .= "\t$actuator state to $state\n";
            }
            $rule .= $then;
        } elseif (array_key_exists("compareops", $ruleArray) && sizeof($ruleArray["compareops"])) {
            $rule.=($count==2)?"else when":"";
            $op = convertBlocklyCompOps($ruleArray["compareops"][0]);
            $sensorName = explode(",", $ruleArray["sensors"][0])[0];
            $sensorValue = explode(",", $ruleArray["sensors"][0])[1];
            $lastToCheck = explode(",", $ruleArray["sensors"][0])[2];
            $rule .= "\n\t$sensorName $op $sensorValue | sensitivity: check last $lastToCheck values\n";
            $then = "then\n";
            foreach ($ruleArray["actuators"] as $action) {
                $actuator = explode(",", $action)[0];
                $state = ((int)(explode(",", $action)[1]) == 1) ? "ON" : "OFF";
                $then .= "\t$actuator state to $state\n";
            }
            $rule .= $then;
        } else {
            $rule .= "else\n";
            foreach ($ruleArray["actuators"] as $action) {
                $actuator = explode(",", $action)[0];
                $state = (explode(",", $action)[1] == 1) ? "ON" : "OFF";
                $rule .= "\t$actuator state to $state\n";
            }
        }
        $count++;
    }
}

function iterateWholeXml($xml, &$logicalStatements, &$conditionalsCount)
{
    foreach ($xml->children() as $child) {
        if ($child['type'] == "control_when" || $child['type'] == "when_else"
        ) {
            initializeModuledRuleArray($logicalStatements, $conditionalsCount);
        } elseif ($child['type'] == "logic_operation") {
            if($child->field==NULL){
                return new Exception();
            }
            array_push($logicalStatements[$conditionalsCount]["logicops"], $child->field->__toString());
        } else if ($child['type'] == "my_logic_compare") {
            if(!isset($logicalStatements[$conditionalsCount])){
                return new Exception();
            }
            array_push($logicalStatements[$conditionalsCount]["compareops"], $child->field[0]->__toString());
            $sensorName =($child->value[0]!=NULL)? $child->value[0]->block->field->__toString():null;
            if(!$sensorName||$sensorName=="NULL"){
                throw new Exception();
            }
            $sensorValue = $child->field[1]->__toString();
            $numOfValuesToCheck = $child->field[2]->__toString();
            array_push($logicalStatements[$conditionalsCount]["sensors"], "$sensorName,$sensorValue,$numOfValuesToCheck");
            //print_r($sensors);
        } else if ($child->block["type"] == "execution"
        ) {
            if ($child['name'] == "else" && $child->block['type'] == "execution") {
                $logicalStatements[$conditionalsCount] = array();
                $logicalStatements[$conditionalsCount]["actuators"] = array();
            }
            $actuatorName = $child->block->field->__toString();
            $actuatorValue = ($child->block->value->block!=NULL)?$child->block->value->block->field->__toString():null;
            if($actuatorValue==null||$actuatorName=="NULL"){
                throw new Exception();
            }
            if(!isset($logicalStatements[$conditionalsCount])){
                return new Exception();
            }
            array_push($logicalStatements[$conditionalsCount]["actuators"], "$actuatorName,$actuatorValue");
            //print_r($actuators);
            $conditionalsCount++;
        }
        iterateWholeXml($child, $logicalStatements, $conditionalsCount);
    }
}

function convertBlocklyCompOps($op)
{
    switch ($op) {

        case "EQ":
            return "==";
        case "NEQ":
            return "!=";
        case "LT":
            return "<";
        case "LTE":
            return "<=";
        case "GT":
            return ">";
        case "GTE":
            return ">=";
    }
}

function getOperationOutput($op, $value_a, $value_b)
{
    switch ($op) {

        case "EQ":
            return $value_a == $value_b;
        case "NEQ":
            return $value_a != $value_b;
        case "LT":
            return $value_a < $value_b;
        case "LTE":
            return $value_a <= $value_b;
        case "GT":
            return $value_a > $value_b;
        case "GTE":
            return $value_a >= $value_b;
        case "AND":
            if ($value_a && $value_b)
                return 1;
            break;
        case "OR":
            if ($value_a || $value_b)
                return 1;
            break;

    }

    return 0;

}

function getLastNSensorValues($sensorName, $n)
{
    $measurements = array();
    $db = DBConnect();
    $stmt = $db->prepare("SELECT `sensor-measurement`.`measurement`
    FROM `sensor-measurement`,`sensor`
    WHERE `sensor`.`sensorID` = `sensor-measurement`.`sensorID`
    AND `sensor`.`sensorName`= ?
    ORDER BY `sensor-measurement`.`datetime` DESC");
    $stmt->bind_param('s', $sensorName);
    $stmt->execute();
    $measurementsResult = $stmt->get_result();

    $numOfMeasurements = rowCount($measurementsResult);
    $i = 0;
    if ($numOfMeasurements < 1) {
        echo "\r\nno measurements found\r\n";
    } else {
        while ($i < $n) {
            $measurement = fetchNext($measurementsResult)['measurement'];
            array_push($measurements, $measurement);
            $i++;
        }
    }
    return $measurements;
}

function getActuatorIPAndPort($deviceName)
{
    $db = DBConnect();
    $stmt = $db->prepare("SELECT `ip`,`port` FROM `device`,`device-actuator`,`actuator-controller`,`micro-controller` WHERE
`device-actuator`.`actuatorID` = `actuator-controller`.`actuatorID` AND
`actuator-controller`.`controllerID`=`micro-controller`.`controllerID` AND
`device-actuator`.`deviceID` = `device`.`deviceID` AND
`device`.`deviceName` = ?");
    $stmt->bind_param('s', $deviceName);
    $stmt->execute();
    $ip_and_port = $stmt->get_result();
    $aRow = null;
    if (rowCount($ip_and_port) < 1) {
        echo "\r\nactuator not found\r\n";
    } else {
        $aRow = fetchNext($ip_and_port);
    }
    return $aRow;
}

function getActuatorCommandCode($deviceName,$deviceCommandName)
{
    $db = DBConnect();
    $stmt = $db->prepare("SELECT * FROM `device-command-type`,`device-command-code`,`device` WHERE `device-command-type`.`commandType` = ?
    AND `device-command-code`.`commandTypeID` = `device-command-type`.`commandTypeID`
    AND `device-command-code`.`deviceID` = `device`.`deviceID`
    AND `device`.`deviceName` = ? ");
    $stmt->bind_param('ss', $deviceCommandName,$deviceName);
    $stmt->execute();
    $commandCode = $stmt->get_result();
    $aRow = null;
    if (rowCount($commandCode) < 1) {
        echo "\r\ncode not found for $deviceName actuator and $deviceCommandName command\r\n";
    } else {
        $aRow = fetchNext($commandCode);
    }
    return $aRow["code"];
}

function callActuator($ip, $port, $deviceCode)
{
    $url = "$ip:$port/rf_transmiter?params=$deviceCode";
    $response = \Httpful\Request::get($url)->send();
    return 0;
}

function findFinalOutputOfLogicalOperation($statement)
{
    $tempOutput = 0;
    $compareopsOutputs = array();
    for ($i = 0; $i < sizeof($statement["compareops"]); $i++) {
        $validation = 0;
        $sensorName = explode(",", $statement["sensors"][$i])[0];
        $lastValues = explode(",", $statement["sensors"][$i])[2];
        $measurements = getLastNSensorValues($sensorName, $lastValues);
        for ($j = 0; $j < sizeof($measurements); $j++) {
            $validation += getOperationOutput(
                $statement["compareops"][$i]
                , $measurements[$j]//needs change for correct sensor value
                , explode(",", $statement["sensors"][$i])[1]);
        }
        if (sizeof($measurements) > 0) {
            $compareopsOutputs[$i] = (int)($validation == sizeof($measurements));
        } else {
            $compareopsOutputs[$i] = 0;
        }
    }

    for ($i = 0; sizeof($compareopsOutputs) > 1; $i++) {

        $tempOutput = getOperationOutput(
            $statement["logicops"][$i]
            , $compareopsOutputs[0]//needs change for correct sensor value
            , $compareopsOutputs[1]);
        array_splice($compareopsOutputs, -sizeof($compareopsOutputs), 2);
        if (sizeof($compareopsOutputs) == 0) {
            array_push($compareopsOutputs, $tempOutput);

        } else {
            array_unshift($compareopsOutputs, $tempOutput);
        }


    }
    if (sizeof($compareopsOutputs) == 1) {
        $tempOutput = $compareopsOutputs[0];
    }

    $finalOutput = $tempOutput;

    return $finalOutput;

}







