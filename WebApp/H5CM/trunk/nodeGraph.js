//arrays that need to be global
var nodes = [];
var edges = [];
var networkFA;
var selectedEdge;

 // create an array with nodes
 function initGraphDependenciesLoaded(){
    clickToUse = true;
    //css style and fontawesome icons 
      var optionsFA = {
        //clickToUse : true,
        interaction:{hover:true,
          tooltipDelay:400
        },

        groups: {
          usergroups: {
            shape: 'icon',
            icon: {
              face: 'FontAwesome',
              code: '\uf0c0',
              size: 50,
            //  color: '#57169a'
            }
          },
          users: {
            dataToggle: 'popover',
            shape: 'icon',
            icon: {
              face: 'FontAwesome',
              code: '\uf007',
              size: 50,
             // color: '#aa00ff'
            }
          }
        }
      };

      // create a network
      var containerFA = document.getElementById('mynetwork');

      var dataFA = {
        nodes: nodes,
        edges: edges
      };

      var networkFA = new vis.Network(containerFA, dataFA, optionsFA);

      return networkFA;
}

function updateGraph(){
    var updated_nodes = new vis.DataSet(nodes);
    var updated_edges = new vis.DataSet(edges);
    networkFA.setData({nodes:updated_nodes,edges:updated_edges});
}

function loadEventFunction(){

      networkFA.on("selectEdge", function (params) {
       // document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
       selectedEdge = params.edges[0];
        retrieveBoardsGraph(selectedEdge); //get the id of the node

    });

    networkFA.on("click", function (params) {
        //document.getElementById('eventSpan').innerHTML = '<h2>Click event:</h2>' + JSON.stringify(params, null, 4);
    });

   networkFA.on("hoverNode", function (params) {
       //document.getElementById('eventSpan').innerHTML = '<h2>HoverNode event: </h2>' + JSON.stringify(params, null, 4);
       // console.log('hoverNode Event:', params);
        });

     networkFA.on("blurNode", function (params) {
        //console.log('blurNode Event:', params);
    });

     networkFA.on("showPopup", function (params) {
       // document.getElementById('eventSpan').innerHTML = '<h2>showPopup event: </h2>' + JSON.stringify(params, null, 4);
        
    });
}
