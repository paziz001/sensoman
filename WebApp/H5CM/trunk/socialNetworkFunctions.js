/**
* Created by Angelos Efstathiou
* 8/1/2017
*/
var friendIndex;
var sensornames = [];
var isSensorChecked = [];
var selectedGroup;
var boardsArray = []; //sensor-controllers
var sn_sensorControllers;
var selectedBoardId;

//update Permissions
var initStateR = [];
var changedStateR = [];
var initStateW = [];
var changedStateW = [];

$('#testButton').click(function(){
    $('#requestedSensors-modal').modal('show');
    });

$('#send_invitation').click(function(){
    $('#sendInvitation-modal').modal('show');
    });

$('#create_group').click(function(){
    $('#createGroup-modal').modal('show');
    });	

/*$('#userSettings').click(function(){
    $('#userGlobalSettings-modal').modal('show');
    });*/


//the following functions implement the options that user has for his associated members(friends)
//option 0
$('#requestPermissionsButton').click(function(){
	$('#userSettings-modal').modal('hide');
    setTimeout(permissionsModalShow, 500);	
    });


function permissionsModalShow(){
   $('#requestNewPermissions-modal').modal('show');
    var username = friendname_array[friendIndex];
    var ownerID = friendID_array[friendIndex];
    initializeBoardsListOpt1(username, ownerID);
}

/////////////////////////////////////////////////////

//option 1
$('#requestSensorsButton').click(function(){
  $('#userSettings-modal').modal('hide');
    setTimeout(sensorsModalShow, 500);  
    });   
   	
 function sensorsModalShow(){
   $('#requestNewSensors-modal').modal('show');
    var username = friendname_array[friendIndex];
    var ownerID = friendID_array[friendIndex];
    initializeBoardsList(username,ownerID, 1);
}
/////////////////////////////////////////////////////////

//option 2
$('#updateMemberAccessButton').click(function(){
  $('#userSettings-modal').modal('hide');
    setTimeout(accessMemberModalShow, 500);  
    });

function accessMemberModalShow(){
  $('#updateMemberAccess-modal').modal('show');
  var username = localStorage.getItem("username");
  var ownerID = friendID_array[friendIndex];
  initializeBoardsListOpt3(username, ownerID);
}
////////////////////////////////////////////////////////////////

//option 3
$('#grandAccessToSensorsButton').click(function(){
  $('#userSettings-modal').modal('hide');
    setTimeout(grandAccessToSensorsModalShow, 500);  
    });     

function grandAccessToSensorsModalShow(){
    $('#grandAccessToSensors-modal').modal('show');
    var username = localStorage.getItem("username");
    var ownerID = friendID_array[friendIndex];
    initializeBoardsListOpt4(username, ownerID);
    
  }
//////////////////////////////////////////////////////////////////////	

$('#send_invitation_button').click(function(e){
  e.preventDefault();
  sendInvitationToUser();
    });

$('#create_group_button').click(function(){
  createGroup();
});

//clear the input text in modal when it is closed
$('#sendInvitation-modal').on('hidden.bs.modal', function (e) {
  document.getElementById("errormsgInvite").innerText="";
  $(this)
    .find("input")
       .val('')
       .end()

  $("#selectGroupList").attr("disabled", false);
});

//clear the input text in modal when it is closed
$('#createGroup-modal').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input, textarea")
       .val('')
       .end()
});



/*function blockEmail(){
 if( $('#sn_username').val().length > 0 ) {
      $('#sendInvitation-modal').find('#sn_email').val('').end()
  document.getElementById("sn_email").disabled = true;
} else {
    if($('#sn_email').prop('disabled'))
      document.getElementById("sn_email").disabled = false;


  }
}

function blockUsername(){
  console.log($('#sn_email').val().length);
  var inputLength = $('#sn_email').val().length;
  if( inputLength > 0 ) {
      $('#sendInvitation-modal').find('#sn_username').val('').end()
  document.getElementById("sn_username").disabled = true;
} else {
  console.log('here');
    if($('#sn_username').prop('disabled')){
      console.log('here2');
      document.getElementById("sn_username").disabled = false;
    }

  }
}

function blockUsername2(){
 document.getElementById("sn_username").disabled = true;
}

if($('#sn_email').val().length===0){
  if($('#sn_username').prop('disabled')){
      console.log('here2');
      document.getElementById("sn_username").disabled = false;
    }

}*/


$("#inviteUserToJoinAGroup").click(function() {

  $('#selectGroupList option').filter(function() { 
    return ($(this).text() == selectedGroup); //To select Blue
  }).prop('selected', true);

  $("#selectGroupList").attr("disabled", true);
  $('#viewGroup-modal').modal('hide');
  setTimeout(sendInvitation, 500);

});


function sendInvitation(){
  $('#sendInvitation-modal').modal('show');
}

function createGroupModal(){
   $('#createGroup-modal').modal('show');
}
function viewGroupModal(name){
  selectedGroup = name;
  retrieveUsersInGroup(name);
  var foundO = groupOwner_array.includes(name);
  var foundN = groupName_array.includes(name);
  //invitations to join the group can occur only by owner
  if(foundO)
  $("#inviteUserToJoinAGroup").attr("disabled", false);
  else if(foundN)
    $("#inviteUserToJoinAGroup").attr("disabled", true);

   $('#viewGroup-modal').modal('show');
}

function displaySettings(index){
friendIndex = index; //which user is selected
 $('#userSettings-modal').modal('show');
}


 $('#userSettings-modal').on('show.bs.modal', function(e) {
    var username = friendname_array[friendIndex];
    $('#userSettingModalId').text(username);

    //$(e.currentTarget).find('p[id="userSettingModalId"]').val(username);
});


function isAvailableToUser(){
  var token = localStorage.getItem("token");
  var friendID = friendID_array[friendIndex];
  var sensorName = $('#sn_requestNewSensorsList option:selected').text();
 
  $.ajax({
        url: "../retrieve/retrieveAvailableSensors.php",
        method: "POST",
        async: true,
        data: {token: token, friendID: friendID, sensorName: sensorName}
    }).done(function (data) {
      console.log(data);
      if(data.available == 'yes'){
       $("#sn_availableSensor").prop('checked', true);
         // var id = parseInt($('#sn_requestNewSensorsList').children(":selected").attr("id"));
          //isSensorChecked[id] = 1;
       $(".availableSensor").attr("disabled", true);
       } else  if(data.available == 'no'){
            $("#sn_availableSensor").prop('checked', false);
            $(".availableSensor").attr("disabled", false);
            var id = parseInt($('#sn_requestNewSensorsList').children(":selected").attr("id"));
            if(isSensorChecked[id])
              $("#sn_availableSensor").prop('checked', true);
       }
    });
}

//initializes the boards list that user owns
/*function initializeBoardsList(username, option){
   var token = localStorage.getItem("token");
    $.ajax({
        url: "../retrieve/retrieveSensorControllers.php",
        method: "POST",
        async: true,
        data: {token: token, username: username}
    }).done(function (data) {
      console.log(data);
      var controllerNames = data.sensorControllers;
      var sensorControllers;
      switch(option){
        case 0 : sensorControllers = $("#sn_boardsList"); break;
        case 1 : sensorControllers = $("#sn_requestNewBoardsList"); break;
        case 2 : sensorControllers = $("#sn_boardsListMember"); break;
        case 3 : sensorControllers = $("#sn_grandBoardAccessList"); break;
      }

        sensorControllers.empty();
      if(data.message =="No sensor-controllers found."){
           sensorControllers.append('<option value="NoBoards">'+ data.message +'</option>');
           $(".availableSensor").attr("disabled", true);
      }else{
        $.each(controllerNames, function (index, value) {
            sensorControllers.append('<option value="' + value.split(',')[1] + '">' + value.split(',')[0] + '</option>');
            //console.log('<option value="' + value.split(',')[1] + '">' + value.split(',')[0] + '</option>');
        });
        
        }

        initializeSensorsList(option);
    });
    
}*/

//initializes the sensors list attached to the choosed sensor-controller(board)
/*function initializeSensorsList(option) {
    var token = localStorage.getItem("token");
    var sensorsList;
    var sensorControllerID;
        //each option for each window
        switch(option){
        case 0 :  sensorsList = $("#sn_sensorsList"); sensorControllerID = $('#sn_boardsList option:selected').val(); break;//request new permissions break;
        case 1 :  sensorsList = $("#sn_requestNewSensorsList"); sensorControllerID = $('#sn_requestNewBoardsList option:selected').val(); break;
        case 2 :  sensorsList = $("#sn_sensorsListMember"); sensorControllerID = $('#sn_boardsListMember option:selected').val(); break;//update member access break;
        case 3 :  sensorsList = $("#sn_grandSensorsAccessList"); sensorControllerID = $('#sn_grandBoardAccessList option:selected').val(); break;
      }
      //no sensor - controllers(boards) found so it's pointless to try to retrieve any sensors 
      if(sensorControllerID =='NoBoards'){
          sensorsList.empty();
          sensorsList.append('<option value="NoSensors"> No sensors found </option>');
          $(".availableSensor").attr("disabled", true);
        return;
      }

    $.ajax({
        url: "../retrieve/individual-sensors.php",
        method: "POST",
        async: true,
        data: {token: token, controllerID: sensorControllerID}
    }).done(function (data) {
        var sensorNames = data.sensors;
        sensorsList.empty();
        if (data.message == "No sensors found.") {
          sensorsList.append('<option value="NoSensors">'+ data.message +'</option>');
          $(".availableSensor").attr("disabled", true);
        } else {
           $(".availableSensor").attr("disabled", false);
            $.each(sensorNames, function (index, value) {
              sensornames.push(value.split(',')[0]);
                sensorsList.append('<option id="'+index+'" value="' + value.split(',')[1] + '">' + value.split(',')[0] + '</option>');
                //console.log('<option id="'+index+'" value="' + value.split(',')[1] + '">' + value.split(',')[0] + '</option>');
            });
         isAvailableToUser();    
        }
    });
    
}*/

$("#sn_requestNewSensorsList").on('change', function() {
  isAvailableToUser();
})



$('#sn_availableSensor').change(function () {
    var isChecked = $('#sn_availableSensor:checked').length > 0;
    var id = parseInt($('#sn_requestNewSensorsList').children(":selected").attr("id"));
    if(isChecked){
      isSensorChecked[id] = 1;
    } else
      isSensorChecked[id] = 0;
  });

/*$('#tableRequestBoards').find('tr td').click( function(){
  alert('You clicked row '+ ($(this).index()+1) );
});*/

/*$('#tableRequestBoards').on('click', function (e, row, $element) {
  //boardsArray[0].id;
   alert(boardsArray[0].id);
});*/

/*$('#tableRequestBoards').on('click', function (e, row, $element) {
  //boardsArray[0].id;
  // alert(boardsArray[0].id);
   alert('You clicked row '+ ($(this).find('tr').data('time')) );
});*/


/*$('td').click(function() {
    alert($(this).closest('tr').find('input[type=checkbox]').val());
});*/
/*$('#tableRequestBoards').click(function(){
     alert($('tr td').index());
  });*/

/*$('#thetable').find('tr').click( function(){
  alert('You clicked row '+ ($(this).index()+1) );
});
*/

/*$('body').on('click', '#tableRequestBoards tr td', function(e, row, $element){
  alert('You clicked row '+ ($('#tableRequestBoards tr td').index()+1) );
  //alert($('#tableRequestBoards').index()+1);
  //initializeSensorsList(1);
  //whenever any td is clicked, this function will get called
});*/

/*$('#sn_boardsList').on('change', function() {
  initializeSensorsList(0);
});

$('#sn_requestNewBoardsList').on('change', function() {
  initializeSensorsList(1);
});

$('#sn_boardsListMember').on('change', function() {
  initializeSensorsList(2);
});

$('#sn_grandBoardAccessList').on('change', function() {
  initializeSensorsList(3);
});*/

//request Sensors is enable
/*$('#requestNewSensorsButton').click(function(){
  var requestAnswer = [];
  var length = $('#sn_requestNewSensorsList > option').length;
    for(i =0; i<length; i++){
      if(isSensorChecked[i])
        requestAnswer.push(sensornames[i]);
    }
    if(requestAnswer.length<1)
       swal({
  title: "Error!",
  text: "Choose at least one sensor! :)",
  type: "warning",
  showCancelButton: false,
  confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Cancel",
  closeOnConfirm: false
});
    else{
      swal({
 // title: "Error!",
  title : "Request access for the following sensor(s) :",
  text: requestAnswer, 
  type: "success",
  showCancelButton: true,
  //confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn btn-primary",
  confirmButtonText: "Request",
  closeOnConfirm: true
},
function(){
  var token =  localStorage.getItem("token");
  var friendID = friendID_array[friendIndex];
 // var input = {token:token, userID:userID};
  var sensorName = $('#sn_requestNewSensorsList option:selected').text();

     $.ajax({
        url: "../upload/uploadRequestForSensors.php",
        method: "POST",
        async: true,
        data: {token: token, friendID: friendID, sensorName: sensorName}
    }).done(function (data) {
      console.log(data);

    });
  });
    }
  //swal("Request access for the following sensor(s):", requestAnswer, "success");  

    });
*/

//-----------------------------------------------option2---------------------------------------------------
$('#requestNewSensorsButton').click(function(){
  var atLeastOneIsChecked = $('input[name="requestSensorsCheckBox"]:checked').length > 0;
  var requestSensors = [];
  var requestSensorNames = []; 
  var requestedReadValues = [];
  var requestedWriteValues = [];
  var updatePermissions = [];
  if(atLeastOneIsChecked){
   $("input[name='requestSensorsCheckBox']:checked").each( function () {
    var res =$(this).val().split(",");  
    requestSensors.push(res[0]);
    requestSensorNames.push(res[1]);
    requestedReadValues.push('1');
    requestedWriteValues.push('0');
    updatePermissions.push('0');
    //console.log($(this).val());
  });


    swal({
 // title: "Error!",
  title : "Request access for the following sensor(s) :",
  text: requestSensorNames, 
  type: "success",
  showCancelButton: true,
  //confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn btn-primary",
  confirmButtonText: "Request",
  closeOnConfirm: true
},
function(){
  var token =  localStorage.getItem("token");
  var receiverID = friendID_array[friendIndex];
  var boardID = selectedBoardId;
  var input = {token:token, receiverID:receiverID, sensorIDs:requestSensors, boardID:boardID, readValues:requestedReadValues, writeValues:requestedWriteValues, updatePermissions:updatePermissions};
  SensoMan('SensoManRequestSensors', input);
  document.addEventListener('SensoManRequestSensorsContextValueEvent', function (e12) {
    var response = cms.SensoManRequestSensors.SensoManRequestSensors;
    var username = friendname_array[friendIndex];
    var ownerID = friendID_array[friendIndex];
    //reload tables
    swal("Success!",response.message,"success");
    initializeSensorsList(ownerID, selectedBoardId, 1);

  },false); 
  });
  


  } else{
    swal({
  title: "Error!",
  text: "Choose at least one sensor! :)",
  type: "warning",
  showCancelButton: false,
  confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Cancel",
  closeOnConfirm: false
});

  }

//$("input[name='requestSensorsCheckBox']:not(:checked)").each(function (){

//});
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//OPTION 4//////////////////////////////////////////////////////GRAND ACCESS TO SENSORS
$('#grandAccessSensorsButton').click(function(){
  var atLeastOneIsChecked = $('input[name="grandSensorsCheckBox"]:checked').length > 0;
  var requestSensors = [];
  var requestSensorNames = [];  
  if(atLeastOneIsChecked){
   $("input[name='grandSensorsCheckBox']:checked").each( function () {
    var res =$(this).val().split(",");  
    requestSensors.push(res[0]);
    requestSensorNames.push(res[1]);
    //console.log($(this).val());
  });


    swal({
 // title: "Error!",
  title : "Give access to the following sensor(s) :",
  text: requestSensorNames, 
  type: "success",
  showCancelButton: true,
  //confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn btn-primary",
  confirmButtonText: "Request",
  closeOnConfirm: true
},
function(){
  var ownerID = friendID_array[friendIndex];
  for(i = 0; i<requestSensors.length; i++){
  var token =  localStorage.getItem("token");
  var username = localStorage.getItem("username");
  var input = {token:token, username:username, watcherID:friendID_array[friendIndex], sensorID:requestSensors[i], boardID:selectedBoardId, readValue:'1', writeValue:'0', updatePermission:'0'};
  SensoMan('SensoManUpdateSharedSensors', input);
  document.addEventListener('SensoManUpdateSharedSensorsContextValueEvent', function (e12) {
    var response = cms.SensoManUpdateSharedSensors.SensoManUpdateSharedSensors;
    var username = friendname_array[friendIndex];
    
    //reload tables
    swal("Success!",response.message,"success");

  },false);

  
  }//FOR
initializeSensorsListOpt4(ownerID, selectedBoardId, 1);
  });
  


  } else{
    swal({
  title: "Error!",
  text: "Choose at least one sensor! :)",
  type: "warning",
  showCancelButton: false,
  confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Cancel",
  closeOnConfirm: false
});

  }

//$("input[name='requestSensorsCheckBox']:not(:checked)").each(function (){

//});
});

//---------------------------------OPTION 1--------------------------------------------------------------
//OPTION 1//////////////////////////////////////////////////////REQUEST NEW PERMISSIONS
$('#requestNewPermissionsButton').click(function(){
 // console.log("HERE");
 permNames = [];
 permIDs = [];
 permReadValues = [];
 permWriteValues = [];
 updPerm = [];
 
 
 //iterate read checkboxes
  $("input[name='requestNewPermissionsReadCheckBox']").each( function () {
     var res =$(this).val().split(",");
     if(changedStateR[res[0]] == true){
           updPerm.push('1');
           permNames.push(res[2]);
           permIDs.push(res[1]);
           var s = $('#ch'+res[0]).is(":checked");
            if(s == true)
              permReadValues.push('1');
            else  permReadValues.push('0');
           
           //initialize write value
           if(initStateW[res[0]] == true)
           permWriteValues.push('1');
         else permWriteValues.push('0');
     }
     
  });

//iterate write checkboxes
$("input[name='requestNewPermissionsWriteCheckBox']").each( function () {
     var res =$(this).val().split(",");
     if(changedStateW[res[0]] == true){
      //den exei katagrafei akoma o sensor
          if (permNames.indexOf(res[2]) === -1){
            
            updPerm.push('1');
            permNames.push(res[2]);
            permIDs.push(res[1]);
            //init read value

             if(initStateR[res[0]] == true)
              permReadValues.push('1');
            else permReadValues.push('0');


           var s = $('#chW'+res[0]).is(":checked");
            if(s == true)
           permWriteValues.push('1');
         else permWriteValues.push('0');

          }else{ 
            
           var s = $('#chW'+res[0]).is(":checked");
            if(s == true)
              permWriteValues[permNames.indexOf(res[2])]='1';
            else  permWriteValues[permNames.indexOf(res[2])]='0';
          }
           
     }
    
  });

  if(permNames.length > 0){
        swal({
 // title: "Error!",
  title : "Request changes for the following sensor(s) :",
  text: permNames, 
  type: "success",
  showCancelButton: true,
  //confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn btn-primary",
  confirmButtonText: "Request",
  closeOnConfirm: true
},
function(){
  /*console.log(permNames);
  console.log(permReadValues);
  console.log(permWriteValues);*/
  var token =  localStorage.getItem("token");
  var receiverID = friendID_array[friendIndex];
  var boardID = selectedBoardId;
  var input = {token:token, receiverID:receiverID, sensorIDs:permIDs, boardID:boardID, readValues:permReadValues, writeValues:permWriteValues, updatePermissions:updPerm};
  SensoMan('SensoManRequestSensors', input);
  document.addEventListener('SensoManRequestSensorsContextValueEvent', function (e12) {
    var response = cms.SensoManRequestSensors.SensoManRequestSensors;
    var username = friendname_array[friendIndex];
    var ownerID = friendID_array[friendIndex];
    //reload tables
    swal("Success!",response.message,"success");
    initializeSensorsListOpt1(ownerID, selectedBoardId, 1);

  },false);



  });

  } else {
     swal({
  title: "Error!",
  text: "There are no changes! :)",
  type: "warning",
  showCancelButton: false,
  confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Cancel",
  closeOnConfirm: false
});
  }

    
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////OPTION 3////////////////////////////////////////////////////////
 $('#updatePermissionsButton').click(function(){
 // console.log("HERE");
 permNames = [];
 permIDs = [];
 permReadValues = [];
 permWriteValues = [];
 updPerm = [];
 
 
 //iterate read checkboxes
  $("input[name='updatePermissionsReadCheckBox']").each( function () {
     var res =$(this).val().split(",");
     if(changedStateR[res[0]] == true){
           updPerm.push('1');
           permNames.push(res[2]);
           permIDs.push(res[1]);
           var s = $('#chU'+res[0]).is(":checked");
            if(s == true)
              permReadValues.push('1');
            else  permReadValues.push('0');
           
           //initialize write value
           if(initStateW[res[0]] == true)
           permWriteValues.push('1');
         else permWriteValues.push('0');
     }
     
  });

//iterate write checkboxes
$("input[name='updatePermissionsWriteCheckBox']").each( function () {
     var res =$(this).val().split(",");
     if(changedStateW[res[0]] == true){
      //den exei katagrafei akoma o sensor
          if (permNames.indexOf(res[2]) === -1){
            
            updPerm.push('1');
            permNames.push(res[2]);
            permIDs.push(res[1]);
            //init read value

             if(initStateR[res[0]] == true)
              permReadValues.push('1');
            else permReadValues.push('0');


           var s = $('#chUW'+res[0]).is(":checked");
            if(s == true)
           permWriteValues.push('1');
         else permWriteValues.push('0');

          }else{ 
            
           var s = $('#chUW'+res[0]).is(":checked");
            if(s == true)
              permWriteValues[permNames.indexOf(res[2])]='1';
            else  permWriteValues[permNames.indexOf(res[2])]='0';
          }
           
     }
    
  });

  if(permNames.length > 0){
        swal({
 // title: "Error!",
  title : "Request changes for the following sensor(s) :",
  text: permNames, 
  type: "success",
  showCancelButton: true,
  //confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn btn-primary",
  confirmButtonText: "Request",
  closeOnConfirm: true
},
function(){
  /*console.log(permNames);
  console.log(permReadValues);
  console.log(permWriteValues);*/
  for(i = 0; i<permIDs.length; i++){
  var token =  localStorage.getItem("token");
  var watcherID = friendID_array[friendIndex];
  var boardID = selectedBoardId;
  var username = localStorage.getItem("username");
  console.log(watcherID+" "+permIDs[i]+" "+permReadValues[i]+" "+permWriteValues[i]+" "+boardID);
  var input = {token:token, username:username, watcherID:watcherID, sensorID:permIDs[i], boardID:boardID, readValue:permReadValues[i], writeValue:permWriteValues[i], updatePermission:updPerm[i]};
  SensoMan('SensoManUpdateSharedSensors', input);
  document.addEventListener('SensoManUpdateSharedSensorsContextValueEvent', function (e12) {
    var response = cms.SensoManUpdateSharedSensors.SensoManUpdateSharedSensors;
    var username = friendname_array[friendIndex];
   
    //reload tables
    

  },false);

} //FOR

swal("Success!",response.message,"success");
 var ownerID = friendID_array[friendIndex];
    initializeSensorsListOpt3(ownerID, selectedBoardId, 1);
  });

  } else { //there are no changes
     swal({
  title: "Error!",
  text: "There are no changes! :)",
  type: "warning",
  showCancelButton: false,
  confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Cancel",
  closeOnConfirm: false
});
  }

    
});





////////////////////////////////////////////////////////////////////////////////////////////////////////
function retrieveBoardsGraph(userID){

  var token = localStorage.getItem("token");
  var input = {token:token, ownerID:userID};
  SensoMan('SensoManRetrieveSensorControllersGraph', input);
  var controllers;
  document.addEventListener('SensoManRetrieveSensorControllersGraphContextValueEvent', function (e12) {
     controllers = cms.SensoManRetrieveSensorControllersGraph.SensoManRetrieveSensorControllersGraph;

           var t = $('#boards-table-graph').DataTable(
        {
            "pageLength": 5,
            "showNEntries": false,
            "bDestroy":true
        }
    );
        $('#graph-boards-modal').unbind();
        $('#graph-boards-modal').modal('show');

        $('#graph-boards-modal').on('hidden.bs.modal', function () {
         t.rows().remove().draw();
                        });

        var boards = [];
       // boards = sn_sensorControllers.resultControllers;
       //var resultControllers = [];
       if(controllers.status == 'success'){
        $.each(controllers.sharedBoards, function (key, controller) {
                var name = controller["controllerName"];
                var controllerID = controller["controllerID"];
                  boards.push({"Name":controller["controllerName"],
                      "IP":controller["IP"],
                      "Port": controller["Port"],
                      "ModulesNo": "<a contenteditable='true' id='editable2' href='javascript:void(0);' style='text-align: center;' onclick='return retrieveSensorsGraph(\"" + controllerID + "\");'>"+controller["no_of_modules"]+"</a>",
                    });
        });

        var id = 'editable2';

        $('#graph-boards-modal').on('shown.bs.modal', function () {
          $.each(boards, function(key,value){
            
          t.row.add([value["Name"],
                     value["IP"],
                     value["Port"],
                     value["ModulesNo"],
                     ]).draw(false);
                    
            });

        }); 
        
        
     }else {t.row.add("There are no shared boards.");}

    // console.log(controllers);
    },false); 


}

function editMeasurement(){
//console.log(id);
  document.getElementById('editable2').addEventListener("input", function() {
          alert("input event fired");
        }, false);}

function graphSensorsModalShow(){
      $('#graph-sensors-modal').modal('show');
  }

function retrieveSensorsGraph(controllerID){
  $('#graph-boards-modal').modal('hide');

 var token = localStorage.getItem("token");
 var ownerID = selectedEdge;
  var input = {token:token, ownerID:ownerID, controllerID:controllerID};
  SensoMan('SensoManRetrieveSensors', input);
  document.addEventListener('SensoManRetrieveSensorsContextValueEvent', function (e12) {
    alert("here");
    response = cms.SensoManRetrieveSensors.SensoManRetrieveSensors;
    if(response.status == 'success'){
      var sensors = [];
      var availableSensors = response.availableSensors;
      var sensorNames = response.sensors;
      var sensorIDs = response.sensorIDs;
      var sensorTypes = response.sensorTypes;
      var sensorFrequencies = response.sensorFrequency;
      var sensorMeasurements = response.sensorMeasurements;
      var sensorTimestamps = response.sensorTimestamps;
      
      for(i=0; i< sensorNames.length; i++){
        if(sensorIDs.includes(availableSensors[i]) == true){
        sensors.push({
            "Sensor Name" : sensorNames[i],
            "Sensor Type" : sensorTypes[i],
            "SensorFrequency" : sensorFrequencies[i],
            "Measurement" : sensorMeasurements[i],
            "Timestamp" : sensorTimestamps[i],
        });
      }
    }
      var t = $('#sensors-table-graph').DataTable(
        {
            "pageLength": 5,
            "showNEntries": false,
            "bDestroy":true,
            "editable":true
        }
    );
        $('#graph-sensors-modal').unbind();
        setTimeout(graphSensorsModalShow,500);
        //$('#graph-sensors-modal').modal('show');

        $('#graph-sensors-modal').on('hidden.bs.modal', function () {
         t.rows().remove().draw();
                        });

        console.log(sensors);
        $('#graph-sensors-modal').on('shown.bs.modal', function () {
          $.each(sensors, function(key,value){
            
          t.row.add([value["Sensor Name"],
                     value["Sensor Type"],
                     value["SensorFrequency"],
                     value["Measurement"],
                     value["Timestamp"],
                     ]).draw(false);
                    
            });

        });        


    }
},false); 
        
        //sensors=sn_sensorControllers.resultSensors[controllerName];

}

//////////////////////////////OPTION 2////////////////////////////////////////////////////////////////////////////////////////////////
//initializes the boards list that user owns
function initializeBoardsList(username, ownerID, option){
  var token = localStorage.getItem("token");
  var input = {token:token, username:username};
  SensoMan('SensoManRetrieveSensorControllers', input);
  document.addEventListener('SensoManRetrieveSensorControllersContextValueEvent', function (e12) {
  var response = cms.SensoManRetrieveSensorControllers.SensoManRetrieveSensorControllers;
  $('#tableRequestBoards tbody').empty();
  if(response.status == 'success'){
    var sensorControllers = response.sensorControllers;
    boardsArray = [];
    for(i =0; i<sensorControllers.length; i++){
        var res =sensorControllers[i].split(",");                     
        $('#tableRequestBoards tbody').append('<tr onclick="initializeSensorsList(\'' + ownerID + '\',\'' + res[1] + '\',\'' + option + '\')"><td style="min-width:200px">'+res[0]+'</td></tr>');
        //boardsArray[i] = res[1];
        boardsArray.push({"id": res[1], //board id
              "boardName": res[0] //name
    });
        }
        initializeSensorsList(ownerID, boardsArray[0].id, option);
  } else{
    $('#tableRequestBoards tbody').append('<tr><td>'+response.message+'</td></tr>');
  }
  },false); 

  }



  //initializes the sensors list attached to the choosed sensor-controller(board)
  function initializeSensorsList(ownerID, controllerID, option) {
  var token = localStorage.getItem("token");
  var input = {token:token, ownerID:ownerID, controllerID:controllerID};
  SensoMan('SensoManRetrieveSensors', input);
  document.addEventListener('SensoManRetrieveSensorsContextValueEvent', function (e12) {
    response = cms.SensoManRetrieveSensors.SensoManRetrieveSensors;
    $('#tableRequestSensors tbody').empty();
      if(response.status == 'success'){
        var sharedSensors = [];
        var sensors = []; //names
        var sensorIDs = [];
       
        sharedSensors = response.availableSensors;
        sensors = response.sensors;
        sensorIDs = response.sensorIDs;
        sensorTemps = response.sensorTypes;
        requestedSensors = response.requestedSensors;


        for(i =0; i<sensors.length; i++){
          if (sharedSensors.indexOf(sensorIDs[i]) > -1) {                                                                                                                                     
                // sensor is already available
                $('#tableRequestSensors tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input type="checkbox" checked disabled></td></tr>');
            } else {
                //sensor already requested
                if(requestedSensors.indexOf(sensorIDs[i]) > -1){
                  $('#tableRequestSensors tbody').append('<tr class="active" title ="Waiting for approval.."><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input type="checkbox" disabled></td></tr>');
                } else{
                  //still not available
                  $('#tableRequestSensors tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input name ="requestSensorsCheckBox" value = '+sensorIDs[i]+','+sensors[i]+' type="checkbox"></td></tr>');
                }

                  }
            }

          } else{
            $('#tableRequestSensors tbody').append('<tr><td>'+response.message+'</td></tr>');
          }
      
  },false); 
  
  selectedBoardId = controllerID; //store the current selected board
  //var index = sensorIDs.indexOf(controllerID);
  //selectedBoardName = sensors[index] // store the name of selected board
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//$('.myCheckbox').prop('checked', true);


//initializes the boards list that user owns -- OPTION 1
function initializeBoardsListOpt1(username, ownerID){
  var option = 0;
  var token = localStorage.getItem("token");
  var input = {token:token, username:username};
  SensoMan('SensoManRetrieveSensorControllers', input);
  document.addEventListener('SensoManRetrieveSensorControllersContextValueEvent', function (e12) {
  var response = cms.SensoManRetrieveSensorControllers.SensoManRetrieveSensorControllers;
  $('#tableRequestBoardsPermissions tbody').empty();
  if(response.status == 'success'){
    var sensorControllers = response.sensorControllers;
    boardsArray = [];
    for(i =0; i<sensorControllers.length; i++){
        var res =sensorControllers[i].split(",");                     
        $('#tableRequestBoardsPermissions tbody').append('<tr onclick="initializeSensorsListOpt1(\'' + ownerID + '\',\'' + res[1] + '\')"><td style="min-width:200px">'+res[0]+'</td></tr>');
        //boardsArray[i] = res[1];
        boardsArray.push({"id": res[1], //board id
              "boardName": res[0] //name
    });
        }
        initializeSensorsListOpt1(ownerID, boardsArray[0].id);
  } else{
    $('#tableRequestBoardsPermissions tbody').append('<tr><td>'+response.message+'</td></tr>');
  }
  },false); 

  }


function initializeSensorsListOpt1(ownerID, controllerID) {
  var token = localStorage.getItem("token");
  var input = {token:token, ownerID:ownerID, controllerID:controllerID};
  SensoMan('SensoManRetrieveSensors', input);
  document.addEventListener('SensoManRetrieveSensorsContextValueEvent', function (e12) {
    response = cms.SensoManRetrieveSensors.SensoManRetrieveSensors;
    $('#tableRequestSensorsPermissions tbody').empty();
      if(response.status == 'success'){
        var sharedSensors = [];
        var sensors = []; //names
        var sensorIDs = [];
        var sensorTemps = [];
        var requestedSensors =[];
        var readValues = [];
        var writeValues =[];
       
        sharedSensors = response.availableSensors;
        sensors = response.sensors;
        sensorIDs = response.sensorIDs;
        sensorTemps = response.sensorTypes;
        requestedSensors = response.requestedSensors;
        readValues = response.readValue;
        writeValues = response.writeValue;
        initStateR = [];
        initStateW = [];
        changedStateR =[];
        changedStateW =[];
       
        for(i =0; i<sensors.length; i++){
          if (sharedSensors.indexOf(sensorIDs[i]) > -1) {
            x = sharedSensors.indexOf(sensorIDs[i]);
              if(readValues[x] == '1' && writeValues[x]== '1'){
                if(requestedSensors.indexOf(sensorIDs[i]) > -1){
                 $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'" checked disabled>&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox" checked disabled></td></tr>');
                  initStateR[i] = true;
                  initStateW[i] = true;
                }else{
                  $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'" checked>&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox" checked></td></tr>');
                  initStateR[i] = true;
                  initStateW[i] = true;
                }
              }else if(readValues[x] == '0' && writeValues[x]== '0'){
                if(requestedSensors.indexOf(sensorIDs[i]) > -1){
                  $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'" disabled>&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox" disabled></td></tr>');
                  initStateR[i] = false;
                  initStateW[i] = false;
                }else{
                  $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'">&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox"></td></tr>');
                  initStateR[i] = false;
                  initStateW[i] = false;
                }
              } else if(readValues[x] == '1' && writeValues[x]== '0'){
                if(requestedSensors.indexOf(sensorIDs[i]) > -1){
                  $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'" checked disabled>&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox" disabled></td></tr>');
                  initStateR[i] = true;
                  initStateW[i] = false;
                } else{
                  $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'" checked>&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox"></td></tr>');
                  initStateR[i] = true;
                  initStateW[i] = false;
                }
              } else if(readValues[x] == '0' && writeValues[x]== '1'){
                if(requestedSensors.indexOf(sensorIDs[i]) > -1){
                  $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'" disabled>&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox" disabled checked></td></tr>');
                  initStateR[i] = false;
                  initStateW[i] = true;
                } else {
                  $('#tableRequestSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1R" name ="requestNewPermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="ch'+i+'">&nbsp&nbsp<input class="opt1W" name ="requestNewPermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chW'+i+'" type="checkbox" checked></td></tr>');
                  initStateR[i] = false;
                  initStateW[i] = true;
                }
              }


              } //if
            } //for

          } else{
            $('#tableRequestSensorsPermissions tbody').append('<tr><td>'+response.message+'</td></tr>');
          }
          //store wich permissions has been changed
          $('.opt1R:input:checkbox').change(function () {
         var res =$(this).val().split(",");
         var s = $('#ch'+res[0]).is(":checked");
         if(s == initStateR[res[0]])
         changedStateR[res[0]] = false;
         else changedStateR[res[0]] = true;  
        });


        $('.opt1W:input:checkbox').change(function () {
         var res =$(this).val().split(",");
         var s = $('#chW'+res[0]).is(":checked");
         console.log("checked : "+$('#chW'+res[0]).is(":checked"));
         if(s == initStateW[res[0]])
         changedStateW[res[0]] = false;
         else changedStateW[res[0]] = true;  

        });
      
  },false); 
  
  selectedBoardId = controllerID; //store the current selected board
  //var index = sensorIDs.indexOf(controllerID);
  //selectedBoardName = sensors[index] // store the name of selected board
  }

//$('.theClass:checkbox:checked')
  


  //initializes the boards list that user owns -- OPTION 3
function initializeBoardsListOpt3(username, ownerID){
  var option = 0;
  var token = localStorage.getItem("token");
  var input = {token:token, username:username};
  SensoMan('SensoManRetrieveSensorControllers', input);
  document.addEventListener('SensoManRetrieveSensorControllersContextValueEvent', function (e12) {
  var response = cms.SensoManRetrieveSensorControllers.SensoManRetrieveSensorControllers;
  $('#tableUpdateBoardsPermissions tbody').empty();
  if(response.status == 'success'){
    var sensorControllers = response.sensorControllers;
    boardsArray = [];
    for(i =0; i<sensorControllers.length; i++){
        var res =sensorControllers[i].split(",");                     
        $('#tableUpdateBoardsPermissions tbody').append('<tr onclick="initializeSensorsListOpt3(\'' + ownerID + '\',\'' + res[1] + '\',\'' + option + '\')"><td style="min-width:200px">'+res[0]+'</td></tr>');
        //boardsArray[i] = res[1];
        boardsArray.push({"id": res[1], //board id
              "boardName": res[0] //name
    });
        }
        initializeSensorsListOpt3(ownerID, boardsArray[0].id,option);
  } else{
    $('#tableUpdateBoardsPermissions tbody').append('<tr><td>'+response.message+'</td></tr>');
  }
  },false); 

  }


function initializeSensorsListOpt3(ownerID, controllerID,option) {
  var token = localStorage.getItem("token");
  var input = {token:token, ownerID:ownerID, controllerID:controllerID};
  SensoMan('SensoManRetrieveSensorsOpt3', input);
  document.addEventListener('SensoManRetrieveSensorsOpt3ContextValueEvent', function (e12) {
    response = cms.SensoManRetrieveSensorsOpt3.SensoManRetrieveSensorsOpt3;
    $('#tableUpdateSensorsPermissions tbody').empty();
      if(response.status == 'success'){
        var sharedSensors = [];
        var sensors = []; //names
        var sensorIDs = [];
        var sensorTemps = [];
        var requestedSensors =[];
        var readValues = [];
        var writeValues =[];
        sharedSensors = response.availableSensors;
        sensors = response.sensors;
        sensorIDs = response.sensorIDs;
        sensorTemps = response.sensorTypes;
        requestedSensors = response.requestedSensors;
        readValues = response.readValue;
        writeValues = response.writeValue;
        initStateR = [];
        initStateW = [];
        changedStateR =[];
        changedStateW =[];
        for(i =0; i<sensors.length; i++){
          if (sharedSensors.indexOf(sensorIDs[i]) > -1) {
            x = sharedSensors.indexOf(sensorIDs[i]);
              if(readValues[x] == '1' && writeValues[x]== '1'){
                 $('#tableUpdateSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1UR" name ="updatePermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="chU'+i+'" checked>&nbsp&nbsp<input class="opt1UW" name ="updatePermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chUW'+i+'" type="checkbox" checked></td></tr>');
                  initStateR[i] = true;
                  initStateW[i] = true;
              }else if(readValues[x] == '0' && writeValues[x]== '0'){
                  $('#tableUpdateSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1UR" name ="updatePermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="chU'+i+'">&nbsp&nbsp<input class="opt1UW" name ="updatePermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chUW'+i+'" type="checkbox"></td></tr>');
                  initStateR[i] = false;
                  initStateW[i] = false;
              } else if(readValues[x] == '1' && writeValues[x]== '0'){
                  $('#tableUpdateSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1UR" name ="updatePermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="chU'+i+'" checked>&nbsp&nbsp<input class="opt1UW" name ="updatePermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chUW'+i+'" type="checkbox"></td></tr>');
                  initStateR[i] = true;
                  initStateW[i] = false;
              } else if(readValues[x] == '0' && writeValues[x]== '1'){
                  $('#tableUpdateSensorsPermissions tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input class="opt1UR" name ="updatePermissionsReadCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' type="checkbox" id="chU'+i+'">&nbsp&nbsp<input class="opt1UW" name ="updatePermissionsWriteCheckBox" value = '+i+','+sensorIDs[i]+','+sensors[i]+' id="chUW'+i+'" type="checkbox" checked></td></tr>');
                  initStateR[i] = false;
                  initStateW[i] = true;
              }


              } //if
            } //for

          } else{
            $('#tableUpdateSensorsPermissions tbody').append('<tr><td>'+response.message+'</td></tr>');
          }
          //store wich permissions has been changed
          $('.opt1UR:input:checkbox').change(function () {
         var res =$(this).val().split(",");
         var s = $('#chU'+res[0]).is(":checked");
         if(s == initStateR[res[0]])
         changedStateR[res[0]] = false;
         else changedStateR[res[0]] = true;  
        });


        $('.opt1UW:input:checkbox').change(function () {
         var res =$(this).val().split(",");
         var s = $('#chUW'+res[0]).is(":checked");
         console.log(res[0]+" checked : "+$('#chUW'+res[0]).is(":checked"));
         if(s == initStateW[res[0]])
         changedStateW[res[0]] = false;
         else changedStateW[res[0]] = true;  
         console.log(changedStateW[res[0]]);
        });
      
  },false); 
  
  selectedBoardId = controllerID; //store the current selected board
  //var index = sensorIDs.indexOf(controllerID);
  //selectedBoardName = sensors[index] // store the name of selected board
  }

  function initializeBoardsListOpt4(username, ownerID){
  var option = 0;
  var token = localStorage.getItem("token");
  var input = {token:token, username:username};
  SensoMan('SensoManRetrieveSensorControllers', input);
  document.addEventListener('SensoManRetrieveSensorControllersContextValueEvent', function (e12) {
  var response = cms.SensoManRetrieveSensorControllers.SensoManRetrieveSensorControllers;
  $('#tableAccessBoards tbody').empty();
  if(response.status == 'success'){
    var sensorControllers = response.sensorControllers;
    boardsArray = [];
    for(i =0; i<sensorControllers.length; i++){
        var res =sensorControllers[i].split(",");                     
        $('#tableAccessBoards tbody').append('<tr onclick="initializeSensorsListOpt4(\'' + ownerID + '\',\'' + res[1] + '\',\'' + option + '\')"><td style="min-width:200px">'+res[0]+'</td></tr>');
        //boardsArray[i] = res[1];
        boardsArray.push({"id": res[1], //board id
              "boardName": res[0] //name
    });
        }
        initializeSensorsListOpt4(ownerID, boardsArray[0].id, option);
  } else{
    $('#tableAccessBoards tbody').append('<tr><td>'+response.message+'</td></tr>');
  }
  },false); 

  }


  function initializeSensorsListOpt4(ownerID, controllerID, option) {
  var token = localStorage.getItem("token");
  var input = {token:token, ownerID:ownerID, controllerID:controllerID};
  SensoMan('SensoManRetrieveSensors', input);
  document.addEventListener('SensoManRetrieveSensorsContextValueEvent', function (e12) {
    response = cms.SensoManRetrieveSensors.SensoManRetrieveSensors;
    $('#tableAccessSensors tbody').empty();
      if(response.status == 'success'){
        var sharedSensors = [];
        var sensors = []; //names
        var sensorIDs = [];
       
        sharedSensors = response.availableSensors;
        sensors = response.sensors;
        sensorIDs = response.sensorIDs;
        sensorTemps = response.sensorTypes;
        requestedSensors = response.requestedSensors;

        for(i =0; i<sensors.length; i++){
          if (sharedSensors.indexOf(sensorIDs[i]) > -1) {                                                                                                                                     
                // sensor is already available
                $('#tableAccessSensors tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input type="checkbox" checked disabled></td></tr>');
            } else {
                //sensor already requested
                /*if(requestedSensors.indexOf(sensorIDs[i]) > -1){
                  $('#tableAccessSensors tbody').append('<tr class="active" title ="Waiting for approval.."><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input type="checkbox" disabled></td></tr>');
                } else{*/
                  //still not available
                  $('#tableAccessSensors tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input name ="grandSensorsCheckBox" value = '+sensorIDs[i]+','+sensors[i]+' type="checkbox"></td></tr>');
               // }

                  }
            }

          } else{
            $('#tableRequestSensors tbody').append('<tr><td>'+response.message+'</td></tr>');
          }
      
  },false); 
  
  selectedBoardId = controllerID; //store the current selected board
  //var index = sensorIDs.indexOf(controllerID);
  //selectedBoardName = sensors[index] // store the name of selected board
  }

