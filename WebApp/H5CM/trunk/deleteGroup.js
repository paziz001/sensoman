function deleteGroup(name){
swal({
  title: "Are you sure?",
  //text: "You will not be able to see each other measurements!",
  text: "All connections with users in this group will be disbanded!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Delete",
  closeOnConfirm: false
},

function(){
  var token =  localStorage.getItem("token");
	var username = localStorage.getItem("username");
	var input = {token:token, username:username, groupName:name};
	SensoMan('SensoManDeleteGroup', input);
	document.addEventListener('SensoManDeleteGroupContextValueEvent', function (e12) {
    var response = cms.SensoManDeleteGroup.SensoManDeleteGroup;
    if(response.status =="success"){
    swal("Deleted!", name+" has been deleted.", "success");
    retrieveFriends();
    retrieveGroups();
  }else
    swal("Not Deleted!",response.message, "error");

  },false);
});
}