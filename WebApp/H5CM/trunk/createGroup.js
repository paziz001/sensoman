
/*
Created by Angelos
*/
function createGroup(){
	var username = localStorage.getItem("username");
	var token =  localStorage.getItem("token");
	var groupName = document.getElementById("sn_create_group").value;
	var description = document.getElementById("sn_group_description").value;
 	var input = {token:token, username:username, groupName:groupName, description:description};

 	SensoMan('SensoManCreateGroup', input);
	document.addEventListener('SensoManCreateGroupContextValueEvent', function (e12) {
		var response = cms.SensoManCreateGroup.SensoManCreateGroup;
		if(response.status == 'success'){
			$('#createGroup-modal').modal('hide');
			setTimeout(createGroupSuccessMsg(groupName), 500);
		}else{
			$('#createGroup-modal').modal('hide');
			setTimeout(createGroupFailureMsg(), 500);
		}
	},false);
}


function createGroupSuccessMsg(name){
	var str1 = "Group";
		var res = str1.concat(name);
		 res = res.concat(" is successfully created!"); 
		swal("Success!", res, "success");
}

function createGroupFailureMsg(){
		swal("Oops...", "Error in creation!", "error");
}