﻿    // this function will be called when the
    // promises (all dependencies) are done loading
    function tempDependenciesLoaded(){
        var token=localStorage.getItem("token");
        callSensoManTemperatureMeasurements(token); //Initialize Measurements
        initializeNotifications(token); //initialize the notifications to the user
    };

    function callSensoManTemperatureMeasurements(token){

    var postparam={token:token, sensorType:"temperature"};
    SensoMan('SensoManChart', postparam);
    // to handle events when the SensoMan value changes
    document.addEventListener('SensoManChartContextValueEvent', function (e12) {
        document.getElementById("loading").style.display="none";
        document.getElementById("chart").style.visibility="visible";
        var temperatureMeasurements = cms.SensoManChart.SensoManChart.Sensors;
        $("#sensors").empty();
        for	(index = 0; index < temperatureMeasurements.length; index++) {
            var sensorName=temperatureMeasurements[index].SensorName;
            var measurements=temperatureMeasurements[index].Measurements;
            sensorNames.push(sensorName);
            sensorMeasurements.push(measurements);
            $("#sensors").append('<li role="presentation"><a role="menuitem" tabindex='+index +'href="#" onclick="loadTempGraph('+index+')">'+sensorName+'</a></li>');
        }
    }, false);
}

function loadTempGraph(index){
    var categories=[];
    var measurements=[];
        for (j=0; j<sensorMeasurements[index].length; j++) {
            var cat = sensorMeasurements[index][j].Timestamp;
            var meas = parseFloat(sensorMeasurements[index][j].Measurement);
            categories.push(cat);
            measurements.push(meas);
        }
    $(function () {
        $('#temperatureChart').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Temperature taken by Sensor'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Temperature'
                },
                labels: {
                    formatter: function () {
                        return this.value + '°C';
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                    name: sensorNames[index],
                    marker: {
                        symbol: 'square'
                    },
                    data: measurements
                }]
        })})}