var workspace;
/**
 * Created by User on 16/02/2016.
 */
/*requests for blockly workspace*/
function getSensorControllerNames() {
    var token = localStorage.getItem("token");
    var username = localStorage.getItem("username");
    $.ajax({
        url: "../retrieve/sensor-controllers.php",
        method: "POST",
        async: false,
        data: {token: token, username: username}
    }).done(function (data) {
        var controllerNames = data.sensorControllers;
        sensorControllers = ($("#sensor-controllers").html() != undefined) ? $("#sensor-controllers") : $("#sensor-controllers2");
        sensorControllers.empty();
        $.each(controllerNames, function (index, value) {
            sensorControllers.append('<option value="' + value.split(',')[1] + '">' + value.split(',')[0] + '</option>');
        });

    });
}

function getActuatorControllerNames() {
    var token = localStorage.getItem("token");
    var username = localStorage.getItem("username");
    $.ajax({
        url: "../retrieve/actuator-controllers.php",
        method: "POST",
        data: {token: token, username: username}
    }).done(function (data) {
        var controllerNames = data.actuatorControllers;
        $("#actuator-controllers").empty();
        $.each(controllerNames, function (index, value) {
            $("#actuator-controllers").append('<option value="' + value.split(',')[1] + '">' + value.split(',')[0] + '</option>');
        })
    });
}

function getRule(rule_id) {
    var token = localStorage.getItem("token");
    var username = localStorage.getItem("username");
    var rule = "null";
    $.ajax({
        url: "../retrieve/rule_xml.php",
        method: "POST",
        async: false,
        data: {token: token, username: username, ruleID: rule_id}
    }).done(function (data) {
        rule = data.rule_definition;
    });
    return rule;
}


function initializeRulesTable() {
    var token = localStorage.getItem("token");
    var username = localStorage.getItem("username");
    var rules = null;
    $.ajax({
        url: "../retrieve/rules.php",
        method: "POST",
        async: false,
        data: {token: token, username: username}
    }).done(function (data) {
        rules = data.rules;
    });
    return rules;
}

function initializeDevicesBlocklyDropdowns() {
    var token = localStorage.getItem("token");
    var actuatorControllerID = $('#actuator-controllers option:selected').val()
    var deviceNames = [];
    if (actuatorControllerID == undefined) {
        deviceNames = [["select a device", "select a device"]];
        return deviceNames;
    }
    $.ajax({
        url: "../retrieve/devices.php",
        method: "POST",
        async: false,
        data: {token: token, controllerID: actuatorControllerID}
    }).done(function (data) {
        if (data.message == "No devices found.") {
            deviceNames.push([data.message, data.message]);
        } else {
            $.each(data.devices, function (index, value) {
                deviceNames.push([value, value]);
            });
        }
    });
    return deviceNames;
}

function initializeSensorBlocklyDropdowns() {
    var token = localStorage.getItem("token");
    var sensorControllerID = $('#sensor-controllers option:selected').val()
    var sensorNames = [];

    if (sensorControllerID == undefined) {
        sensorNames = [["select sensor", "NULL"]];
        return sensorNames;
    }
    $.ajax({
        url: "../retrieve/individual-sensors.php",
        method: "POST",
        async: false,
        data: {token: token, controllerID: sensorControllerID}
    }).done(function (data) {
        if (data.message == "No sensors found.") {
            sensorNames.push([data.message, data.message]);
        } else {
            $.each(data.sensors, function (index, value) {
                sensorNames.push([value, value]);
            });
        }

    });
    return sensorNames;
}
Blockly.Blocks['control_when'] = {
    init: function () {
        this.appendValueInput("when")
            .appendField("when")
            .setCheck('Boolean');
        this.appendStatementInput("then")
            .setCheck('execution')
            .appendField("then");
        this.setPreviousStatement(true, "else");
        this.setColour(210);
        this.setTooltip('This block can be used to start making a rule.' +
        'The when section can get either a logic operation block or a relation operation block.The <<then>' +
        ' section can get device blocks to be controlled.');
        this.contextMenu = false;
    }
};

Blockly.Blocks['when_else'] = {
    init: function () {
        this.appendValueInput("when")
            .setCheck('Boolean')
            .appendField("when");
        this.appendStatementInput("then")
            .setCheck('execution')
            .appendField("then");
        this.appendStatementInput("else")
            .setCheck(['execution', 'else'])
            .appendField("else");
        this.setColour(210);
        this.setTooltip("This block can be used to " +
        "make a rule do something else if it's " +
        "conditions aren't met. The else section can " +
        "have a <<when then>> block or just device blocks to be controlled. ");
        this.contextMenu = false;
    }
};

Blockly.Blocks['execution'] = {
    init: function () {
        this.appendValueInput("actuator_value")
            .appendField("switch")
            .appendField(new Blockly.FieldDropdown(initializeDevicesBlocklyDropdowns), "set_value");
        this.setPreviousStatement(true, "execution");
        this.setNextStatement(true, "execution");
        this.setColour(290);
        this.setTooltip('The device to be controlled.');
        this.setHelpUrl('http://www.example.com/');
        this.contextMenu = false;

    },
    getVars: function () {
        return [this.getFieldValue('set_value')];
    }
};


Blockly.Blocks['sensor'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown(initializeSensorBlocklyDropdowns), "select_sensor")
            .appendField("value");
        this.setOutput(true, "sensor");
        this.setColour(210);
        this.setTooltip("It can be placed to a relational comparison block to compare the sensor's value.");
        this.setHelpUrl('http://www.example.com/');
        this.contextMenu = false;
    },
    getVars: function () {
        return [this.getFieldValue('select_sensor')];
    }
};

Blockly.Blocks['device_state'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["ON", "1"], ["OFF", "0"]]), "state");
        this.setOutput(true, "switch");
        this.setColour(210);
        this.setTooltip('Action for device.');
        this.setHelpUrl('http://www.example.com/');
        this.contextMenu = false;
    }
};

Blockly.Blocks['digital_value'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown([["HIGH", "1"], ["LOW", "0"]]), "VALUE");
        this.setOutput(true, "Number");
        this.setColour(210);
        this.setTooltip('Digital sensor value.');
        this.setHelpUrl('http://www.example.com/');
        this.contextMenu = false;
    }
};

Blockly.Blocks['my_logic_compare'] = {
    /**
     * Block for comparison operator.
     * @this Blockly.Block
     */
    init: function () {
        var OPERATORS = this.RTL ? [
            ['=', 'EQ'],
            ['\u2260', 'NEQ'],
            ['>', 'LT'],
            ['\u2265', 'LTE'],
            ['<', 'GT'],
            ['\u2264', 'GTE']
        ] : [
            ['=', 'EQ'],
            ['\u2260', 'NEQ'],
            ['<', 'LT'],
            ['\u2264', 'LTE'],
            ['>', 'GT'],
            ['\u2265', 'GTE']
        ];
        this.appendValueInput("sensor_name")
            .setCheck("sensor");

        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown(OPERATORS), "OP");
        this.appendDummyInput()
            .appendField(" ");
        this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput('0',
                Blockly.FieldTextInput.numberValidator), 'NUM');
        this.appendDummyInput("sensor_value")
            .appendField("| check last");
        this.appendDummyInput()
            .appendField(new Blockly.FieldTextInput('0',
                Blockly.FieldTextInput.numberValidator), 'NUM');
        this.appendDummyInput("sensor_value")
            .appendField("values");
        this.setInputsInline(true);

        this.setOutput(true, "Boolean");
        this.setColour(210);
        this.setTooltip('Place a sensor and compare its value with the "X" last values it has taken');
        this.setHelpUrl('http://www.example.com/');
        this.prevBlocks_ = [null, null];
        this.contextMenu = false;
    }
};
Blockly.Blocks['logic_operation'] = {
    /**
     * Block for logical operations: 'and', 'or'.
     * @this Blockly.Block
     */
    init: function () {
        var OPERATORS =
            [[Blockly.Msg.LOGIC_OPERATION_AND, 'AND'],
                [Blockly.Msg.LOGIC_OPERATION_OR, 'OR']];
        this.setHelpUrl(Blockly.Msg.LOGIC_OPERATION_HELPURL);
        this.setColour(Blockly.Blocks.logic.HUE);
        this.setOutput(true, 'Boolean');
        this.appendValueInput('A')
            .setCheck('Boolean');
        this.appendValueInput('B')
            .setCheck('Boolean')
            .appendField(new Blockly.FieldDropdown(OPERATORS), 'OP');
        this.setInputsInline(true);
        // Assign 'this' to a variable for use in the tooltip closure below.
        var thisBlock = this;
        this.contextMenu = false;
        this.setTooltip(function () {
            var op = thisBlock.getFieldValue('OP');
            var TOOLTIPS = {
                'AND': Blockly.Msg.LOGIC_OPERATION_TOOLTIP_AND,
                'OR': Blockly.Msg.LOGIC_OPERATION_TOOLTIP_OR
            };
            return TOOLTIPS[op];
        });
    }
};
/**
 * Populate the currently selected pane with content generated from the blocks.
 */
function uploadRule() {
    var xmlDom = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    var xmlText = Blockly.Xml.domToPrettyText(xmlDom);
    var response = null;
    rule_id = "";
    var token = localStorage.getItem("token");
    $.ajax({
        url: "../upload/upload_rule.php",
        method: "POST",
        async: false,
        data: {token: token, rule: xmlText, ruleID: rule_id}
    }).done(function (data) {

        response = data;
    }).error(function (data) {
        response = data;
    });

    return response;

};

function updateRule(rule_id) {
    var xmlDom = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    var xmlText = Blockly.Xml.domToPrettyText(xmlDom);
    var token = localStorage.getItem("token");
    var response = null;
    $.ajax({
        url: "../upload/upload_rule.php",
        method: "POST",
        async: false,
        data: {token: token, rule: xmlText, ruleID: rule_id}
    }).done(function (data) {
        response = data;
    }).error(function () {
        response = data;
    });

    return response;

};




