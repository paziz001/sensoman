var scriptsURLGroups = new Array(
						   "../H5CM/trunk/modules/sensors/SensoMan.js",
						   //"../H5CM/trunk/deleteGroup.js"
						   //"../H5CM/trunk/socialNetworkFunctions.js"
						   //"../H5CM/trunk/retrieveUsersInGroup.js",
						   //"../H5CM/trunk/nodeGraph.js"//,
						   //friendfirst
						   );

function retrieveGroups() {
dependenciesLoaded=retrieveGroupsDependenciesLoaded;
loadDependencies(scriptsURLGroups);

}
var groupOwner_array=[]; 
var groupName_array=[];



function retrieveGroupsDependenciesLoaded(){
	var token =  localStorage.getItem("token");
	var username = localStorage.getItem("username");
	var input = {token:token, username:username};
	SensoMan('SensoManRetrieveGroups', input);
	document.addEventListener('SensoManRetrieveGroupsContextValueEvent', function (e12) {
		var response = cms.SensoManRetrieveGroups.SensoManRetrieveGroups;
		$('#sn_groups_list').empty();
		$('#sn_groups_list').append('<li><a style="cursor:pointer" onclick="createGroupModal()" id="create_group"><i class="fa fa-plus fa-fw"></i> Create Group</a></li><li class="divider"></li>');
		if(response.status == 'success'){

			//initialize group list in send invitation modal
			$('#selectGroupList').empty();
   			$('#selectGroupList').append('<option value="0" selected>Select a group from the list</option>');

			if(response.message == "Successfully retrieved both owner's and associated groups"){
				$('#sn_groups_list').append('<li class="listErrorsMessages">'+username+' groups <li class="divider"></li></li>');
				//clear arrays
				groupOwner_array=[]; 
				groupName_array=[];
				var groupNames = response.groups;
				var ownerGroups = response.ownerGroups;

			for(i =0; i<ownerGroups.length; i++){
				var name = ownerGroups[i];
				groupOwner_array.push(name);													
				$('#sn_groups_list').append('<li><a><i class="fa fa-group fa-fw" style="cursor:pointer" onclick="viewGroupModal(\'' + name + '\')"></i> '+name+' <span style="margin-left: 0;" class="fa fa-trash fa-fw userSettings pull-right" onclick="deleteGroup(\'' + name + '\')" ></span></a><li class="divider"></li></li>');
				$('#selectGroupList').append('<option value="">'+name+'</option>');
				}
				
				$('#sn_groups_list').append('<li class="listErrorsMessages">Other groups <li class="divider"></li></li>');
			for(i =0; i<groupNames.length; i++){
				var name = groupNames[i];
				groupName_array.push(name);
				$('#sn_groups_list').append('<li><a><i class="fa fa-group fa-fw" style="cursor:pointer" onclick="viewGroupModal(\'' + name + '\')"></i> '+name+' </a><li class="divider"></li></li>');
				}

			}
			else if(response.message == "Successfully retrieved owner's groups."){
				$('#sn_groups_list').append('<li class="listErrorsMessages">'+username+' groups <li class="divider"></li></li>');
				var ownerGroups = response.ownerGroups;
				groupOwner_array=[]; 
				for(i =0; i<ownerGroups.length; i++){
					var name = ownerGroups[i];
					groupOwner_array.push(name);
					$('#sn_groups_list').append('<li><a><i class="fa fa-group fa-fw" style="cursor:pointer" onclick="viewGroupModal(\'' + name + '\')"></i> '+name+' <span style="margin-left: 0;" class="fa fa-trash fa-fw userSettings pull-right" onclick="deleteGroup(\'' + name + '\')" ></span></a><li class="divider"></li></li>');
					$('#selectGroupList').append('<option value="">'+name+'</option>');
				}
			}
			else if(response.message == "Successfully retrieved associated groups"){
				
				$('#sn_groups_list').append('<li class="listErrorsMessages">Other groups <li class="divider"></li></li>');
				var groupNames = response.groups;
				groupName_array=[];
				for(i =0; i<groupNames.length; i++){
				var name = groupNames[i];
				groupName_array.push(name);
				$('#sn_groups_list').append('<li><a><i class="fa fa-group fa-fw" style="cursor:pointer" onclick="viewGroupModal(\'' + name + '\')"></i> '+name+' </a><li class="divider"></li></li>');
				}
			}

		} else{
			$('#sn_groups_list').append('<li class="listErrorsMessages">'+response.message+'</li>');
		}
		},false);



}


