/**
 * Created by Stella Constantinou on 2/3/2015.
 */


// first thing is to get the Singleton instance of the ContextManager
var cms = ContextManagerSingleton.getInstance("application");

// define the scripts to be loaded
var scriptsURL = new Array(

    "../H5CM/trunk/modules/sensors/FacebookConnect.js",
    "../H5CM/trunk/modules/sensors/FacebookInformation.js",
    "../H5CM/trunk/modules/reasoners/FacebookPosts.js"
);

// Load scripts using Promises with jQuery getScript()
// and wait until all dependencies are loaded
loadDependencies(scriptsURL);

// this function will be called when the
// promises (all dependencies) are done loading
function dependenciesLoaded(){
    initCAWApp();
};

function initCAWApp(){


    FacebookConnect('email, user_birthday',
        'http://www.cs.ucy.ac.cy/~sconst05/epl435/H5CM/trunk/assets/fb/channel.html');
    showstuff('fbLogin');

    document.addEventListener('facebookConnectContextValueEvent', function (e6) {
        var fbStatus = cms.facebookConnect.facebookConnect;
        if (fbStatus == "user_connected"){
            hidestuff('fbLogin');
            FacebookIsConnected();
            showstuff('fbLogout');
        }
        else if (fbStatus == "login_cancelled"){
            alert('Login to use the application.');
        }
        else if (fbStatus == "user_logout"){
            hidestuff('fbLogout');
            showstuff('fbLogin');
        }
    }, false);

}

function FacebookIsConnected() {

    var fbData = new Array("name", "email", "gender", "username", "birthday");
    // to get the initial value of the FacebookInfo
    FacebookInformation(fbData);

    document.addEventListener('facebookInfoContextValueEvent', function (e9) {
        var fbInfo = cms.facebookInfo.facebookInfo;
        console.log("fbInformation: " + fbInfo);
        var fbInfoData = fbInfo.split("@%@");
        var fb_data = 'Good to see you, ' + fbInfoData[0] + '.' +
            ' Email: ' + fbInfoData[1] + '.' + ' Gender: ' + fbInfoData[2] + '.' +
            ' Username: ' + fbInfoData[3] + '.' + ' Birthday: ' + fbInfoData[4] + '.';

        var picture = 'https://graph.facebook.com/' + fbInfoData[3] + '/picture?type=large'

        appendImageToElement('fbInfo', picture, 200, 200, 'FB Profile Picture');

        if (fbInfo)
            document.getElementById("fbInfo").style.display = "block";

        document.getElementById("facebookInfo").innerHTML = fb_data;
    }, false);


    // to call the FacebookPosts reasoner module
    FacebookPosts();

    // Listen for the facebookPostsContextValueEvent
    document.addEventListener('facebookPostsContextValueEvent', function (e10) {
        document.getElementById("facebookPosts").innerHTML =
            "FacebookPosts: " + cms.facebookPosts.facebookPosts;
        console.log(cms.facebookPosts.facebookPosts);
    }, false);

}






