/**
 * Created by Stella Constantinou on 1/3/2015.
 */
function SensoMan(context, parameters) {
    var RestMain = 'http://localhost:8080/sensoman/';
    var singletonContextManager = ContextManagerSingleton.getInstance(context);
    var RestfulServiceURL;
    if (context == 'SensoManConnect') {
        RestfulServiceURL = RestMain + 'connect.php';
    }
    else if (context == 'SensoManLogout') {
        RestfulServiceURL = RestMain + 'logout.php';
    }
    else if (context == 'SensoManRegister') {
        RestfulServiceURL = RestMain + 'upload/registration.php';
    }
    else if (context == 'SensoManNotify') {
        RestfulServiceURL = RestMain + 'retrieve/notification.php';
    }
    else if (context == 'SensoManFulNotifications') {
        RestfulServiceURL = RestMain + 'upload/fullfill-jobs.php';
    }
    else if (context == 'SensoManChart') {
        RestfulServiceURL = RestMain + 'retrieve/measurementsType.php';
    }
    else if (context == 'SensoManController') {
        RestfulServiceURL = RestMain + 'upload/upload_controller.php';
    }
    else if (context == 'SensoManCSV') {
        RestfulServiceURL = '';
    }
    else if (context == 'SensoManML') {
        RestfulServiceURL = RestMain + 'retrieve/CSVsimpleML.php';
    }
    else if (context == 'SensoManControllers') {
        RestfulServiceURL = RestMain + 'retrieve/controllers.php';

    }else if (context == 'SensoManRules') {
        RestfulServiceURL = RestMain + 'retrieve/rules.php';

    }else if (context == 'SensoManActuatorsByController') {
        RestfulServiceURL = RestMain + 'retrieve/devices.php';

    }else if (context == 'SensoManSensorsByController') {
        RestfulServiceURL = RestMain + 'retrieve/devices.php';

    }else if (context == 'SensoManRuleById') {
        RestfulServiceURL = RestMain + 'retrieve/rule_xml.php';

    }else if (context == 'SensoManActuatorControllers') {
        RestfulServiceURL = RestMain + 'retrieve/actuator-controllers.php';
    }
    else if (context == 'SensoManSensorControllers') {
        RestfulServiceURL = RestMain + 'retrieve/sensor-controllers.php';
    }
    //social network
	else if (context == 'SensoManSendInvitation') {
		RestfulServiceURL = RestMain + 'upload/sendInvitation.php';
	}
    else if (context == 'SensoManRetrieveRequests'){
        RestfulServiceURL = RestMain + 'retrieve/retrieveRequests.php';
    }
    else if (context == 'SensoManAcceptInvitation'){
        RestfulServiceURL = RestMain + 'upload/acceptInvitation.php';
    }
    else if (context == 'SensoManRetrieveFriends'){
        RestfulServiceURL = RestMain + 'retrieve/retrieveFriends.php';
    }
    else if (context == 'SensoManDeleteFriend'){
        RestfulServiceURL = RestMain + 'delete/deleteFriend.php';
    }
    else if (context == 'SensoManCreateGroup'){
        RestfulServiceURL = RestMain + 'upload/createGroup.php';
    }
    else if (context == 'SensoManRetrieveGroups'){
        RestfulServiceURL = RestMain + 'retrieve/retrieveGroups.php';
    }
    else if (context == 'SensoManRetrieveUsersInGroup'){
        RestfulServiceURL = RestMain + 'retrieve/retrieveUsersInGroup.php';
    }
    else if (context == 'SensoManRetrieveSensorControllers'){
        RestfulServiceURL = RestMain + 'retrieve/sensor-controllers.php';
    }
    else if (context == 'SensoManRetrieveSensors'){
        RestfulServiceURL = RestMain + 'retrieve/retrieveSensors.php';
    }
    else if (context == 'SensoManRetrieveSensorControllersGraph'){
        RestfulServiceURL = RestMain + 'retrieve/retrieveAvailableBoards.php';
    }
    else if (context == 'SensoManRequestSensors'){
        RestfulServiceURL = RestMain + 'upload/uploadRequestForSensors.php';
    }
    else if (context == 'SensoManUpdateSharedSensors'){
        RestfulServiceURL = RestMain + 'upload/updateSharedSensors.php';
    }
    else if (context == 'SensoManRetrieveSensorsOpt3'){
        RestfulServiceURL = RestMain + 'retrieve/retrieveSensors.php';
    }
    else if (context == 'SensoManDeleteGroup'){
        RestfulServiceURL = RestMain + 'delete/deleteGroup.php';
    }

    console.log(RestfulServiceURL);
    console.log(context);
    console.log(parameters);
    $.ajax({
        type: "post",
        url: RestfulServiceURL,
        data: parameters,
        success: function (result) {
            var response = result;
            console.log(response);
            if (result.status == "token expired") {
                var token = localStorage.getItem("token");
                postparam = {token: token};
                swal({
                        title: "Session has expired",
                        text: "Please connect again to gain access.",
                        type: "info",
                        showCancelButton: false,
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    },
                    function(){
                        localStorage.removeItem("token");
                        localStorage.removeItem("current");
                        localStorage.removeItem("type");
                        localStorage.removeItem("username");
                        localStorage.removeItem("email");
                        localStorage.removeItem("userlat");
                        localStorage.removeItem("userlng");
                        window.location.href = "login.html";
                    });

            }
            if (context == 'SensoManConnect') {
                singletonContextManager.SensoManConnect.SensoManConnect = response;
                document.dispatchEvent(singletonContextManager.SensoManConnectContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManConnect");
            }
            else if (context == 'SensoManLogout') {
                singletonContextManager.SensoManLogout.SensoManLogout = response;
                document.dispatchEvent(singletonContextManager.SensoManLogoutContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManLogout");
            }
            else if (context == 'SensoManRegister') {
                singletonContextManager.SensoManRegister.SensoManRegister = response;
                document.dispatchEvent(singletonContextManager.SensoManRegisterContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRegister");
            }
            else if (context == 'SensoManNotify') {
                singletonContextManager.SensoManNotify.SensoManNotify = response;
                document.dispatchEvent(singletonContextManager.SensoManNotifyContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManNotify");
            }
            else if (context == 'SensoManFulNotifications') {
                singletonContextManager.SensoManFulNotifications.SensoManFulNotifications = response;
                document.dispatchEvent(singletonContextManager.SensoManFulNotificationsContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManFulNotifications.");
            }
            else if (context == 'SensoManChart') {
                singletonContextManager.SensoManChart.SensoManChart = response;
                document.dispatchEvent(singletonContextManager.SensoManChartContextValueEvent);
                console.log(JSON.stringify(result) + "\nSensoMan is fired -> Requested SensoMan is returned: SensoManChart.");
            }
            else if (context == 'SensoManController') {
                singletonContextManager.SensoManController.SensoManController = response;
                document.dispatchEvent(singletonContextManager.SensoManControllerContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManController.");
            } else if (context == 'SensoManCSV') {
                singletonContextManager.SensoManCSV.SensoManCSV = response;
                document.dispatchEvent(singletonContextManager.SensoManCSVContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManCSV.");
            }
            else if (context == 'SensoManML') {
                singletonContextManager.SensoManML.SensoManML = response;
                document.dispatchEvent(singletonContextManager.SensoManMLContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManML.");
            }
            else if (context == 'SensoManControllers') {
                singletonContextManager.SensoManControllers.SensoManControllers= response;
                document.dispatchEvent(singletonContextManager.SensoManControllersContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManControllers.");
            }else if (context == 'SensoManRules') {
                singletonContextManager.SensoManRules.SensoManRules = response;
                document.dispatchEvent(singletonContextManager.SensoManRulesContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRules.");
            }else if (context == 'SensoManActuatorsByController') {
                singletonContextManager.SensoManActuatorsByController.SensoManActuatorsByController = response;
                document.dispatchEvent(singletonContextManager.SensoManActuatorsByControllerContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManActuatorsByController.");
            }else if (context == 'SensoManSensorsByController') {
                singletonContextManager.SensoManSensorsByController.SensoManSensorsByController = response;
                document.dispatchEvent(singletonContextManager.SensoManSensorsByControllerContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManSensorsByController.");
            }else if (context == 'SensoManRuleById') {
                singletonContextManager.SensoManRuleById.SensoManRuleById= response;
                document.dispatchEvent(singletonContextManager.SensoManRuleByIdContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRuleById.");
            }else if (context == 'SensoManActuatorControllers') {
                singletonContextManager.SensoManActuatorControllers.SensoManActuatorControllers = response;
                document.dispatchEvent(singletonContextManager.SensoManActuatorControllersContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManSensorsByController.");
            }
            else if (context == 'SensoManSensorControllers') {
                singletonContextManager.SensoManSensorControllers.SensoManSensorControllers= response;
                document.dispatchEvent(singletonContextManager.SensoManSensorControllersContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManSensorControllers.");
            }
            else if(context == 'SensoManSendInvitation'){
                singletonContextManager.SensoManSendInvitation.SensoManSendInvitation= response;
                document.dispatchEvent(singletonContextManager.SensoManSendInvitationContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManSendInvitation");
            }
            else if(context == 'SensoManRetrieveRequests'){
                singletonContextManager.SensoManRetrieveRequests.SensoManRetrieveRequests= response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveRequestsContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveRequests");
            }
            else if(context == 'SensoManAcceptInvitation'){
                singletonContextManager.SensoManAcceptInvitation.SensoManAcceptInvitation= response;
                document.dispatchEvent(singletonContextManager.SensoManAcceptInvitationContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManAcceptInvitation");
            }
            else if(context == 'SensoManRetrieveFriends'){
                singletonContextManager.SensoManRetrieveFriends.SensoManRetrieveFriends= response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveFriendsContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveFriends");
            }
            else if(context == 'SensoManDeleteFriend'){
                singletonContextManager.SensoManDeleteFriend.SensoManDeleteFriend= response;
                document.dispatchEvent(singletonContextManager.SensoManDeleteFriendContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManDeleteFriend");
            }
            else if(context == 'SensoManCreateGroup'){
                singletonContextManager.SensoManCreateGroup.SensoManCreateGroup= response;
                document.dispatchEvent(singletonContextManager.SensoManCreateGroupContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManCreateGroup");
            }
            else if(context == 'SensoManRetrieveGroups'){
                singletonContextManager.SensoManRetrieveGroups.SensoManRetrieveGroups= response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveGroupsContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveGroups");
            }
            else if(context == 'SensoManRetrieveUsersInGroup'){
                singletonContextManager.SensoManRetrieveUsersInGroup.SensoManRetrieveUsersInGroup= response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveUsersInGroupContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveUsersInGroup");
            }
            else if(context == 'SensoManRetrieveSensorControllers'){
                singletonContextManager.SensoManRetrieveSensorControllers.SensoManRetrieveSensorControllers = response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveSensorControllersContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveSensorControllers");
            }
            else if(context == 'SensoManRetrieveSensors'){
                singletonContextManager.SensoManRetrieveSensors.SensoManRetrieveSensors = response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveSensorsContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveSensors");
            }
            else if(context == 'SensoManRetrieveSensorControllersGraph'){
                singletonContextManager.SensoManRetrieveSensorControllersGraph.SensoManRetrieveSensorControllersGraph = response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveSensorControllersGraphContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveSensorControllersGraph");
            }
            else if(context == 'SensoManRequestSensors'){
                singletonContextManager.SensoManRequestSensors.SensoManRequestSensors = response;
                document.dispatchEvent(singletonContextManager.SensoManRequestSensorsContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRequestSensors");
            }
            else if(context == 'SensoManUpdateSharedSensors'){
                singletonContextManager.SensoManUpdateSharedSensors.SensoManUpdateSharedSensors = response;
                document.dispatchEvent(singletonContextManager.SensoManUpdateSharedSensorsContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManUpdateSharedSensors");
            }
            else if(context == 'SensoManRetrieveSensorsOpt3'){
                singletonContextManager.SensoManRetrieveSensorsOpt3.SensoManRetrieveSensorsOpt3 = response;
                document.dispatchEvent(singletonContextManager.SensoManRetrieveSensorsOpt3ContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManRetrieveSensorsOpt3");
            }
            else if(context == 'SensoManDeleteGroup'){
                singletonContextManager.SensoManDeleteGroup.SensoManDeleteGroup = response;
                document.dispatchEvent(singletonContextManager.SensoManDeleteGroupContextValueEvent);
                console.log("SensoMan is fired -> Requested SensoMan is returned: SensoManDeleteGroup");
            }

        },
        error: function () {
            console.log("SensoMan occured an error");
        }
    });

}
