function MyRestfulService(RestfulServiceURL, context, parameters){
	
		var singletonContextManager = ContextManagerSingleton.getInstance(context);
		$.ajax({
           type:"post",
		   url: RestfulServiceURL,
           data: parameters,
		   success: function(result){

			    var response=result;
			    if (context=='restfulService1'){
					singletonContextManager.restfulService1.restfulService1 = response;
					document.dispatchEvent(singletonContextManager.restfulService1ContextValueEvent);
					console.log( "RestfulService is fired -> Requested RestfulService is returned: restfulService1" );
				}
				else if (context == 'restfulService2'){
					singletonContextManager.restfulService2.restfulService2 = response;
               document.dispatchEvent(singletonContextManager.restfulService2ContextValueEvent);
               console.log( "RestfulService is fired -> Requested RestfulService is returned: restfulService2" );
               }
				else if (context == 'restfulService3'){
					singletonContextManager.restfulService3.restfulService3 = response;
					document.dispatchEvent(singletonContextManager.restfulService3ContextValueEvent);
					console.log( "RestfulService is fired -> Requested RestfulService is returned: restfulService3" );
				}
				else if (context == 'restfulService4'){
					singletonContextManager.restfulService4.restfulService4 = response;
					document.dispatchEvent(singletonContextManager.restfulService4ContextValueEvent);
					console.log( "RestfulService is fired -> Requested RestfulService is returned: restfulService4" );
				}
				else if (context == 'restfulService5'){
                    console.log("restful5")
                    console.log(response);
					singletonContextManager.restfulService5.restfulService5 = response;
					document.dispatchEvent(singletonContextManager.restfulService5ContextValueEvent);
					console.log( "RestfulService is fired -> Requested RestfulService is returned: restfulService5." );
				}
                else if (context == 'restfulService6'){
                    singletonContextManager.restfulService6.restfulService6 = response;
                    document.dispatchEvent(singletonContextManager.restfulService6ContextValueEvent);
                    console.log( "RestfulService is fired -> Requested RestfulService is returned: restfulService6." );
                }
                else if( context=='restfulService7'){
                    singletonContextManager.restfulService7.restfulService7 = response;
                    document.dispatchEvent(singletonContextManager.restfulService7ContextValueEvent);
                    console.log( "RestfulService is fired -> Requested RestfulService is returned: restfulService7." );
                }
			 
		   },
            error: function(){
                console.log("error");
            }
		});
		
}




