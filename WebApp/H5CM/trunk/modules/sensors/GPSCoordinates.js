function GPSCoordinates() {

		var singletonContextManager = ContextManagerSingleton.getInstance("gpsCoordinates");
			
		if (navigator.geolocation != undefined) {
		//TODO change function parameters
		var location_timeout = setTimeout("geolocFail()", 10000);

                navigator.geolocation.getCurrentPosition(showPosition,function(error) {
				clearTimeout(location_timeout);
				geolocFail();
				});
				

		}
		else {
			return "Not supported on your device or browser.";
		}	   
       

		function showPosition(position){
		 clearTimeout(location_timeout);
			singletonContextManager.gpsLatitude.gpsLatitude = position.coords.latitude;
			singletonContextManager.gpsLongitude.gpsLongitude = position.coords.longitude;

			document.dispatchEvent(singletonContextManager.gpsContextValueEvent);
			
			console.log( "GPSCoordinates showPosition is fired -> Requested position is returned." );
		}
		
		
		function monitorPosition(position){	   
			// Log that a newer, perhaps more accurate
			// position has been found.
			console.log( "monitorPosition is fired -> A new GPS Position was Found." );
 
			// Set the new position of the existing marker.
			singletonContextManager.gpsLatitude.gpsLatitude = position.coords.latitude;
			singletonContextManager.gpsLongitude.gpsLongitude = position.coords.longitude;

			document.dispatchEvent(singletonContextManager.gpsContextValueEvent);
			
			console.log( "GPSCoordinates monitorPosition is fired -> Requested position is returned." );
		}
		
		function geolocFail(){
		clearTimeout(location_timeout);
		console.log("Failed to load Map -> Failed to get current position");
		}
		
}


