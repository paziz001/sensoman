// define the scripts to be loaded
var scriptsURL2 = new Array(
						   "../H5CM/trunk/modules/sensors/SensoMan.js"
						   );

var userID_array=[]; 
var username_array=[]; 
var requestedGroups_array=[];

var sUsernames = [];
var sUserIDs = [];
var scontrollerNames = [];
var scontrollerIDs = [];
var ssensorNames = [];
var ssensorIDs = [];
var swriteValues = [];
var sreadValues = [];
var spermissions =[];

var acceptSensor = true;

function retrieveRequests() {
dependenciesLoaded=retrieveRequestsDependenciesLoaded;
loadDependencies(scriptsURL2);

}


function retrieveRequestsDependenciesLoaded(){
	var token =  localStorage.getItem("token");
	var username = localStorage.getItem("username");
	var input = {token:token, username:username};
	SensoMan('SensoManRetrieveRequests', input);
	document.addEventListener('SensoManRetrieveRequestsContextValueEvent', function (e12) {
		var request_response = cms.SensoManRetrieveRequests.SensoManRetrieveRequests;
		$('#sn_notifications_list').empty();
		$('#sn_notifications_list').append('<li style="text-align: center; min-width:350px;"> Friend Requests <li class="divider"></li> <ul class="dropdown-menu dropdown-user"></ul></li>');
		if(request_response.statusFriend == "success"){
			var usernames = request_response.Usernames;
			var userIDs = request_response.UserIDs;
			var requestedGroupNames = request_response.groups;
			username_array = [];
			userID_array = [];
			requestedGroups_array=[];
		for(i =0; i<usernames.length; i++){
			var name = usernames[i];
			var userID = userIDs[i];
			var groupName =requestedGroupNames[i];
			userID_array.push(userID);
			username_array.push(name);
			requestedGroups_array.push(groupName);
			if(groupName == 'Not Available')
				groupName = '';
			else groupName = ' ('+groupName+')';
			$("#sn_notifications_list").append('<li><a><i class="fa fa-address-card" aria-hidden="true"></i> '+username_array[i]+''+groupName+'<button type="button" class="btn btn-default pull-right" style="padding: 1px 3px; margin-left:5px;" onClick="deleteRequest('+i+')">Delete Request</button> <button type="button" class="btn btn-primary pull-right" style="padding: 1px 3px;" onClick="acceptInvitation('+i+')">Accept</button></a><li class="divider"></li></li>');
			
		}
		} else {
			$('#sn_notifications_list').append('<li class="listErrorsMessages">There are no friend requests.</li>');
			$('#sn_notifications_list').append('<li class="divider"></li>');
		}

		
		if(request_response.statusSensor == 'success'){
			sUsernames=request_response.sUsernames;
			sUserIDs=request_response.sUserIDs;
			scontrollerNames=request_response.controllerNames;
			ssensorNames=request_response.sensorNames;
			swriteValues=request_response.writeValues;
			ssensorIDs=request_response.sensorIDs;
			scontrollerIDs=request_response.controllerIDs;
			sreadValues = request_response.readValues;
			spermissions = request_response.permissions;

			$('#sn_notifications_list').append('<li style="text-align: center; min-width:350px;"> Sensor Requests <li class="divider"></li> <ul class="dropdown-menu dropdown-user"></ul></li>');

			$('#sn_notifications_list').append('<a href="javascript:void(0)" id="retrievedRequestedSensors"> Requested Sensors</a>');

			$('#sn_notifications_list').append('<li class="divider"></li>');

			$('#retrievedRequestedSensors').click(function(){
   			 $('#requestedSensors-modal').modal('show');
   			 initializeRequestedSensorsTable();
   			 });

			if(acceptSensor == true){
				initializeRequestedSensorsTable();
				acceptSensor = false;
			}
		}
		else {
			$('#requestedSensorsArray tbody').empty();
			$('#sn_notifications_list').append('<li style="text-align: center; min-width:350px;"> Sensor Requests <li class="divider"></li> <ul class="dropdown-menu dropdown-user"></ul></li>');
			$('#sn_notifications_list').append('<li class="listErrorsMessages">There are no sensor requests.</li>');
			$('#sn_notifications_list').append('<li class="divider"></li>');
		}

		//retrieveSensorRequests();

	},false);
	//console.log(username);
}

function initializeRequestedSensorsTable(){
	$('#requestedSensorsArray tbody').empty();
	for(i = 0; i<ssensorIDs.length; i++){
		if(swriteValues[i] == '1' && sreadValues[i] == '1')
		$('#requestedSensorsArray tbody').append('<tr><td style = "min-width:150px">'+sUsernames[i]+'</td><td style = "min-width:150px">'+scontrollerNames[i]+'</td><td style = "min-width:150px">'+ssensorNames[i]+'</td><td style = "min-width:150px" ><input type="checkbox" checked disabled></td><td style = "min-width:150px" ><input type="checkbox" checked disabled></td><td style = "min-width:150px" ><button onclick="acceptRequestedSensors(\'' + sUserIDs[i] + '\',\'' + ssensorIDs[i] + '\',\'' + scontrollerIDs[i] + '\',\'' +'1'+ '\',\'' + '1' + '\',\'' + spermissions[i] + '\')" type="button" class="btn btn-primary">Accept</button></td></tr>');
		if(swriteValues[i] == '0' && sreadValues[i] == '1')
		$('#requestedSensorsArray tbody').append('<tr><td style = "min-width:150px">'+sUsernames[i]+'</td><td style = "min-width:150px">'+scontrollerNames[i]+'</td><td style = "min-width:150px">'+ssensorNames[i]+'</td><td style = "min-width:150px" ><input type="checkbox" checked disabled></td><td style = "min-width:150px" ><input type="checkbox" disabled></td><td style = "min-width:150px" ><button onclick="acceptRequestedSensors(\'' + sUserIDs[i] + '\',\'' + ssensorIDs[i] + '\',\'' + scontrollerIDs[i] + '\',\'' +'1'+ '\',\'' + '0' + '\',\'' + spermissions[i] + '\')" type="button" class="btn btn-primary">Accept</button></td></tr>');
		if(swriteValues[i] == '1' && sreadValues[i] == '0')
		$('#requestedSensorsArray tbody').append('<tr><td style = "min-width:150px">'+sUsernames[i]+'</td><td style = "min-width:150px">'+scontrollerNames[i]+'</td><td style = "min-width:150px">'+ssensorNames[i]+'</td><td style = "min-width:150px" ><input type="checkbox" checked disabled></td><td style = "min-width:150px" ><input type="checkbox" checked disabled></td><td style = "min-width:150px" ><button onclick="acceptRequestedSensors(\'' + sUserIDs[i] + '\',\'' + ssensorIDs[i] + '\',\'' + scontrollerIDs[i] + '\',\'' +'0'+ '\',\'' + '1' + '\',\'' + spermissions[i] + '\')" type="button" class="btn btn-primary">Accept</button></td></tr>');
		if(swriteValues[i] == '0' && sreadValues[i] == '0')
		$('#requestedSensorsArray tbody').append('<tr><td style = "min-width:150px">'+sUsernames[i]+'</td><td style = "min-width:150px">'+scontrollerNames[i]+'</td><td style = "min-width:150px">'+ssensorNames[i]+'</td><td style = "min-width:150px" ><input type="checkbox" checked disabled></td><td style = "min-width:150px" ><input type="checkbox" disabled></td><td style = "min-width:150px" ><button onclick="acceptRequestedSensors(\'' + sUserIDs[i] + '\',\'' + ssensorIDs[i] + '\',\'' + scontrollerIDs[i] + '\',\'' +'0'+ '\',\'' + '0' + '\',\'' + spermissions[i] + '\')" type="button" class="btn btn-primary">Accept</button></td></tr>');
		
	}
	//style="min-width:150px";
		// $('#tableRequestSensors tbody').append('<tr><td style = "min-width:150px">'+sensors[i]+'</td><td style = "min-width:150px">'+sensorTemps[i]+'</td><td style = "text-align:center; min-width:70px" ><input type="checkbox" checked disabled><span class="fa fa-trash fa-fw userSettings pull-right"></span></td></tr>');}

	}

function acceptRequestedSensors(watcherID, sensorID, boardID, readValue, writeValue, updatePermissions){
	var token =  localStorage.getItem("token");
	var username = localStorage.getItem("username");
	var input = {token:token, username:username, watcherID:watcherID, sensorID:sensorID, boardID:boardID, readValue:readValue, writeValue:writeValue, updatePermission:updatePermissions};
	SensoMan('SensoManUpdateSharedSensors', input);
	document.addEventListener('SensoManUpdateSharedSensorsContextValueEvent', function (e12) {
		var request_response = cms.SensoManUpdateSharedSensors.SensoManUpdateSharedSensors;
		if(request_response.status == 'success'){
			swal("Success!", request_response.message , "success");
			 sUsernames = [];
			 sUserIDs = [];
			 scontrollerNames = [];
			 scontrollerIDs = [];
			 ssensorNames = [];
			 ssensorIDs = [];
			 swriteValues = [];
			 acceptSensor = true;
			 retrieveRequestsDependenciesLoaded();
		}
		else{
			swal("Oops!",request_response.message, "warning");
		}
		},false);

}