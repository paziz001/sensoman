
function brightDependenciesLoaded(){
        var token=localStorage.getItem("token");
        callSensoManBrightnessMeasurements(token); //Initialize Measurements
        initializeNotifications(token); //initialize the notifications to the user
    };

    function callSensoManBrightnessMeasurements(token){

        var postparam={token:token, sensorType:"LDR"};
        SensoMan('SensoManChart', postparam);
        // to handle events when the SensoMan value changes
        document.addEventListener('SensoManChartContextValueEvent', function (e12) {
            document.getElementById("loading").style.display="none";
            document.getElementById("chart").style.visibility="visible";
            var status = cms.SensoManChart.SensoManChart.status;
            var LDRMeasurements = cms.SensoManChart.SensoManChart.Sensors;
            $('#sensors').empty();
            for	(index = 0; index < LDRMeasurements.length; index++) {
                var sensorName=LDRMeasurements[index].SensorName;
                var measurements=LDRMeasurements[index].Measurements;
                sensorNames.push(sensorName);
                sensorMeasurements.push(measurements);
                $("#sensors").append('<li role="presentation"><a role="menuitem" tabindex='+index +'href="#" onclick="loadBrightnessGraph('+index+')">'+sensorName+'</a></li>');
            }

        }, false);
    }

function loadBrightnessGraph(index){
    console.log("entered load graph");
    var categories=[];
    var measurements=[];
    for (j=0; j<sensorMeasurements[index].length; j++) {
        var cat = sensorMeasurements[index][j].Timestamp;
        var meas = parseInt(sensorMeasurements[index][j].Measurement);
        categories.push(cat);
        measurements.push(meas);
    }
    $(function () {
        $('#brightnessChart').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Brightness taken by Sensor'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Ω'
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [
                {
                    name: sensorNames[index],
                    marker: {
                        symbol: 'circle'
                    },
                    data: measurements}]
        })})}