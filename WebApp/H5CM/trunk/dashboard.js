/**
 * Created by User on 14/02/2016.
 * Updated by Evangelos Aristodemou
 */

var token = localStorage.getItem("token");


// this function will be called when the
// promises (all dependencies) are done loading
function dashboardDependenciesLoaded() {
    // to to call the GPSCoordinates module

    GPSCoordinates();
        var token = localStorage.getItem("token");
        var email = localStorage.getItem("email");
        var type = localStorage.getItem("type");
        var username = localStorage.getItem("username");
         //Set the User Profile
        $("#emailPr").html(email);
        $("#usernamePr").html(username);
        $("#userTypePr").html(type);

        //If user is simple, he/she does not have the ability to add controller
        if (type == "simple") {
            document.getElementById("controller").style.visibility = "hidden";
        }else if(type == "owner" || type == "owner-admin"){
            $("#notsMenu").parents(".dropdown").css("display","none");
        }

    // Listen for the gpsContextValueEvent
    document.addEventListener('gpsContextValueEvent', function (e1) {
        //Get the User Info

        localStorage.setItem("userlat",cms.gpsLatitude.gpsLatitude);
        localStorage.setItem("userlng",cms.gpsLongitude.gpsLongitude);


        SensoManControllers(token); //get the number of active controllers
        initializeMapController(); //initialize the map for add new controller
        initializeNotifications(token); //initialize the notifications to the user

    }, false);

    $('#addController').click(function(){
        $('#add-controller-modal').modal('show');
    });

    $('#add-controller-modal').on('shown.bs.modal',function(){
        initializeMapController();
    });

    $("#save-controller").click(function () {
        var token = localStorage.getItem("token");
        var controller = document.getElementById("microcontroller").value;
        if (controller.length > 0) {
            if (markersArray.length > 0) {
                console.log(markersArray);
                var position = markersArray[0].position;
                var posLat = position.lat();
                var posLon = position.lng();
                var postparam = {token: token, controller: controller, latitude: posLat, longitude: posLon};
                console.log(postparam);
                SensoMan('SensoManController', postparam);
                document.addEventListener('SensoManControllerContextValueEvent', function (e6) {
                    var response = cms.SensoManController.SensoManController;
                    if (response.status == "success") {
                        swal({title:"The micro-controller has been added successfully !",type:"success"});
                        $("#add-controller-modal").modal('hide');
                        document.getElementById("microcontroller").value = "";
                        document.getElementById("errormessage").innerText ="";
                    }
                    else {
                        document.getElementById("errormessage").innerText = "An error occurred";
                }
                    document.getElementById("microcontroller").value = "";
                }, false);
            }
            else {
                //error
                document.getElementById("errormessage").innerText = "Enter location for the controller"
                $("#add-controller-modal").scrollTop(0);
            }
        }
        else {
            document.getElementById("errormessage").innerText = "Enter a valid name for the Microcontroller."
            $("#add-controller-modal").scrollTop(0);
        }
    });


};

function SensoManControllers(token) {
    //Set the parameters and Call SensoMan Service for Sensors
    var latitude = cms.gpsLatitude.gpsLatitude;
    var longitude = cms.gpsLongitude.gpsLongitude;
    var postparam = {token: token, longitude: longitude, latitude: latitude, radius: 250};
    SensoMan('SensoManControllers', postparam);
    // to handle events when the SensoMan value changes
    document.addEventListener('SensoManControllersContextValueEvent', function (e2) {
        console.log("woot");
        var controllers = cms.SensoManControllers.SensoManControllers;
        sn_sensorControllers = controllers; //angelos
        //Itialize the Map of Controllers
        initializeMap(controllers);
    }, false);
}

function initializeMapController() {

    var latitude = cms.gpsLatitude.gpsLatitude;
    var longitude = cms.gpsLongitude.gpsLongitude;

	//latitude = 5; //added by angelos
	//longitude = 5; //added by angelos

    var mapProp = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 9,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("locationController"), mapProp);


    google.maps.event.addListener(map, 'click', function (event) {
        placeMarker(event.latLng);
    });


    function placeMarker(location) {
        NEW_CONT_LOCATION = location;
        deleteOverlays();
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        markersArray.push(marker);
        function deleteOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);

                }
                markersArray.length = 0;

            }
        }
    }
}

/*Initialize the Map*/
function initializeMap(controllers) {
    console.log(controllers.resultControllers);
    mainMarkers=[];
    var latitude = cms.gpsLatitude.gpsLatitude;
    var longitude = cms.gpsLongitude.gpsLongitude;

	//latitude = 5; //added by angelos
	//longitude = 5; //added by angelos

    var mapProp = {
        center: {lat: latitude, lng: longitude},
        zoom: 9
    };

    var mainMap = new google.maps.Map(document.getElementById("controllersMap"), mapProp);

    //var location = {
    //    center:{lat:latitude,lng:longitude},
    //    count: sensors.count
    //};

    //var locationCircle;
    //// Construct the circle for each value in location.
    //// Note: We scale the area of the circle based on the population.
    //var locationOptions = {
    //    strokeColor: '#4209E0',
    //    strokeOpacity: 0.5,
    //    strokeWeight: 2,
    //    fillColor: '#4209E0',
    //    fillOpacity: 0.8,
    //    map: mainMap,
    //    center: location.center,
    //    radius: Math.sqrt(location.count) * 1000,
    //    clickable:true
    //};
    ////Add the circle for this city to the map.
    //locationCircle = new google.maps.Circle(locationOptions);
    var infowindow;
    var t = $('#sensors-table').DataTable(
        {
            "pageLength": 5,
            "showNEntries": false,
            "bDestroy":true
        }
    );
    $.each(controllers.resultControllers, function (key, controller) {
            console.log(controller["longitude"]);
            var latLng = new google.maps.LatLng(controller["latitude"],
                controller["longitude"]);
            var marker = new google.maps.Marker({
                position: latLng
            });
            var contentString = "<span class='controllerName infowindow-style'><span>Name: </span><span>" + controller["name"] + "</span></span></span>" +
                "<br>" +
                //"<span class='infowindow-style'><span>Role: </span><span>" + controller["role"] + "</span></span>" +
                //"<br>" +
                "<span class='infowindow-style'><span>Number of Modules: </span> <a id='"+controller["name"]+"' href='#'>" + controller["numOfModules"] + "</a></span>" +
                "<br>" +
                "<span class='infowindow-style'><span>IP: </span><span>" + controller["ip"] + "</span></span>" +
                "<br>" +
                "<span class='infowindow-style'><span>Port: </span><span>" + controller["port"] + "</span></span>" +
                "<br>";

            google.maps.event.addListener(marker, "click", function () {
                if (infowindow)infowindow.close();
                infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    maxWidth: 200
                });
                //infowindow = new google.maps.InfoWindow({content: contentString});
                infowindow.open(mainMap, marker);

                google.maps.event.addDomListener(infowindow, 'domready', function() {
                    $('#'+controller["name"]).click(function (e) {
                        e.preventDefault();
                        var controllerName = $(this).parents("span").siblings(".controllerName").children("span:nth-child(2)").html();
                        $('#sensors-modal').unbind();
                        $('#sensors-modal').modal('show');
                        $('#sensors-modal').on('hidden.bs.modal', function () {
                            t.rows().remove().draw();
                        });
                        $('#sensors-modal').on('shown.bs.modal', function () {
                            console.log(controllerName);
                            console.log(cms.SensoManControllers.SensoManControllers.resultSensors);
                            var sensors = cms.SensoManControllers.SensoManControllers.resultSensors[controllerName];

                            $.each(sensors, function (key, value) {

                                t.row.add([value["Sensor Name"],
                                    value["Sensor Type"],
                                    value["SensorFrequency"],
                                    value["Measurement"],
                                    value['Timestamp']])
                                    .draw(false);
                                    console.log(t.row);
                            });
                        });



                    });
                });

            });


            //function createMarker(name, latlng) {
            //    var marker = new google.maps.Marker({position: latlng, map: map});
            //    google.maps.event.addListener(marker, "click", function () {
            //        if (infowindow)infowindow.close();
            //        infowindow = new google.maps.InfoWindow({content: name});
            //        infowindow.open(map, marker);
            //    });
            //    return marker;
            //}


            //google.maps.event.addListener(marker, 'click', (function (marker, infowindow) {
            //    return function () {
            //        //$.each(mainMarkers,function(key,value){
            //        //    value.infoWindow.close();
            //        //});
            //        if (infowindow) {
            //            infowindow.
            //        }
            //        infowindow.open(mainMap, marker);
            //        $('.controllerModules a').click(function () {
            //            alert($(this).parents("span").siblings(".controllerName").html());
            //        });
            //    };
            //})(marker, infowindow));
            mainMarkers.push(marker);
        }
    )
    ;
    var markerCluster = new MarkerClusterer(mainMap, mainMarkers);
}
