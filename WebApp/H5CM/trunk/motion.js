    // this function will be called when the
    // promises (all dependencies) are done loading
    function motionDependenciesLoaded(){
        var token=localStorage.getItem("token");
        callSensoManMotionMeasurements(token); //Initialize Measurements
        initializeNotifications(token); //initialize the notifications to the user
    };

    function callSensoManMotionMeasurements(token){
        var postparam={token:token, sensorType:"motion"};
        SensoMan('SensoManChart', postparam);
        // to handle events when the SensoMan value changes
        document.addEventListener('SensoManChartContextValueEvent', function (e12) {
            document.getElementById("loading").style.display="none";
            document.getElementById("chart").style.visibility="visible";
            var motionMeasurements = cms.SensoManChart.SensoManChart.Sensors;
            $('#sensors').empty();
            for	(index = 0; index < motionMeasurements.length; index++) {
                var sensorName=motionMeasurements[index].SensorName;
                var measurements=motionMeasurements[index].Measurements;
                sensorNames.push(sensorName);
                sensorMeasurements.push(measurements);

                $("#sensors").append('<li role="presentation"><a role="menuitem" tabindex='+index +'href="#" onclick="loadMotionGraph('+index+')">'+sensorName+'</a></li>');
            }

        }, false);
    }

function loadMotionGraph(index) {
    var categories = [];
    var measurements = [];
    for (j = 0; j < sensorMeasurements[index].length; j++) {
        var cat = sensorMeasurements[index][j].Timestamp;
        var meas = sensorMeasurements[index][j].Measurement;
        if (meas=="HIGH"){
            meas=1;
        }
        else{
            meas=0;
        }
        categories.push(cat);
        measurements.push(meas);

    }
    $(function () {
        $('#motionChart').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Motion taken by Sensor'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Motion'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [
                {
                    name: sensorNames[index],
                    marker: {
                        symbol: 'square'
                    },
                    data: measurements

                }
            ]
        })})}