/*
* Evangelos Aristodemou
*/

var scriptsURL = new Array(
    "../H5CM/trunk/modules/sensors/SensoMan.js"
);
loadDependencies(scriptsURL);
var username = localStorage.getItem("username");
var token = localStorage.getItem("token");
var email = localStorage.getItem("email");
var type = localStorage.getItem("type");

function checkToken(s){
  if (s == "token expired") {
    swal({
            title: "Session has expired",
            text: "Please connect again to gain access.",
            type: "info",
            showCancelButton: false,
            confirmButtonText: "OK",
            closeOnConfirm: false
        },
        function(){
            localStorage.removeItem("token");
            localStorage.removeItem("current");
            localStorage.removeItem("type");
            localStorage.removeItem("username");
            localStorage.removeItem("email");
            localStorage.removeItem("userlat");
            localStorage.removeItem("userlng");
            window.location.href = "login.html";
            return false;
        });
  }
  return true;
}

function changePassword(){
  var old = document.getElementById('oldpassword').value;
  var new1 = document.getElementById('newpassword1').value;
  var new2 = document.getElementById('newpassword2').value;
  var path = '../edit/edit_password.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        if (result.status=="success"){
          swal("Saved!", "Password has been changed successfully.", "success");
          $('#changepassword').modal('hide');
        }
        else{
          document.getElementById('passmes').innerHTML = result.message;
        }
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token + "&newpassword1=" + new1 + "&newpassword2=" + new2 + "&oldpassword=" + old);
}

function changeUsername(){
  var name = document.getElementById('name').value;
  var mes = document.getElementById('namemes');
  if (name==""){
    mes.innerHTML = "Give a username.";
    return;
  }
  for (var i=0; i<name.length; i++){
    var c = name.charAt(i);
    if ((c<'a' || c>'z') && (c<'A' || c>'Z') && (c<'0' || c>'9') && (c!='-') && (c!='_')){
      mes.innerHTML="Not valid username. (Use only letters and numbers)";
      return;
    }
  }
  var path = '../edit/edit_username.php';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var result = JSON.parse(this.responseText);
      if (checkToken(result.message)){
        if (result.status=="success"){
          swal("Saved!", "Username has been changed successfully.", "success");
          $('#changeusername').modal('hide');
          username = name;
          document.getElementById('username').innerHTML = name;
          document.getElementById('user').innerHTML = name;
          localStorage.setItem("username", name);
        }
        else{
          mes.innerHTML = result.message;
        }
      }
    }
  };
  xhttp.open("POST", path, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("token=" + token + "&username=" + name);
}

$(document).ready(function(){
  $("#username").html(username);
  $("#user").html(username);
  $("#email").html(email);
  $("#usertype").html(type);
});

$('#changepassword').on('show.bs.modal', function (event) {
  $("#passmes").html('');
  $("#oldpassword").val('');
  $("#newpassword1").val('');
  $("#newpassword2").val('');
})

$('#changeusername').on('show.bs.modal', function (event) {
  $("#namemes").html('');
  $("#name").val(username);
})
