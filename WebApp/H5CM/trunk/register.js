/**
 * Created by Stella Constantinou on 7/4/2015.
 * update by Angelos Efstathiou on 27/11/2016 :
 *  - fixed few bugs about error messages
 */
// main class ContextAwareApplication

// first thing is to get the Singleton instance of the ContextManager
var cms = ContextManagerSingleton.getInstance("application");
var markersArray=[];
// define the scripts to be loaded
var scriptsURL = new Array(
    "../H5CM/trunk/modules/sensors/GPSCoordinates.js",
    "../H5CM/trunk/modules/sensors/SensoMan.js"
);

// Load scripts using Promises with jQuery getScript()
// and wait until all dependencies are loaded
loadDependencies(scriptsURL);


// this function will be called when the
// promises (all dependencies) are done loading
function dependenciesLoaded(){
    initCAWApp();
};

function initCAWApp(){
    // to to call the GPSCoordinates module
    GPSCoordinates();

    // Listen for the gpsContextValueEvent
    document.addEventListener('gpsContextValueEvent', function (e1) {

        //else {
        var token = localStorage.getItem("token");
        var latitude = cms.gpsLatitude.gpsLatitude;
        var longitude = cms.gpsLongitude.gpsLongitude;
        initializeMap(longitude,latitude);
    }, false);
}



/*Initialize the Map*/
function initializeMap(longitude, latitude) {
    var mapProp = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 9,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("locationController"), mapProp);

    google.maps.event.addListener(map, 'click', function (event) {
        placeMarker(event.latLng);
    });


    function placeMarker(location) {
        deleteOverlays();
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        markersArray.push(marker);
        function deleteOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);

                }
                markersArray.length = 0;

            }
        }


    }
}

$("#typeUser").change(function(){
    var valueOption=$( "select option:selected" ).val();
 
       if(valueOption=="admin-owner"){
           $("#microcontroller").attr("disabled", false);
           $("#microcontroller-type").attr("disabled", false);
           $("#mapClass").attr("hidden", false);
       }
       else if(valueOption=="owner"){
           $("#microcontroller").attr("disabled", false);
           $("#microcontroller-type").attr("disabled", false);
           $("#mapClass").attr("hidden", false);
       }
       else if (valueOption=="simple"){
           $("#microcontroller").attr("disabled", true);
           $("#microcontroller-type").attr("disabled", true);
           $("#mapClass").attr("hidden", true);
       }


});


function checkRegister(){
	
    var email=document.getElementById("email").value;
    if (email.length>0){
        var username=document.getElementById("username").value;
        if (username.length>0){
            var password=document.getElementById("password").value;
            if (password.length>0){
                var password1=document.getElementById("password1").value;
                if (password1.length>0){
                    if (password==password1){
                        var type=$("#typeUser option:selected").val();
                        if ((type=="admin") || (type=="admin-owner") || (type=="owner")){
                            var controller=document.getElementById("microcontroller").value;
                            var controllerType=$("#microcontroller-type option:selected").val();
                            if (controller.length>0){
                                if (markersArray.length>0) {
                                    var position = markersArray[0].getPosition();
                                    var posLat=position.lat();
                                    var posLon=position.lng();
                                    console.log(posLat);
                                    console.log(posLon);
                                    var postparam={email: email, username:username, password:password, usertype:type,controller:controller,controllertype:controllerType, latitude:posLat, longitude:posLon};
                                }
                                else {
                                    //error
                                    document.getElementById("errormessage").innerText="Enter location for the controller"
									var invalidLocation = true;
                                }
                            }
                            else{
								
                                document.getElementById("errormessage").innerText="Enter a valid name for the Microcontroller."
								var invalidControllerName = true;
                            }
                        }
                        else if (type=="simple"){
                            var postparam={email: email, username:username, password:password,usertype:type};
                        }
                        
						
                        //added by angelos
						if(!invalidLocation && !invalidControllerName){
							//make Request
                        SensoMan("SensoManRegister",postparam);
                        document.addEventListener('SensoManRegisterContextValueEvent', function (e12) {
                             var response=cms.SensoManRegister.SensoManRegister;
                             if (response.status=="success"){
                                window.location.href="login.html";
                                
                             }
                             else{
                                document.getElementById("errormessage").innerText="Something goes wrong. Duplicated attributes."
                                
                             }
                             $("#email,#username,#password,#password1,#typeUser, #microcontroller").removeAttr("disabled").val("");
                             $("#option3").selected;
                             initializeMap(cms.gpsLongitude.gpsLongitude,cms.gpsLatitude.gpsLatitude);
                        }, false);}
                    }
                    else{
                        document.getElementById("errormessage").innerText="Enter same passwords."
                    }
                }
                else{
                    document.getElementById("errormessage").innerText="Enter a valid password for verification"
                }

            }
            else{
                document.getElementById("errormessage").innerText="Enter a valid password."
            }
        }
        else{
            document.getElementById("errormessage").innerText="Enter valid username."
        }
    }
    else{
        document.getElementById("errormessage").innerText="Enter valid email."
    }



}




