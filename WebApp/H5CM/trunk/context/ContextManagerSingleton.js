function loadDependencies(scriptsURL) {
    // *** Create an array to hold our promises
    var promises = [];

    for (var i = 0; i < scriptsURL.length; i++) {
        // *** Store the promise returned by getSizeSettingsFromPage in a variable
        promise = $.getScript(scriptsURL[i]);

        promise.then(function (scriptsURL) {
            // *** When the promise is resolved
            //console.log('Loading dependency: ' + scriptsURL[i] + ' OK!');
        });

        // // *** Add the promise returned by getScript to the array
        promises.push(promise);
    }
    // *** Call dependenciesLoaded after all promises have been resolved
    Q.all(promises).then(dependenciesLoaded);
}

var ContextManagerSingleton = (function () {

    var instance;

    function createInstance() {
		
        // create the singleton object context manager instance
        var object = new Object("ContextManagerSingleton Instance.");
        // create the custom context events for the context manager
        object.gpsContextValueEvent;
        object.batteryLevelContextValueEvent;
        object.isBatteryChargingContextValueEvent;
        object.deviceMotionContextValueEvent;

        // create the helper variables for the context manager
        object.wsCounter = 0;
        object.restfulCounter = 0;
        object.SensoManCounter = 0;

        instance = object;
    }

    return {
        getInstance: function (context) {

            if (!instance) {
                console.log('creating the ContextManagerSingleton instance.');

                // *** Create an array to hold our promises
                var promises = [];
                var scriptsURL = new Array("../H5CM/trunk/context/ContextUI_Utilities.js");

                for (var i = 0; i < scriptsURL.length; i++) {
                    // *** Store the promise returned by getSizeSettingsFromPage in a variable
                    promise = $.getScript(scriptsURL[i]);

                    promise.then(function (scriptsURL) {
                        // *** When the promise is resolved
                        //console.log('Loading dependency: ' + scriptsURL[i] + ' OK!');
                    });

                    // // *** Add the promise returned by getScript to the array
                    promises.push(promise);
                }
                // *** Call dependenciesLoaded after all promises have been resolved
                Q.all(promises).then(createInstance());
            }

            if (context == "batteryLevel") {
                instance.batteryLevel = {batteryLevel: "context"};

                instance.batteryLevelContextValueEvent = new CustomEvent("batteryLevelContextValueEvent", {
                        detail: {message: "batteryLevelContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("batteryLevel context property initialised");
            }
            else if (context == "isBatteryCharging") {
                instance.isBatteryCharging = {isBatteryCharging: "context"};

                instance.isBatteryChargingContextValueEvent = new CustomEvent("isBatteryChargingContextValueEvent", {
                        detail: {message: "isBatteryChargingContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("isBatteryCharging context property initialised");
            }
            else if (context == "gpsCoordinates") {
                instance.gpsLongitude = {gpsLongitude: "context"};
                instance.gpsLatitude = {gpsLatitude: "context"};

                instance.gpsContextValueEvent = new CustomEvent("gpsContextValueEvent", {
                        detail: {message: "gpsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
					
                );
				
                console.log("gpsCoordinates context property initialised");
				//document.dispatchEvent(instance.gpsContextValueEvent);
            }
            else if (context == "deviceMotion") {
                instance.dm_rawAcceleration = {dm_rawAcceleration: "context"};
                instance.dm_tiltLR = {dm_tiltLR: "context"};
                instance.dm_tiltFB = {dm_tiltFB: "context"};

                instance.deviceMotionContextValueEvent = new CustomEvent("deviceMotionContextValueEvent", {
                        detail: {message: "deviceMotionContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("deviceMotion context property initialised");
            }
            else if (context == "deviceOrientation") {
                instance.do_tiltLR = {do_tiltLR: "context"};
                instance.do_tiltFB = {do_tiltFB: "context"};
                instance.do_direction = {do_direction: "context"};

                instance.deviceOrientationContextValueEvent = new CustomEvent("deviceOrientationContextValueEvent", {
                        detail: {message: "deviceOrientationContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("deviceOrientation context property initialised");
            }
            else if (context == "networkStatus") {
                instance.networkStatus = {networkStatus: "context"};
                instance.networkStatusContextValueEvent = new CustomEvent("networkStatusContextValueEvent", {
                        detail: {message: "networkStatusContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("networkStatus context property initialised");
            }
            else if (context == "facebookConnect") {
                instance.facebookConnect = {facebookConnect: "context"};
                instance.facebookConnectContextValueEvent = new CustomEvent("facebookConnectContextValueEvent", {
                        detail: {message: "facebookConnectContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("facebookConnect context property initialised");
            }
            else if (context == "facebookInfo") {
                instance.facebookInfo = {facebookInfo: "context"};
                instance.facebookInfoContextValueEvent = new CustomEvent("facebookInfoContextValueEvent", {
                        detail: {message: "facebookInfoContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("facebookInfo context property initialised");
            }
            else if (context == "linkedInConnect") {
                instance.linkedInConnect = {linkedInConnect: "context"};
                instance.linkedInConnectContextValueEvent = new CustomEvent("linkedInConnectContextValueEvent", {
                        detail: {message: "linkedInConnectContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("linkedInConnect context property initialised");
            }
            else if (context == "linkedInInfo") {
                instance.linkedInInfo = {linkedInInfo: "context"};
                instance.linkedInInfoContextValueEvent = new CustomEvent("linkedInInfoContextValueEvent", {
                        detail: {message: "linkedInInfoContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("linkedInInfo context property initialised");
            }
            else if (context.substring(0, 10) == "webService") {
                if (instance.wsCounter == 0) {
                    instance.webService1 = {webService1: "context"};
                    instance.webService1ContextValueEvent = new CustomEvent("webService1ContextValueEvent", {
                            detail: {message: "webService1ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("webService1 context property initialised");
                    instance.wsCounter++;
                }
                else if (instance.wsCounter == 1) {
                    instance.webService2 = {webService2: "context"};
                    instance.webService2ContextValueEvent = new CustomEvent("webService2ContextValueEvent", {
                            detail: {message: "webService2ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("webService2 context property initialised");
                    instance.wsCounter++;
                }
                else if (instance.wsCounter == 2) {
                    instance.webService3 = {webService3: "context"};
                    instance.webService3ContextValueEvent = new CustomEvent("webService3ContextValueEvent", {
                            detail: {message: "webService3ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("webService3 context property initialised");
                    instance.wsCounter++;
                }
                else if (instance.wsCounter == 3) {
                    instance.webService4 = {webService4: "context"};
                    instance.webService4ContextValueEvent = new CustomEvent("webService4ContextValueEvent", {
                            detail: {message: "webService4ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("webService4 context property initialised");
                    instance.wsCounter++;
                }
                else if (instance.wsCounter == 4) {
                    instance.webService5 = {webService5: "context"};
                    instance.webService5ContextValueEvent = new CustomEvent("webService5ContextValueEvent", {
                            detail: {message: "webService5ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("webService5 context property initialised");
                    instance.wsCounter++;
                }
            }
            else if (context.substring(0, 14) == "restfulService") {
                if (instance.restfulCounter == 0) {
                    instance.restfulService1 = {restfulService1: "context"};
                    instance.restfulService1ContextValueEvent = new CustomEvent("restfulService1ContextValueEvent", {
                            detail: {message: "restfulService1ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("restfulService1 context property initialised");
                    instance.restfulCounter++;
                }
                else if (instance.restfulCounter == 1) {
                    instance.restfulService2 = {restfulService2: "context"};
                    instance.restfulService2ContextValueEvent = new CustomEvent("restfulService2ContextValueEvent", {
                            detail: {message: "restfulService2ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("restfulService2 context property initialised");
                    instance.restfulCounter++;
                }
                else if (instance.restfulCounter == 2) {
                    instance.restfulService3 = {restfulService3: "context"};
                    instance.restfulService3ContextValueEvent = new CustomEvent("restfulService3ContextValueEvent", {
                            detail: {message: "restfulService3ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("restfulService3 context property initialised");
                    instance.restfulCounter++;
                }
                else if (instance.restfulCounter == 3) {
                    instance.restfulService4 = {restfulService4: "context"};
                    instance.restfulService4ContextValueEvent = new CustomEvent("restfulService4ContextValueEvent", {
                            detail: {message: "restfulService4ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("restfulService4 context property initialised");
                    instance.restfulCounter++;
                }
                else if (instance.restfulCounter == 4) {
                    instance.restfulService5 = {restfulService5: "context"};
                    instance.restfulService5ContextValueEvent = new CustomEvent("restfulService5ContextValueEvent", {
                            detail: {message: "restfulService5ContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("restfulService5 context property initialised");
                    instance.restfulCounter++;
                }
            }
            else if (context.substring(0, 8) == "SensoMan") {
                if (context == "SensoManConnect") {
                    instance.SensoManConnect = {SensoManConnect: "context"};
                    instance.SensoManConnectContextValueEvent = new CustomEvent("SensoManConnectContextValueEvent", {
                            detail: {message: "SensoManConnectContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManConnect context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManLogout") {
                    instance.SensoManLogout = {SensoManLogout: "context"};
                    instance.SensoManLogoutContextValueEvent = new CustomEvent("SensoManLogoutContextValueEvent", {
                            detail: {message: "SensoManLogoutContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManLogout context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManRegister") {
                    instance.SensoManRegister = {SensoManRegister: "context"};
                    instance.SensoManRegisterContextValueEvent = new CustomEvent("SensoManRegisterContextValueEvent", {
                            detail: {message: "SensoManRegisterContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManRegister context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManNotify") {
                    instance.SensoManNotify = {SensoManNotify: "context"};
                    instance.SensoManNotifyContextValueEvent = new CustomEvent("SensoManNotifyContextValueEvent", {
                            detail: {message: "SensoManNotifyContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManNotify context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManFulNotifications") {
                    instance.SensoManFulNotifications = {SensoManFulNotifications: "context"};
                    instance.SensoManFulNotificationsContextValueEvent = new CustomEvent("SensoManFulNotificationsContextValueEvent", {
                            detail: {message: "SensoManFulNotificationsContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManFulNotifications context property initialised");
                    instance.SensoManCounter++;
                } else if (context == "SensoManChart") {
                    instance.SensoManChart = {SensoManChart: "context"};
                    instance.SensoManChartContextValueEvent = new CustomEvent("SensoManChartContextValueEvent", {
                            detail: {message: "SensoManChartContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManChart context property initialised");
                    instance.SensoManCounter++;
                } else if (context == "SensoManController") {
                    instance.SensoManController = {SensoManController: "context"};
                    instance.SensoManControllerContextValueEvent = new CustomEvent("SensoManControllerContextValueEvent", {
                            detail: {message: "SensoManControllerContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManController context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManCSV") {
                    instance.SensoManCSV = {SensoManCSV: "context"};
                    instance.SensoManCSVContextValueEvent = new CustomEvent("SensoManCSVContextValueEvent", {
                            detail: {message: "SensoManCSVContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManCSV context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManML") {
                    instance.SensoManML = {SensoManML: "context"};
                    instance.SensoManMLContextValueEvent = new CustomEvent("SensoManMLContextValueEvent", {
                            detail: {message: "SensoManMLContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManML context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManControllers") {
                    instance.SensoManControllers= {SensoManControllers: "context"};
                    instance.SensoManControllersContextValueEvent = new CustomEvent("SensoManControllersContextValueEvent", {
                            detail: {message: "SensoManControllersContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManControllers context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == 'SensoManRules') {
                    instance.SensoManRules = {SensoManRules: "context"};
                    instance.SensoManRulesContextValueEvent = new CustomEvent("SensoManRulesContextValueEvent", {
                            detail: {message: "SensoManRulesContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManRules context property initialised");
                    instance.SensoManCounter++;
                }else if (context == 'SensoManActuatorsByController') {
                    instance.SensoManActuatorsByController = {SensoManActuatorsByController: "context"};
                    instance.SensoManActuatorsByControllerContextValueEvent = new CustomEvent("SensoManActuatorsByControllerContextValueEvent", {
                            detail: {message: "SensoManActuatorsByControllerContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManActuatorsByController context property initialised");
                    instance.SensoManCounter++;
                }else if (context == 'SensoManSensorsByController') {
                    instance.SensoManSensorsByController = {SensoManSensorsByController: "context"};
                    instance.SensoManSensorsByControllerContextValueEvent = new CustomEvent("SensoManSensorsByControllerContextValueEvent", {
                            detail: {message: "SensoManSensorsByControllerContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManSensorsByController context property initialised");
                    instance.SensoManCounter++;
                }else if (context == 'SensoManRuleById') {
                    instance.SensoManRuleById = {SensoManRuleById: "context"};
                    instance.SensoManRuleByIdContextValueEvent = new CustomEvent("SensoManRuleByIdContextValueEvent", {
                            detail: {message: "SensoManRuleByIdContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManRuleById context property initialised");
                    instance.SensoManCounter++;
                }else if (context == 'SensoManActuatorControllers') {
                    instance.SensoManActuatorControllers= {SensoManActuatorControllers: "context"};
                    instance.SensoManActuatorControllersContextValueEvent = new CustomEvent("SensoManActuatorControllersContextValueEvent", {
                            detail: {message: "SensoManActuatorControllersContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManActuatorControllers context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == 'SensoManSensorControllers') {
                    instance.SensoManSensorControllers= {SensoManSensorControllers: "context"};
                    instance.SensoManSensorControllersContextValueEvent= new CustomEvent("SensoManSensorControllersContextValueEvent", {
                            detail: {message: "SensoManSensorControllersContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManSensorControllers context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManDeleteRule") {
                    instance.SensoManDeleteRule= {SensoManDeleteRule: "context"};
                    instance.SensoManDeleteRuleContextValueEvent = new CustomEvent("SensoManDeleteRuleContextValueEvent", {
                            detail: {message: "SensoManDeleteRuleContextValueEvent", time: new Date()},
                            bubbles: true, cancelable: true
                        }
                    );
                    console.log("SensoManDeleteRule context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context =="SensoManSendInvitation"){
                instance.SensoManSendInvitation = {SensoManSendInvitation: "context"};
                instance.SensoManSendInvitationContextValueEvent = new CustomEvent("SensoManSendInvitationContextValueEvent", {
                    detail: {message: "SensoManSendInvitationContextValueEvent", time: new Date()},
                    bubbles: true, cancelable: true

                    }
                );
                console.log("SensoManSendInvitation context property initialised");
                instance.SensoManCounter++;
                }
                else if (context =="SensoManRetrieveRequests"){
                    instance.SensoManRetrieveRequests ={SensoManRetrieveRequests: "context"};
                    instance.SensoManRetrieveRequestsContextValueEvent = new CustomEvent("SensoManRetrieveRequestsContextValueEvent", {
                         detail: {message: "SensoManRetrieveRequestsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveRequests context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context =="SensoManAcceptInvitation"){
                    instance.SensoManAcceptInvitation ={SensoManAcceptInvitation: "context"};
                    instance.SensoManAcceptInvitationContextValueEvent = new CustomEvent("SensoManAcceptInvitationContextValueEvent", {
                         detail: {message: "SensoManAcceptInvitationContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManAcceptInvitation context property initialised");
                    instance.SensoManCounter++;
                }

                else if (context =="SensoManRetrieveFriends"){
                    instance.SensoManRetrieveFriends ={SensoManRetrieveFriends: "context"};
                    instance.SensoManRetrieveFriendsContextValueEvent = new CustomEvent("SensoManRetrieveFriendsContextValueEvent", {
                         detail: {message: "SensoManRetrieveFriendsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveFriends context property initialised");
                    instance.SensoManCounter++;
                }

                else if (context =="SensoManDeleteFriend"){
                    instance.SensoManDeleteFriend ={SensoManDeleteFriend: "context"};
                    instance.SensoManDeleteFriendContextValueEvent = new CustomEvent("SensoManDeleteFriendContextValueEvent", {
                         detail: {message: "SensoManDeleteFriendContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManDeleteFriend context property initialised");
                    instance.SensoManCounter++;
                }

                else if (context == "SensoManCreateGroup"){
                    instance.SensoManCreateGroup ={SensoManCreateGroup: "context"};
                    instance.SensoManCreateGroupContextValueEvent = new CustomEvent("SensoManCreateGroupContextValueEvent", {
                         detail: {message: "SensoManCreateGroupContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManCreateGroup context property initialised");
                    instance.SensoManCounter++;
                }
                 else if (context == "SensoManRetrieveGroups"){
                    instance.SensoManRetrieveGroups ={SensoManRetrieveGroups: "context"};
                    instance.SensoManRetrieveGroupsContextValueEvent = new CustomEvent("SensoManRetrieveGroupsContextValueEvent", {
                        detail: {message: "SensoManRetrieveGroupsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveGroups context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManRetrieveUsersInGroup"){
                    instance.SensoManRetrieveUsersInGroup ={SensoManRetrieveUsersInGroup: "context"};
                    instance.SensoManRetrieveUsersInGroupContextValueEvent = new CustomEvent("SensoManRetrieveUsersInGroupContextValueEvent", {
                        detail: {message: "SensoManRetrieveUsersInGroupContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveUsersInGroup context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManRetrieveSensorControllers"){
                    instance.SensoManRetrieveSensorControllers ={SensoManRetrieveSensorControllers: "context"};
                    instance.SensoManRetrieveSensorControllersContextValueEvent = new CustomEvent("SensoManRetrieveSensorControllersContextValueEvent", {
                        detail: {message: "SensoManRetrieveSensorControllersContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveSensorControllers context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManRetrieveSensors"){
                    instance.SensoManRetrieveSensors ={SensoManRetrieveSensors: "context"};
                    instance.SensoManRetrieveSensorsContextValueEvent = new CustomEvent("SensoManRetrieveSensorsContextValueEvent", {
                        detail: {message: "SensoManRetrieveSensorsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveSensors context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManRetrieveSensorControllersGraph"){
                    instance.SensoManRetrieveSensorControllersGraph ={SensoManRetrieveSensorControllersGraph: "context"};
                    instance.SensoManRetrieveSensorControllersGraphContextValueEvent = new CustomEvent("SensoManRetrieveSensorControllersGraphContextValueEvent", {
                        detail: {message: "SensoManRetrieveSensorControllersGraphContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveSensorControllersGraph context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManRequestSensors"){
                    instance.SensoManRequestSensors ={SensoManRequestSensors: "context"};
                    instance.SensoManRequestSensorsContextValueEvent = new CustomEvent("SensoManRequestSensorsContextValueEvent", {
                        detail: {message: "SensoManRequestSensorsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRequestSensors context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManUpdateSharedSensors"){
                    instance.SensoManUpdateSharedSensors ={SensoManUpdateSharedSensors: "context"};
                    instance.SensoManUpdateSharedSensorsContextValueEvent = new CustomEvent("SensoManUpdateSharedSensorsContextValueEvent", {
                        detail: {message: "SensoManUpdateSharedSensorsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManUpdateSharedSensors context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManRetrieveSensorsOpt3"){
                    instance.SensoManRetrieveSensorsOpt3 ={SensoManRetrieveSensorsOpt3: "context"};
                    instance.SensoManRetrieveSensorsOpt3ContextValueEvent = new CustomEvent("SensoManRetrieveSensorsOpt3ContextValueEvent", {
                        detail: {message: "SensoManRetrieveSensorsOpt3ContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManRetrieveSensorsOpt3 context property initialised");
                    instance.SensoManCounter++;
                }
                else if (context == "SensoManDeleteGroup"){
                    instance.SensoManDeleteGroup ={SensoManDeleteGroup: "context"};
                    instance.SensoManDeleteGroupContextValueEvent = new CustomEvent("SensoManDeleteGroupContextValueEvent", {
                        detail: {message: "SensoManDeleteGroupContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    });
                    console.log("SensoManDeleteGroup context property initialised");
                    instance.SensoManCounter++;
                }




            }
            else if (context == "facebookPosts") {
                instance.facebookPosts = {facebookPosts: "context"};
                instance.facebookPostsContextValueEvent = new CustomEvent("facebookPostsContextValueEvent", {
                        detail: {message: "facebookPostsContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("facebookPosts context property initialised");
            }
            else if (context == "batteryAnalyser") {
                instance.batteryAnalyser = {batteryAnalyser: "context"};
                instance.batteryAnalyserContextValueEvent = new CustomEvent("batteryAnalyserContextValueEvent", {
                        detail: {message: "batteryAnalyserContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("batteryAnalyser context property initialised");
            }
            else if (context == "activityRecognizer") {
                instance.activityRecognizer = {activityRecognizer: "context"};
                instance.activityRecognizerContextValueEvent = new CustomEvent("activityRecognizerContextValueEvent", {
                        detail: {message: "activityRecognizerContextValueEvent", time: new Date()},
                        bubbles: true, cancelable: true
                    }
                );
                console.log("ActivityRecognizer context property initialised");
            }
            else if (context == "application") {
				
                console.log("no action required. simply return singleton instance");
            }

            return instance;
        }

    };

})();