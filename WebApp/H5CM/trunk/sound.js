    // this function will be called when the
    // promises (all dependencies) are done loading
    function soundDependenciesLoaded(){
        var token=localStorage.getItem("token");
        callSensoManSoundMeasurements(token); //Initialize Measurements
        initializeNotifications(token); //initialize the notifications to the user

    };

function callSensoManSoundMeasurements(token){

    var postparam={token:token, sensorType:"sound"};
    SensoMan('SensoManChart', postparam);
    // to handle events when the SensoMan value changes
    document.addEventListener('SensoManChartContextValueEvent', function (e12) {
        document.getElementById("loading").style.display="none";
        document.getElementById("chart").style.visibility="visible";
        var soundMeasurements = cms.SensoManChart.SensoManChart.Sensors;
        for	(index = 0; index < soundMeasurements.length; index++) {
            var sensorName=soundMeasurements[index].SensorName;
            var measurements=soundMeasurements[index].Measurements;
            sensorNames.push(sensorName);
            sensorMeasurements.push(measurements);
            $('#sensors').empty();
            $("#sensors").append('<li role="presentation"><a role="menuitem" tabindex='+index +'href="#" onclick="loadSoundGraph('+index+')">'+sensorName+'</a></li>');
        }

    }, false);
}

function loadSoundGraph(index){
    var categories=[];
    var measurements=[];
    for (j=0; j<sensorMeasurements[index].length; j++) {
        var cat = sensorMeasurements[index][j].Timestamp;
        var meas = parseFloat(sensorMeasurements[index][j].Measurement);
        categories.push(cat);
        measurements.push(meas);
    }
    $(function () {
        $('#soundChart').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Sound taken by Sensor'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Decibel'
                },
                labels: {
                    formatter: function () {
                        return this.value + 'DB';
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [
                {
                    name: sensorNames[index],
                    marker: {
                        symbol: 'circle'
                    },
                    data: measurements
                }]
        })})}