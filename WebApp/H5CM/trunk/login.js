// first thing is to get the Singleton instance of the ContextManager
var cms = ContextManagerSingleton.getInstance("application");


// define the scripts to be loaded
var scriptsURL = new Array(
    "../H5CM/trunk/modules/sensors/SensoMan.js"
);
loadDependencies(scriptsURL);
// this function will be called when the
// promises (all dependencies) are done loading
function dependenciesLoaded(){
    initCAWApp();
};


function initCAWApp(){
    $('#login').click(function(e){
        checkValidation(e);
    });
}

function checkValidation(e){
    e.preventDefault();
    var username=document.getElementById("username").value;
    var password=document.getElementById("password").value;

    var credentials={username:username, password:password};
    SensoMan("SensoManConnect", credentials);
    document.addEventListener('SensoManConnectContextValueEvent', function (e12) {
        var response=cms.SensoManConnect.SensoManConnect;
        console.log("Response: " + response.message);
        if (response.message=="Successfully connected."){
            console.log(response.message);
            var token=response.token;
            var type=response.userType;
            var email=response.email;
            var username1=response.username;
            localStorage.setItem("token", token);
            localStorage.setItem("type", type);
            localStorage.setItem("email", email);
            localStorage.setItem("username", username1);
            if (typeof localStorage["current"] == 'undefined') {
                var current="index.html";
            }
            else{
                var current=localStorage.getItem("current");
            }
            window.location.href=current;
        }
        else{
            document.getElementById("errormessage").innerText="Incorrect username and password."
        }

        $("#username,#password,#login").removeAttr("disabled").val("");


    }, false);


}
