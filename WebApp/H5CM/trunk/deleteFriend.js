function deletefriend(index){
  swal({
  title: "Are you sure?",
  text: "You will not be able to see each other measurements!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Delete",
  closeOnConfirm: false
},

function(){
  var token =  localStorage.getItem("token");
  var userID = friendID_array[index];
  var username = friendname_array[index];
  var input = {token:token, userID:userID};
  SensoMan('SensoManDeleteFriend', input);
  document.addEventListener('SensoManDeleteFriendContextValueEvent', function (e12) {
    var response=cms.SensoManDeleteFriend.SensoManDeleteFriend;
    if(response.status =="success"){
    swal("Deleted!", username+" has been deleted.", "success");
    retrieveFriends();
  }else
    swal("Not Deleted!",response.message, "error");

  },false);
});
	
};