// main class ContextAwareApplication

/*
* Updated by Evangelos Aristodemou
* Delete the token of user in logout function.
*/

// first thing is to get the Singleton instance of the ContextManager
var cms = ContextManagerSingleton.getInstance("application");
//Arrays that need to be global
var messages=[];
var datetimes=[];
var nots=[];
var markersArray=[];
var mainMarkers=[];
var mainMap;
var NEW_CONT_LOCATION;
var dependenciesLoaded;
var sensorNames=[];
var sensorMeasurements=[];

// define the scripts to be loaded
var scriptsURL = new Array(
                            "../H5CM/trunk/modules/sensors/GPSCoordinates.js",
						   "../H5CM/trunk/modules/sensors/SensoMan.js"
						   );


    //Check for the validation of token
    function loading(tab){
	//alert("caller is " + arguments.callee.caller.toString());
        if (typeof localStorage["token"] == 'undefined') {
            localStorage.setItem("current", "index.html");
            window.location.href = "login.html";
            }
        else{
            // Load scripts using Promises with jQuery getScript()
            // and wait until all dependencies are loaded
            sensorNames=[];
            sensorMeasurements=[];
            if(tab=="dashboard"){
                dependenciesLoaded=dashboardDependenciesLoaded;
            }else if(tab=="temp"){
                document.getElementById("chart").style.visibility="hidden";

                dependenciesLoaded=tempDependenciesLoaded;
            }else if(tab=="hum"){
                document.getElementById("chart").style.visibility="hidden";
                dependenciesLoaded=humdependenciesLoaded;
            }else if(tab=="sound"){
                document.getElementById("chart").style.visibility="hidden";
                dependenciesLoaded=soundDependenciesLoaded;
            }else if(tab=="bright"){
                document.getElementById("chart").style.visibility="hidden";
                dependenciesLoaded=brightDependenciesLoaded;
            }else if(tab=="motion"){
                document.getElementById("chart").style.visibility="hidden";
                dependenciesLoaded=motionDependenciesLoaded;
            }

            loadDependencies(scriptsURL);
        }
    };
function initializeNotifications(token){
    //var usersNotifications='http://thesis.in.cs.ucy.ac.cy/datasensors/retrieve/notification.php';
    var postparam={token:token};
    SensoMan('SensoManNotify', postparam);
    document.addEventListener('SensoManNotifyContextValueEvent', function (e5) {
        var notifications = cms.SensoManNotify.SensoManNotify.Notifications;
        $("#notificationsList").empty();
        messages=[];
        datetimes=[];
        nots=[];
        messages.length=0;
        datetimes.length=0;
        nots.length=0;
        if (notifications.length>0) {
            for (index = 0; index < notifications.length; index++) {
                var message = notifications[index].message;
                var datetime = notifications[index].datetime;
                var not = notifications[index].notificationID;
                messages.push(message);
                datetimes.push(datetime);
                nots.push(not);
                $("#notificationsList").append('<li style="cursor:pointer"><a onclick="fullfillNotification(' + index + ')"><div><i class="glyphicon glyphicon-ok""></i>  ' + messages[index] + '</div><div><i class="pull-right text-muted small">' + datetimes[index] + '</i></div></a></li>');
            }
        }
        else{
            $("#notificationsList").append('<li><a><div><i class="fa fa-info-circle"></i>'+" No tasks to be taken"+'</div></a></li>');
        }

    }, false);
}

function fullfillNotification(index){
    swal({
            title: "Mark this task as done?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#449d44",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function () {
            var token=localStorage.getItem("token");
            var message=messages[index];
            var notificationID=nots[index];
            var postparam={token:token,message:message, notificationID:notificationID};
            SensoMan('SensoManFulNotifications', postparam);
            document.addEventListener('SensoManFulNotificationsContextValueEvent', function (e5) {
                initializeNotifications(token);
            },false);

        });

}


$("#logout").click(function(){
    var token=localStorage.getItem("token");
    postparam={token: token};
    SensoMan('SensoManLogout', postparam);
    document.addEventListener('SensoManLogoutContextValueEvent', function (e3) {
        localStorage.removeItem("token");
        localStorage.removeItem("current");
        localStorage.removeItem("type");
        localStorage.removeItem("username");
        localStorage.removeItem("email");
        localStorage.removeItem("userlat");
        localStorage.removeItem("userlng");
        console.log("deleting token: " + token);
        var path = '../delete/delete_token.php';
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            window.location.href="../pages/login.html";
          }
        };
        xhttp.open("POST", path, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("token=" + token);
    }, false);
});
