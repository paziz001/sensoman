
function acceptInvitation(index){
	var username = username_array[index];
	var groupName = requestedGroups_array[index];
	if(groupName == 'Not Available')
		var str = 'user '+username+' has send you a request!';
	else var str = 'user '+username+' has send you a request to join '+groupName+' group!';
	swal({
 // title: "Error!",
  title : "Accept:",
  text: str, 
  type: "success",
  showCancelButton: true,
  //confirmButtonColor: "#DD6B55",
  confirmButtonClass: "btn btn-primary",
  confirmButtonText: "Accept",
  closeOnConfirm: true
},
function(){
	acceptInvitationRequest(index,groupName);
  });
}



function acceptInvitationRequest(index,groupName){
	var token = localStorage.getItem("token");
	var username = username_array[index];
	var userID = userID_array[index];
	var input = {token:token, username:username, userID:userID, groupName:groupName};
	SensoMan('SensoManAcceptInvitation', input);
	document.addEventListener('SensoManAcceptInvitationContextValueEvent', function (e12) {
		var response = cms.SensoManAcceptInvitation.SensoManAcceptInvitation;
		if(response.status == "success"){
			acceptInvitationSuccessMsg(username);
			//update friends and requests list
			retrieveFriends();
			retrieveRequests();
			retrieveGroups();
		}
		else{
			console.log(response.message);
		}

		},false);
	
}

function acceptInvitationSuccessMsg(username){
	var str1 = "You are now connected to ";
		var res = str1.concat(username); 
		swal("Success!", res, "success");
}