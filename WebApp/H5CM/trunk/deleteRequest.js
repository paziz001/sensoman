function deleteRequest(index){
	var token = localStorage.getItem("token");
	var userID = userID_array[index];
	var input = {token:token, userID:userID};
	$.ajax({
        url: "../delete/deleteRequest.php",
        method: "POST",
        async: true,
        data: {token:token, userID:userID}
    }).done(function (data) {
    	retrieveRequestsDependenciesLoaded();
    });
}