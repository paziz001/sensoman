/**
 * 
 */
// first thing is to get the Singleton instance of the ContextManager
//var cms = ContextManagerSingleton.getInstance("application");

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

//send invitation to user 
function sendInvitationToUser(){
	var emailFlag = false;
var usernameOrEmail = document.getElementById("sn_username_email").value;
if(validateEmail(usernameOrEmail)){
	 emailFlag = true;
}
 var groupName;

 if($('#selectGroupList option:selected').val()!='0')
  groupName = $('#selectGroupList option:selected').text();
 else groupName = 'Not Available';
 var token =  localStorage.getItem("token");
 var input = {token:token, usernameOrEmail:usernameOrEmail, emailFlag:emailFlag, groupName:groupName};


SensoMan('SensoManSendInvitation', input);
document.addEventListener('SensoManSendInvitationContextValueEvent', function (e12) {
	var response=cms.SensoManSendInvitation.SensoManSendInvitation;
	if(response.status == "success"){
		$('#sendInvitation-modal').modal('hide');
		setTimeout(sendInvitationSuccessMsg(usernameOrEmail), 500);
	}
	else{
		document.getElementById("errormsgInvite").innerText=response.message;
		$("#sn_username,#sn_email,#send_invitation_button").removeAttr("disabled").val("");
	}

	},false);

}
/*var token =  localStorage.getItem("token");
var username=document.getElementById("sn_username").value;
var email=document.getElementById("sn_email").value;
var flag = true;	
var input = {token:token, username:username, email:email, flag:flag};
SensoMan('SensoManSendInvitation', input);
document.addEventListener('SensoManSendInvitationContextValueEvent', function (e12) {
	var response=cms.SensoManSendInvitation.SensoManSendInvitation;
	if(response.status == "success"){
		$('#sendInvitation-modal').modal('hide');
		setTimeout(sendInvitationSuccessMsg(username), 500);
	}
	else{

		document.getElementById("errormsgInvite").innerText=response.message;
		$("#sn_username,#sn_email,#send_invitation_button").removeAttr("disabled").val("");
	}

	},false);
*/

function sendInvitationSuccessMsg(username){
	var str1 = "You have send invitation to ";
		var res = str1.concat(username); 
		swal("Success!", res, "success");
}
