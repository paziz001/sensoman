/**
 * Created by Stella Constantinou on 21/3/2015.
 */
/**
 * Created by Stella Constantinou on 25/3/2015.
 */
// main class ContextAwareApplication

// first thing is to get the Singleton instance of the ContextManager
var cms = ContextManagerSingleton.getInstance("application");
//Arrays that need to be global
var messages=[];
var datetimes=[];
var nots=[];
var locationNames=[];
var locationMessages=[];
var locationM=[];

// define the scripts to be loaded
var scriptsURL = new Array(
    "../H5CM/trunk/modules/sensors/SensoMan.js",
    "../H5CM/trunk/modules/sensors/FacebookConnect.js",
    "../H5CM/trunk/modules/sensors/FacebookInformation.js",
    "../H5CM/trunk/modules/reasoners/FacebookPosts.js"
);

//Check for the validation of token
function loading(){

    if (typeof localStorage["token"] == 'undefined') {
        localStorage.setItem("current", "facebookML.html");
        window.location.href = "login.html";
    }
    else{

            document.getElementById("ML").style.visibility="hidden";
            document.getElementById("loading").style.visibility="hidden";
            loadDependencies(scriptsURL);

    }
};

// this function will be called when the
// promises (all dependencies) are done loading
function dependenciesLoaded(){
    initCAWApp();

};


function initCAWApp(){

    var token=localStorage.getItem("token");
    initializeNotifications(token); //initialize the notifications to the user
    FacebookConnect('email, user_birthday',
        'http://www.cs.ucy.ac.cy/~sconst05/epl435/H5CM/trunk/assets/fb/channel.html');
    showstuff('fbLogin');

    document.addEventListener('facebookConnectContextValueEvent', function (e6) {
        var fbStatus = cms.facebookConnect.facebookConnect;
        if (fbStatus == "user_connected"){
            hidestuff('fbLogin');
            FacebookIsConnected();
            showstuff('fbLogout');
            document.getElementById("facebook").style.display="none";
        }
        else if (fbStatus == "login_cancelled"){
            alert('Login to use the application.');
        }
        else if (fbStatus == "user_logout"){
            hidestuff('fbLogout');
            showstuff('fbLogin');
        }
    }, false);



}


function FacebookIsConnected() {

    var fbData = new Array("name", "email", "gender", "username", "birthday");
    // to get the initial value of the FacebookInfo
    FacebookInformation(fbData);

    document.addEventListener('facebookInfoContextValueEvent', function (e9) {
        var fbInfo = cms.facebookInfo.facebookInfo;
        console.log("fbInformation: " + fbInfo);
        var fbInfoData = fbInfo.split("@%@");
        var fb_data = 'Good to see you, ' + fbInfoData[0] + '.' +
            ' Email: ' + fbInfoData[1] + '.' + ' Gender: ' + fbInfoData[2] + '.' +
            ' Username: ' + fbInfoData[3] + '.' + ' Birthday: ' + fbInfoData[4] + '.';

        var picture = 'https://graph.facebook.com/' + fbInfoData[3] + '/picture?type=large'

        appendImageToElement('fbInfo', picture, 200, 200, 'FB Profile Picture');

        if (fbInfo)
            document.getElementById("fbInfo").style.display = "block";

        document.getElementById("facebookInfo").innerHTML = fb_data;
    }, false);


    // to call the FacebookPosts reasoner module
    FacebookPosts();

    // Listen for the facebookPostsContextValueEvent
    document.addEventListener('facebookPostsContextValueEvent', function (e10) {
        document.getElementById("facebookPosts").innerHTML =
            "FacebookPosts: " + cms.facebookPosts.facebookPosts;
        console.log(cms.facebookPosts.facebookPosts);
    }, false);

}




function makeFacebookML(token){
    var postparam={token: token};
    SensoMan('SensoManML', postparam);
    document.addEventListener('SensoManMLContextValueEvent', function (e12) {
        document.getElementById("loading").style.display="none";
        document.getElementById("ML").style.visibility="visible";
        var ml = cms.SensoManML.SensoManML.resultlocation;
        for	(index = 0; index < ml.length; index++) {
            var nameLoc=ml[index].longitude + " , " +ml[index].latitude;
            var messageLoc=ml[index].MachineLearningMessage;
            var locationMeasurements=ml [index].Measurements;
            locationM.push(locationMeasurements);
            locationNames.push(nameLoc);
            locationMessages.push(messageLoc);
            $("#locations").append('<li role="presentation"><a role="menuitem" tabindex='+index +'href="#" onclick="loadGraph('+index+')">'+nameLoc+'</a></li>');
        }
    }, false);
}

function loadGraph(index){

    $("#yes").remove();
    $("#no").remove();
    var categories=[];
    var measurements=[];
    for (j=0; j<locationM[index].length; j++) {
        var cat = locationM[index][j].Timestamp;
        var meas = locationM[index][j].NumPerson;
        categories.push(cat);
        measurements.push(meas);
    }
    $(function () {
        $('#personChart').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Calculation of People'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Number Of People'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: locationNames[index],
                marker: {
                    symbol: 'square'
                },
                data: measurements
            }]
        })});
    if (locationMessages[index]=="YES"){
        $("#MLSimple").append('<div class="alert alert-danger" id="yes" role="alert">Suspicious Number of People Detected</div>');
    }
    else{
        $("#MLSimple").append('<div class="alert alert-success" id="no" role="alert">Normal conditions are detected</div>');
    }

}
function initializeNotifications(token){
    //var usersNotifications='http://thesis.in.cs.ucy.ac.cy/datasensors/retrieve/notification.php';
    var postparam={token:token};
    SensoMan('SensoManNotify', postparam);
    document.addEventListener('SensoManNotifyContextValueEvent', function (e5) {
        var notifications = cms.SensoManNotify.SensoManNotify.Notifications;
        $("#notificationsList").empty();
        messages=[];
        datetimes=[];
        nots=[];
        messages.length=0;
        datetimes.length=0;
        nots.length=0;
        if (notifications.length>0) {
            for (index = 0; index < notifications.length; index++) {
                var message = notifications[index].message;
                var datetime = notifications[index].datetime;
                var not = notifications[index].notificationID;
                messages.push(message);
                datetimes.push(datetime);
                nots.push(not);
                $("#notificationsList").append('<li><a onclick="fullfillNotification(' + index + ')"><div><i class="glyphicon glyphicon-ok""></i>  ' + messages[index] + '</div><div><i class="pull-right text-muted small">' + datetimes[index] + '</i></div></a></li>');
            }
        }
        else{
            $("#notificationsList").append('<li><a><div><i class="fa-info-circle""></i>'+" No tasks to be taken"+'</div></a></li>');
        }

    }, false);
}

function fullfillNotification(index){
    var token=localStorage.getItem("token");
    var message=messages[index];
    var notificationID=nots[index];
    var postparam={token:token,message:message, notificationID:notificationID};
    SensoMan('SensoManFulNotifications', postparam);
    document.addEventListener('SensoManFulNotificationsContextValueEvent', function (e5) {
        initializeNotifications(token);
    },false);
}



$("#logout").click(function() {
    var token = localStorage.getItem("token");
    postparam = {token: token};
    SensoMan('SensoManLogout', postparam);
    document.addEventListener('SensoManLogoutContextValueEvent', function (e3) {
        localStorage.removeItem("token");
        localStorage.removeItem("current");
        localStorage.removeItem("type");
        localStorage.removeItem("username");
        localStorage.removeItem("email");
        window.location.href = "login.html";
    }, false);
});

function clickFB(){
    document.getElementById("fb").style.display="none";
    FacebookLogin('http://www.cs.ucy.ac.cy/~sconst05/epl435/H5CM/trunk/ContextAwareApplication.html');
    var token = localStorage.getItem("token");
    document.getElementById("loading").style.visibility="visible";
    makeFacebookML(token);
}