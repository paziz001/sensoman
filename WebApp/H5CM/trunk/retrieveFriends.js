var scriptsURLFriends = new Array(
						   "../H5CM/trunk/modules/sensors/SensoMan.js",
						   "../H5CM/trunk/nodeGraph.js"//,
						  // "../H5CM/trunk/socialNetworkFunctions.js"
						   );

var friendID_array=[]; 
var friendname_array=[];

function retrieveFriends() {
dependenciesLoaded=retrieveFriendsDependenciesLoaded;
loadDependencies(scriptsURLFriends);

}


function retrieveFriendsDependenciesLoaded(){
	var token =  localStorage.getItem("token");
	var username = localStorage.getItem("username");
	var input = {token:token, username:username};
	SensoMan('SensoManRetrieveFriends', input);
	document.addEventListener('SensoManRetrieveFriendsContextValueEvent', function (e12) {
		var response = cms.SensoManRetrieveFriends.SensoManRetrieveFriends;
		//update graph data
		nodes = [];
		edges =[];
		//user node
		
		nodes.push({"id": '0',
				"title":"<div style='color: #337AB7; min-width:100px; font-family: Helvetica; text-align:center; border-bottom:solid 1px;'>"+username+"</div><div class='graphToolTip'>email</div><div class='graphToolTip'>user type</div>",
				"label": username,
				"group": "users"
				});
		$('#users_list').empty();
		$('#users_list').append('<li><a style="cursor:pointer" onclick="sendInvitation()" id="send_invitation"><i class="fa fa-user-plus fa-fw"></i> Send Invitation</a></li><li class="divider"></li>');
		if(response.status == 'success'){
			var usernames = response.Usernames;
			var userIDs = response.UserIDs;
			friend_ID_array = [];
			friendname_array = [];
		//update friend's list
		//update graph		 
		for(i =0; i<usernames.length; i++){
			var name = usernames[i];
			var userID = userIDs[i];
			friendID_array.push(userID);
			friendname_array.push(name);
			nodes.push({"id": userID,
							"label": name,
							"group": "users"
		});
			edges.push({"from": '0',"to":userID, "id":userID});
			$('#users_list').append('<li><a><i class="fa fa-user-circle fa-fw"></i> '+friendname_array[i]+' <span style="margin-left: 0;" class="fa fa-trash fa-fw userSettings pull-right" onclick="deletefriend('+i+')" ></span><span class="fa fa-wrench fa-fw userSettings pull-right " onclick="displaySettings('+i+')"></span></a><li class="divider"></li></li>');
		}
		}else {
			$('#users_list').append('<li class="listErrorsMessages">'+response.message+'</li>');
		}
		//create network
		if(firstTimeInitializeGraph){
		networkFA = initGraphDependenciesLoaded();
		loadEventFunction();
		firstTimeInitializeGraph = false;
	} else{
		//update network
		updateGraph();
	}
},false);

}

