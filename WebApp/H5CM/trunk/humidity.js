function humdependenciesLoaded(){
        var token=localStorage.getItem("token");
        callSensoManHumidityMeasurements(token); //Initialize Measurements
        initializeNotifications(token); //initialize the notifications to the user
    };



function callSensoManHumidityMeasurements(token){

    var postparam={token:token, sensorType:"humidity"};
    SensoMan('SensoManChart', postparam);
    // to handle events when the SensoMan value changes
    document.addEventListener('SensoManChartContextValueEvent', function (e12) {
        document.getElementById("loading").style.display="none";
        document.getElementById("chart").style.visibility="visible";
        var humMeasurements = cms.SensoManChart.SensoManChart.Sensors;
        $('#sensors').empty();
        for	(index = 0; index < humMeasurements.length; index++) {
            var sensorName=humMeasurements[index].SensorName;
            var measurements=humMeasurements[index].Measurements;
            sensorNames.push(sensorName);
            sensorMeasurements.push(measurements);

            $("#sensors").append('<li role="presentation"><a role="menuitem" tabindex='+index +'href="#" onclick="loadHumidityGraph('+index+')">'+sensorName+'</a></li>');
        }

    }, false);
}

function loadHumidityGraph(index){
    var categories=[];
    var measurements=[];
    for (j=0; j<sensorMeasurements[index].length; j++) {
        var cat = sensorMeasurements[index][j].Timestamp;
        var meas = parseFloat(sensorMeasurements[index][j].Measurement);
        categories.push(cat);
        measurements.push(meas);
    }
    $(function () {
        $('#humidityChart').highcharts({
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Humidity taken by Sensor'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Humidity'
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: sensorNames[index],
                marker: {
                    symbol: 'square'
                },
                data: measurements
            }]
        })})}