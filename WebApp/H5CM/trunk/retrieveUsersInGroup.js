
var scriptsURLUserGroups = new Array(
						   //"../H5CM/trunk/socialNetworkFunctions.js",//,
						  //  "../H5CM/trunk/retrieveGroups.js",
						   "../H5CM/trunk/modules/sensors/SensoMan.js"
						   //friendfirst
						   );

var gl_name;
function retrieveUsersInGroup(name) {
dependenciesLoaded=retrieveUsersInGroupDependenciesLoaded;
loadDependencies(scriptsURLUserGroups);
gl_name = name;

}

function retrieveUsersInGroupDependenciesLoaded(){

	var token =  localStorage.getItem("token");
	var username = localStorage.getItem("username");
	var input = {token:token, username:username, groupName:gl_name}; 
	SensoMan('SensoManRetrieveUsersInGroup', input);
	document.addEventListener('SensoManRetrieveUsersInGroupContextValueEvent', function (e12) {
		var response = cms.SensoManRetrieveUsersInGroup.SensoManRetrieveUsersInGroup;
		$('#usersInGroupTable tbody').empty();
		if(response.status == 'success'){
			var usernames = response.Usernames;
			var emails = response.Emails;
			var types = response.Types;
			for(i =0; i<usernames.length; i++){											
				$('#usersInGroupTable tbody').append('<tr><td style="width:30%">'+usernames[i]+'</td><td style="width:50%">'+emails[i]+'</td><td style="width:5% ">'+types[i]+'</td></tr>');
				}
		} else{
			$('#usersInGroupTable tbody').append('<tr><td>'+response.message+'</td></tr>');
		}
	},false);
}
