<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SensoMan - Code Generator">
    <meta name="author" content="Evangelos Aristodemou">

    <title>SensoMan - Code Generator</title>
    <link rel="shortcut icon" href="../images/infa_sensor.png">

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- sweet alert CSS -->
    <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../code_generator/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
	<!-- font-awesome 4.7.0-->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

	<link rel="import" href="sendInvitation.html">
	<script src="../H5CM/trunk/assets/js/googlePlacesScript.js"  type="text/javascript"></script>


</head>



<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.html"><span id="logo-dashboard">SensoMan</span></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span id="username"></span>
                    <i title ="Logout" class="fa fa fa-user-circle fa-fw" style="width: auto;"></i>
                </a>
                <ul class="dropdown-menu dropdown-user" id="userMenu">
                    <li><a style="cursor:pointer" id="settings" href="../pages/settings.html"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
                    <li><a style="cursor:pointer" id="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="../pages/index.html" id="dashboard"><i class="fa fa-dashboard fa-fw" style="font-size:17px"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="myFiles.php" id="myfiles"><i class="fa fa-file-text-o fa-fw" style="font-size:17px"></i> My files</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!-- Page Content -->
    <div id="page-wrapper">

        <h2>Code Generator</h2>
        <div class="container-fluid">

          <p style="font-size:12pt;">Code Generator generates code which can be deployed to your IoT micro-controller.<br>
          Complete the form below by giving your preferences and click "GENERATE" to get <br>
          your code file and step by step instructions about how to build your sensor circuit.</p><br><br>

          <div id="message"></div>

        <form action="../code_generator/getFile.php" method="post">

          <fieldset class="scheduler-border" id ="fileinfo">
            <legend class="scheduler-border">File information</legend>
              <div class="form-group col-md-5">
                <label>File name</label>
                <input type="text" name="fileName" class="form-control" placeholder="Enter file name" id="filename">
              </div>
            </fieldset>

          <button type="button" class="btn btn-primary col-md-5" id="addexm">Add Existing Micro-controller</button>
          <button type="button" class="btn btn-primary col-md-5" id="addnm">Add New Micro-controller</button>


            <fieldset class="scheduler-border" id ="newcontroller">
              <legend class="scheduler-border">Micro-controller</legend>
              <p id="message_controller"></p>
              <div class="form-group col-md-5">
                <label>Type</label>
                <select class="form-control" id="controllerType" onchange="selectControllerType(this)">
                  <option value="-1" id="n0" selected>Choose micro-controller type...</option>
                  <option value="Arduino">Arduino</option>
                  <option value="Raspberry Pi">Raspberry Pi</option>
                  <option value="Beaglebone Black">Beaglebone Black</option>
                  <option value="Intel Galileo">Intel Galileo</option>
                </select>
              </div>
              <input type="hidden" id="hiddencontrollerType" name="controllerType" value="-1">
              <input type="hidden" id="hiddencontrollerID" name="controllerID" value="">
            <div class="form-group form-group col-md-5">
              <label>Name</label>
              <input type="text" class="form-control" name="controllerName" placeholder="Enter micro-controller name" id="controllerName">
            </div>
            <div class="form-group col-md-5">
              <label>Time Interval</label>
              <input type="text" id="timeInterval1" name="timeInterval1" class="form-control" aria-describedby="timeHelp" placeholder="Enter time in seconds">
              <i><small id="timeHelp" class="form-text text-muted">The amount of time to wait (in seconds) until new measurements will be taken from the sensors.</small></i>
            </div>
            <div class="form-group col-md-5">
              <label>Internet Conntection</label>
              <select id="internet1" name="internet1" class="form-control" aria-describedby="internethelp" onchange="selectModule(this)">
                <option value="-1" selected>Select controller</option>
              </select>
              <i><small id="internethelp" class="form-text text-muted">If you choose a method to connect your micro-controller to the internet, then all the measurements of the sensors will be saved to the database.</small></i>
            </div>
            <div style="clear: both;">
              <button type="button" id="savecontroller" class="btn btn-primary" onclick="saveController(this)">Save to continue</button>
              <button type="button" id= "changecontroller1" class="btn btn-primary">Change</button><br>
            </div>
          </fieldset>

          <fieldset class="scheduler-border" id ="existingcontroller">
            <legend class="scheduler-border">Micro-controller</legend>
            <i>Select a micro-controller that already exists.</i><br><br>
            <div class="form-group col-md-5">
              <label for="controller">Micro-controller</label>
              <select id="existingcontrollers" class="form-control" name="excontrollerName" onchange="selectController()">
                <option id="c0" value="0" selected>Choose micro-controller...</option>
              </select>
            </div>
            <div class="form-group form-group col-md-5" style="margin-right: 20pt;">
              <label>Type</label>
              <input type="text" class="form-control" id="excontrollerType" name="excontrollerType" readonly>
            </div>
            <input type="hidden" id="hiddenexcontrollerID" name="excontrollerID" value="">
            <div class="form-group col-md-5">
              <label>Time Interval</label>
              <input type="text" id="timeInterval2" name="timeInterval2" class="form-control" aria-describedby="timeHelp" placeholder="Enter time in seconds">
              <i><small id="timeHelp" class="form-text text-muted">The amount of time to wait (in seconds) until new measurements will be taken from the sensors.</small></i>
            </div>
            <div class="form-group col-md-5">
              <label>Internet Connection</label>
              <select id="internet2" name="internet2" class="form-control" aria-describedby="internethelp" onchange="selectModule(this)">
                <option value="-1" selected>Select controller</option>
              </select>
              <i><small id="internethelp" class="form-text text-muted">If you choose a method to connect your micro-controller to the internet, then all the measurements of the sensors will be saved to the database.</small></i>
            </div>
            <button type="button" class="btn btn-primary" id="changecontroller2" style="clear: both;">Change</button><br>
        </fieldset>

        <div id="wifi-info"></div>
        <b><p id="message" style="clear:both;"></p></b>
        <div id="sensors"></div>

          <button type="button" class="btn btn-primary" id="adds">Add Sensor</button>
          <button type="button" class="btn btn-primary" id="generate">GENERATE</button>
          <div id="sbt"></div>

        </form>

        </div>
    </div>
    <!-- /.container-fluid -->


    <!-- /#page-wrapper -->
</div>


<script src="../bower_components/markerclustererplus/dist/markerclusterer.min.js"></script>
<!-- jQuery -->
<script src="../bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>


<!-- sweet alert-->
<script src="../bower_components/sweetalert/dist/sweetalert.min.js"></script>
<!-- Datatables -->
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<!-- Index Javascript (Map, Call MyRestfulService - H5CM) -->
<script src="../H5CM/trunk/assets/js/Q.js" type="text/javascript"></script>
<!--vis graph -->
<script type="text/javascript" src="../graph_resources/vis-4.18.1/dist/vis.js"></script>
<script src="../H5CM/trunk/dashboard.js"></script>
<script src="../H5CM/trunk/context/ContextManagerSingleton.js" type="text/javascript"></script>
<script src="../H5CM/trunk/assets/js/sha-256.js" type="text/javascript"></script>
<script src="../H5CM/trunk/myContextAwareApplication.js"></script>

<!-- JavaScript code for codeGenerator -->
<script src="../code_generator/form.js" type="text/javascript"></script>
<script src="../code_generator/check.js" type="text/javascript"></script>


</body>

</html>
