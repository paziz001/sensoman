<!DOCTYPE html>
<! index.html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SensoMan - Dashboard">
    <meta name="author" content="Stella Constantinou">

    <title>SensoMan - Dashboard</title>
    <link rel="shortcut icon" href="../images/infa_sensor.png">

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- sweet alert CSS -->
    <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
	<!-- font-awesome 4.7.0-->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

	<link rel="import" href="sendInvitation.html">
	<script src="../H5CM/trunk/assets/js/googlePlacesScript.js"  type="text/javascript"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBsY2y5LbvgNWY8T8Gt0KKbtw_-Lt0qHa8"></script>
    <script src="../bower_components/markerclustererplus/dist/markerclusterer.min.js"></script>
    <![endif]-->
    <style>
        /*********************CSS for     *********************
        */
        /*********************edit-delete-rule.html********************/
        .blocklyToolboxDiv {
            z-index: 9999 !important;
        }

        /*********************CSS for     **********************/
        /*********************dashboard.html**********************/

        .infowindow-style span:nth-child(1) {
            font-size: medium;
            font-weight: bold;
        }
        #notificationsList {
            max-height: 250px !important;
            overflow: scroll;
        }

		.groupIcon:after {
		content:url(../images/meeting-icon.png);
		width:100%;
		border:0;
		//alt:Null;
		height: auto;

		}

    </style>
</head>

<body onload="main()">


<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.html"><span id="logo-dashboard">SensoMan</span></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">

			<!-- Social Network - group list and members list>
            <!-- /.dropdown -->
			<li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i title ="Settings" class="fa fa-gear fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user" id="settings_list">
                    <li><a style="cursor:pointer" id="option1"><i class="fa fa-user-plus fa-fw"></i> Option 1</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>

			<li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i title ="Users List" class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user" id="users_list">
                    <li><a style="cursor:pointer" id="send_invitation"><i class="fa fa-user-plus fa-fw"></i> Send Invitation</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>

			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" id="sn_groups" href="#">
					<i title ="Groups List" class="fa fa-group fa-fw"></i> <i class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-user" id="sn_groups_list">
					<li><a style="cursor:pointer" id="create_group"><i class="fa fa-plus fa-fw"></i> Create Group</a>
						<li class="divider"></li>
					</li>
				</ul>

			</li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" id="notsMenu" href="#">
                    <i style="cursor:pointer" title ="Notifications" class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-alerts" id="notificationsList">

                </ul>
                <!-- /.dropdown-alerts -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i title ="Logout" class="fa fa-sign-out fa-fw"></i>
                </a>
                <ul class="dropdown-menu dropdown-user" id="userMenu">
                    <li><a style="cursor:pointer" id="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>

        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="index.html" id="dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="code_generator.php" id="codeGenerator"><i class="fa fa-file-code-o"></i> Code Generator</a>
                    </li>
                    <li id="rules-tab">
                        <a href="#"><i class="fa fa-hand-o-up"></i> Rules<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#" id="addRule"> Add Rule</a>
                            </li>
                            <li>
                                <a href="#" id="edit-delete-rule"> Edit/Delete Rule</a>
                            </li>
                            <li>
                                <a href="#" id="tutorial-examples"> Helpful Rule Examples</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Sensor Charts<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#" id="temp_chart"> Temperature</a>
                            </li>
                            <li style="display:none">
                                <a href="#" id="sound_chart"> Sound </a>
                            </li>
                            <li>
                                <a href="#" id="hum_chart"> Humidity </a>
                            </li>
                            <li>
                                <a href="#" id="motion_chart"> Motion </a>
                            </li>
                            <li>
                                <a href="#" id="bright_chart"> Brightness </a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>


                    <li style="display:none">
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Sensor Machine Learning<span
                                class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#" id="simple_ml">Simple ML</a>
                            </li>
                            <li>
                                <a href="#" id="fb_ml">Facebook ML</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li style="display:none">
                        <a href="#"><i class="fa fa-files-o fa-fw"></i> Pages<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="https://docs.google.com/forms/d/1RUOUCkeHWo79gekO_s6Cwn-iMuVjAjCp19d1lRb72Pk/viewform?c=0&w=1"
                                   target="_blank">Download Mobile App</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!-- Page Content -->
    <div id="page-wrapper">

        <div class="container-fluid">


        </div>
    </div>
    <!-- /.container-fluid -->


    <!-- /#page-wrapper -->
</div>

</div>


<?php
include 'sendInvitation.php';
?>


<!-- /#wrapper -->
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCfi4g9c6hCRmLgcRta89q2IwLXEch54SU"></script>

<script src="../bower_components/markerclustererplus/dist/markerclusterer.min.js"></script>
<!-- jQuery -->
<script src="../bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>

<!-- sweet alert-->
<script src="../bower_components/sweetalert/dist/sweetalert.min.js"></script>
<!-- Datatables -->
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<!-- Index Javascript (Map, Call MyRestfulService - H5CM) -->
<script src="../H5CM/trunk/assets/js/Q.js" type="text/javascript"></script>
<script src="../H5CM/trunk/dashboard.js"></script>
<script src="../H5CM/trunk/context/ContextManagerSingleton.js" type="text/javascript"></script>
<script src="../H5CM/trunk/assets/js/sha-256.js" type="text/javascript"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>
    var cms; //<--------------------------------------------------------???????
</script>
<script src="../H5CM/trunk/myContextAwareApplication.js"></script>
<script src="../H5CM/trunk/sound.js"></script>
<script src="../H5CM/trunk/temperature.js"></script>
<script src="../H5CM/trunk/humidity.js"></script>
<script src="../H5CM/trunk/brightness.js"></script>
<script src="../H5CM/trunk/motion.js"></script>
<script src="../libs/blockly/appengine/storage.js"></script>
<script src="../libs/blockly/blockly_compressed.js"></script>
<script src="../libs/blockly/blocks_compressed.js"></script>
<script src="../libs/blockly/msg/js/en.js"></script>
<script src="../H5CM/trunk/rules.js"></script>
<script src="../H5CM/trunk/sendInvitation.js"></script>

<script>

//$('#send_invitation').click(function(){

  //  $('#sendInvitation-modal').modal('show');
//	alert('adsfsdfsdfsadfsafsds');
  //  });
   // $('#sendInvitation-modal').on('shown.bs.modal',function(){
    //    initializeMapController();
//		alert('adsfsdfsdfsadfsafsds6465465465');
 //   });

    var Code = {};
    var rule_xml = null;
    var rule_id = "";
    function main() {
        var type = localStorage.getItem("type");
        if (type == "simple") {
            document.getElementById("rules-tab").style.display = "none";
            $("#notsMenu").parents(".dropdown").css("display","none");
        }else if(type == "owner" || type == "owner-admin"){
            $("#notsMenu").parents(".dropdown").css("display","none");
        }
        $('#page-wrapper .container-fluid').load('dashboard.html', function () {

            loading("dashboard");

            //placeControllers();
        });

		//$('.social-networks-modals').load('sendInvitation.html');

    }


    $('#dashboard').click(function (e) {
        e.preventDefault();
        if ($('.blocklyToolboxDiv').length);
        $(".blocklyToolboxDiv").css("display", "none");
        main();
    });

    $('#temp_chart').click(function (e) {
        e.preventDefault();
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");


        $('#page-wrapper .container-fluid').load('temperatureChart.html', function () {
            loading("temp");
        });

    });
    $('#sound_chart').click(function (e) {
        e.preventDefault();
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");
        $('#page-wrapper .container-fluid').load('soundChart.html', function () {
            loading("sound");
        });
    });

    $('#hum_chart').click(function (e) {
        e.preventDefault();
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");
        $('#page-wrapper .container-fluid').load('humidityChart.html', function () {
            loading("hum");
        });
    });
    $('#motion_chart').click(function (e) {
        e.preventDefault();
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");
        $('#page-wrapper .container-fluid').load('motionChart.html', function () {
            loading("motion");
        });
    });
    $('#bright_chart').click(function (e) {
        e.preventDefault();
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");
        $('#page-wrapper .container-fluid').load('brightnessChart.html', function () {
            loading("bright");
        });
    });
    $('#simple_ml').click(function () {
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");
        $('#page-wrapper .container-fluid').load('simpleML.html');
    });
    $('#fb_ml').click(function () {
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");
        $('#page-wrapper .container-fluid').load('facebookML.html');
    });
    $('#addRule').click(function (e) {
        refreshAddRuleTab(e);
    });

        $('#tutorial-examples').click(function(){
            if ($('.blocklyToolboxDiv').length)
                $(".blocklyToolboxDiv").css("display", "none");
            $('#page-wrapper .container-fluid').load('rule-examples.html');
        });

    $('#edit-delete-rule').click(function (e) {
        if ($('.blocklyToolboxDiv').length)
            $(".blocklyToolboxDiv").css("display", "none");
        e.preventDefault();
        $('#page-wrapper .container-fluid').load('edit-delete-rule.html', function () {
            getSensorControllerNames();
            getActuatorControllerNames();
            var rules = initializeRulesTable();
            var t = $('#edit-rules-table').DataTable(
                    {
                        "pageLength": 5,
                        "showNEntries": false
                    }
            );


            var workspace = Blockly.inject('editblockly',
                    {
                        grid: {
                            spacing: 25,
                            length: 3,
                            colour: '#ccc',
                            snap: true
                        },
                        media: '../libs/blockly/media/',
                        toolbox: document.getElementById('edittoolbox'),
                        zoom: {
                            controls: true,
                            wheel: true,
                            startScale: 0.8,
                            maxScale: 1.5,
                            minScale: 1.0,
                            scaleSpeed: 1.2
                        }
                    });
            $('.paginate_button').click(function () {
                console.log("fire");
                Blockly.fireUiEvent(window, 'resize');
            });

            $(".save-changes-btn").click(function () {
                var response = updateRule(rule_id);
                if(response.status=="success"){
                    rules = initializeRulesTable();
                    console.log(t);
                    t.rows().remove().draw();

                    $.each(rules, function (key, value) {
                        t.row.add(['<a href="#"><i class="fa fa-pencil" id="' + value["ruleID"] + '" style="color:#ff9548">' +
                        ' Edit </i></a><a href="#"><i class="fa fa-times" style="color:#e00917">' +
                        ' Delete</i></a>'
                            , "<pre>" + value['description'] + "</pre>"]).draw(false);
                    });
                    swal({title:response.message,type:"success"});
                }else{
                    swal({title: "An error has occured".message,type:"warning"});
                }
            });

            $.each(rules, function (key, value) {
                t.row.add(['<a href="#"><i class="fa fa-pencil" id="' + value["ruleID"] + '" style="color:#ff9548">' +
                ' Edit </i></a><a href="#"><i class="fa fa-times" style="color:#e00917">' +
                ' Delete</i></a>'
                    , "<pre>" + value['description'] + "</pre>"]).draw(false);
            });
            $('#edit-rules-table tbody').on("click", ".fa-times", function (e) {
                if ($(this).parents('tr').hasClass('selected')) {
                    $(this).parents('tr').removeClass('selected');
                    $(".save-changes-btn").attr("disabled", "disabled");
                }
                else {
                    t.$('tr.selected').removeClass('selected');
                    $(this).parents('tr').addClass('selected');
                    $(".save-changes-btn").removeAttr("disabled");
                }
                swal({
                            title: "Delete this rule?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No",
                            closeOnConfirm: false,
                            closeOnCancel: true
                        },
                        function () {
                            var ruleID = $('.selected .fa-times').parents("a").siblings("a").children().attr("id");
                            console.log(ruleID);
                            var params = {token:localStorage.getItem("token"),ruleID:ruleID};
                            $.ajax({
                                method:"post",
                                url:"../delete/deleteRule.php",
                                data:params,
                                async:false
                            }).done(function(response){
                                if(response.status=="fail"){
                                    swal({
                                        title: "An error has occured",
                                        type: "info"
                                    });
                                }else{
                                    swal("Deleted!", "The rule has been deleted", "success");
                                    e.preventDefault();
                                    t.row('.selected').remove().draw(false);
                                }

                            });

                        });

            });

            $('#edit-rules-table tbody').on("click", ".fa-pencil", function (e) {
                console.log('clicked');
                if ($(this).parents('tr').hasClass('selected')) {
                    $(this).parents('tr').removeClass('selected');
                    $(".save-changes-btn").attr("disabled", "disabled");
                }
                else {
                    t.$('tr.selected').removeClass('selected');
                    $(this).parents('tr').addClass('selected');
                    $(".save-changes-btn").removeAttr("disabled");
                }
                rule_id = $(this).attr("id");
                rule_xml = getRule(rule_id);
                e.preventDefault();
                try {
                    rule_xml = Blockly.Xml.textToDom(rule_xml);
                    console.log(rule_xml);
                } catch (e) {
                    BlocklyStorage.alert(BlocklyStorage.XML_ERROR + '\nXML: ' + rule_xml);
                    return;
                }
                Blockly.getMainWorkspace().clear();
                Blockly.Xml.domToWorkspace(rule_xml, Blockly.getMainWorkspace());

                $("html, body").animate({scrollTop: $(document).height()}, 1000);
            });


        });


    });

    function refreshAddRuleTab(e) {
        e.preventDefault();
        $('#page-wrapper .container-fluid').load('newrule.html', function () {
            Code.workspace = Blockly.inject('blocklyDiv',
                    {
                        grid: {
                            spacing: 25,
                            length: 3,
                            colour: '#ccc',
                            snap: true
                        },
                        media: '../libs/blockly/media/',
                        toolbox: document.getElementById('toolbox'),
                        zoom: {
                            controls: true,
                            wheel: true,
                            startScale: 1.3,
                            maxScale: 1.7,
                            minScale: 0.8,
                            scaleSpeed: 1.2
                        }
                    });
            $('#add-rule-btn').click(function (e) {
                e.preventDefault();
                var response = uploadRule();
                if(response.status=="success"){
                    Blockly.mainWorkspace.clear();
                    swal({title:response.message,type:"success"});
                }else if(response.status=="fail"){
                    swal({title: response.message,type:"warning"});
                }else{
                    swal({title: "An error has occured",type:"warning"});
                }

            });
            $("text:contains(switch)+.blocklyEditableText").attr('id', 'sensorDropdownButton');

            getSensorControllerNames();
            getActuatorControllerNames();

        });
    }

</script>
</body>

</html>
