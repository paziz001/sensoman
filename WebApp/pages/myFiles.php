<?php
require "../code_generator/arduino.php";
require "../code_generator/raspberry.php";
require "../code_generator/beaglebone_black.php";
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="SensoMan - My Files">
    <meta name="author" content="Evangelos Aristodemou">

    <title>SensoMan - Code Generator</title>
    <link rel="shortcut icon" href="../images/infa_sensor.png">

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- sweet alert CSS -->
    <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../code_generator/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
	<!-- font-awesome 4.7.0-->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../bower_components/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="../bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

	<link rel="import" href="sendInvitation.html">
	<script src="../H5CM/trunk/assets/js/googlePlacesScript.js"  type="text/javascript"></script>


</head>



<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.html"><span id="logo-dashboard">SensoMan</span></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span id="username"></span>
                    <i title ="Logout" class="fa fa fa-user-circle fa-fw" style="width: auto;"></i>
                </a>
                <ul class="dropdown-menu dropdown-user" id="userMenu">
                    <li><a style="cursor:pointer" id="settings" href="../pages/settings.html"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
                    <li><a style="cursor:pointer" id="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="index.html" id="dashboard"><i class="fa fa-dashboard fa-fw" style="font-size:17px"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="code_generator.php" id="codeGenerator"><i class="fa fa-file-code-o" style="font-size:17px"></i> Code Generator</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!-- Page Content -->
    <div id="page-wrapper">

        <h2>My Files</h2>
        <br>
        <div class="container-fluid">

          <!-- TABLE -->

    <div id="test"></div>
    <table class="table table-condensed" style="border-collapse:collapse; width: 70%;" id="maintable">
        <thead>
            <tr>
                <th></th>
                <th>File Name</th>
                <th>Controller Name</th>
                <th>Controller Type</th>
                <th>Sensors</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="filestable"></tbody>
      </table>

      <div class="modal fade" id="editFile" tabindex="-1" role="dialog" aria-labelledby="title">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="title">Edit File</h4>
            </div>
            <div class="modal-body">
              <form>
                <div id="modal-mes" style="color: red; font-weight: bold; font-style: italic"></div>
                <br>
                <div class="form-group">
                  <label for="recipient-name" class="control-label">Time Interval</label>
                  <input type="text" class="form-control time-interval" id="modal-time">
                </div>
                <div class="form-group">
                  <label for="message-text" class="control-label">Sensors</label>
                  <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Name</th>
                      <th scope="col">Type</th>
                      <th scope="col">Pin</th>
                    </tr>
                  </thead>
                  <tbody id="modalsensors">
                  </tbody>
                </table>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary" id="savebtn">Save</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="fileInfo" tabindex="-1" role="dialog" aria-labelledby="title">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="title">File info</h4>
            </div>
            <div class="modal-body" id="info-body" style="max-height: 250pt; overflow-y: scroll;">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

            </div>

          </div>

        </div>
    <!-- /.container-fluid -->

<script src="../bower_components/markerclustererplus/dist/markerclusterer.min.js"></script>
<!-- jQuery -->
<script src="../bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>


<!-- sweet alert-->
<script src="../bower_components/sweetalert/dist/sweetalert.min.js"></script>
<!-- Datatables -->
<script src="../bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>
<!-- Index Javascript (Map, Call MyRestfulService - H5CM) -->
<script src="../H5CM/trunk/assets/js/Q.js" type="text/javascript"></script>
<!--vis graph -->
<script type="text/javascript" src="../graph_resources/vis-4.18.1/dist/vis.js"></script>
<script src="../H5CM/trunk/dashboard.js"></script>
<script src="../H5CM/trunk/context/ContextManagerSingleton.js" type="text/javascript"></script>
<script src="../H5CM/trunk/assets/js/sha-256.js" type="text/javascript"></script>
<script src="../H5CM/trunk/myContextAwareApplication.js"></script>

<!-- JavaScript code for codeGenerator -->
<script src="../code_generator/myFiles.js" type="text/javascript"></script>


</body>

</html>
