<?php
/**
 * Created by PhpStorm.
 * User: Stella Constantinou
 * Date: 29/3/2015
 * Time: 12:47 πμ
 */


include_once('includes/common.php');
require_once('includes/connectdb.php');
include_once('includes/token.php');

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
$method=getRequestMethod();
$date=new DateTime('now');
$ip=getClientIP();

if ($method != 'POST') {
    $response=array(
        "status" => "fail",
        "request time" => $date->format("Y-m-d h:m:s"),
        "IP Address" => $ip,
        "message" => "Action not found for request '".$method." ".$_SERVER['REQUEST_URI']."'"
    );
}
else{
    if (isset($_POST["token"])){
        $token = $_POST["token"];
        //Check the validation of the token
        $sql="SELECT * FROM token WHERE token='".$token."'";
        DBConnect();
        $resultToken=execQuery($sql);
        DBClose();
        $numToken=rowCount($resultToken);
        if ($numToken<1){
            $response=array(
                "status" => "fail",
                "request time" => $date->format("Y-m-d h:m:s"),
                "IP Address" => $ip,
                "message" => "You have to provide a valid token. Non valid Connection"
            );
        }
        else{
            while($aRow=fetchNext($resultToken)){
                $valid=$aRow["validUntil"];
                $userID=$aRow["userID"];
            }
            if (checkToken($valid)){
                //Find the controller Info
                $sql="UPDATE `user` SET `lastLoginWeb`='".$date->format("Y-m-d h:m:s")."' WHERE userID=".$userID;
                DBConnect();
                $result=execQuery($sql);
                DBClose();

                $response=array(
                    "status" => "success",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Successfully logout"
                );
            }
            else{
                $response=array(
                    "status" => "fail",
                    "request time" => $date->format("Y-m-d h:m:s"),
                    "IP Address" => $ip,
                    "message" => "Non valid token"
                );
            }
        }
    }
    else{
        $response=array(
            "status" => "fail",
            "request time" => $date->format("Y-m-d h:m:s"),
            "IP Address" => $ip,
            "message" => "You have to provide your current token"
        );
    }
}
echo json_encode($response);


