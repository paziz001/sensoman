#SensoMan: Remote management of context sensors#

SensoMan is a sensor management system that manages a collection
of sensors spread in the environment connected to specific boards.

It has been implemented by the University of Cyprus (Department of 
Computer Science) with the participation of different developers.

SensoMan is licensed under the GPL license version 3.0 or higher.

SensoMan uses various libraries including the following:
jQuery, blocky, sweetalert, datatables

If you want to cite this work, please use:
Athina C. Paphitou, Stella Constantinou, Georgia M. Kapitsaki:
SensoMan: Remote management of context sensors. WIMS 2015: 19:1-19:6